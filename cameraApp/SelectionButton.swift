//
//  SelectionButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 1/23/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation

class SelectionButton : UIButton {
  
  override func awakeFromNib() {
    translatesAutoresizingMaskIntoConstraints = false
    layer.borderColor = Const.RED.cgColor
    layer.cornerRadius = 0
    layer.borderWidth = 1
  }
  
  override var intrinsicContentSize : CGSize {
 
    return CGSize(width: 110, height: 30)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
}
