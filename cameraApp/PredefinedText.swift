//
//  PredefinedText.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/7/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase

class PredefinedText {
  let text : String!
  var selected : Bool!
  let key : String!
  
  
  init(text : String, selected : Bool, key : String){
    self.text = text
    self.selected = selected
    self.key = key
  }
  
  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
    self.text = result!["text"]! as! String
    self.selected = result!["selected"]! as! Bool
    self.key = result!["key"]! as! String
  }
  
  func toDictionary() -> Dictionary<String, Any> {
    return [Const.PREDEFINED_TEXT : text, Const.SELECTED : selected, Const.KEY : key!]
  }
  
}
