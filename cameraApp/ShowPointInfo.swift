//
//  ShowPointInfo.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 2/8/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
// Photos Library
import Photos
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FBSDKShareKit
import MBProgressHUD
import MessageUI


class ShowPointInfo: UIViewController, MFMailComposeViewControllerDelegate {
 
  
  @available(iOS 4.0, *)
  public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
    print("something")
  }


  var image : Image!
  var album : String!
  @IBOutlet weak var accuracy: UILabel!
  @IBOutlet weak var easting: UILabel!
  @IBOutlet weak var northing: UILabel!
  @IBOutlet weak var gZone: UILabel!
  @IBOutlet weak var definedFind: UILabel!
  @IBOutlet weak var predText: UILabel!
  @IBOutlet weak var id: UILabel!
  @IBOutlet weak var date: UILabel!
  @IBOutlet weak var picture: UIImageView!
  @IBOutlet weak var background: UIImageView!
  @IBOutlet weak var eastingLabel: UILabel!
  @IBOutlet weak var northingLabel: UILabel!
  @IBOutlet weak var zoneLabel: UILabel!
  @IBOutlet weak var noPhotoUploadedLabel: UILabel!
  @IBOutlet weak var longDescription: UITextView!
  
  let imagePlaceholderData = UIImageJPEGRepresentation(UIImage(named: "placeholderImage")!, 1)

  
  let defaults = UserDefaults.standard

  
  //Album Variables
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = true
  var key = String()
  
  var images = [Image]()
  var indexes = [String]()
  
  var numberOfImages : Int = 0
  var content : FBSDKShareLinkContent = FBSDKShareLinkContent()
  
  var tempCurrentIndex : Int = 0
  var selectedEmails = [String]()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    navigationItem.title = image.albumName
    navigationItem.backBarButtonItem?.title = "Back"
    setButtons()
    key = image.key
    
    image = fetchImage()
  
    setLabels(image: image)
    tempCurrentIndex = images.index(where: {$0 == image})!
    numberOfImages = images.count

  }
  
  override func viewDidLayoutSubviews() {
    background.clipsToBounds = true
    background.layer.masksToBounds = true
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
      noPhotoUploadedLabel.isHidden = true
      setTextLabel()
      setLabels(image: self.image)
      setButtons()
  
  }

  func setTextLabel(){
   
  }
  
  func setButtons() {
    let backImageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    let nextImageButton = UIButton(frame: CGRect(x: 0, y:0, width: 25, height: 25))
    
    let downloadImageButton = UIButton(frame: CGRect(x: 0, y:0, width: 37, height: 27))

    backImageButton.addTarget(self, action: #selector(ShowPointInfo.backInfo), for: .touchUpInside)
    nextImageButton.addTarget(self, action: #selector(ShowPointInfo.nextInfo), for: .touchUpInside)
    
    downloadImageButton.addTarget(self, action: #selector(ShowPointInfo.downloadImage), for: .touchUpInside)

    let backImage = UIImage(named: "previous")
    let nextImage = UIImage(named: "next")
    
    let downloadImage = UIImage(named: "download-red")
    
    backImageButton.setImage(backImage!, for: .normal)
    nextImageButton.setImage(nextImage!, for: .normal)
    
    downloadImageButton.setImage(downloadImage!, for: .normal)
    
    
    let nextButton = UIBarButtonItem(customView: nextImageButton)
    let backButton = UIBarButtonItem(customView: backImageButton)
    
    let downloadButton = UIBarButtonItem(customView: downloadImageButton)
    
    //Email Button
    let emailButtonImage = UIButton(frame: CGRect(x: 0, y: 0, width: 31, height: 25))
    let emailImage = UIImage(named: "email-blank")
    emailButtonImage.setImage(emailImage!, for: .normal)
    emailButtonImage.addTarget(self, action: #selector(sendEmail), for: .touchUpInside)
    let mailButton = UIBarButtonItem(customView: emailButtonImage)
    
    
    let trashButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(ShowPointInfo.deletePoint(_:)))
    trashButton.tintColor = Const.RED
    
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    nextButton.tintColor = Const.RED
    backButton.tintColor = Const.RED
    
    //if self.image.url != nil && self.image.url != "" {
      toolbarItems = [ downloadButton, spacer, mailButton, spacer, trashButton]
    //} else {
    // toolbarItems = [ spacer, trashButton]
    //}

    let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(goToEdit))
    editButton.tintColor = Const.RED
    
    navigationItem.rightBarButtonItems = [nextButton, backButton, editButton]
    navigationController?.setToolbarHidden(false, animated: true)

  }
  
  func goToEdit(){
    performSegue(withIdentifier: "goToEditImage", sender: self)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "goToEditImage" {
      let dest : EditImageTVC = segue.destination as! EditImageTVC
      dest.imageKey = image.key
    }
  }
  
  
  func downloadImage(){
    var loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Downloading..."
    loadingNotification.hide(animated: true, afterDelay: 3.5)

    
    let url = self.image.url
    if url != "" {
      downloadImageBy(url!)
    } else {
      let image = UIImage(data: self.image.imgData! as Data )
      self.saveImageToCustomAlbum(image: Helpers.Instance.textToImage(inImage: image!, image: self.image))
    }
    
    MBProgressHUD.hide(for: self.view, animated: true)
    
    loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .fade
    loadingNotification.label.text = "Photo downloaded."
    loadingNotification.hide(animated: true, afterDelay: 1.5)
  
  }
  
  func downloadImageBy(_ URL: String){

    
    let httpsReference = Storage.storage().reference(forURL: URL)
    
    httpsReference.getData(maxSize: 1*1024*1024) { (data, error) in
   
      if (error != nil){
        print("something happend: func download at ShowPointInfo.swift line 135")
        return
      } else {
        let image = UIImage(data: data!)
        self.saveImageToCustomAlbum(image: Helpers.Instance.textToImage(inImage: image!, image: self.image))
        
      }
      
    }

  }
  
  
  @objc func sendEmail(){
  
    
    let url = self.image.url
    if url != "" {
      sendEmailFrom(url!)
    } else {
      self.handleEmail(info: self.image, self.image.imgData as? Data)
    }
  }
 
  
  func sendEmailFrom(_ URL: String){
    let httpsReference = Storage.storage().reference(forURL: URL)
    
    httpsReference.getData(maxSize: 1*1024*1024) {(data, error) in
    
    if (error != nil){
      print("something happend: func download at ShowPointInfo.swift line 135")
      return
    } else {
    
      self.handleEmail(info: self.image, data)
    
     }
    
      }
  }
  
  func handleEmail(info: Image, _ data: Data?) {
    if MFMailComposeViewController.canSendMail() {
      
      let imageData = UIImage(data: data!)
      let attachmentImage = Helpers.Instance.textToImage(inImage: imageData!, image: self.image)
      
      let composeVC = MFMailComposeViewController()
      composeVC.mailComposeDelegate = self
      
      let appName = defaults.string(forKey: "appName")!
      let content = gpsInfoStr(texts: info)
      
      fetchSelectedEmails()
      
      if selectedEmails.count == 0 {
        selectedEmails = [""]
      }
      
      let filename =  "\(self.image.albumName.slugify())-\(info.initialsStamp.slugify())"


      
      composeVC.setToRecipients(selectedEmails)
      composeVC.setSubject("\(self.album!) - by \(appName)")
      let neutral = UIImageJPEGRepresentation(attachmentImage, 1)
      composeVC.addAttachmentData(neutral!, mimeType: "image/jpeg", fileName: filename + ".jpg")
      
      
      
      composeVC.setMessageBody(content, isHTML: true)
      
      // Present the view controller modally.
      present(composeVC, animated: true)
    } else {
      let alert = Helpers.Instance.alertUser(title: "ERROR!", message: "Please make sure you have the Mail App installed on your phone.")
      present(alert, animated: true, completion: nil)
    }

  }
  
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    switch result {
    case .cancelled:
      print("Mail cancelled")
    case .saved:
      print("Mail saved")
    case .sent:
      let message = Helpers.Instance.showHUBMessage(message: "Email Sent", view: self.view)
      
      DispatchQueue.main.async(execute: { () -> Void in
        message.hide(animated: true, afterDelay: 1.2)
      })
      
    case .failed:
      print("Mail sent failure: \(error!.localizedDescription)")
    }
    controller.dismiss(animated: true, completion: nil)
  }
  
  func matches(for regex: String, in text: String) -> [String] {
    
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let nsString = text as NSString
      let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
      return results.map { nsString.substring(with: $0.range)}
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return []
    }
  }
  
  func gpsInfoStr(texts: Image) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""

    var accuStr = ""
    
    if texts.accuracy == "1" {
      accuStr  = "<b>Accuracy:</b> Added"
    } else {
      accuStr  = "<b>Accuracy:</b> ± \(texts.accuracy)m"
    }
    
    let datumStr = "<strong>CoordSys: </strong>\(texts.coordinateSystem)"
    let category = "<strong>Category: </strong>\(texts.symbolText!)"
    
    let dateStr = "\(texts.timestamp)"
    
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 {
      //inverted to put East up and Nor down
      lonStr = "<strong>Northing:</strong> \(texts.latitude)"
      latStr = "<strong>Easting:</strong> \(texts.longitude)"
      zoneStr = "<strong>\(datum == 6 ? "Grid Ref" : "Zone:" )</strong> \(texts.zone)</strong>"
      
      result =  "<strong>" + dateStr + "</strong></br></br>" +
        "<strong>Project Name:</strong> \(self.album!) </br>" +
        accuStr + "</br>" +
        latStr + "</br>" +
        lonStr + "</br>" +
        zoneStr + "</br>" +
        category + "</br>" +
        datumStr + "</br>" +
        (self.image.ownerText.isEmpty ? "" : "<strong>Text added: </strong> \(self.image.ownerText)") +
      "</br></br><h6><i>Created by iSmartDetect</br>Find us on App Store.</i></h6>"
      
    } else {
      latStr = "<strong>Latitude:</strong> \(image.latitude)"
      lonStr = "<strong>Longitude:</strong> \(image.longitude)"
      zoneStr = ""
      
      
      result =  "<strong>" + dateStr + "</strong></br></br>" +
        "<strong>Project Name:</strong> \(self.album!) </br>" +
        accuStr + "</br>" +
        latStr + "</br>" +
        lonStr + "</br>" +
        category + "</br>" +
        datumStr + "</br>" +
        (self.image.ownerText.isEmpty ? "" : "<strong>Text added: </strong> \(self.image.ownerText)") +
      "</br></br><h6><i>Created by iSmartDetect</br>Find us on App Store.</i></h6>"
      
    }
    
    return result
  }

  
  func fetchSelectedEmails(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.EMAIL_ENTITY)
    
    request.predicate = NSPredicate(format: "selected = YES")
    
    let emails = try? manageContext.fetch(request)
    for email in emails as! [EmailData]{
      if email.selected == true {
        let new = email.email!
        self.selectedEmails.append(new)
      }
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    navigationController?.setToolbarHidden(true, animated: true)
    
  }
  
  func facebookShare(){

  }
  
  
  func nextInfo(){
    nextImage()
  }
  
  func backInfo(){
    backImage()
  }
  
  func setLabels(image: Image){
    let coordinateSystem = image.coordinateSystem
    
    if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
      eastingLabel.text = "Longitude:"
      northingLabel.text = "Latitude:"
      zoneLabel.isHidden = true
      gZone.isHidden = true
      
    } else {
      eastingLabel.text = "Easting:"
      northingLabel.text = "Northing:"
      if coordinateSystem == Const.COOR_UTM_UK_GREF {
        zoneLabel.isHidden = false
        gZone.isHidden = false
        zoneLabel.text = "Grid Ref:"
      } else {
        zoneLabel.isHidden = false
        gZone.isHidden = false
      }
     

    }
    

    if image.url! != "" {
      background.sd_setImage(with: URL(string: image.url!))
      picture.sd_setImage(with: URL(string: image.url!))
    } else {
      if image.imgData != nil {
        background.image = UIImage(data: image.imgData! as Data)
        picture.image = UIImage(data: image.imgData! as Data)
      } else {
        background.image = #imageLiteral(resourceName: "placeholderImage")
        picture.image = #imageLiteral(resourceName: "placeholderImage")
      }
    }
    
    id.text = image.initialsStamp
    date.text = image.timestamp
    accuracy.text = image.accuracyValue
    easting.text = image.longitude
    gZone.text = image.datumAndZone
    northing.text = image.latitude
    definedFind.text = image.symbolText
    predText.text = image.ownerText
    
    if let text = image.longDesc {
      longDescription.text = "DESCRIPTION: \n" + text
    }
    
    
  }
  
  func backImage() {
    let id = mod((tempCurrentIndex - 1), numberOfImages)
    tempCurrentIndex = Int(id)
    self.image = self.images[tempCurrentIndex]
    setLabels(image: returnImageFound(key: self.images[id].key))
  }
  
  func nextImage() {
    let id = mod((tempCurrentIndex + 1), numberOfImages)
    tempCurrentIndex = Int(id)
    self.image = self.images[tempCurrentIndex]
    setLabels(image: returnImageFound(key: self.images[id].key))
  }
  
  func returnImageFound(key: String) -> Image {
    var newImage : Image!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let img = result[0] as! ImageData
      
      var imageDataTemp : NSData!
      
      if img.imgData != nil {
        imageDataTemp = img.imgData
      } else {
        imageDataTemp = imagePlaceholderData! as NSData
      }
      
      let image = Image(id: Int(img.id), latitude: img.latitude!, longitude: img.longitude!, zone: img.gZone!, accuracy: img.acurracy!, timestamp: img.timestamp!, albumName: img.albumName!, coordinateSystem: img.coordinateSystem!, ownerText: img.note!, key: img.key!, desc: img.desc!, url: img.url!, initials: img.initials, symbolLetter: img.symbolLetter, symbolText: img.symbolText, createdAt: img.createdAt, updatedAt: img.updatedAt, imgData: imageDataTemp, longDesc : img.longDesc)
      
      newImage = image

    }
    return newImage
  
  }
  

  func fetchImages(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", self.album!)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      if results.count > 0 {
        for result in results as! [ImageData]{
          
          let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc : result.longDesc )
          
          
          self.images.append(image)
          }
        } else {
        
          dismiss(animated: true, completion: nil)
          
       }
      
    } catch {
      print("error")
    }
  }
  
}



extension ShowPointInfo {
  func deletePoint(_ sender: UIBarButtonItem) {
    let alert = UIAlertController(title: "Delete Photo", message: "Are you sure you want to  delete this photo? This also will delete your photo and all its data.", preferredStyle: .alert)
    
    let delete = UIAlertAction(title: "Delete", style: .destructive) { (action) in
      self.handleDeletePoint()
    }
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    alert.addAction(delete)
    alert.addAction(cancel)
    
    present(alert, animated: true, completion: nil)
    
    
  }
  

  func handleDeletePoint(){
    let image =  self.image
    let key = (image?.key)!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = key
          let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
          ref.removeValue()
          print("Deleted Image Data on firebase: ", key)
          
          self.images = []
          fetchImages()
          self.numberOfImages = images.count
          if numberOfImages > 0 {
            nextInfo()
          }
          
        } else {
          print("Error trying to Image Data.")
        }
        
      }catch {
        print("error")
      }
    }
    
  }

  
  func mod(_ a: Int, _ n: Int) -> Int {
    precondition(n > 0, "modulus must be positive")
    let r = a % n
    return r >= 0 ? r : r + n
  }
  
  
  func saveImageToCustomAlbum(image: UIImage) {
    //Check if the folder exists, if not, create it
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", self.album)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
    
    let listAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
    let albumsSize: Int = listAlbums.count
    
    if albumsSize < 2 {
      UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
      return
    }
    
    if let first_Obj : AnyObject = collection.firstObject{
      //found the album
      self.albumFound = true
      self.assetCollection = first_Obj as! PHAssetCollection
      
      
      //save the image in a specific folder (album)
      PHPhotoLibrary.shared().performChanges({
        let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
        //Inverted Longitude / Latitude
        assetChangeRequest.location = CLLocation(latitude: self.image.decimalLongitude, longitude: self.image.decimalLatitude)
        let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
        let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
        
        let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
        albumChangeRequest!.addAssets(fastEnumeration)
        
      }, completionHandler: nil)
    }else{
      
      //Album placeholder for the asset collection, used to reference collection in completion handler
      var albumPlaceholder : PHObjectPlaceholder!
      //create the folder
      NSLog("\nFolder \"%@\" does not exist\nCreating now...", self.album)
      PHPhotoLibrary.shared().performChanges({
        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.album)
        albumPlaceholder = request.placeholderForCreatedAssetCollection
      },
       completionHandler: {(success:Bool, error:Error?)in
        if(success){
          print("Successfully created folder")
          self.defaults.set(0, forKey: "albumIndex")
          self.albumFound = true
          let collection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumPlaceholder.localIdentifier], options: nil)
          self.assetCollection = collection.firstObject! as PHAssetCollection
          
          //save the image in a specific folder (album)
          PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            //Inverted Longitude / Latitude
            assetChangeRequest.location = CLLocation(latitude: self.image.decimalLongitude, longitude: self.image.decimalLatitude)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            
            let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
            albumChangeRequest!.addAssets(fastEnumeration)
          }, completionHandler: nil)
          
        }else{
          print("Error creating folder")
          self.albumFound = false
        }
      })
    }
  }

  func fetchImage() -> Image {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      
      let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      return image
      
    } catch {
      print("error")
    }
    return image
  }

  
}
