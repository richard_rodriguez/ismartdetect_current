//
//  PauseButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/9/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class PauseButton: UIButton {

  override func awakeFromNib() {
    if DeviceType.IS_IPHONE_4_OR_LESS {
      if let image = UIImage(named: "pause-small") {
        self.setImage(image, for: .normal)
      }
    } else {
      if let image = UIImage(named: "pause14") {
        self.setImage(image, for: .normal)
      }
    }

    
  }

}
