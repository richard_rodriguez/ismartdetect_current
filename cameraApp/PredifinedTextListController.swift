//  PredifinedTextController.swift
//  Created by Richard Rodriguez on 10/26/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation
import FirebaseAuth
import FirebaseDatabase
import CoreData

class PredifinedTextList: UITableViewController {
  
  let defaults = UserDefaults.standard
  var tempPredText : [String]!
  var predefinedTexts : [PredefinedText]!
  var updateSecond : Timer!

  var handle: UInt!
  var ref : DatabaseReference!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    if let _ = Auth.auth().currentUser?.uid {
      let responde = ObserversProvider.Instance.observer(child: Const.PREDEFINED_TEXTS, option: .predefinedText)
      handle = responde.handle
      ref = responde.ref
    }
    
    predefinedTexts = []
    fetchPredefinedTexts()
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startTimer()

  }
  
  func fetching(){
    predefinedTexts = []
    fetchPredefinedTexts()
    self.tableView.reloadData()
  }
  
  
  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    stopTimer()
    
    if let _ = Auth.auth().currentUser?.uid {
      ref.removeObserver(withHandle: handle!)
    }
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return predefinedTexts.count
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PredText")
    cell?.textLabel!.text = predefinedTexts[indexPath.row].text
    
    if predefinedTexts[indexPath.row].selected == true {
      cell?.accessoryType = .checkmark
    } else {
      cell?.accessoryType = .none
    }

    cell?.tintColor = Const.RED
    return cell!
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if let cell = tableView.cellForRow(at: indexPath) {
      if cell.accessoryType == UITableViewCellAccessoryType.none {
        cell.accessoryType = .checkmark
        predefinedTexts[indexPath.row].selected = true
        edit(predefinedText: predefinedTexts[indexPath.row])
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = predefinedTexts[indexPath.row].key!
          let ref = Database.database().reference().child(uid).child(Const.PREDEFINED_TEXTS).child(key)
          let new : Dictionary<String, Any> = [Const.PREDEFINED_TEXT : predefinedTexts[indexPath.row].text,
                                               Const.SELECTED : predefinedTexts[indexPath.row].selected]
          ref.updateChildValues(new)
        }

        
      } else if cell.accessoryType == UITableViewCellAccessoryType.checkmark{
        cell.accessoryType = .none
        predefinedTexts[indexPath.row].selected = false
        edit(predefinedText: predefinedTexts[indexPath.row])
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = predefinedTexts[indexPath.row].key!
          let ref = Database.database().reference().child(uid).child(Const.PREDEFINED_TEXTS).child(key)
          let new : Dictionary<String, Any> = [Const.PREDEFINED_TEXT : predefinedTexts[indexPath.row].text,
                                               Const.SELECTED : predefinedTexts[indexPath.row].selected]
          ref.updateChildValues(new)
        }
      }
    }
  }
  
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == UITableViewCellEditingStyle.delete {
      if delete(predefinedText: predefinedTexts[indexPath.row]) {
        if let uid = Auth.auth().currentUser?.uid {
          if let key = predefinedTexts[indexPath.row].key {
            let ref = Database.database().reference().child(uid).child(Const.PREDEFINED_TEXTS).child(key)
            ref.removeValue()
          } else {
            print("Error trying to deleting predeterminedText.")
            return
          }
        }
        
        predefinedTexts = []
        fetchPredefinedTexts()
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        self.tableView.reloadData()
        
      } else {
        print("Error trying to delete predefinedTexts on CoreData.")
      }
    }
  }
  
  @IBAction func addNewPredText(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "New", message: "Enter a new Predefined Stamp Text", preferredStyle: UIAlertControllerStyle.alert)
    
        alert.addTextField { (textField) in
          textField.placeholder = "Insert at least one character"
          textField.autocapitalizationType = .sentences
        }
    
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: {
          (action) -> Void in
          let textf = (alert.textFields?[0])! as UITextField
    
          if self.validate(predText: textf.text!) {
            
            let key = Database.database().reference().child(Const.PREDEFINED_TEXTS).childByAutoId().key
            let predefinedText = PredefinedText(text: textf.text!, selected: true, key: key )
            self.add(new: predefinedText)
            self.predefinedTexts = []
            self.fetchPredefinedTexts()
            
            let lastIndex = self.predefinedTexts.count - 1
            let insertionIndexPath = IndexPath(item: lastIndex, section: 0)
            self.tableView.insertRows(at: [insertionIndexPath], with: .automatic)
            self.tableView.cellForRow(at: insertionIndexPath)?.accessoryType = .checkmark

         
          } else {
            let error = UIAlertController(title: "Hey!", message: "Insert at least one character.", preferredStyle: UIAlertControllerStyle.alert)
            error.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.destructive, handler: nil))
            self.present(error, animated: true, completion: nil)
          }
        })
        )
    
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
          (action) -> Void in

        })
        )
        
        present(alert, animated: true, completion: nil)
    
  }
  

  
  func validate(predText: String) -> Bool {
    return predText.count > 1 ? true : false
  }
  
  
  func fetchPredefinedTexts(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.PREDEFINED_TEXT_ENTITY)
    
    let sortDescriptor = NSSortDescriptor(key: Const.PREDEFINED_TEXT, ascending: true)
    request.sortDescriptors = [sortDescriptor]
    
    do {
      let predefinedTexts = try manageContext.fetch(request)
      print(predefinedTexts.count)
      for predefinedText in predefinedTexts as! [PredefinedTextData] {
        print(predefinedText.text ?? "")
        let new = PredefinedText(text: predefinedText.text!, selected: predefinedText.selected, key: predefinedText.key!)
        self.predefinedTexts.append(new)
      }
    } catch {
      print("Error fetching predeterminedText")
    }
  }
  
  func add(new predefinedText: PredefinedText){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: "PredefinedTextData", into: manageContext)
    
    new.setValue(predefinedText.text, forKey: Const.PREDEFINED_TEXT)
    new.setValue(predefinedText.selected, forKey: Const.SELECTED)
    new.setValue(predefinedText.key, forKey: Const.KEY)
    
    do{
      try manageContext.save()
      print("PredefinedText saved on CoreData. Func calls on Add PredText button.")
      
      DBProvider.Instance.add(new: predefinedText)
      
    }catch {
      print("Error saving predeterminedText")
    }
  }
  
  func edit(predefinedText: PredefinedText){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PredefinedTextData")
    
    request.predicate = NSPredicate(format: "key = %@", predefinedText.key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! PredefinedTextData
      managedObject.setValue(predefinedText.selected!, forKey: "selected")
      
      do{
        try manageContext.save()
        print("data updated.")
      }catch {
        print("Error updating predeterminedText")
      }
    }
  }
  
  func delete(predefinedText: PredefinedText) -> Bool{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PredefinedTextData")
    
    request.predicate = NSPredicate(format: "key = %@", predefinedText.key)
    if let result = try? manageContext.fetch(request){
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        return true
      }catch {
        print("Error deliting predeterminedText")
        return false
      }
    }
    return true
  }
  
  
  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(PredifinedTextList.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }
  
}

