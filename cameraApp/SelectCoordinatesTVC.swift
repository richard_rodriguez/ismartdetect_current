//
//  SelectCoordinatesTVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 7/19/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//


import UIKit
import CoreData
import FirebaseAuth
import FirebaseDatabase


@objc class SelectCoordinatesTVC: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate, UITextFieldDelegate {
  
  var key = String()
  var image : Image!
  @IBOutlet weak var coordSysField: UITextField!
  @IBOutlet weak var eastingLabel: UILabel!
  @IBOutlet weak var northingLabel: UILabel!
  @IBOutlet weak var zoneLabel: UILabel!
  @IBOutlet weak var zoneCell: UITableViewCell!
  @IBOutlet weak var latBandCell: UITableViewCell!
  @IBOutlet weak var hemisphereCell: UITableViewCell!
  
  @IBOutlet weak var eastingTF: UITextField!
  @IBOutlet weak var northingField: UITextField!
  @IBOutlet weak var zoneField: UITextField!
  @IBOutlet weak var latBandTF: UITextField!
  @IBOutlet weak var hemisphereTF: UITextField!
  var initialLatitude = 0.0
  var initialLongitude = 0.0
  
  
  let locationManager = CLLocationManager()

  
  var isWGS84_DK = true
  
  var latitude = ""
  var longitude = ""
  var fZone = ""
  var desc = String()
  
  var selectedRow = 0
  
  var coordSysCode = String()
  
  //DMS field
  @IBOutlet weak var DMSStackLat: UIStackView!
  @IBOutlet weak var degreeLat: UITextField!
  @IBOutlet weak var secondLat: UITextField!
  @IBOutlet weak var minuteLat: UITextField!
  @IBOutlet weak var orientationLat: UITextField!
  
  @IBOutlet weak var DMSStackLon: UIStackView!
  @IBOutlet weak var degreeLon: UITextField!
  @IBOutlet weak var secondLon: UITextField!
  @IBOutlet weak var minuteLon: UITextField!
  @IBOutlet weak var orientationLon: UITextField!
  
  @IBOutlet weak var DMStackLat: UIStackView!
  @IBOutlet weak var degreeDMLat: UITextField!
  @IBOutlet weak var decimalDMLat: UITextField!
  @IBOutlet weak var orientationDMLat: UITextField!
  
  @IBOutlet weak var DMStackLon: UIStackView!
  @IBOutlet weak var degreeDMLon: UITextField!
  @IBOutlet weak var decimalDMLon: UITextField!
  @IBOutlet weak var orientationDMLon: UITextField!
  
  
  
  let coordinatesSytemsCode = ["DMS-WGS84",
                               "DM-WGS84",
                               "DD-WGS84",
                               "UTM-WGS84",
                               "UTM-ETRS89",
                               "UTM-WGS84",
                               "UTM-UK-GREF"]
  
  
  let coordinateSystem = [0: "LAT/LON DMS WGS84",
                          1: "LAT/LON DM  WGS84",
                          2: "LAT/LON DD  WGS84",
                          3: "UTM  WGS84 DK/N",
                          4: "UTM  ETRS89 DK East",
                          5: "UTM  WGS84",
                          6: "UTM  UK Grid Refence"]
  
  
  let coordsPicker = UIPickerView()
  
  
  override func viewWillAppear(_ animated: Bool) {
    image = fetchImage()
    
    initializeLocationManager()
    
    let rowOfCoordinatesSystem = coordinatesSytemsCode.index(of: image.coordinateSystem)!
    
    latitude = image.latitude
    longitude = image.longitude
    fZone = image.zone
    
    coordSysField.delegate = self
    coordSysCode = image.coordinateSystem
    
    coordSysField.text = coordinateSystem[rowOfCoordinatesSystem]
    setTextFields(rowOfCoordinatesSystem, latitude: latitude, longitude: longitude, zone: image.zone, coordinateSystem: image.coordinateSystem)
    
    // picker.isHidden = true
    //coordSysField.text = image.coordinateSystem
    
    coordsPicker.delegate = self
    
    let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
    coordSysField.inputAccessoryView = toolBar
    coordSysField.inputView = coordsPicker
  
    DMStackLat.isHidden = true
    DMStackLon.isHidden = true
    
    DMSStackLat.isHidden = true
    DMSStackLon.isHidden = true
    
    
  
  }
  
  
  
  @objc override func viewDidLoad() {
    super.viewDidLoad()
    NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
    NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
    
    
    self.navigationController?.navigationBar.topItem?.title = "Back"
    navigationItem.title = "Coordinates"
  }

  @objc func cancel(){
    print("Canceled")
    deleteAImage()
    //_ = navigationController?.popViewController(animated: true)
    dismiss(animated: true, completion: nil)

    //_ = navigationController?.popToRootViewController(animated: true)
    
  }
  
  @objc func dismissPicker() {
    print("print something")
    view.endEditing(true)
  }
  
  
  
  func setTextFields(_ row : Int, latitude : String, longitude : String, zone: String, coordinateSystem : String ){
    
    if row == 0 || row == 1 || row == 2 {
      eastingLabel.text = "Longitude"
      northingLabel.text = "Latitude"
      hemisphereCell.isHidden = true
      latBandCell.isHidden = true
      zoneCell.isHidden = true
      
      if row == 0 {
        //eastingTF.isHidden = true
        //DMSStackLat.isHidden = false
        DMStackLat.isHidden = true
        
        //northingField.isHidden = true
        //DMSStackLon.isHidden = false
        DMStackLon.isHidden = true
        
        if latitude != "" && coordinateSystem == Const.COOR_DMS_WGS84 {
          //let splitCoordinateLat = Helpers.Instance.getDegMinSecOri(coord: latitude)
          eastingTF.text = latitude
          //degreeLat.text = splitCoordinateLat.degree
          //minuteLat.text = splitCoordinateLat.minute
          //secondLat.text = splitCoordinateLat.second
          //orientationLat.text = splitCoordinateLat.orientation
          
         
          northingField.text = longitude
          //let splitCoordinateLon = Helpers.Instance.getDegMinSecOri(coord: longitude)
          //degreeLon.text = splitCoordinateLon.degree
          //minuteLon.text = splitCoordinateLon.minute
          //secondLon.text = splitCoordinateLon.second
          //orientationLon.text = splitCoordinateLon.orientation
          
        }
      } else if row == 1  {
        //eastingTF.isHidden = true
        //DMStackLat.isHidden = false
        DMSStackLat.isHidden = true
        
        //northingField.isHidden = true
        //DMStackLon.isHidden = false
        DMSStackLon.isHidden = true
        
        if latitude != "" && coordinateSystem == Const.COOR_DM_WGS84 {
          //let splitCoordinateLat = Helpers.Instance.getDegMinOri(coord: latitude)
          
          //degreeDMLat.text = splitCoordinateLat.degree
          //decimalDMLat.text = splitCoordinateLat.minute
          //orientationDMLat.text = splitCoordinateLat.orientation
          
          eastingTF.text = latitude
          northingField.text = longitude
          
          //let splitCoordinateLon = Helpers.Instance.getDegMinOri(coord: longitude)
          //degreeDMLon.text = splitCoordinateLon.degree
          //decimalDMLon.text = splitCoordinateLon.minute
          //orientationDMLon.text = splitCoordinateLon.orientation
          
        }
        
      } else if row == 2 && coordinateSystem == Const.COOR_DD_WGS84 {
        eastingTF.isHidden = false
        DMStackLat.isHidden = true
        DMSStackLat.isHidden = true
        
        northingField.isHidden = false
        DMStackLon.isHidden = true
        DMSStackLon.isHidden = true
        
        if latitude != "" {
          eastingTF.text = latitude
          northingField.text = longitude
        }
        
        
      }
      
    } else {
      zoneCell.isHidden = false
      eastingTF.isHidden = false
      DMStackLat.isHidden = true
      DMSStackLat.isHidden = true
      
      northingField.isHidden = false
      DMStackLon.isHidden = true
      DMSStackLon.isHidden = true
      
    
      
      
      eastingLabel.text = "Easting"
      northingLabel.text = "Northing"
      zoneField.placeholder = "1 - 60"
      latBandTF.placeholder = "C - X"
      hemisphereTF.placeholder = "S or N"
      
      if coordinateSystem == Const.COOR_UTM_UK_GREF {
        zoneLabel.text = "Grid Ref."
        zoneField.placeholder = ""
        latBandCell.isHidden = true
        hemisphereCell.isHidden = true
      } else {
        latBandCell.isHidden = false
        hemisphereCell.isHidden = false
      }

      
      
      if image.latitude != "" && ( coordinateSystem == Const.COOR_UTM_WGS84 || coordinateSystem == Const.COOR_UTM_UK_GREF || coordinateSystem == Const.COOR_UTM_ETRS89 ) {
        eastingTF.text = longitude
        northingField.text = latitude
        
        if coordinateSystem == Const.COOR_UTM_UK_GREF {
          zoneLabel.text = "Grid Ref."
          zoneField.placeholder = ""
          zoneField.text = zone
          latBandCell.isHidden = true
          hemisphereCell.isHidden = true
        } else {
          latBandCell.isHidden = false
          hemisphereCell.isHidden = false
          let zoneSplited = Helpers.Instance.split(zone: zone)
          zoneLabel.text = "Zone"
          zoneField.text = zoneSplited.zone
          latBandTF.text = zoneSplited.latBand
          hemisphereTF.text = zoneSplited.hemisphere
        }
        
      }
      
    }
  }
  
  
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "goToPickLocation" {
      let dest : PickLocationMapVC = segue.destination as! PickLocationMapVC
      dest.key = self.key
      dest.isWGS84_DK = self.isWGS84_DK
      dest.initialLatitude = self.initialLatitude
      dest.initialLongitude = self.initialLongitude
    }
  }
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    //
    coordsPicker.selectRow(0, inComponent: 0, animated: false)
    

    if image.latitude == "" {
      setTextFields(0, latitude: "", longitude: "", zone: "", coordinateSystem: Const.COOR_DMS_WGS84)
    } else {
      let DMS = Helpers.Instance.coordinateToDMS(lat: image.decimalLatitude, lon: image.decimalLongitude)
      latitude = DMS.latitude
      longitude = DMS.longitude
      setTextFields(0, latitude: latitude, longitude: longitude, zone: "", coordinateSystem: Const.COOR_DMS_WGS84)
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //getLatAndLon(row: selectedRow)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)

    updateAImage()
    
    
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 2 && indexPath.row == 0 {
      tableView.deselectRow(at: indexPath, animated: true)
      performSegue(withIdentifier: "goToPickLocation", sender: self)
    }
  }
  
  
  func fetchImage() -> Image {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      return image
      
    } catch {
      print("error")
    }
    return image
  }
  
 }


extension SelectCoordinatesTVC {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return 7
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    if image.latitude != "" {
      if row == 0 {
        
        let DMS = Helpers.Instance.coordinateToDMS(lat: image.decimalLatitude, lon: image.decimalLongitude)
        latitude = DMS.latitude
        longitude = DMS.longitude
        
        updateAImage()
        setTextFields(row, latitude: latitude, longitude: longitude, zone : "", coordinateSystem: coordinatesSytemsCode[row])
      
      } else if row == 1 {
        
        let DM = Helpers.Instance.DMStoDM(lat: image.decimalLatitude, lon: image.decimalLongitude)
        latitude = DM.latitude
        longitude = DM.longitude
       
        updateAImage()
        setTextFields(row, latitude: latitude, longitude: longitude, zone : "", coordinateSystem: coordinatesSytemsCode[row])
        
      } else if row == 2 {
        latitude = String(round(image.decimalLatitude*100000.0) / 100000.0)
        longitude = String(round(image.decimalLongitude*100000.0) / 100000.0)

        updateAImage()
        setTextFields(row, latitude: latitude, longitude: longitude, zone : image.zone, coordinateSystem: coordinatesSytemsCode[row])
      
      } else if row == 3 {

        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinates: CLLocationCoordinate2D(latitude: image.decimalLongitude, longitude: image.decimalLatitude))
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(image.decimalLongitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        fZone = String(gridZone) + subZone + " " + hemisphere
        
        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]

        updateAImage()
        setTextFields(row, latitude: latitude, longitude: longitude, zone : fZone, coordinateSystem: coordinatesSytemsCode[row])
        
      } else if row == 4 {
        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesETRS: CLLocationCoordinate2D(latitude: image.decimalLongitude, longitude: image.decimalLatitude))
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(image.decimalLongitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        fZone = String(gridZone) + subZone + " " + hemisphere
        
        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]
        
        updateAImage()
        setTextFields(row, latitude: latitude, longitude: longitude, zone : fZone, coordinateSystem: coordinatesSytemsCode[row])

      } else if row == 5 {
        
        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesGrl: CLLocationCoordinate2D(latitude: image.decimalLongitude, longitude: image.decimalLatitude))
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(image.decimalLongitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        fZone = String(gridZone) + subZone + " " + hemisphere
        updateAImage()

        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]

        updateAImage()

        setTextFields(row, latitude: latitude, longitude: longitude, zone : fZone, coordinateSystem: coordinatesSytemsCode[row])

      } else if row == 6 {
        let coord = Helpers.Instance.LLtoNE(latitude: image.decimalLongitude, longitude: image.decimalLatitude)
        let gridReference = Helpers.Instance.NE2NGR(easting: Double(coord.eastring)!, northing: Double(coord.northing)!)
        let northing = coord.northing.components(separatedBy: ".")[0]
        let easting = coord.eastring.components(separatedBy: ".")[0]
        let gridRef = gridReference
        
        latitude = northing
        longitude = easting
        fZone = gridRef
        updateAImage()

        setTextFields(row, latitude: latitude, longitude: longitude, zone : fZone, coordinateSystem: coordinatesSytemsCode[row])

      }
    } else {
      setTextFields(row, latitude: "", longitude: "", zone: "", coordinateSystem: coordinatesSytemsCode[row])
    }

    
    if row == 5 {
      isWGS84_DK = false
    } else {
      isWGS84_DK = true
    }
    
    selectedRow = row
    coordSysCode = coordinatesSytemsCode[row]
    coordSysField.text = coordinateSystem[row]
    updateAImage()
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return coordinateSystem[row] as String?
  }
  
  func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 48.0
  }
  
  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      
      if result.count > 0 {
        let managedObject = result[0] as! ImageData
       
        
        let description = buildDescription( image.latitude == "" ? "0" : String(image.decimalLatitude), image.longitude == "" ? "0" : String(image.decimalLongitude) )
        
        managedObject.setValue(latitude, forKey: Const.LATITUDE)
        managedObject.setValue(longitude, forKey: Const.LONGITUDE)
        managedObject.setValue(fZone, forKey: Const.ZONE)
        managedObject.setValue(coordSysCode, forKey: Const.COORDINATE_SYSTEM)
        managedObject.setValue( description, forKey: Const.DESC)

        
        do{
          try manageContext.save()
          
          print("Image updated.")
          
          
        }catch {
          print("Error trying to editing the note.")
        }
      }
    }
  }

  func buildDescription(_ lon: String, _ lat: String) -> String {
    return "<\(lat),\(lon)>"
  }
  
  
  func getHemisphereValue(_ letter : String) -> Int {
    if letter == "s" || letter == "S" {
      return 1
    } else {
      return 0
    }
  
  }

  
  func deleteAImage(){
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        /*
         if let uid = Auth.auth().currentUser?.uid {
         let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
         ref.removeValue()
         print("Deleted Image Data on firebase: ", key)
         } else {
         print("Error trying to Image Data.")
         }
         */
      }catch {
        print("error")
      }
    }
    
  }
  
  
  
  
  
  
  
}

extension SelectCoordinatesTVC {
  func initializeLocationManager() {
    locationManager.delegate = self
    locationManager.requestLocation()
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    initialLatitude =  (location?.coordinate.latitude)!
    initialLongitude = (location?.coordinate.longitude)!
    
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
  }
  
}


































