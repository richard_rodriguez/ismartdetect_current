//
//  GPSInfo.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/15/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class GPSInfo: UITextView {

  override func awakeFromNib() {
    
    var multiplier : CGFloat!
    
    if DeviceType.IS_IPHONE_4_OR_LESS {
      multiplier = 0.75
    } else {
      multiplier = 1
    }
    
    let screenSize: CGRect = UIScreen.main.bounds
    let porcentageFontSize = CGFloat(20/320.0)
    font = UIFont(name: "Helvetica Bold", size: (screenSize.width * porcentageFontSize * multiplier) )

    
  }


}
