//
//  PredifinedTextOption.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 10/26/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation

class PredefinedTextOption: UITableViewController {
  
  let defaults = UserDefaults.standard
  
  
  
  @IBOutlet weak var predTextSwitch: UISwitch!
  @IBOutlet weak var addPredTextCell: UITableViewCell!
  
  
  
  var albumName : String!
  
  override func viewWillAppear(_ animated: Bool) {
    albumName = defaults.string(forKey: "albumName")

    
    predTextSwitch.setOn(defaults.bool(forKey: "activePredifinedTexts"), animated: true)
    
    if predTextSwitch.isOn {
      addPredTextCell.isHidden = false
    } else {
      addPredTextCell.isHidden = true
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  @IBAction func predSwitchPressed(_ sender: UISwitch) {
    if predTextSwitch.isOn {
      addPredTextCell.isHidden = false
      
      
      defaults.set(true, forKey: "activePredifinedTexts")
      defaults.set("", forKey: albumName! + "predText")

      
      
      if let temp = defaults.string(forKey: albumName! + "predText" + "temp") {
       defaults.set(temp, forKey: albumName! + "predText")
      }
      
    } else {
      addPredTextCell.isHidden = true
      defaults.set(false, forKey: "activePredifinedTexts")
      
      if let temp = defaults.string(forKey: albumName! + "predText") {
        defaults.set(temp, forKey: albumName! + "predText" + "temp")
        defaults.set("", forKey: albumName! + "predText")
      }
     
    }
    
    //DBProvider.Instance.saveSetting()
  }

}
