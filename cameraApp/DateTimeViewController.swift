//
//  DateTimeViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 9/11/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation

class DateTimeViewController: UITableViewController {
  
  let defaults = UserDefaults.standard
  
  @IBOutlet weak var timeFormatSwitch: UISwitch!
  
  override func viewWillAppear(_ animated: Bool) {
    timeFormatSwitch.setOn(defaults.bool(forKey: "dateFormatSwitch"), animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
  }
  
  
  @IBAction func swichPressed(_ sender: UISwitch) {
    if timeFormatSwitch.isOn {
      defaults.set(true, forKey: "dateFormatSwitch")
    } else {
      defaults.set(false, forKey: "dateFormatSwitch")
    }
    
    //DBProvider.Instance.saveSetting()
    
  }
  
  
}

