//
//  DefinedFind.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 1/25/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct DefinedFind {
  let index : Int
  let letter : String
  let text : String
  let key : String
  
  init(index: Int, letter: String, text: String, key: String) {
    self.index = index
    self.letter = letter
    self.text = text
    self.key = key
  }
  
  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
      self.index = result!["index"]! as! Int
      self.letter = result!["letter"]! as! String
      self.text = result!["text"] as! String
      self.key = result!["key"]! as! String
  }
  
  func toDictionary() -> Dictionary<String, Any> {
    return [Const.INDEX : index, Const.LETTER : letter, Const.TEXT : text, Const.KEY : key]
  }
}
