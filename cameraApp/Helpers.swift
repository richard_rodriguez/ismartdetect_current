//  Helpers.swift
//  Created by Richard Rodriguez on 11/30/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import Foundation
import CryptoSwift
import CoreData
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import Photos
import AEXML
import MBProgressHUD
import SwiftyJSON
import KeychainSwift

class Helpers {
  
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = false
  private static let _instance = Helpers()
  let keychain = KeychainSwift()
  
  static var Instance : Helpers {
    return _instance
  }

  let CONST = 1242.0
  let TEXT_FORMAT = [0:(64.0/1242.0), 1:(92/1242.0), 2:(120.0/1242.0), 3:(148.0/1242.0)]
  let DATA_FORMAT = [0 : "DMS-WGS84", 1 : "DM-WGS84", 2: "DD-WGS84", 3: "UTM-WGS84", 4: "UTM-ETRS89", 5: "UTM-WGS84"]
  let TRANS = [0:1.0, 1:0.8, 2:0.6, 3:0.4, 4:0.2]
  
  // MARK: - Coordinate Conversion Functions
  func DMStoDM(lat: Double, lon: Double) -> (latitude: String, longitude: String) {
    var multiLat = lat * 3600
    multiLat.formTruncatingRemainder(dividingBy: 3600.0)
    var modLon = abs(multiLat)
    modLon.formTruncatingRemainder(dividingBy: 60)
    
    var mulLon = lon * 3600
    mulLon.formTruncatingRemainder(dividingBy: 3600.0)
    var mulLonMod = abs(mulLon)
    mulLonMod.formTruncatingRemainder(dividingBy: 60.0)
    
    
    let latDegrees = abs(Int(lat))
    let latMinutes = abs(Int(multiLat)) / 60
    let latSeconds = Double(modLon)
    let lonDegrees = abs(Int(lon))
    let lonMinutes = abs(Int(lon * 3600) % 3600) / 60
    let lonSeconds = Double(mulLonMod)
    
    return (String(format:"%d° %.4f\' %@", latDegrees, ((latSeconds/60.0)+Double(latMinutes)), latDegrees >= 0 ? "N" : "S"),
            String(format:"%d° %.4f\' %@", lonDegrees, ((lonSeconds/60.0)+Double(lonMinutes)), lonDegrees >= 0 ? "E" : "W"))
  }
  
  
  
  func coordinateToDMS(lat: Double, lon: Double) -> (latitude: String, longitude: String) {
    var multiLat = lat * 3600
    multiLat.formTruncatingRemainder(dividingBy: 3600.0)
    var modLon = abs(multiLat)
    modLon.formTruncatingRemainder(dividingBy: 60)
    
    var mulLon = lon * 3600
    mulLon.formTruncatingRemainder(dividingBy: 3600.0)
    var mulLonMod = abs(mulLon)
    mulLonMod.formTruncatingRemainder(dividingBy: 60.0)
    
    let latDegrees = abs(Int(lat))
    let latMinutes = abs(Int(multiLat)) / 60
    let latSeconds = Double(modLon)
    let lonDegrees = abs(Int(lon))
    let lonMinutes = abs(Int(lon * 3600) % 3600) / 60
    let lonSeconds = Double(mulLonMod)
    
    return (String(format:"%d°%d'%.2f\"%@", latDegrees, latMinutes, latSeconds, latDegrees >= 0 ? "N" : "S"),
            String(format:"%d°%d'%.2f\"%@", lonDegrees, lonMinutes, lonSeconds, lonDegrees >= 0 ? "E" : "W"))
  }
  
  
  func getDegMinSecOri(coord : String) -> (degree: String, second : String, minute : String, orientation : String){
    let degree  = coord.components(separatedBy: "°")[0] + "°"
    let second = coord.components(separatedBy: "°")[1].components(separatedBy: "\'")[0] + "'"
    let minute = coord.components(separatedBy: "°")[1].components(separatedBy: "'")[1].components(separatedBy: "\"")[0] + "\""
    let orientation = coord.components(separatedBy: "°")[1].components(separatedBy: "'")[1].components(separatedBy: "\"")[1]
    
    return (degree : degree, second : second, minute : minute, orientation : orientation)
  }
  
  func getDegMinOri(coord : String) -> (degree : String, minute : String, orientation : String) {
    
    let degree = coord.components(separatedBy: "°")[0] + "°"
    let minute = coord.components(separatedBy: "°")[1].components(separatedBy: "'")[0].trimmingCharacters(in: .whitespaces)
    let orientation = coord.components(separatedBy: "°")[1].components(separatedBy: "'")[1].trimmingCharacters(in: .whitespaces)
    
    return (degree : degree, minute : minute, orientation: orientation)
    
  }
  
  func UTMzdl(latDeg: Int) -> String{
    var UTMzdlChars=["C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X","X"]
    
    if(-80<=latDeg&&latDeg<=84){
      return UTMzdlChars[Int(floor(Double((latDeg+80)/8)))]
    }else{
      //Not normally reached
      print("No zdl: UTM is not valid for Lat :\(latDeg)")
      return ""
    }
  }
  
  
  
  
  //MARK: - Alerts Functions
  func alertUser(title: String, message: String) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(ok)
    return alert
  }
  
  func firstName(fullName: String) -> String{
    let firstName : String!
    firstName = fullName.components(separatedBy: " ")[0]
    return firstName!
  }
  
  
    //MARK: - Encrypted and Descrypted
    func encryptString(text: String) -> [UInt8]{
      do {
        
        let aes = try AES(key: "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw", iv: "gqLOHUioQ0QjhuvI") // aes128
        let ciphertext = try aes.encrypt(text.utf8.map({$0}))
        
        return ciphertext
      } catch {
        print(error)
        return [UInt8]()
      }
    }
  
    func decryptString(text: [UInt8]) -> String{
      do {
        let aes = try AES(key: "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw", iv: "gqLOHUioQ0QjhuvI") // aes128
        let result = try aes.decrypt((text))
        
        return (String(bytes: result, encoding: String.Encoding.utf8)!)
        
      } catch {
        print(error)
        return ""
      }
    }
  
  
  // Delete all data for a Entity function
  func deleteAllData(entity: String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let managedContext = appDelegate.managedObjectContext
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
    fetchRequest.returnsObjectsAsFaults = false
    
    do {
      let results = try managedContext.fetch(fetchRequest)
      for managedObject in results {
        let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
        managedContext.delete(managedObjectData)
      }
    } catch let error as NSError {
      print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
    }
  }
  
  
  func undefinedDefinedFinds(){
    editDefinedFind(index: 1, text: "undefined", letter: "x")
    editDefinedFind(index: 2, text: "undefined", letter: "a")
    editDefinedFind(index: 3, text: "undefined", letter: "b")
    editDefinedFind(index: 4, text: "undefined", letter: "c")
    editDefinedFind(index: 5, text: "undefined", letter: "d")
    editDefinedFind(index: 6, text: "undefined", letter: "e")
    editDefinedFind(index: 7, text: "undefined", letter: "f")
    editDefinedFind(index: 8, text: "undefined", letter: "o")
    editDefinedFind(index: 9, text: "undefined", letter: "v")
  }
  
  
  
  func editDefinedFind(index: Int, text: String, letter: String) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.DEFINED_FIND_ENTITY)
    
    request.predicate = NSPredicate(format: "letter = %@", letter)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! DefinedFindData
      managedObject.setValue(text, forKey: "text")
      
      let key = (result[0] as! DefinedFindData).key
      
      do{
        try manageContext.save()
        let definedFind = DefinedFind(index: index, letter: letter, text: text, key: key!)
        edit(definedFind)
        print("Defined Find  updated.")
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
    
  }

  func edit(_ definedFind : DefinedFind){
    if let uid = Auth.auth().currentUser?.uid {
      let ref = Database.database().reference().child(uid).child(Const.DEFINED_FINDS).child(definedFind.key)
      ref.updateChildValues(definedFind.toDictionary())
    }
  }
  
  func deleteAllCoreData(){
    deleteAllData(entity: Const.ALBUM_ENTITY)
    deleteAllData(entity: Const.NOTE_ENTITY)
    print("llegó aquí")
    deleteAllData(entity: Const.PREDEFINED_TEXT_ENTITY)
    deleteAllData(entity: Const.EMAIL_ENTITY)
    deleteAllData(entity: Const.IMAGE_ENTITY)
    Helpers.Instance.saveDefinedFinds()
  }
  
  //set and get Email/Name
  func cleanDefaultsData(){
    defaults.set("", forKey: Const.LOGIN_USER_NAME)
    defaults.set("", forKey: Const.EMAIL_LOGIN)
    defaults.set("", forKey: Const.LOGIN_TYPE)
    defaults.set("", forKey: Const.PROFILE_URL)
  }
  
  
  func setDefaultsData(email: String, name: String, loginType: String, profileURL: String? = nil){
    let firstName = self.firstName(fullName: name)
    defaults.set(firstName, forKey: Const.LOGIN_USER_NAME)
    defaults.set(email, forKey: Const.EMAIL_USED_FOR_LOGIN)
    defaults.set(loginType, forKey: Const.LOGIN_TYPE)
    defaults.set(profileURL, forKey: Const.PROFILE_URL)
  }

  
  ///Get Name, Email, profileImageURL
  func getInitialProfile() -> User?{
    
    var name : String?
    var profileURL : String?
    var email : String?
    var provider : String?
     

    if Auth.auth().currentUser != nil {
      let user = Auth.auth().currentUser!
      
      if (user.photoURL) != nil {
        profileURL =  String(describing: (user.photoURL!))
        //new?.profileImageURL = profileURL // + "?sz=250"
      } else {
        profileURL = ""
      }
      
      if user.providerData[0].providerID != nil {
        provider = user.providerData[0].providerID
      } else {
        provider = ""
      }
      
      if (user.displayName) != nil {
        name = (user.displayName)!
      } else {
        name = ""
      }
 
      if (user.email) != nil {
        email = (user.email)!
      } else {
        email = ""
      }
      
      let new = User(name: name!, email: email!, profileImageURL: profileURL!, provider: provider!)
      return new

    } else {
      print("User no logged in from function: getInitialProfile() on Helpers line 169")
    }
    
    return User(name: "", email: "", profileImageURL: "", provider: "")
  }
  
  
//  func changeDefaultAlbumStatus(_ user : User, status: Bool){
//      let id = user.uid
//      let ref = Database.database().reference().child(id).child(Const.PERSONAL_INFO)
//      let data : Dictionary<String, Any> = ["default_album_created": status]
//      ref.updateChildValues(data)
//  }

  
  func setDefaultsSetting(){   
    defaults.set(true, forKey: "dateFormatSwitch")
    defaults.set(true, forKey: "textShadow")
    defaults.set(3, forKey: "dataFormat")
    
    defaults.set(2, forKey: "fontSize")
    defaults.set(64.0, forKey: "fontSizeValue")
    defaults.set([], forKey: "emailListArray")
    defaults.set([], forKey: "selectedEmails")
    defaults.set([], forKey: "predTextListArray")
    defaults.set([], forKey: "selectedPredTexts")
    defaults.set(false, forKey: "saveOriginal")
    defaults.set(false, forKey: "dateTimeOnly")
    defaults.set(true, forKey: "emailOptionActive")
    defaults.set(true, forKey: "activePredifinedTexts")
    defaults.set(false, forKey: Const.ACTIVE_STAMP_PROJECT_NAME)

  }
  
  func setFirstDefaults() {
    defaults.set("iSmartDetect", forKey: "appName")
    defaults.set(true, forKey: "HasLaunched")
    defaults.set(defaults.string(forKey: "appName"), forKey: "albumName")
    defaults.set(true, forKey: "dateFormatSwitch")
    defaults.set(true, forKey: "textShadow")
    defaults.set(5, forKey: "dataFormat")
    defaults.set(2, forKey: "fontSize")
    defaults.set(64.0, forKey: "fontSizeValue")
    defaults.set([], forKey: "emailListArray")
    defaults.set([], forKey: "selectedEmails")
    defaults.set([], forKey: "predTextListArray")
    defaults.set([], forKey: "selectedPredTexts")
    defaults.set(false, forKey: "saveOriginal")
    defaults.set(false, forKey: "dateTimeOnly")
    defaults.set(true, forKey: "emailOptionActive")
    defaults.set(true, forKey: "activePredifinedTexts")
    defaults.set(false, forKey: Const.ACTIVE_STAMP_PROJECT_NAME)
  }
  
  
  
  func logoutFacebook() {
    if (FBSDKAccessToken.current() != nil) {
      let loginManager: FBSDKLoginManager = FBSDKLoginManager()
      //for logging out from facebook
      loginManager.logOut()
    }
  }

  
  
// Handler Strings
  func respondInCommasForm(for image: ImageData) -> String {
    let initialsStamp = image.initials! + String(image.id)
    
    var longDescription = ""
    
    print(longDescription)
    if image.longDesc == "Add a description..." {
      longDescription = ""
    } else if image.longDesc == nil {
      longDescription = ""
    } else {
      longDescription = image.longDesc!
    }
    
    print(longDescription)
    
    let acurracy = (image.acurracy == "1" ? "Added" : image.acurracy)
    
    
    return ("\(initialsStamp), \(image.longitude!), \(image.latitude!), \(image.gZone!), \(image.coordinateSystem!), \(acurracy!), \(image.timestamp!), \(escapeCommas(text: image.albumName!)), \(image.note!), \(image.symbolText!), \(longDescription)\n")
  }
  
  func subtitleInfoNoOnwerTextUTM(for image: ImageData) -> String {
   let initialsStamp = image.initials! + String(image.id)
   return "E:\(image.longitude!), N:\(image.latitude!) - \(image.acurracy!)m" + "\nID: " + initialsStamp + " | Date: \(image.timestamp!)"
  }
  
  func subtitleInfoOnwerTextUTM(for image: ImageData) -> String {
    let initialsStamp = image.initials! + String(image.id)
    return "E:\(image.longitude!), N:\(image.latitude!) - \(image.acurracy!)m" + " | \(image.note!)" + "\nID: " + initialsStamp + " | Date: \(image.timestamp!)"
  }
  
  func subtitleInfoNoOwnerTextLL(for image: ImageData) -> String {
    let initialsStamp = image.initials! + String(image.id)
    return "Lon:\(image.longitude!), Lat:\(image.latitude!) - \(image.acurracy!)m" + "\nID: " + initialsStamp + " | Date: \(image.timestamp!)"
  }
  
  func subtitleInfoOwnerTextLL(for image: ImageData)  -> String {
    let initialsStamp = image.initials! + String(image.id)
    return  "Lon:\(image.longitude!), Lat:\(image.latitude!) - \(image.acurracy!)m" + " | \(image.note!)" + "\nID: " + initialsStamp + " | Date: \(image.timestamp!)"
  }

  
  func escapeCommas(text: String) -> String{
    return text.replacingOccurrences(of: ",", with: " ")
  }

  // Add text to Images
  func textToImage(inImage: UIImage, image: Image)-> UIImage {
    
    let compressedImage = UIImage(data: UIImageJPEGRepresentation(inImage, 0.8)!)!

    
    
    let imageWidthSize = Double(compressedImage.size.width)
    let imageHeightSize = Double(compressedImage.size.height)
    var fontSize = (TEXT_FORMAT[defaults.integer(forKey: "fontSize")]!) * imageWidthSize
    print("font size: ", fontSize)
    print("width size: ", imageWidthSize)
    print("height size: ", imageHeightSize)
    
    //I know, it's late night.
    if imageWidthSize == imageHeightSize {
      fontSize = fontSize * 0.50 
    } else {
      fontSize = fontSize * 0.50
    }
    
   
    let TRES = CGFloat( (3/CONST)*imageWidthSize )
    let DIEZ = CGFloat( (10/CONST)*imageWidthSize )
    let CINCO = CGFloat( (5/CONST) * imageWidthSize )
    let QUINCE = CGFloat( (15/CONST)*imageWidthSize )
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    let transLevel = TRANS[defaults.integer(forKey: "trans")]!
    let textColor: UIColor = UIColor.white.withAlphaComponent(CGFloat(transLevel))
    let textFont: UIFont = UIFont(name: "Helvetica Bold", size: CGFloat( fontSize ))!
    var shadowSize : Double!
    var shadowRadiusSize : Double!
    let shadowActive = defaults.bool(forKey: "textShadow")
    var dateStr = ""
    let albumStr = image.albumName
    
    
    if shadowActive == true {
      shadowSize = fontSize / 9.6
      shadowRadiusSize = fontSize / 8
    } else {
      shadowSize = 0
      shadowRadiusSize = 0
    }
    
    
    let shadow : NSShadow = NSShadow()
    shadow.shadowOffset = CGSize(width: shadowSize, height: shadowSize)
    shadow.shadowColor = UIColor.black
    shadow.shadowBlurRadius = CGFloat(shadowRadiusSize)
    
    
    //Setup the image context using the passed image.
    let scale = UIScreen.main.scale
    UIGraphicsBeginImageContextWithOptions(compressedImage.size, false, scale)
    
    
    //Setups up the font attributes that will be later used to dictate how the text should be drawn
    let textFontAttributes = [
      NSAttributedStringKey.font: textFont,
      NSAttributedStringKey.foregroundColor: textColor,
      NSAttributedStringKey.shadow : shadow,
      ]
    
    
    //Put the image into a rectangle as large as the original image.
    compressedImage.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: compressedImage.size.width, height: compressedImage.size.height)) )
    
    
    //texts to add in the images
    let datum = image.coordinateSystem
    
    if datum == Const.COOR_UTM_WGS84 || datum == Const.COOR_UTM_UK_GREF || datum == Const.COOR_UTM_ETRS89 {
      //inverted to put East up and Nor down
      latStr = "N: \(image.latitude)"
      lonStr = "E: \(image.longitude)"
      zoneStr = "\(image.zone)"
    }else {
      latStr = "Lat: \(image.latitude)"
      lonStr = "Lon: \(image.longitude)"
      zoneStr = ""
    }
    
    var accuStr = ""
    
    if image.accuracy == "1" {
      accuStr = zoneStr + "   " + "Added"
    } else {
      accuStr = zoneStr + "   " + "+/-  \(image.accuracy)m"
    }
    
    let datumStr = image.coordinateSystem
    
    let isDateTimeOnly = defaults.bool(forKey: "dateTimeOnly")
    
    if isDateTimeOnly {
      dateStr = "\(image.timestamp)"
    } else {
      if image.initials != "" || image.initials != nil {
        dateStr = "\(image.timestamp)" + "  /  " + image.initials! + String( image.id )
      } else {
        dateStr = "\(image.timestamp)" + "  /  " + String( image.id )
      }
    }
    
    
    
    //size texts
    //let coorStrSize = (coorStr as NSString).size(attributes: textFontAttributes)
    //Por hacer el tamaño de las letras HEIGHT es el emismo para las todas las letras
    //TPor hacer el refactor cod
    let dateStrSize = (dateStr as NSString).size(withAttributes: textFontAttributes)
    let accuStrSize  = (accuStr as NSString).size(withAttributes: textFontAttributes)
    let datumStrSize = (datumStr as NSString).size(withAttributes: textFontAttributes)
    let ownTextSize = (image.ownerText as NSString).size(withAttributes: textFontAttributes)
    let albumTextSize = (albumStr as NSString).size(withAttributes: textFontAttributes)
    let standardHeightSize = dateStrSize.height
    
    
    
    var yDatePosition = compressedImage.size.height - (standardHeightSize*2) - TRES - CINCO
    
    if image.ownerText == "" || image.ownerText == "\n" {
      yDatePosition = compressedImage.size.height - (standardHeightSize) - CINCO
    } else {
      let ownTextRect = CGRect(origin: CGPoint(x:(compressedImage.size.width/2) - (ownTextSize.width/2) + CINCO, y:compressedImage.size.height - (standardHeightSize) - CINCO), size: CGSize(width: compressedImage.size.width - DIEZ, height: compressedImage.size.height))
          image.ownerText.draw(in: ownTextRect, withAttributes: textFontAttributes)
    }
    
    // Creating a point within the space that is as bit as the image.
    if isDateTimeOnly {
      let dateRect = CGRect(origin: CGPoint(x:(compressedImage.size.width/2) - (dateStrSize.width/2), y:yDatePosition), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      dateStr.draw(in: dateRect, withAttributes: textFontAttributes)
      
      if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
        let albumRect = CGRect(origin: CGPoint(x:((compressedImage.size.width/2) - (albumTextSize.width/2)), y: (yDatePosition - standardHeightSize )), size: CGSize(width: compressedImage.size.width - QUINCE, height: compressedImage.size.height))
        albumStr.draw(in: albumRect, withAttributes: textFontAttributes)
      }

      
    }else {
      let lonRect = CGRect(origin: CGPoint(x:CINCO, y:CINCO), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      let latRect = CGRect(origin: CGPoint(x:CINCO, y:CINCO + standardHeightSize + TRES), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      let datumRect = CGRect(origin: CGPoint(x:compressedImage.size.width - datumStrSize.width - CINCO, y:CINCO), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      let accuRect = CGRect(origin: CGPoint(x:compressedImage.size.width - accuStrSize.width - CINCO, y:CINCO + accuStrSize.height + TRES ), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      let dateRect = CGRect(origin: CGPoint(x:(compressedImage.size.width/2) - (dateStrSize.width/2), y:yDatePosition), size: CGSize(width: compressedImage.size.width - QUINCE , height: compressedImage.size.height))
      
      if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
        let albumRect = CGRect(origin: CGPoint(x:((compressedImage.size.width/2) - (albumTextSize.width/2)), y: (yDatePosition - standardHeightSize )), size: CGSize(width: compressedImage.size.width - QUINCE, height: compressedImage.size.height))
        albumStr.draw(in: albumRect, withAttributes: textFontAttributes)
      }
      
      //Now Draw the text into an image.
      latStr.draw(in: latRect, withAttributes: textFontAttributes)
      lonStr.draw(in: lonRect, withAttributes: textFontAttributes)
      dateStr.draw(in: dateRect, withAttributes: textFontAttributes)
      accuStr.draw(in: accuRect, withAttributes: textFontAttributes)
      datumStr.draw(in: datumRect, withAttributes: textFontAttributes)
      
    }
    
    // Create a new image out of the images we have created
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    
    // End the context now that we have the image we need
    UIGraphicsEndImageContext()
    //And pass it back up to the caller.
//
//      var finalCompressedImage : UIImage!
//
//    if (newImage.size.width != newImage.size.height) {
//      finalCompressedImage = resizeImage(image: newImage, withSize: CGSize(width: 2000, height: 1000))
//    } else {
//      finalCompressedImage = resizeImage(image: newImage, withSize: CGSize(width: 1000, height: 1000))
//    }
    
    
    return newImage
  }
  
  //MARK: - ALBUMS HELPERS
  func new(album name: String) {
    //Check if the folder exists, if not, create it
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", name)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions)
    
    if let first_Obj:AnyObject = collection.firstObject{
      //found the album
      self.albumFound = true
      self.assetCollection = first_Obj as! PHAssetCollection
    }else{
      //Album placeholder for the asset collection, used to reference collection in completion handler
      var albumPlaceholder:PHObjectPlaceholder!
      //create the folder
      NSLog("\nFolder \"%@\" does not exist\nCreating now ...", name)
      PHPhotoLibrary.shared().performChanges({
        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
        
        albumPlaceholder = request.placeholderForCreatedAssetCollection
      },
       completionHandler: {(success:Bool, error:Error?)in
        if(success){
          print("Successfully created folder from Helpers.Instance.addNewAlbum function, line 455")
          self.albumFound = true
          let collection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumPlaceholder.localIdentifier], options: nil)
          self.assetCollection = collection.firstObject! as PHAssetCollection
          
        }else{
          print("Error creating folder rom Helpers.Instance.addNewAlbum function, line 461")
          self.albumFound = false
        }
      })
    }
  }

  
  func add(new album: Album) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.ALBUM_ENTITY, into: manageContext)
    new.setValuesForKeys(album.toDictionary())
    do{
      try manageContext.save()
      print("Album saved on CoreData.")
      
      DBProvider.Instance.add(new: album, model: .album)
      
    }catch {
      print("Error trying to saving Album on Coredata")
    }
  }
  
  func unselectedAllAlbum(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContent = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    //request.predicate = NSPredicate(format: "a")
    if let albums = try? manageContent.fetch(request){
      for album in albums as! [AlbumData] {
        //let album = albums[0] as! AlbumData
        album.setValue(false, forKey: "selected")
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = album.key
          let ref = Database.database().reference().child(uid).child(Const.ALBUMS).child(key!)
          let new : Dictionary<String, Any> = [Const.NAME : album.name!,
                                               Const.KEY : album.key!]
          ref.updateChildValues(new)
          print("Album updated on Firebase.")
        }
        
        
        do{
          try manageContent.save()
          print("data updated.")
        }catch {
          print("error")
        }
      }
    }
  }

  
  func select(album name: String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    request.predicate = NSPredicate(format: "name = %@", name)
    if let result = try? manageContext.fetch(request){
      if result.count > 0 {
        let managedObject = result[0] as! AlbumData
        managedObject.setValue(true, forKey: "selected")
        let albumKey = managedObject.key!
        do{
          try manageContext.save()
          
          if let uid = Auth.auth().currentUser?.uid {
            let key = albumKey
            let ref = Database.database().reference().child(uid).child(Const.ALBUMS).child(key)
            let new : Dictionary<String, Any> = [Const.NAME : name,
                                                 Const.KEY : albumKey,
                                                 Const.SELECTED : true]
            ref.updateChildValues(new)
            print("Album updated on Firebase.")
          }
          
          print("Album updated on CoreData.")
        }catch {
          print("error")
        }
      }
    }
  }
  

  func albumList() -> [String] {
    var albumList = [String]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    do {
      let albums = try manageContext.fetch(request)
      for album in albums as! [AlbumData]{
        let newAlbum = Album(name: album.name!, key: album.key!, owner: album.owner, createdAt: album.createdAt, updatedAt: album.updatedAt)
        albumList.append(newAlbum.name)
      }
    } catch {
      print("Error fetching all Notes")
    }
  
    return albumList
  }

  
  func defaulfAlbumExist() -> Bool {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    request.predicate = NSPredicate(format: "name CONTAINS[c] 'iSmartDetect'") //, Const.APP_NAME)
    
    if let result = try? manageContext.fetch(request){
      if result.count > 0 {
        return true
      } else {
        return false
      }
    } else {
      return false
    }

  }
  
  
  func delete(album name: String) {
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", name)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions)
    
    if let first_Obj:AnyObject = collection.firstObject{
      print("found the album")
      self.assetCollection = first_Obj as! PHAssetCollection
      PHPhotoLibrary.shared().performChanges({
        PHAssetCollectionChangeRequest.deleteAssetCollections(collection)
      }, completionHandler: { (success, error) in
        if (success) {
          print("Successfully deleted folder from Helpers.Instance.addNewAlbum function, line 455")
        } else {
          print("Error deleting folder rom Helpers.Instance.addNewAlbum function, line 461: ", error!)
        }
      })
    }else{
      print("\nFolder \"%@\" does not exist\n", name)
    }
  }

  
    func checkPhotoLibraryPermission() {
      let status = PHPhotoLibrary.authorizationStatus()
  
      switch status {
        case .authorized:
          break
        case .denied, .restricted:
          print(".denied, .restricted")
        case .notDetermined:
          print("notDetermined")
  
          PHPhotoLibrary.requestAuthorization() { status in
            switch status {
              case .authorized: break
              case .denied, .restricted:
                self.showPhotoLibraryAlert()
                print(".denied, .restricted after request")
              case .notDetermined:
                
              break
              // won't happen
            }
          }
      }
  }


  func showPhotoLibraryAlert(){
    let alert = UIAlertController(
      title: "Action Required!",
      message: "Photo Library access required for this app.",
      preferredStyle: UIAlertControllerStyle.alert
    )
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) -> Void in
      UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
    }))
    
    alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: nil))
  }
  
  
  //MARK: - RETRIVING DATA FROM URL
  func retriveData(coor: CLLocationCoordinate2D, completion: @escaping (_ result: CLLocationCoordinate2D?) -> Void){
    let latitude = coor.latitude
    let longitude = coor.longitude
    
    let link = "http://geo.oiorest.dk/adresser/\(latitude),\(longitude)"
    //let link = "http://geo.oiorest.dk/etrs89.json?wgs84=\(latitude),\(longitude)"
    let url = NSURL(string: link)
    URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
      
      if error != nil {
        print(error!.localizedDescription)
        return
      }
      
      do {
        
        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: AnyObject]
        
        print(json)
 
        let lat = 1.0
        let lon = 2.0
        
        completion(CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(lon)))
        
      } catch let jsonError {
        print("entró: jsonError")
        print(jsonError)
       return
      }
      }.resume()
  }

  
  func retriveData(_ latitude : Double, _ longitude: Double, completion: @escaping (_ result: String) -> Void ) {
    let link = "http://dawa.aws.dk/adgangsadresser/reverse?x=\(longitude)&y=\(latitude)"
    
    let url = NSURL(string: link)
    URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
      
      if error != nil {
        print(error!.localizedDescription)
        return
      }
      
      let json = JSON(data: data!)
      let a = json["ejerlav"]["navn"].string!
      let b = json["jordstykke"]["matrikelnr"].string!
      let c =  a + ". Matr. \(b)"
      completion(c)

      }.resume()

  }
  
  func getMagicToolText(_ latitude : Double, _ longitude: Double, completion: @escaping (_ result : String) -> Void ){
    getURLandMatrikelnr(latitude, longitude) { (url, matr) in
      self.getNavnAndResult(link: url, matr: matr, completion: { (result) in
        completion(result)
      })
    }
  }
  
  func getURLandMatrikelnr(_ latitude : Double, _ longitude: Double, completion: @escaping (_ url: String, _ matr : String ) -> Void ) {
    let link =  "http://dawa.aws.dk/jordstykker/reverse?x=\(longitude)&y=\(latitude)"

    
    let url = NSURL(string: link)
    URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
      
      if error != nil {
        print(error!.localizedDescription)
        return
      }
      
      let json = JSON(data: data!)
      guard let matr = json["matrikelnr"].string else { return }
      guard let url = json["ejerlav"]["href"].string else { return }
     
      completion(url, matr)
      
      }.resume()
  }

  func getNavnAndResult(link : String, matr : String, completion: @escaping (_ navn : String) -> Void){
    let url = NSURL(string: link)
    URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
      
      if error != nil {
        print(error!.localizedDescription)
        return
      }
      
      let json = JSON(data: data!)
      let a = json["navn"].string!
      let b = matr
      let c = "Matr. \(b). " + a
      
      completion(c)
      
      }.resume()

  }
  
  
  
  
  func saveDefinedFinds(){
    defaults.set("undefined", forKey: "x")
    defaults.set("undefined", forKey: "a")
    defaults.set("undefined", forKey: "b")
    defaults.set("undefined", forKey: "c")
    defaults.set("undefined", forKey: "d")
    defaults.set("undefined", forKey: "e")
    defaults.set("undefined", forKey: "f")
    defaults.set("undefined", forKey: "o")
    defaults.set("undefined", forKey: "v")
    
  }

  
  //Show Info Helpers
  func showHUBMessage(message: String, view : UIView) -> MBProgressHUD{
    
    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .zoomIn
    loadingNotification.bezelView.color = UIColor.black
    loadingNotification.customView =  UIImageView(image: UIImage(named: "correct8"))
    loadingNotification.label.textColor = UIColor.white
    loadingNotification.label.text = message
    
    return loadingNotification
  }
  
  
  ///Dates convertions
  func stringToDate(_ str: String)->Date{
    let formatter = DateFormatter()
    //formatter.dateFormat="yyyy-MM-dd hh:mm:ss Z"
    formatter.dateFormat="MM/dd/yy, h:mm:ss a"
    return formatter.date(from: str)!
  }
  
  func dateToString(_ str: Date)->String{
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle=DateFormatter.Style.medium
    return dateFormatter.string(from: str)
  }
  
  func checkFirstAlbumExist(albumName: String) -> Bool {
    return Helpers.Instance.albumList().contains(albumName) ? true : false
  }
  
  func createFirstAlbum() {
        let key = Database.database().reference().child(Const.ALBUMS).childByAutoId().key
        let date = Date().timeIntervalSince1970
        var owner = keychain.get("secure_digits") ?? ""
    
        if owner == "" {
          owner = "0000"
        }
    
        let albumName = Const.APP_NAME
    
        let album = Album(name: albumName, key: key, owner: owner, createdAt: date, updatedAt: date)
    
    
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let manageContext = appDelegate.managedObjectContext
        let new = NSEntityDescription.insertNewObject(forEntityName: Const.ALBUM_ENTITY, into: manageContext)
        new.setValuesForKeys(album.toDictionary())
        do{
          try manageContext.save()
          print("Album saved on CoreData.")
        }catch {
          print("Error trying to saving Album on Coredata")
        }
  }
  
  func records(for album: String) -> Int {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      return results.count
    } catch {
      print("error")
    }
    return 0
  }
  
  func fetchImage(from key : String) -> Image {
    var image : Image!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      let result = results[0] as! ImageData
      image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      return image
      
    } catch {
      print("error")
    }
    return image
  }
  


  
  
}

extension Helpers {
  //UK Ref coords
  
  func LLtoNE(latitude : Double, longitude: Double) -> (northing : String, eastring: String){
    let deg2rad = Double.pi / 180.0
    
    let WGS84_AXIS = 6378137;
    let WGS84_ECCENTRIC = 0.00669438037928458;
    let OSGB_AXIS = 6377563.396;
    let OSGB_ECCENTRIC = 0.0066705397616;
    let nLat = latitude;  // 51.5 degrees north
    let nLon = longitude;  // 0.5 degrees west (negative for west)
    let phip = nLat * deg2rad;
    let lambdap = nLon * deg2rad;
    
    let geo = transform(phip, lambdap, Double(WGS84_AXIS), WGS84_ECCENTRIC, 0, OSGB_AXIS, OSGB_ECCENTRIC, -446.448, 125.157, -542.06, -0.1502, -0.247, -0.8421, 20.4894);
    
    
    let phi = geo.latitude  // * deg2rad      // convert latitude to radians
    let lam = geo.longitude //  * deg2rad   // convert longitude to radians
    let a = 6377563.396       // OSGB semi-major axis
    let b = 6356256.909   // OSGB semi-minor axis
    let e0 = 400000.0           // OSGB easting of false origin
    let n0 = -100000          // OSGB northing of false origin
    let f0 = 0.9996012717     // OSGB scale factor on central meridian
    let e2 = 1 - (b*b)/(a*a) //0.0066705397616  // OSGB eccentricity squared
    let lam0 = -2 * deg2rad  // OSGB false east
    let phi0 = 49 * deg2rad   // OSGB false north
    let  af0 = a * f0
    let  bf0 = b * f0
    
    // easting
    let slat2 = sin(phi) * sin(phi)
    let nu = af0 / sqrt(1 - e2 * slat2)
    let rho = (af0 * (1 - e2)) / pow((1 - e2 * slat2), 1.5)
    let eta2 = (nu / rho) - 1
    
    let clat3 = cos(phi) * cos(phi) * cos(phi) //pow(cos(phi),3)
    let tlat2 = tan(phi) * tan(phi)
    let clat5 = cos(phi) * cos(phi) * cos(phi) * cos(phi) * cos(phi) //pow(cos(phi), 5)
    let tlat4 = tan(phi) * tan(phi) * tan(phi) * tan(phi) //pow(tan(phi), 4)
    
    
    // northing
    let n = (a - b) / (a + b)
    let M = Marc(bf0, n, phi0, phi)
    
    let I = M + Double(n0)
    let II = (nu / 2.0) * sin(phi) * cos(phi)
    let III = ((nu / 24.0) * sin(phi) * clat3 * (5 - tlat2 + (9 * eta2)))
    let IIIA = ((nu / 720.0) * sin(phi) * clat5) * (61 - (58 * tlat2) + tlat4)
    let IV = nu * cos(phi)
    let V = (nu / 6.0) * clat3 * ((nu / rho) - tlat2)
    let VI = (nu / 120.0) * clat5 * ((5.0 - 18.0 * tlat2 + tlat4 + (14.0 * eta2) - (58.0 * tlat2 * eta2)))
    
    let p = lam - lam0
    let north = I + ((p * p) * II) + (pow(p, 4) * III) + (pow(p, 6) * IIIA)
    let east = e0 + (p * IV) + (pow(p, 3) * V) + (pow(p, 5) * VI)
    
    let northing = String(north);      // convert to string
    let easthing = String(east);       // ditto
    
    print(M)
    
    
    return (northing, easthing)
    
  }
  
  func Marc(_ bf0: Double, _ n: Double, _ phi0 : Double, _ phi : Double) -> Double {
    let Ma = ((1 + n + ((5.0 / 4.0) * (n * n)) + ((5.0 / 4.0) * (n * n * n))) * (phi - phi0))
    let Mb = (((3 * n) + (3 * (n * n)) + ((21.0 / 8.0) * (n * n * n))) * (sin(phi - phi0)) * (cos(phi + phi0)))
    let Mc = ((((15.0 / 8.0) * (n * n)) + ((15.0 / 8.0) * (n * n * n))) * (sin(2 * (phi - phi0))) * (cos(2 * (phi + phi0))))
    let Md = (((35.0 / 24.0) * (n * n * n)) * (sin(3 * (phi - phi0))) * (cos(3 * (phi + phi0))))
    
    let Marc = bf0 * (Ma - Mb + Mc - Md)
    
    return Marc
  }
  
  func NE2NGR(easting: Double, northing: Double) -> String {
    var eX = easting / 500000.0
    var nX = northing / 500000.0
    var tmp = floor(eX)-5.0 * floor(nX)+17.0
    nX = 5 * (nX - floor(nX))
    eX = 20.0 - 5.0 * floor(nX) + floor(5.0 * (eX - floor(eX)))
    
    if (eX > 7.5) {
      eX = eX + 1
    }
    
    if (tmp > 7.5) {
      tmp = tmp + 1
    }
    
    let eing = String(Int(easting))
    let ning = String(Int(northing))
    
    let resultEing = eing.substring(from:eing.index(eing.endIndex, offsetBy: -5))
    let resultNing = ning.substring(from:ning.index(ning.endIndex, offsetBy: -5))
    
    let ngr = String(describing: UnicodeScalar(Int(tmp) + 65)!) + String(describing: UnicodeScalar(Int(eX) + 65)!) + " " + String(describing: resultEing) + " " + String(describing: resultNing)
    return ngr
    
  }
  
  func transform(_ lat : Double, _ lon : Double, _ a : Double, _ e : Double, _ h : Double, _ a2 : Double, _ e2 : Double, _ xp : Double, _ yp : Double, _ zp : Double, _ xr : Double, _ yr : Double, _ zr : Double, _ s : Double) -> (latitude: Double, longitude: Double, height: Double) {
    // convert to cartesian; lat, lon are radians
    let deg2rad = Double.pi / 180.0
    
    let sf = s * 0.000001;
    var v = a / (sqrt(1 - (e * sin(lat) * sin(lat) )));
    let x = (v + h) * cos(lat) * cos(lon);
    let y = (v + h) * cos(lat) * sin(lon);
    let z = ((1 - e) * v + h) * sin(lat);
    // transform cartesian
    let xrot = (xr / 3600) * deg2rad;
    let yrot = (yr / 3600) * deg2rad;
    let zrot = (zr / 3600) * deg2rad;
    let hx = x + (x * sf) - (y * zrot) + (z * yrot) + xp;
    let hy = (x * zrot) + y + (y * sf) - (z * xrot) + yp;
    let hz = (-1 * x * yrot) + (y * xrot) + z + (z * sf) + zp;
    
    // Convert back to lat, lon
    let lon = atan(hy / hx);
    let p = sqrt((hx * hx) + (hy * hy));
    var lat = atan(hz / (p * (1 - e2)));
    v = a2 / (sqrt(1 - e2 * (sin(lat) * sin(lat))));
    var errvalue = 1.0;
    var lat0 = 0.0;
    
    while (errvalue > 0.001){
      lat0 = Double(atan((hz + e2 * v * sin(lat)) / p));
      errvalue = abs(lat0 - lat);
      lat = lat0;
    }
    
    let h = p / cos(lat) - v;
    //var geo = { latitude: lat, longitude: lon, height: h };  // object to hold lat and lon
    return (lat, lon, h);
  }
  
  func split(zone: String) -> (zone: String, latBand : String, hemisphere: String){
    let fHemisphere = zone.components(separatedBy: " ")[1]
    let fLatBand = zone.components(separatedBy: " ")[0].trimmingCharacters(in: CharacterSet(charactersIn: "01234567890."))
    let fZone = zone.components(separatedBy: " ")[0].trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted)
    
    return (zone : fZone, latBand : fLatBand, hemisphere: fHemisphere )
    
    
  }
  
  func DMtoDD(latDeg: Double, latMin: Double, latDir: String?, longDeg: Double, longMin: Double, longDir: String?) -> CLLocationCoordinate2D {
    var latitude = CLLocationDegrees()
    if latDeg > 0 {
      latitude = CLLocationDegrees(latDeg + ((latMin*60)/3600))
      if latDir == "S" {latitude *= -1}
    }
    else{
      latitude = CLLocationDegrees((latDeg * -1) + ((latMin*60)/3600))
      latitude *= -1
    }
    var longitude = CLLocationDegrees()
    if longDeg > 0 {
      longitude = CLLocationDegrees(longDeg + ((longMin*60)/3600))
      if longDir == "W" {longitude *= -1}
    }
    else{
      longitude = CLLocationDegrees((longDeg * -1) + ((longMin*60)/3600))
      longitude *= -1
    }
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  func DMStoDD(latDeg: Double, latMin: Double, latSec: Double, latDir: String?, longDeg: Double, longMin: Double, longSec: Double, longDir: String?) -> CLLocationCoordinate2D {
    var latitude = CLLocationDegrees()
    if latDeg > 0 {
      latitude = CLLocationDegrees(latDeg + ((latMin*60)/3600) + (latSec/3600))
      if latDir == "S" {latitude *= -1}
    }
    else{
      latitude = CLLocationDegrees((latDeg * -1) + ((latMin*60)/3600) + (latSec/3600))
      latitude *= -1
    }
    
    var longitude = CLLocationDegrees()
    if longDeg > 0 {
      longitude = CLLocationDegrees(longDeg + ((longMin*60)/3600) + (longSec/3600))
      if longDir == "W" {longitude *= -1}
    }
    else{
      longitude = CLLocationDegrees((longDeg * -1) + ((longMin*60)/3600) + (longSec/3600))
      longitude *= -1
    }
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }



}

extension Helpers {
  func buildDescription(_ lat: String, _ lon: String) -> String {
    return "<\(lat),\(lon)>"
  }
  
  func heart(_ accuracy : String) -> String {
    let acc = Double(accuracy)
    
    if acc! <= 6 {
      return "💚"
    } else if (acc! > 6 && acc! <= 15) {
      return "💛"
    } else {
      return "❤️"
    }
  }
  
}

extension Helpers {
  func colorTheme() -> UIColor {
    let screenshot = defaults.integer(forKey: "backgroundColor")
    
    switch screenshot {
    case 0:
      return UIColor(red: 234/255, green: 32/255, blue: 52/255, alpha: 1)
    case 1:
      return UIColor(red: 52/255, green: 32/255, blue: 234/255, alpha: 1)
    case 2:
       return UIColor(red: 32/255, green: 234/255, blue: 52/255, alpha: 1)
    case 3:
      return UIColor(red: 234/255, green: 32/255, blue: 52/255, alpha: 1)
    default:
      return  UIColor(red: 234/255, green: 234/255, blue: 52/255, alpha: 1)
    }
  }
  
  func getDecrypt(_ data : AnyObject) -> String {
    
    
    if (data is Array<UInt8>) {
      return decryptString(text: (data as! [UInt8]))
    } else {
      return data as! String
    }
  }
  
}

extension Helpers {
  func resizeImage(image: UIImage, withSize: CGSize) -> UIImage {
    
    var actualHeight: CGFloat = image.size.height
    var actualWidth: CGFloat = image.size.width
    let maxHeight: CGFloat = withSize.width
    let maxWidth: CGFloat = withSize.height
    var imgRatio: CGFloat = actualWidth/actualHeight
    let maxRatio: CGFloat = maxWidth/maxHeight
    let compressionQuality = 0.7//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
      if(imgRatio < maxRatio) {
        //adjust width according to maxHeight
        imgRatio = maxHeight / actualHeight
        actualWidth = imgRatio * actualWidth
        actualHeight = maxHeight
      } else if(imgRatio > maxRatio) {
        //adjust height according to maxWidth
        imgRatio = maxWidth / actualWidth
        actualHeight = imgRatio * actualHeight
        actualWidth = maxWidth
      } else {
        actualHeight = maxHeight
        actualWidth = maxWidth
      }
    }
    
    let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
    UIGraphicsBeginImageContext(rect.size)
    image.draw(in: rect)
    let image: UIImage  = UIGraphicsGetImageFromCurrentImageContext()!
    let imageData = UIImageJPEGRepresentation(image, CGFloat(compressionQuality))
    UIGraphicsEndImageContext()
    
    let resizedImage = UIImage(data: imageData!)
    return resizedImage!
    
  }
}

extension Helpers {
  func countFiles() -> Int {
    var albumList = [String]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    var total = 0
    
    do {
      let files = try manageContext.fetch(request)
      total = files.count
    } catch {
      print("Error fetching all Notes")
    }
    
    return total
  }
  
}
