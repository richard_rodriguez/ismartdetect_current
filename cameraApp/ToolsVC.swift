//
//  ToolsVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/26/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation

class ToolsVC: UITableViewController {
  
  let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.tintColor = Const.RED
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
    
    print("default value: ", defaults.integer(forKey: "numberOfFiles"))
    print("database value: ",  Helpers.Instance.countFiles())
    
    if indexPath.section == 0 && indexPath.row == 0 {
      
      //if  Helpers.Instance.countFiles() >= defaults.integer(forKey: "numberOfFiles")  {
        performSegue(withIdentifier: "goToProjectNames", sender: self)
//      } else {
//        //let porcentage = (Helpers.Instance.countFiles() / defaults.integer(forKey: "numberOfFiles")) * 100
//
//        let alert = UIAlertController(title: "Wait", message: "The app still downloading files, it will show this option when finish.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//      }
    } else if indexPath.section == 0 && indexPath.row == 5 {
//      if Helpers.Instance.countFiles() >= defaults.integer(forKey: "numberOfFiles") {
        performSegue(withIdentifier: "goToMapAndPhotos", sender: self)
//      } else {
//        let alert = UIAlertController(title: "Wait", message: "The app still downloading files, it will show this option when finish.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//
//        present(alert, animated: true, completion: nil)
//      }
    }
    
    
  }
  
  
  
  
  override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    if section == 0 {
      return "iSmartDetect \(nsObject!)"
    } else {
      return nil
    }
  }
  
  @IBAction func close(_ sender: Any) {
    DispatchQueue.main.async {
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  
}

