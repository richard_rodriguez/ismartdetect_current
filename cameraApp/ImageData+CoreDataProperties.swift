//
//  ImageData+CoreDataProperties.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 6/13/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation
import CoreData


extension ImageData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageData> {
        return NSFetchRequest<ImageData>(entityName: "ImageData")
    }

    @NSManaged public var acurracy: String?
    @NSManaged public var albumName: String?
    @NSManaged public var coordinateSystem: String?
    @NSManaged public var createdAt: Double
    @NSManaged public var desc: String?
    @NSManaged public var gZone: String?
    @NSManaged public var id: Int32
    @NSManaged public var initials: String?
    @NSManaged public var key: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
    @NSManaged public var note: String?
    @NSManaged public var symbolLetter: String?
    @NSManaged public var symbolText: String?
    @NSManaged public var timestamp: String?
    @NSManaged public var updatedAt: Double
    @NSManaged public var url: String?
    @NSManaged public var imgData: NSData?
    @NSManaged public var longDesc: String?

}
