//
//  EmailOptionViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 10/16/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//


import Foundation


class EmailOption: UITableViewController {
  
  let defaults = UserDefaults.standard
  
  @IBOutlet weak var emailOptionSwitch: UISwitch!
  @IBOutlet weak var addEmailsTCell: UITableViewCell!
  
  
  override func viewWillAppear(_ animated: Bool) {
    emailOptionSwitch.setOn(defaults.bool(forKey: "emailOptionActive"), animated: true)
    
    if emailOptionSwitch.isOn {
      addEmailsTCell.isHidden = false
    } else {
      addEmailsTCell.isHidden = true
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
  }
  
  @IBAction func switchAction(_ sender: UISwitch) {
    if emailOptionSwitch.isOn {
      addEmailsTCell.isHidden = false
      defaults.set(true, forKey: "emailOptionActive")
    } else {
      addEmailsTCell.isHidden = true
      defaults.set(false, forKey: "emailOptionActive")
    }
    
    //DBProvider.Instance.saveSetting()

  }
}
