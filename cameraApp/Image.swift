//
//  Image.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/5/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase
import CryptoSwift

struct Image {
  let id : Int
  let latitude : String
  let longitude : String
  let zone : String
  let accuracy : String
  let timestamp : String
  let albumName : String
  let coordinateSystem : String
  let ownerText : String
  let key : String
  let desc : String
  let url : String?
  let initials : String?
  let symbolLetter : String?
  let symbolText : String?
  let createdAt: Double
  let updatedAt: Double
  let imgData: NSData?
  let longDesc : String?
  
  var initialsStamp : String {
    if let initials = initials {
      return "\(initials) \(id)"
    } else {
      return "\(id)"
    }
    
    
  }
  
  
  var datumAndZone : String {
    return self.coordinateSystem.components(separatedBy: "-")[1] + " " + self.coordinateSystem.components(separatedBy: "-")[0] + " " + self.zone
  }
  
  var accuracyValue : String {
    return self.accuracy == "1" ? "Added" : self.accuracy
  }
  
  var decimalLatitude : Double {
    return location().latitude
  }
  
  var decimalLongitude : Double {
    return location().longitude
  }
  
  
  init(id: Int, latitude: String, longitude : String, zone : String, accuracy : String, timestamp : String, albumName : String, coordinateSystem : String, ownerText : String, key : String, desc: String, url : String? = "", initials: String? = "", symbolLetter : String? = "x", symbolText : String? = "undefined", createdAt : Double, updatedAt: Double, imgData: NSData?, longDesc : String? ){
    self.id = id
    self.latitude = latitude
    self.longitude = longitude
    self.zone = zone
    self.accuracy = accuracy
    self.timestamp = timestamp
    self.albumName = albumName
    self.coordinateSystem = coordinateSystem
    self.ownerText = ownerText
    self.key = key
    self.desc = desc
    self.url = url!
    self.initials = initials ?? ""
    self.symbolText = symbolText
    self.symbolLetter = symbolLetter
    self.createdAt = createdAt
    self.updatedAt = updatedAt
    self.imgData = imgData
    self.longDesc = longDesc
  }
  

  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
    self.id = result![Const.ID] as! Int
    self.latitude = Helpers.Instance.getDecrypt( result![Const.LATITUDE]! )
    self.longitude = Helpers.Instance.getDecrypt( result![Const.LONGITUDE]! )
    self.zone = result![Const.ZONE] as! String
    self.accuracy = result![Const.ACCURRARY] as! String
    self.timestamp = result![Const.TIMESTAMP] as! String
    self.albumName = result![Const.ALBUM_NAME] as! String
    self.coordinateSystem = result![Const.COORDINATE_SYSTEM] as! String
    self.ownerText = result![Const.OWNER_TEXT] as! String
    self.key = result![Const.KEY] as! String
    self.desc = Helpers.Instance.getDecrypt( result![Const.DESC]! )
    self.url = result![Const.URL] as? String
    self.initials = result![Const.INITIALS] as? String
    self.symbolText = result![Const.SYMBOL_TEXT] as? String
    self.symbolLetter = result![Const.SYMBOL_LETTER] as? String
   // self.imgData = result![Const.IMG_DATA] as? String

    if let imgDataValue = result![Const.IMG_DATA] as? String {
      self.imgData = NSData(base64Encoded: imgDataValue, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
    } else {
      self.imgData = UIImagePNGRepresentation(UIImage(named: "placeholderImage")!)! as NSData
    }

    if let createdAtValue = result![Const.CREATED_AT] {
      self.createdAt = createdAtValue as! Double
    } else {
      self.createdAt = Date().timeIntervalSince1970
    }
    
    if let updatedAtValue = result![Const.UPDATED_AT] {
      self.updatedAt = updatedAtValue as! Double
    } else {
      self.updatedAt = Date().timeIntervalSince1970
    }
    
    if let longDescription = result![Const.LONG_DESCRIPTION] {
      self.longDesc = longDescription as? String
    } else {
      self.longDesc = ""
    }
    
    
  }
  
  
  
  
  func toDictionary() -> Dictionary<String, Any> {
    var data : Data!
    
    if let imageData = imgData {
      data = imageData as Data!
    } else {
      data = UIImageJPEGRepresentation( UIImage(named: "placeholderImage")!, 1)
    }
    
    return [Const.ID : id,
            Const.LATITUDE : latitude, //Helpers.Instance.encryptString(text: latitude),
            Const.LONGITUDE : longitude, //Helpers.Instance.encryptString(text: longitude),
            Const.ZONE : zone,
            Const.ACCURRARY: accuracy,
            Const.TIMESTAMP: timestamp,
            Const.ALBUM_NAME: albumName,
            Const.COORDINATE_SYSTEM: coordinateSystem,
            Const.OWNER_TEXT: ownerText,
            Const.KEY : key,
            Const.DESC : desc, //Helpers.Instance.encryptString(text: desc),
            Const.URL : url ?? "",
            Const.INITIALS : initials ?? "",
            Const.SYMBOL_TEXT : symbolText ?? "",
            Const.SYMBOL_LETTER : symbolLetter ?? "",
            Const.CREATED_AT : createdAt,
            Const.UPDATED_AT : updatedAt,
            Const.IMG_DATA : data.base64EncodedString(options: .lineLength64Characters),
            Const.LONG_DESCRIPTION : longDesc ?? ""]
  }
  
  
  func toCoreDataDictionary() -> Dictionary<String, Any> {
    return [Const.ID : id,
            Const.LATITUDE : latitude,
            Const.LONGITUDE : longitude,
            Const.ZONE : zone,
            Const.ACCURRARY: accuracy,
            Const.TIMESTAMP: timestamp,
            Const.ALBUM_NAME: albumName,
            Const.COORDINATE_SYSTEM: coordinateSystem,
            Const.OWNER_TEXT: ownerText,
            Const.KEY : key,
            Const.DESC : desc,
            Const.URL : url ?? "",
            Const.INITIALS : initials ?? "",
            Const.SYMBOL_TEXT : symbolText ?? "",
            Const.SYMBOL_LETTER : symbolLetter ?? "",
            Const.CREATED_AT : createdAt,
            Const.UPDATED_AT : updatedAt,
            Const.IMG_DATA : imgData!,
            Const.LONG_DESCRIPTION : longDesc ?? ""
    ]

  }
  
  
  func location() -> (latitude: Double, longitude: Double) {
    let query = desc
    let regex = try! NSRegularExpression(pattern:"<(.*?)>", options: [])
    let tmp = query as NSString
    var endResults = [String]()
    
    regex.enumerateMatches(in: query, options: [], range: NSMakeRange(0, query.count)) { result, flags, stop in
      if let range = result?.range(at: 1) {
        endResults.append(tmp.substring(with: range))
      }
    }
    
    let coord = endResults[0]
    return (Double(coord.components(separatedBy: ",")[1])!, Double(coord.components(separatedBy: ",")[0])!)
  }
  
  func generalTimestamp() -> String {
    let regex = "@.+[0-9]\\s"
    let text = desc
    
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let nsString = text as NSString
      let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
      return results.map { nsString.substring(with: $0.range) }[0].components(separatedBy: "@ ")[1]
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return ""
    }
  }
  
}




extension Image: Equatable {}

func ==(lhs: Image, rhs: Image) -> Bool {
  let areEqual = lhs.id == rhs.id &&
    lhs.desc == rhs.desc
  
  return areEqual
}
