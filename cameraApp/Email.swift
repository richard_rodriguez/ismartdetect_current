//
//  Email.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/6/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase

typealias dict = [String: AnyObject]


struct Email {
  let email : String!
  var selected : Bool!
  let key : String!
  
  init(email: String, selected: Bool, key: String) {
    self.email = email
    self.selected = selected
    self.key = key
  }
  
  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
    self.email = result!["email"]! as! String
    self.selected = result!["selected"]! as! Bool
    self.key = result!["key"]! as! String
  }
  
  
  func toDictionary() -> Dictionary<String, Any> {
    return [Const.EMAIL : email!, Const.SELECTED : selected, Const.KEY : key!]
  }

}
