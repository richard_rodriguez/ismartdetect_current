//
//  BackgroundColorVC.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 9/19/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class BackgroundColorVC: UITableViewController {

  let defaults = UserDefaults.standard
  
  override func viewWillAppear(_ animated: Bool) {
    selectRow(index: (defaults.integer(forKey: "backgroundColor")))
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
  }
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
    
    defaults.set(indexPath.row, forKey: "backgroundColor")
  }
  
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .none
    }
  }
  
  func selectRow(index: Int) {
    let rowToSelect:IndexPath = IndexPath(row: index, section: 0);  //slecting 0th row with 0th section
    self.tableView.selectRow(at: rowToSelect, animated: false, scrollPosition: UITableViewScrollPosition.middle);
    
    if let cell = tableView.cellForRow(at: rowToSelect) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
  }
  
  
  
}
