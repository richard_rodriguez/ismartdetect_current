//
//  SubscriptionInfoVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 5/15/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class SubscriptionInfoVC: UIViewController {

  @IBOutlet weak var text: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    
    let htmlString =  "<div style=\"font-size: 16px;font-family: 'HelveticaNeue-Light'\">" +
    "<h1> Premium Plan</h1>" +
    "<p>You may purchase an auto-renewing Premium Plan subscription through an In-App Purchase.  (If you do not maintain a subscription you cannot use our server to backup your files and sync content.)</P> " +
    "<p><strong>The subscription will give you access to these premium features:</P></strong> " +
    "<p>- Backup and Restore Services </P> " +
    "<p>- Encrypted GPS data</P> " +
    "<p>- Multi-user use with real-time sync</P> " +
    "<p>- Show Photos on the Map</P> " +
    "<p>- Download Photos in Multi-user</P> " +
    "<p>- Clone files between all IOS Devices</P><br/> " +
    "<p><strong>• Auto-renewable monthly</P> </strong>" +
    "<p><strong>• 1 month ($0.99) but first 3 months free</P></strong> " +
    "<p><strong>• Your subscription will be charged to your iTunes account at confirmation of purchase and will be automatically renewed (at the duration selected) unless auto-renew is turned off at least 24 hours before the end of the current period.</P> </strong>" +
    "<p><strong>• Cancel a subscription by turning off auto-renewal.  The remaining duration of the subscription will remain active until the next billing cycle and terminate. </P> </strong>" +
    "<p><strong>• You can manage your subscription and/or turn off auto-renewal by visiting your iTunes Account Settings after purchase.</P> </strong></div>"
    
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)

    let attributedString = try! NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    //let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
    
    text.attributedText = attributedString
    
  }

  @IBAction func goToPolicyLink(_ sender: Any) {
    let url = URL(string: "https://ismartdetect.com/app-privacy-policy/")
    UIApplication.shared.openURL(url!)
  }
  
  @IBAction func goToTermsLink(_ sender: Any) {
    let url = URL(string: "https://ismartdetect.com/terms-of-use/")
    UIApplication.shared.openURL(url!)
  }
  
  
}
