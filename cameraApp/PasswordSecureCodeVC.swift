//
//  PasswordSecureCodeVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/29/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import SmileLock
import KeychainSwift

class PasswordSecureCodeVC: UIViewController {

  @IBOutlet weak var passwordStackView: UIStackView!
  let keychain = KeychainSwift()

  //MARK: Property
  var passwordContainerView: PasswordContainerView!
  let kPasswordDigit = 4
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //create PasswordContainerView
    passwordContainerView = PasswordContainerView.create(in: passwordStackView, digit: kPasswordDigit)
    passwordContainerView.delegate = self
    
    passwordContainerView.tintColor = UIColor.darkGray
    passwordContainerView.highlightedColor = Const.RED

  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationItem.title = "Type your digits"
    passwordContainerView.clearInput()
  }
}



extension PasswordSecureCodeVC: PasswordInputCompleteProtocol {
  func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
    if validation(input) {
      validationSuccess()
    } else {
      validationFail()
    }
  }
  
  public func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
    if success {
      self.validationSuccess()
    } else {
      passwordContainerView.clearInput()
    }
  }
}

private extension PasswordSecureCodeVC {
  func validation(_ input: String) -> Bool {
    let secureDigits = keychain.get("secure_digits") ?? ""
    return input == secureDigits
  }
  
  func validationSuccess() {
    performSegue(withIdentifier: "editSecureDigits", sender: self)
  }
  
  func validationFail() {
    passwordContainerView.wrongPassword()
  }
}
