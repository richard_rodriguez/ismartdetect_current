//  ImagesCVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/25/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import CoreData
import SDWebImage
import KeychainSwift


private let reuseIdentifier = "ImageCell"

@objc class ImagesCVC: UICollectionViewController, UICollectionViewDelegateFlowLayout, SDWebImageManagerDelegate {
    var images = [Image]()
    var album : String!
    var albumNameList = [String]()
    var responde = ""
    var albumNameListFiltered = [String]()
    var appName : String!
    let keychain = KeychainSwift()
    let placeholder = UIImage(named: "placeholderImage")
  
  //1.0.18
  var handleImages: UInt!
  var refImages : DatabaseReference!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
      navigationItem.backBarButtonItem?.title = "Back"
    navigationController?.setToolbarHidden(false, animated: true)

    self.images = []
    _ = fetchData()
    self.collectionView?.reloadData()
  }
  
  
  override func didReceiveMemoryWarning() {
    print("didReceiveMemoryWarning")
    SDWebImageManager.shared().imageCache?.clearMemory()

  }
    override func viewDidLoad() {
      super.viewDidLoad()
      //startObservers()
      self.navigationItem.title = "Photos"
      SDWebImageManager.shared().delegate = self
      SDWebImageDownloader.shared().shouldDecompressImages = false
      
      let exportButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(GoogleMapVC.exportData(_:)))
      
      let listingButtonImage = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 27))
      let listingImage = UIImage(named: "listing-bold")
      listingButtonImage.setImage(listingImage!, for: .normal)
      listingButtonImage.addTarget(self, action: #selector(GoogleMapVC.deletePoints(_:)), for: .touchUpInside)
      let deleteButton = UIBarButtonItem(customView: listingButtonImage)
      
      let label = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 21))
      label.textAlignment = .center
      label.text = album!
      
      let albumName = UIBarButtonItem(customView: label)
      let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
      
      exportButton.tintColor  = Const.RED
      deleteButton.tintColor = UIColor.red
      
      toolbarItems = [exportButton, spacer, albumName, spacer, deleteButton]
      
      navigationController?.setToolbarHidden(false, animated: true)
      
      
    }
  
  override func viewWillDisappear(_ animated: Bool) {
    SDWebImageManager.shared().imageCache?.clearMemory()
    //stopObservers()

  }
  


  
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

  
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
    
    var imageURL = images[indexPath.row].url!
    
    if imageURL == "" {
      imageURL = Const.PLACEHOLDER_URL
    }
    
    
    
    DispatchQueue.global(qos: .background).async {
      if self.images[indexPath.row].url! != "" {
        cell.image.sd_setShowActivityIndicatorView(true)
        cell.image.sd_setIndicatorStyle(.gray)
        
        cell.image.sd_setImage(with: URL(string: (imageURL))!) { (image, error, SDImageCacheType, url) in
          if error != nil {
            return
          } else {
            DispatchQueue.main.async() { () -> Void in
              cell.image.image = image
              cell.image.layer.borderWidth = 0.5
              cell.image.layer.borderColor = UIColor.gray.cgColor
              cell.image.backgroundColor = UIColor(red: 10/255, green: 10/255, blue: 10/255, alpha: 0.75)
            }
            
          }
        }
      } else {
        if self.images[indexPath.row].imgData != nil {
          DispatchQueue.main.async() { () -> Void in
            cell.image.image = UIImage(data: self.images[indexPath.row].imgData! as Data)
            cell.image.layer.borderWidth = 0.5
            cell.image.layer.borderColor = UIColor.gray.cgColor
            cell.image.backgroundColor = UIColor(red: 10/255, green: 10/255, blue: 10/255, alpha: 0.75)
          }
        } else {
          DispatchQueue.main.async() { () -> Void in
            cell.image.image = self.placeholder //UIImage(data: self.images[indexPath.row].imgData! as Data)
            cell.image.layer.borderWidth = 0.5
            cell.image.layer.borderColor = UIColor.gray.cgColor
            cell.image.backgroundColor = UIColor(red: 10/255, green: 10/255, blue: 10/255, alpha: 0.75)
          }
        }

        
      }
      
    }
    
    let image = images[indexPath.row]
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    var dateStr = ""
    
    let coordinateSystem = image.coordinateSystem
    
    if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
      latStr = "Lat: \(image.latitude)"
      lonStr = "Lon: \(image.longitude)"
      zoneStr = ""
      
    } else {
      latStr = "N: \(image.latitude)"
      lonStr = "E: \(image.longitude)"
      zoneStr = "\(image.zone)"
    }
    
    var accuStr = ""
    
    if image.accuracy == "1" {
      accuStr = zoneStr + "   " + "Added"
    } else {
      accuStr = zoneStr + "   " + "+/-  \(image.accuracy)m"
    }
    
    let datumStr = "\(image.coordinateSystem)"
    
    let id = image.id
    
    let isDateTimeOnly = defaults.bool(forKey: "dateTimeOnly")
    
    
    
    if isDateTimeOnly {
      dateStr = "\(image.timestamp)"
    } else {
      if image.initials != "" {
        dateStr = "\(image.timestamp)" + "  /  " + image.initials! + " - No " + String( describing: id )
      } else {
        dateStr = "\(image.timestamp)" + "  /  No " + String( describing: id )
      }
    }
    
    cell.easting.shadowOffset = CGSize(width: 1, height: 1)
    cell.easting.shadowColor = UIColor.black
    cell.northing.shadowOffset = CGSize(width: 1, height: 1)
    cell.northing.shadowColor = UIColor.black
    cell.coordinateSystem.shadowOffset = CGSize(width: 1, height: 1)
    cell.coordinateSystem.shadowColor = UIColor.black
    cell.zoneAccuracy.shadowOffset = CGSize(width: 1, height: 1)
    cell.zoneAccuracy.shadowColor = UIColor.black
    cell.line1.shadowOffset = CGSize(width: 1, height: 1)
    cell.line1.shadowColor = UIColor.black
    cell.line2.shadowOffset = CGSize(width: 1, height: 1)
    cell.line2.shadowColor = UIColor.black
    cell.line3.shadowOffset = CGSize(width: 1, height: 1)
    cell.line3.shadowColor = UIColor.black
    
    if defaults.bool(forKey: "dateTimeOnly") {
      cell.line1.text = ""
      cell.line2.text = ""
      cell.line3.text = image.timestamp
      cell.easting.text = ""
      cell.northing.text = ""
      cell.coordinateSystem.text = ""
      cell.zoneAccuracy.text = ""
    } else {
      
      cell.easting.text = lonStr
      cell.northing.text = latStr
      cell.coordinateSystem.text = datumStr
      cell.zoneAccuracy.text = accuStr
      
      
      if image.ownerText == "" ||  image.ownerText == "\n" {
        if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
          cell.line1.text = ""
          cell.line2.text = self.album!
          cell.line3.text = dateStr
        } else {
          cell.line1.text = ""
          cell.line2.text = ""
          cell.line3.text = dateStr
        }
        
      } else {
        if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
          cell.line1.text = self.album!
          cell.line2.text = dateStr
          cell.line3.text = image.ownerText
        } else {
          cell.line1.text = ""
          cell.line2.text = dateStr
          cell.line3.text = image.ownerText
        }
        
      }
    }
    
    return cell
    
  }

  
  
  
  func applyBlurEffect(image: UIImage) -> UIImage {
      let imageToBlur = CIImage(image: image)
      let blurfilter = CIFilter(name: "CIGaussianBlur")
      blurfilter?.setValue(5, forKey: kCIInputRadiusKey)
      blurfilter?.setValue(imageToBlur, forKey: "inputImage")
      let resultImage = blurfilter?.value(forKey: "outputImage") as! CIImage
      var blurredImage = UIImage(ciImage: resultImage)
    let cropped:CIImage=resultImage.cropped(to: CGRect(x: 0, y: 0, width: (imageToBlur?.extent.size.width)!, height: imageToBlur!.extent.size.height))
      blurredImage = UIImage(ciImage: cropped)
    
      return blurredImage
  }
  
//  func reduceInCase(image: UIImage) -> UIImage{
//    if image.size.width == image.size.height {
//      return image
//    } else {
//      return image.scaleImage(toSize: CGSize(width: image.size.width * 0.33333, height: image.size.height * 0.33333))!
//    }
//  }
  
  func exportData(_ sender: UIButton){
    exportHandler(sender)
  }
  
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    performSegue(withIdentifier: "showInfo", sender: self)
  }
  
  func deletePoints(_ sender: UIButton) {
    performSegue(withIdentifier: "deletePoints", sender: self)
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "deletePoints" == segue.identifier {
      let DestViewController : ExportViewController = segue.destination as! ExportViewController
      DestViewController.albumName = self.album
    } else if "showInfo" == segue.identifier {
      let DestVC : ShowPointInfo = segue.destination as! ShowPointInfo
      DestVC.image = self.images[(self.collectionView?.indexPathsForSelectedItems?[0].row)!]
      DestVC.images = self.images
      DestVC.album = self.album!
    } else if "goToHome" == segue.identifier {
      let _ : ViewController = segue.destination as! ViewController
    }
  }
  
}


extension ImagesCVC {
  
  func fetchData() -> String{
    responde = ""
    
    let datum = defaults.integer(forKey: "dataFormat")
    if datum == 3 || datum == 4 || datum == 5 {
      responde = responde + Const.EASTING_NORTHING_CVS_HEADER + "\n"
    } else {
      responde = responde + Const.LATITUDE_LONGITUDE_CVS_HEADER + "\n"
    }
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", self.album!)
    request.predicate = predicate
    
    let sortDescriptor = NSSortDescriptor(key: Const.TIMESTAMP, ascending: false)
    request.sortDescriptors = [sortDescriptor]
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      if results.count > 0 {
        for result in results as! [ImageData]{
          
                    
          var dataImage : NSData!
          
          if let dataImageValue = result.imgData {
            dataImage = dataImageValue
          } else {
            dataImage = UIImagePNGRepresentation(UIImage(named: "placeholderImage")!)! as NSData
          }

          var longDescription = ""
          
          if let longDescValue = result.longDesc {
              longDescription = longDescValue
          }
          
          let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: dataImage, longDesc : longDescription)
          
          
          self.images.append(image)
          responde = responde + Helpers.Instance.respondInCommasForm(for: result)
          
          
          let datum = defaults.integer(forKey: "dataFormat")
          if datum == 3 || datum == 4 || datum == 5 {
            if (result.note!.isEmpty) {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOnwerTextUTM(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          } else {
            if result.note!.isEmpty {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOwnerTextLL(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          }
        }
      } else{
        let error = UIAlertController(title: "Hey!", message: "This Project has not more records.", preferredStyle: UIAlertControllerStyle.alert)
        error.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
          (action) -> Void in
          
          self.dismiss(animated: true, completion: nil)
          
        }))
        present(error, animated: true, completion: nil)
      }
      
    } catch {
      print("error")
    }
    
    print(responde)
    return responde
    
  }
  
  func exportHandler(_ sender: UIButton) {
    let alertController = UIAlertController(title: "Export Files", message: "Please select the files you want to export.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "CSV File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      
      self.exportDatabase()
    })
    
    let kmlAction = UIAlertAction(title: "KML File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportKMLDB()
    })
    
    let kmlcvsAction = UIAlertAction(title: "CSV/KML Files", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportCSVandKML()
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(kmlcvsAction)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  
  
  @IBAction func exportFiles(_ sender: UIButton) {
    exportHandler(sender)
  }
  
  
  func gpsInfoStr(texts: ImageData) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""

    var accuStr = ""
    
    if texts.acurracy == "1" {
      accuStr  = "<b>Accuracy:</b> Added"
    } else {
      accuStr  = "<b>Accuracy:</b> ± \(texts.acurracy!)m"
    }
    
    let datumStr = "<b>CoordSys:</b> \(texts.coordinateSystem!)"
    
    var idInfo : String!
    
    if let initial = keychain.get(Const.INITIALS) {
      idInfo = initial + " " + String( texts.id )
    } else {
      idInfo = String( texts.id )
    }
    
    
    
    let dateStr = "<b>\(texts.timestamp!)</b>"
    let symbolText = "<b>Defined Find:</b> \(texts.symbolText!)"
    
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 || datum == 6 {
      //inverted to put East up and Nor down
      lonStr = "<b>Northing:</b> \(texts.latitude!)"
      latStr = "<b>Easting:</b> \(texts.longitude!)"
      zoneStr = "<b>\(datum == 6 ? "Grid Ref:" : "Zone:")</b> \(texts.gZone!)"
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(album!)<br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        zoneStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.note!.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.note!)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
    } else {
      latStr = "<b>Latitude:</b> \(texts.latitude!)"
      lonStr = "<b>Longitude: </b>\(texts.longitude!)"
      zoneStr = ""
      
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(album!) <br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.note!.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.note!)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
      
    }
    
    
    return result
  }
  
  
  func exportDatabase() {
    let exportString = fetchData()
    let file = saveAndExportGeneral(exportString: exportString, ext: "csv")
    presentActivity(files: [file])
    
  }
  
  
  func exportKMLDB(){
    let file = saveAndExportGeneral(exportString: exportKMLFunction(), ext: "kml")
    presentActivity(files: [file])
  }
  
  func exportKMLFunction() -> String{
    var respondeKML : String = ""
    
    var header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n" +
      "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + "\n" +
      "<Document>" + "\n" +
      "<name>\(album!)</name>\"" + "\n"
    
    
    header += iconStyle()
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageData")
    
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album!)
    request.predicate = predicate
    
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      for result in results as! [ImageData]{
        let idInfo : String!
        let query = result.desc!
        let regex = try! NSRegularExpression(pattern:"<(.*?)>", options: [])
        let tmp = query as NSString
        var endResults = [String]()
        
        regex.enumerateMatches(in: query, options: [], range: NSMakeRange(0, query.count)) { result, flags, stop in
          if let range = result?.range(at: 1) {
            endResults.append(tmp.substring(with: range))
          }
        }
        
        let coord = endResults[0]
        let first = coord.components(separatedBy: ",")[1]
        let second = coord.components(separatedBy: ",")[0]
        
        if let initial = keychain.get(Const.INITIALS) {
          idInfo = initial + " " + String( result.id )
        } else {
          idInfo = String( result.id )
        }
        
        
        
        respondeKML =  respondeKML + "<Placemark>" + "\n" +
          "<name>\(idInfo!)</name>" + "\n" +
          "<styleUrl>#\(result.symbolLetter!)StyleMap</styleUrl>" + "\n" +
          "<description> <![CDATA[ \n \(gpsInfoStr(texts: result)) \n]]></description>" + "\n" +
          "<Point><coordinates>\(first),\(second)</coordinates></Point>" + "\n" +
          "</Placemark>" + "\n"
      }
      
    } catch {
      print("error")
    }
    
    let footer = "</Document>" + "\n" +
    "</kml>"
    
    return (header + respondeKML + footer)
  }
  
  
  func iconStyle() -> String {
    var style = ""
    
    let symbols = ["x",
                   "a",
                   "b",
                   "c",
                   "d",
                   "e",
                   "f",
                   "o",
                   "v"]
    
    
    for symbol in symbols {
      style += "<Style id=\"\(symbol)Icon\">" + "\n" +
        "<IconStyle>" + "\n" +
        "<Icon>" + "\n" +
        "<href>http://ismartdetect.com/wp-content/uploads/2017/01/\(symbol)@1.5x.png</href>" + "\n" +
        "</Icon>" + "\n" +
        "</IconStyle>" + "\n" +
        "</Style>" + "\n" + "\n" +
        "<StyleMap id=\"\(symbol)StyleMap\">" + "\n" +
        "<Pair>" + "\n" +
        "<key>normal</key>" + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>" + "\n" +
        "</Pair>" + "\n" +
        "<Pair>"  + "\n" +
        "<key>highlight</key>"  + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>"  + "\n" +
        "</Pair>"  + "\n" +
        "</StyleMap>" + "\n"
    }
    
    
    return style
  }
  
  
  func exportCSVandKML(){
    let firstFile = saveAndExportGeneral(exportString: exportKMLFunction(), ext: "kml" )
    let secondFile = saveAndExportGeneral(exportString: fetchData(), ext: "csv")
    
    presentActivity(files: [firstFile, secondFile])
    
    
  }
  
  
  
  func saveAndExportGeneral(exportString: String, ext: String) -> NSURL {
    var firstActivityItem : NSURL!
    
    let exportFilePath = NSTemporaryDirectory() + "\(album!.slugify())-\(self.appName!.slugify()).\(ext)"
    let exportFileURL = NSURL(fileURLWithPath: exportFilePath)
    
    FileManager.default.createFile(atPath: exportFilePath, contents: NSData() as Data, attributes: nil)
    //var fileHandleError: NSError? = nil
    var fileHandle: FileHandle? = nil
    do {
      fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
    } catch {
      print("Error with fileHandle")
    }
    
    if fileHandle != nil {
      fileHandle!.seekToEndOfFile()
      let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
      fileHandle!.write(csvData!)
      
      fileHandle!.closeFile()
      
      firstActivityItem = NSURL(fileURLWithPath: exportFilePath)
    }
    return firstActivityItem
    
  }
  
  
  func presentActivity(files: [NSURL]){
    let activityViewController : UIActivityViewController = UIActivityViewController(
      activityItems: files, applicationActivities: nil)
    
    activityViewController.excludedActivityTypes = [
      UIActivityType.assignToContact,
      UIActivityType.saveToCameraRoll,
      UIActivityType.postToFlickr,
      UIActivityType.postToVimeo,
      UIActivityType.postToTencentWeibo
    ]
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
      activityViewController.popoverPresentationController?.sourceView = self.view
      self.present(activityViewController, animated: true, completion: nil)
    } else{
      self.present(activityViewController, animated: true, completion: nil)
    }
  }
  
  
}

//1.0.18
extension ImagesCVC {
  
  @IBAction func goToGoogleMaps(_ sender: Any) {
    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoogleMapsSB") as! GoogleMapVC
    controller.album = self.album!
    
    let home = UIBarButtonItem(title: "Home", style: .plain, target: self, action: #selector(goToHome(_:)))
    home.tintColor = Const.RED
    
    controller.navigationItem.rightBarButtonItems = [home]
    controller.navigationController?.navigationBar.tintColor = Const.RED
    self.navigationController?.pushViewController(controller, animated: false)
  }
  
  @IBAction func goToHome(_ sender: Any) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let controller = storyboard.instantiateViewController(withIdentifier: "ViewControllerSB")
    self.present(controller, animated: false, completion: nil)
    //performSegue(withIdentifier: "goToHome", sender: self)
  }
  
  
  func startObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
        let respondeImages = ObserversProvider.Instance.observer(child: Const.IMAGES_DATA, option: .image)
        self.handleImages = respondeImages.handle
        self.refImages = respondeImages.ref
      }
    }
  }
  
  func stopObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
        self.refImages.removeObserver(withHandle: self.handleImages!)
      }
    }
  }

  
}




