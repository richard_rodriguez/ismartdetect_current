//  AuthProvider.swift
//  Created by Richard Rodriguez on 11/29/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation
import FirebaseAuth
import CoreData

typealias LoginHandler = (_ msg: String?) -> Void

struct LoginErrorCode {
  static let INVALID_EMAIL = "Invalid email address, please enter a valid one."
  static let WRONG_PASSWORD = "Wrong password, please enter the correct password."
  static let PROBLEM_CONNECTING = "Problem connecting to database. Please try later."
  static let USER_NOT_FOUND = "User not found, please sign up."
  static let EMAIL_ALREADY_IN_USE = "This email is in use, if you already Signed Up, please go back and Sign In using your email, if not use another email."
  static let WEAK_PASSWORD = "This password is too weak, it should be at least 6 characters long."
  static let TOO_MANY_REQUEST = "You have tried too many times. Try later."
}

class AuthProvider {
  private static let _instance = AuthProvider()
  
  static  var Instance : AuthProvider {
    return _instance
  }

  
  func login(email: String, password: String, loginHandler: LoginHandler?){
    Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
      self.checkIfUserIsVerified()
      
      if error != nil {
        self.handlerErrors(err: error! as NSError, loginHandler: loginHandler)
      } else {
        loginHandler?(nil)
      }
    })
  }
  
  func signUp(name: String, email: String, password: String, loginHandler: LoginHandler?){
    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
      
      if error != nil {
        self.handlerErrors(err: error! as NSError, loginHandler: loginHandler)
      } else {
        
        if user?.uid != nil {
          DBProvider.Instance.saveUser(id: user!.uid, name: name, email: email)
          self.login(email: email, password: password, loginHandler: loginHandler)
        }
      
        let changeRequest = user?.createProfileChangeRequest()
        changeRequest?.displayName = name
        changeRequest?.photoURL = nil
        changeRequest?.commitChanges() { (error) in
          if error != nil {
            print("Error ocurred trying to update displayName Firebase in SignUp(email/pass) on SignUpVC line 57 ")
            return
          } else {
            print("DisplayName was updated using signUp function on SignUpVC.")
          }
        }
        
        
        
        user?.sendEmailVerification(completion: { (error) in
          if error != nil {
            print("Error trying to sending the verification email, on SignUpVC email/pass login func line 65")
            return
          }
          print("Verification Email sent.")
        })
        
      }
        
    })
  }
  
  
  func logout() -> Bool{
    if Auth.auth().currentUser != nil {
      do {
        try Auth.auth().signOut()
        Helpers.Instance.cleanDefaultsData()

        return true
      } catch {
        return false
      }
    }
    return true
  }
  
  private func handlerErrors(err: NSError, loginHandler: LoginHandler?){
    
    if let errCode = AuthErrorCode(rawValue: err.code){
      
      switch errCode {
      case .wrongPassword:
        loginHandler?(LoginErrorCode.WRONG_PASSWORD)
        break
      case .invalidEmail:
        loginHandler?(LoginErrorCode.INVALID_EMAIL)
        break
      case .userNotFound:
        loginHandler?(LoginErrorCode.USER_NOT_FOUND)
        break
      case .emailAlreadyInUse:
        loginHandler?(LoginErrorCode.EMAIL_ALREADY_IN_USE)
        break
      case .tooManyRequests:
        loginHandler?(LoginErrorCode.TOO_MANY_REQUEST)
        break
      case .weakPassword: 
        loginHandler?(LoginErrorCode.WEAK_PASSWORD)
        break
      default:
        loginHandler?(LoginErrorCode.PROBLEM_CONNECTING)
        break
      }
    }
  }
  

  func checkIfUserIsVerified() {
    if let user = Auth.auth().currentUser {
      if user.isEmailVerified {
      }
      else {
        let _ = Helpers.Instance.alertUser(title: "Email is not verified", message: "Please verify your account, using the verification email.");
        print("Email is not verified");
      }
    }
  }

  
  
  
}
