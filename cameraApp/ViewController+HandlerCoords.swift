//  ViewController+HandlerCoords.swift
//  iSmartDetect
//  Created by Richard Rodriguez on 12/2/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

extension ViewController {
  
  func handlerCoords(location: CLLocation){
    let option = defaults.integer(forKey: "dataFormat")
    address["HAcc"] = String(round(location.horizontalAccuracy*10)/10)
    address["Description"] = String(location.description)
    address["LatitudeOr"] = String(location.coordinate.latitude)
    address["LongitudeOr"] = String(location.coordinate.longitude)
    address["TimestampOriginal"] = String(describing: location.timestamp)
    
    let currentDate = location.timestamp
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = dateFormat()
    let convertedDate = dateFormatter.string(from: currentDate as Date)
    
    address["Timestamp"] = convertedDate
    
    if option == 0 {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      let dms = Helpers.Instance.coordinateToDMS(lat: location.coordinate.latitude, lon: location.coordinate.longitude)
      
      address["Latitude"] = dms.latitude
      address["Longitude"] = dms.longitude
      address["Zone"] = ""
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: "")
      
    } else if option == 3 {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      let coor = CLLocationCoordinate2D(latitude: location.coordinate.latitude , longitude: location.coordinate.longitude)
      let result =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinates: coor)
      
      let subZone = Helpers.Instance.UTMzdl(latDeg: Int(location.coordinate.latitude))
      let gridZone = result.gridZone
      
      
      let hemisphere = result.hemisphere.rawValue == 1 ? "S" : "N"
      address["Latitude"] = String(result.northing).components(separatedBy: ".")[0]
      address["Longitude"] = String(result.easting).components(separatedBy: ".")[0]
      address["Zone"] = String(gridZone) + subZone + " " + hemisphere
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: address["Zone"]!)
      
    } else if option == 5 {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      let coor = CLLocationCoordinate2D(latitude: location.coordinate.latitude , longitude: location.coordinate.longitude)
      let result =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesGrl: coor)
      
      let subZone = Helpers.Instance.UTMzdl(latDeg: Int(location.coordinate.latitude))
      let gridZone = result.gridZone
      
      let hemisphere = result.hemisphere.rawValue == 1 ? "S" : "N"
      address["Latitude"] = String(result.northing).components(separatedBy: ".")[0]
      address["Longitude"] = String(result.easting).components(separatedBy: ".")[0]
      address["Zone"] = String(gridZone) + subZone + " " + hemisphere
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: address["Zone"]!)
      
      
      
    } else if option == 4 {
      let latValue = location.coordinate.latitude
      let lonValue = location.coordinate.longitude
      let coordinate = CLLocationCoordinate2D(latitude: latValue , longitude: lonValue )
      // Aquí iba el código comentado readJSON
      
      let responde =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesETRS: coordinate)
      let respondeWGS84 = GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinates: coordinate)
      let subZone = Helpers.Instance.UTMzdl(latDeg: Int(latValue))
      let hemisphere = responde.hemisphere.rawValue == 1 ? "S" : "N"

      let zoneCode = Locale.current.regionCode! as String
      var gridZone = zoneCode == "DK" ? 32 : Int(respondeWGS84.gridZone)
      print(responde.gridZone)

      if gridZone == 31 && subZone == "V" {
        gridZone = 32
      }

      if (gridZone == 32 || gridZone == 33) {
        gridZone = 32

        self.address["Zone"] = String(gridZone) + subZone + " " + hemisphere
        self.address["Latitude"] = String(responde.northing).components(separatedBy: ".")[0]
        self.address["Longitude"] = String(responde.easting).components(separatedBy: ".")[0]

        createFullAddressStr(Timestamp: self.address["Timestamp"]!, Accuracy: self.address["HAcc"]!, Latitude: self.address["Latitude"]!, Longitude: self.address["Longitude"]!, DataFormat: option, Zone: self.address["Zone"]!)

      } else if (gridZone == 34 || gridZone == 35) {

        self.address["Zone"] = String(gridZone) + subZone + " " + hemisphere
        self.address["Latitude"] = String(responde.northing).components(separatedBy: ".")[0]
        self.address["Longitude"] = String(responde.easting).components(separatedBy: ".")[0]

        createFullAddressStr(Timestamp: self.address["Timestamp"]!, Accuracy: self.address["HAcc"]!, Latitude: self.address["Latitude"]!, Longitude: self.address["Longitude"]!, DataFormat: option, Zone: self.address["Zone"]!)
        

      } else {
        self.address["Zone"] = String(gridZone) + subZone + " " + hemisphere
        self.address["Latitude"] = String(responde.northing).components(separatedBy: ".")[0]
        self.address["Longitude"] = String(responde.easting).components(separatedBy: ".")[0]

        createFullAddressStr(Timestamp: self.address["Timestamp"]!, Accuracy: self.address["HAcc"]!, Latitude: self.address["Latitude"]!, Longitude: self.address["Longitude"]!, DataFormat: option, Zone: self.address["Zone"]!)
      }
      
      
    } else if option == 1 {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      let dm = Helpers.Instance.DMStoDM(lat: location.coordinate.latitude, lon: location.coordinate.longitude)
      
      address["Latitude"] = dm.latitude
      address["Longitude"] = dm.longitude
      address["Zone"] = ""
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: "")
      
    } else if option == 6 {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      
      let lat = location.coordinate.latitude  // decimal version of latitude
      let lon = location.coordinate.longitude   // decimal version of longitude
      let coord = LLtoNE(latitude: lat, longitude: lon)
      let gridReference = NE2NGR(easting: Double(coord.eastring)!, northing: Double(coord.northing)!)

      
      address["Latitude"] = coord.northing.components(separatedBy: ".")[0]
      address["Longitude"] = coord.eastring.components(separatedBy: ".")[0]
      address["Zone"] = gridReference
      
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: gridReference)
 
    }
    
    
    else {
      cameraButton.isHidden = false
      pauseButton.isHidden = false
      address["Latitude"] = String(round(location.coordinate.latitude*100000.0) / 100000.0)
      address["Longitude"] = String(round(location.coordinate.longitude*100000.0) / 100000.0)
      address["Zone"] = ""
      
      createFullAddressStr(Timestamp: address["Timestamp"]!, Accuracy: address["HAcc"]!, Latitude: address["Latitude"]!, Longitude: address["Longitude"]!, DataFormat: option, Zone: "")
    }
  }
  
  func createFullAddressStr(Timestamp: String, Accuracy: String, Latitude:  String, Longitude: String, DataFormat: Int, Zone: String) {
    createFullAddressStrBigger(Timestamp: Timestamp, Accuracy: Accuracy, Latitude: Latitude, Longitude: Longitude, DataFormat: DataFormat, Zone: Zone)
    let heart = Helpers.Instance.heart(Accuracy)
    
    if DataFormat == 3 || DataFormat == 4 || DataFormat == 5 {
      full_address_str = "ACCURACY: " + Accuracy + " m \(heart)|EAST: " + Longitude + " |NORTH: " + Latitude + " |ZONE: " + Zone + " |" + DATA_FORMAT[DataFormat]!

    } else if DataFormat == 6 {
      full_address_str = "ACCURACY: " + Accuracy + " m \(heart)|EAST: " + Longitude + " |NORTH: " + Latitude + " |GREF: " + Zone + " |" + DATA_FORMAT[DataFormat]!
    } else {
      full_address_str = "ACCURACY: " + Accuracy + " m \(heart)|LAT: " + Latitude + " |LON: " + Longitude + " |" + DATA_FORMAT[DataFormat]!

    }
  }
  
  func createFullAddressStrBigger(Timestamp: String, Accuracy: String, Latitude:  String, Longitude: String, DataFormat: Int, Zone: String) {
    let timestamp = Timestamp.components(separatedBy: " - ")

    let heart = Helpers.Instance.heart(Accuracy)
    
    if DataFormat == 3 || DataFormat == 4 || DataFormat == 5 {
      full_address_str_bigger = timestamp[0] + " |" + timestamp[1] + "|ACC: " + Accuracy + " m \(heart)|E: " + Longitude + "|N: " + Latitude + " |ZONE: " + Zone +  " |" + DATA_FORMAT[DataFormat]!
    } else if DataFormat == 6 {
      full_address_str_bigger = timestamp[0] + " |" + timestamp[1] + "|ACC: " + Accuracy + " m \(heart)|E: " + Longitude + "|N: " + Latitude + " |GREF: " + Zone + " |" + DATA_FORMAT[DataFormat]!
    }else {
      full_address_str_bigger = timestamp[0] + " |" + timestamp[1] + "|ACC: " + Accuracy + " m \(heart)|LA: " + Latitude + "|LO: " + Longitude + " |" + DATA_FORMAT[DataFormat]!
    }
  }
  
  
  func readJSON(coor: CLLocationCoordinate2D, completion: @escaping (_ result: CLLocationCoordinate2D?) -> Void){
    let latitude = coor.latitude
    let longitude = coor.longitude
    
    let link = "http://geo.oiorest.dk/etrs89.json?wgs84=" + String(latitude) + "," + String(longitude)
    let url = NSURL(string: link)
    URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
      
      if error != nil {
        print(error!.localizedDescription)
        self.full_address_str = self.errorMessage!
        self.full_address_str_bigger = self.errorMessage!
        
        DispatchQueue.main.async(execute: { () -> Void in
          self.gpsDetailsLabel.text = self.errorMessage
          self.cameraButton.isHidden = true
          self.pauseButton.isHidden = true
          self.holdGPSButton.isHidden = true
          self.takeAPictureButton.isHidden = true
        })
        return
      }
      
      do {
        
        DispatchQueue.main.async(execute: { () -> Void in
          self.cameraButton.isHidden = false
          self.pauseButton.isHidden = false
        })
        
        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: AnyObject]
        
        let coordinates = json["coordinates"] as! NSArray
        let results = coordinates[0] as! NSArray
        
        let lat = results[0] as! String
        let lon = results[1] as! String
        
        
        completion(CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(lon)!))
        
      } catch let jsonError {
        print("entró: jsonError")
        print(jsonError)
        
        DispatchQueue.main.async(execute: { () -> Void in
          self.full_address_str = self.errorMessage!
          self.full_address_str_bigger = self.errorMessage!
          self.cameraButton.isHidden = true
          self.pauseButton.isHidden = true
          self.holdGPSButton.isHidden = true
          self.takeAPictureButton.isHidden = true
        })
        return
      }
      }.resume()
  }

  func dateFormat() -> String {
    if defaults.bool(forKey: "dateFormatSwitch") {
      return DATE_FORM[defaults.integer(forKey: "dateFormat")]! + " - " + TIME_FORM[1]!
    } else {
      return DATE_FORM[defaults.integer(forKey: "dateFormat")]! + " - " + TIME_FORM[0]!
    }
  }
  
  
  
}
