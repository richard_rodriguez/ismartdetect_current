//
//  SecureCodeVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/29/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import KeychainSwift

class SecureCodeVC: UIViewController {

  
  @IBOutlet weak var first: UITextField!
  @IBOutlet weak var second: UITextField!
  @IBOutlet weak var third: UITextField!
  @IBOutlet weak var fourth: UITextField!
  
    let keychain = KeychainSwift()
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.navigationController?.navigationBar.topItem?.title = "Back"
      
      if let secureDigits = keychain.get("secure_digits") {
        if secureDigits != "" {
          let digits = Array(secureDigits.characters)
            first.text = String(digits[0])
            second.text = String(digits[1])
            third.text = String(digits[2])
            fourth.text = String(digits[3])
        }
      }
    }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    let text = first.text! + second.text! + third.text! + fourth.text!
    keychain.set(text, forKey: "secure_digits")
  }
  

}
