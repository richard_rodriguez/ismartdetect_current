//
//  AboutUsVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/1/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController, CLLocationManagerDelegate, UIWebViewDelegate {
  
  @IBOutlet weak var text: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let htmlString =  "<div style=\"font-size: 18px;font-family: 'HelveticaNeue-Light'\">" +
    "<p><strong>iSmartDetect</strong> is developed by the team of:</p><br/>" +
    "<p><strong>Richard Rodriguez</strong> - DR. App Developer</p>" +
    "<p><strong>Nikolas Gough</strong> - DK. Metal Detector User and Amateur Archaeologist</p>" +
    "<p><strong>Henrik Rasmussen</strong> - DK. Metal Detector User and Amateur Archaeologist</p>" +
    "<p><strong>Michael Whitlatch</strong> - USA. Marketing</p>" +
    "<p>With the help from metal detector users around the world.</p>" +
    "<p>iSmartDetect began in 2016 and day by day, month by month, the application came to be live. </p>" +
    "<p>The iSmartDetect app was created and we are all very proud of the result.</p>" +
    "<p>Development will continue as we get so many new ideas from the users of the app. </p>" +
    "<p>Future updates will add even more AWESOME tools. </p> " +
    "</div>"
    
    
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
    
    let attributedString = try! NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    text.attributedText = attributedString
    
  }

  @IBAction func goToWebsite(_ sender: Any) {
    let url = URL(string: "https://ismartdetect.com")
    UIApplication.shared.openURL(url!)
  }
  
  
}
