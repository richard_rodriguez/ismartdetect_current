//
//  ZoomController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/2/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import Photos
import MessageUI
//import MBProgressHUD

class ZoomController : UIViewController {
  
  
  @IBOutlet weak var textPassed: UITextView!
  var textP : String! = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    let screenSize: CGRect = UIScreen.main.bounds
    let porcentageFontSize = CGFloat(22/320.0)
    textPassed.text = textP

    textPassed.font = UIFont(name: "Helvetica Bold", size: (screenSize.width * porcentageFontSize) * 1.8)
  
    textPassed.isUserInteractionEnabled = true
    textPassed.isEditable = false
    textPassed.isSelectable = false
    
    let aSelector : Selector = #selector(ViewController.TapFunc)
    let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
    tapGesture.numberOfTapsRequired = 2
    textPassed.addGestureRecognizer(tapGesture)
  }
  
  func TapFunc(){
    performSegue(withIdentifier: "ViewControllerSG", sender: self)
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  
  
  
  
  
}
