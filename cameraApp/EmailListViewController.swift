//  EmailListViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 10/16/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import CoreData

class EmailList: UITableViewController {
  
  var tempEmails : [String]!
  var emails : [Email]!
  var emailsT : [String]!
  
  var updateSecond : Timer!

  var handle: UInt!
  var ref : DatabaseReference!
  
  override func viewWillAppear(_ animated: Bool) {
    if let _ = Auth.auth().currentUser?.uid {
      let responde = ObserversProvider.Instance.observer(child: Const.EMAILS, option: .email)
      handle = responde.handle
      ref = responde.ref
    }

    emails = []
    fetchEmails()
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startTimer()
  }
  
  
  func fetching(){
    emails = []
    fetchEmails()
    self.tableView.reloadData()
  }

  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    stopTimer()

    if let _ = Auth.auth().currentUser?.uid {
      ref.removeObserver(withHandle: handle!)
    }

    
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return emails.count
  }
  
  
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Email")
    cell?.textLabel!.text = emails[indexPath.row].email
    
    if emails[indexPath.row].selected == true {
      cell?.accessoryType = .checkmark
    } else {
      cell?.accessoryType = .none
    }
    
    cell?.tintColor = Const.RED
    return cell!
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if let cell = tableView.cellForRow(at: indexPath) {
      if cell.accessoryType == UITableViewCellAccessoryType.none {
        cell.accessoryType = .checkmark
        emails[indexPath.row].selected = true
        editEmail(email: emails[indexPath.row])

        if let uid = Auth.auth().currentUser?.uid {
          let emailKey = emails[indexPath.row].key
          let ref = Database.database().reference().child(uid).child(Const.EMAILS).child(emailKey!)
          let new : Dictionary<String, Any> = [Const.EMAIL : emails[indexPath.row].email,
                                               Const.SELECTED : emails[indexPath.row].selected]
          ref.updateChildValues(new)
        }
      
        
      } else if cell.accessoryType == UITableViewCellAccessoryType.checkmark{
        cell.accessoryType = .none
        emails[indexPath.row].selected = false
        editEmail(email: emails[indexPath.row])
        
        if let uid = Auth.auth().currentUser?.uid {
          let emailKey = emails[indexPath.row].key
          let ref = Database.database().reference().child(uid).child(Const.EMAILS).child(emailKey!)
          let new : Dictionary<String, Any> = [Const.EMAIL : emails[indexPath.row].email,
                                               Const.SELECTED : emails[indexPath.row].selected]
          ref.updateChildValues(new)
        }
      }
    }
    
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == UITableViewCellEditingStyle.delete {
      
      
      if deleteEmail(email: emails[indexPath.row]) {
        if let uid = Auth.auth().currentUser?.uid {
          if let emailKey = emails[indexPath.row].key {
            let ref = Database.database().reference().child(uid).child(Const.EMAILS).child(emailKey)
            ref.removeValue()
          } else {
            print("Error trying to deleting Email.")
            return
          }
        
        emails = []
        fetchEmails()
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        self.tableView.reloadData()
          
        }
      } else {
        print("Error trying to delete email on CoreData.")
      }
    }
  }
  
 
  @IBAction func addNewEmail(_ sender: UIBarButtonItem) {
    
    
    let alert = UIAlertController(title: "New Email", message: "Enter a new email address", preferredStyle: UIAlertControllerStyle.alert)
    
    alert.addTextField { (textField) in
      textField.placeholder = "Insert valid Email"
      textField.keyboardType = .emailAddress
    }

    
    alert.addAction(UIAlertAction(title: "Add", style: .default, handler: {
      (action) -> Void in
      let textf = (alert.textFields?[0])! as UITextField
      
      if self.validate(YourEMailAddress: textf.text!) {
          print(textf.text!)
        
    
          let key = Database.database().reference().child(Const.EMAILS).childByAutoId().key
          let newEmail = Email(email: textf.text!, selected: true, key: key )
          self.addNewEmail(email: newEmail)
          self.emails = []
          self.fetchEmails()
          let lastIndex = self.emails.count - 1
          let insertionIndexPath = IndexPath(item: lastIndex, section: 0)
          self.tableView.insertRows(at: [insertionIndexPath], with: .automatic)
          self.tableView.cellForRow(at: insertionIndexPath)?.accessoryType = .checkmark
      
      } else {
        let error = UIAlertController(title: "Hey!", message: "\"\(textf.text!)\" does not look like a valid email. Check it out and try again.", preferredStyle: UIAlertControllerStyle.alert)
        error.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.destructive, handler: nil))

        self.present(error, animated: true, completion: nil)
      }
      
    })
    )
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    present(alert, animated: true, completion: nil)
    
  }
  
  
  func validate(YourEMailAddress: String) -> Bool {
    let REGEX: String
    REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
  }
  
  
  func fetchEmails(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EmailData")
    
    do {
      let emails = try manageContext.fetch(request)
      print(emails.count)
      for email in emails as! [EmailData]{
        print(email.email ?? "")
        let new = Email(email: email.email!, selected: email.selected, key: email.key!)
        self.emails.append(new)
      }
      
    } catch {
      print("error")
    }
  }
  
  func addNewEmail(email: Email){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: "EmailData", into: manageContext)
    
    new.setValue(email.email, forKey: Const.EMAIL)
    new.setValue(email.selected, forKey: Const.SELECTED)
    new.setValue(email.key, forKey: Const.KEY)
    
    do{
      try manageContext.save()
      print("data saved.")
      
      DBProvider.Instance.add(new: email)
      
    }catch {
      print("error")
    }
  }
  
  func editEmail(email: Email){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EmailData")
    
    request.predicate = NSPredicate(format: "key = %@", email.key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! EmailData
      managedObject.setValue(email.selected!, forKey: "selected")
      
      do{
        try manageContext.save()
        print("data updated.")
      }catch {
        print("error")
      }
    }
  }
  
  func deleteEmail(email: Email) -> Bool{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EmailData")
    
    request.predicate = NSPredicate(format: "key = %@", email.key)
    
    if let result = try? manageContext.fetch(request){
        let object = result[0]
        manageContext.delete(object as! NSManagedObject)
        
        do{
          try manageContext.save()
          print("data deleted.")
          return true
        }catch {
          print("error")
          return false
        }
      }
    return true
  }


  
  //Timers functions
  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(EmailList.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }
  

}







