//
//  DataFormat.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 8/13/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import MessageUI

class DataFormatController: UITableViewController, MFMailComposeViewControllerDelegate{
  
  let defaults = UserDefaults.standard
  
  override func viewWillAppear(_ animated: Bool) {
    selectRow(index: (defaults.integer(forKey: "dataFormat") ))
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    //DBProvider.Instance.saveSetting()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      if indexPath.section == 0 {
        cell.tintColor = Const.RED
        cell.accessoryType = .checkmark
        defaults.set(indexPath.row, forKey: "dataFormat")
      } else {
        selectRow(index: (defaults.integer(forKey: "dataFormat") ))
        let email = "ismartdetect@gmail.com"
        let url = URL(string: "mailto:\(email)")
        UIApplication.shared.openURL(url!)
      }
      
     
    }
    
  }
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .none
    }
  }
  
  func selectRow(index: Int) {
    let rowToSelect:IndexPath = IndexPath(row: index, section: 0);  //slecting 0th row with 0th section
    self.tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.top);
    
    if let cell = tableView.cellForRow(at: rowToSelect) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
  }
  
}
