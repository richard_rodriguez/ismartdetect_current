//
//  SuscribeButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 4/14/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class SuscribeButton: UIButton {

  override func awakeFromNib() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .white
    frame.size.height = 45
    
    tintColor = UIColor.red
    layer.cornerRadius = 3
    layer.shadowColor = UIColor.darkGray.cgColor
    layer.shadowOffset = CGSize(width: 1, height: 1)
    layer.shadowOpacity = 0.5

    layer.masksToBounds = true
    setTitleColor(Const.RED, for: .normal)
  }

}
