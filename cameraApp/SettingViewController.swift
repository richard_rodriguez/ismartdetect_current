//
//  SettingViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 10/2/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseAuth
import KeychainSwift
import SwiftyStoreKit
import FBSDKLoginKit


class SettingViewController: UITableViewController {
  
  let defaults = UserDefaults.standard
  let keychain = KeychainSwift()
  
  struct SegueTo {
    static let ACCOUNT = "AccontSG"
    static let SIGNIN = "SignInSG"
    static let ENTER_PASSWORD = "enterPassword"
    static let EDIT_SECURE_DIGITS = "editSecureDigits"
  }
  
  let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject?
  
  @IBOutlet weak var syncDataCell: UITableViewCell!
  @IBOutlet weak var dateTimeSwitch: UISwitch!
  @IBOutlet weak var saveOriginals: UISwitch!
  @IBOutlet weak var stampProjectName: UISwitch!
  @IBOutlet weak var unlockPremiumCell : UITableViewCell!
  @IBOutlet weak var unlockLabel: UILabel!
  
 
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)

    if (keychain.getBool("purchased") ?? false) {
       unlockLabel.text = "Premium User & Subscription"
    } else {
       unlockLabel.text = "Unlock Premium Plan"
    }
    
    let appleValidator = AppleReceiptValidator(service: .production)
    SwiftyStoreKit.verifyReceipt(using: appleValidator, password: "1614a6c9bf9d497fa725a0a795d9aa63") { result in
      switch result {
      case .success(let receipt):
        // Verify the purchase of a Subscription
        let purchaseResult = SwiftyStoreKit.verifySubscription(type: .autoRenewable, productId: "com.ngintermarketingaps.ismartdetect.premiumplan", inReceipt: receipt, validUntil: Date())
        
        switch purchaseResult {
        case .purchased(let expiresDate):
          print("Product is valid until \(expiresDate)")
        case .expired(let expiresDate):
           //self.hideTableSection = false
          print("Product is expired since \(expiresDate)")
        case .notPurchased:
          print("The user has never purchased this product")
        }
        
      case .error(let error):
        print("Receipt verification failed: \(error)")
      }
    }
    self.navigationController?.navigationBar.tintColor = Const.RED

    saveOriginals.setOn(defaults.bool(forKey: "saveOriginal"), animated: true)
    dateTimeSwitch.setOn(defaults.bool(forKey: "dateTimeOnly"), animated: true)
    stampProjectName.setOn(defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME), animated: true)

  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  

  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 4 && indexPath.row == 0 {
      
      if let user = Auth.auth().currentUser {
        if user.isEmailVerified || (FBSDKAccessToken.current() != nil) {
          performSegue(withIdentifier: SegueTo.ACCOUNT, sender: self)
        } else {
          performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
        }
      } else {
        performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
      }
    }
    
    if indexPath.section == 6 && indexPath.row == 0 {
      let secureDigits = keychain.get("secure_digits") ?? ""
      
      if secureDigits == "" {
        performSegue(withIdentifier: SegueTo.EDIT_SECURE_DIGITS, sender: self)
      } else {
        performSegue(withIdentifier: SegueTo.ENTER_PASSWORD, sender: self)
      }
      
    }

    if indexPath.section == 7 && indexPath.row == 1 {
      tableView.deselectRow(at: indexPath, animated: true)
      let email = "l3a7w6s1z8y3h9h9@ismartdetect.slack.com,support@ismartdetect.com,ismartdetect@gmail.com?subject=User%20Help"
      //let url = URL(string: "mailto:\(email)")
      
      if let url = URL(string: "mailto:\(email)") {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
      
      //UIApplication.shared.openURL(url!)
    }
    
    if indexPath.section == 7 && indexPath.row == 2 {
      tableView.deselectRow(at: indexPath, animated: true)
      //let email = "l3a7w6s1z8y3h9h9@ismartdetect.slack.com,support@ismartdetect.com,ismartdetect@gmail.com?subject=User%20Help"
      //let url = URL(string: "")
      
      if let url = URL(string: "https://ismartdetect.com/video-tutorials/") {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      }
      
    }
    
  }
  
  
  @IBAction func goToHomescreen(_ sender: Any) {
    DispatchQueue.main.async {
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
  
    if section == 7 {
      return "Version \(nsObject!)"
    } else {
      return nil
    }
  }

  
  
  @IBAction func dateTimeSwitchPressed(_ sender: Any) {
    if dateTimeSwitch.isOn {
      defaults.set(true, forKey: "dateTimeOnly")
    } else {
      defaults.set(false, forKey: "dateTimeOnly")
    }
    
    //DBProvider.Instance.saveSetting()
  
  }
  
  @IBAction func switchPressed(_ sender: UISwitch) {
    if saveOriginals.isOn {
      defaults.set(true, forKey: "saveOriginal")
    } else {
      defaults.set(false, forKey: "saveOriginal")
    }
      //DBProvider.Instance.saveSetting()
  }
  
  
  @IBAction func stampProjectNamePressed(_ sender: UISwitch) {
    if stampProjectName.isOn {
      defaults.set(true, forKey: Const.ACTIVE_STAMP_PROJECT_NAME)
    } else {
      defaults.set(false, forKey: Const.ACTIVE_STAMP_PROJECT_NAME)
    }
    //DBProvider.Instance.saveSetting()

  }
  

}


extension SettingViewController {
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if (section == 4) && (keychain.getBool("purchased") ?? false) == false {
      self.syncDataCell.isHidden = true
      return 0.1
    }
    //keeps all other Headers unaltered
    return super.tableView(tableView, heightForHeaderInSection: section)
  }

  
  override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if (section == 4) && (keychain.getBool("purchased") ?? false) == false {
      //header height for selected section
      return 0.1
    }
    
    return super.tableView(tableView, heightForFooterInSection: section)
  }
  
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 4 { //Index number of interested section
      if (keychain.getBool("purchased") ?? false) == false {
        return 0 //number of row in section when you click on hide
      } else {
        return 1 //number of row in section when you click on show (if it's higher than rows in Storyboard, app will crash)
      }
    } else {
      return super.tableView(tableView, numberOfRowsInSection: section) //keeps inalterate all other rows
    }
  }
  
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if (section == 4) && (keychain.getBool("purchased") ?? false) == false {
      return ""
    }
    return super.tableView(tableView, titleForHeaderInSection: section)
  }

  
  
    
}
