//
//  EditImageTVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 7/5/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import Photos
import FirebaseAuth
import FirebaseDatabase

class EditImageTVC: UITableViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  var image : Image! = nil
  var imageKey = ""
  
  @IBOutlet weak var textView: UITextView!
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  
  @IBOutlet weak var photoView: UIImageView!
  @IBOutlet weak var date: UITextField!
  @IBOutlet weak var accuracy: UITextField!
  @IBOutlet weak var zoneLabel: UILabel!
  //became Coords Info
  @IBOutlet weak var gZone: UITextField!
  @IBOutlet weak var category: UITextField!
  @IBOutlet weak var predText: UITextField!
  @IBOutlet weak var project: UITextField!
  var picker = UIImagePickerController()

  
  
  @IBOutlet weak var zoneCell: UITableViewCell!
  
  var predefinedTexts = [PredefinedText]()
  var albums = [Album]()
  var definedFinds = [String : String]()
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationItem.title = "Edit \(image.initialsStamp)"
    image = fetchImage()
    albums = []
    predefinedTexts = []
    fetchAlbums()
    fetchPredefinedTexts()
    fetchDefinedFind()
    updateTextFields()
    
    
    if textView.text.isEmpty || textView.text == "" ||  textView.text == " "{
      textView.text = "Add a description..."
      textView.textColor = UIColor.lightGray
    }
  }
  
    override func viewDidLoad() {
      super.viewDidLoad()
      
      textView.delegate = self
      
      image = fetchImage()
      
      
  }

  
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    
    if textView.textColor == UIColor.lightGray {
      textView.text = ""
      textView.textColor = UIColor.black
    }
  }
  
  /*
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.row == 5 && zoneHidden {
      return 0.0
    } else {
      return 24.0
    }
  }
  */
  
  func textViewDidEndEditing(_ textView: UITextView) {
    
    if textView.text.isEmpty || textView.text == "" ||  textView.text == " "{
      textView.text = "Add a description..."
      textView.textColor = UIColor.lightGray
    }
  }
  
  func updateTextFields(){
    photoView.image = UIImage(data: (image.imgData)! as Data)
    
    
    if image.url! != "" {
      photoView.sd_setImage(with: URL(string: image.url!))
    } else {
      if image.imgData != nil {
        photoView.image = UIImage(data: image.imgData! as Data)
      } else {
        photoView.image = #imageLiteral(resourceName: "placeholderImage")
      }
    }
    
    
    if image.coordinateSystem == Const.COOR_DD_WGS84 || image.coordinateSystem == Const.COOR_DMS_WGS84 || image.coordinateSystem == Const.COOR_DM_WGS84 {
      gZone.text = "LO: \(image.longitude), LA: \(image.latitude)"
    } else {
      
      if image.coordinateSystem == Const.COOR_UTM_UK_GREF {
        gZone.text = "E: \(image.longitude), N: \(image.latitude), GRef: \(image.zone)"
      } else {
        gZone.text = "E: \(image.longitude), N: \(image.latitude), Z: \(image.zone)"
      }
      
    }
    
//    if image.coordinateSystem == Const.COOR_DD_WGS84 || image.coordinateSystem == Const.COOR_DMS_WGS84 || image.coordinateSystem == Const.COOR_DM_WGS84 {
//      zoneCell.isHidden = true
//      eastingLabel.text = "Longitude"
//      northingLabel.text = "Latitude"
//    } else {
//      zoneCell.isHidden = false
//      
//      if image.coordinateSystem == Const.COOR_UTM_UK_GREF {
//        zoneLabel.text = "Grid Ref."
//      } else {
//        zoneLabel.text = "Zone"
//      }
//      eastingLabel.text = "Easting"
//      northingLabel.text = "Northing"
//    }
    
    date.text = image.timestamp
    accuracy.text = image.accuracyValue
    //easting.text = image.longitude
    //northing.text = image.latitude
    //gZone.text = image.zone
    category.text = image.symbolText!
    predText.text = image.ownerText
    project.text = image.albumName
    textView.text = image.longDesc
  }
  
  

  
  
  
  func fetchDefinedFind() {
    
    self.definedFinds = ["x" : defaults.string(forKey: "x")!,
                         "a" : defaults.string(forKey: "a")!,
                         "b" : defaults.string(forKey: "b")!,
                         "c" : defaults.string(forKey: "c")!,
                         "d" : defaults.string(forKey: "d")!,
                         "e" : defaults.string(forKey: "e")!,
                         "f" : defaults.string(forKey: "f")!,
                         "o" : defaults.string(forKey: "o")!,
                         "v" : defaults.string(forKey: "v")!]
    
  }

  func fetchAlbums(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    
    let sortDescriptor = NSSortDescriptor(key: Const.UPDATED_AT, ascending: false)
    request.sortDescriptors = [sortDescriptor]
    
    
    do {
      let albums = try manageContext.fetch(request)
      for album in albums as! [AlbumData]{
        let newAlbum = Album(name: album.name!, key: album.key!, owner: album.owner, createdAt: album.createdAt, updatedAt: album.updatedAt)
        
        self.albums.append(newAlbum)
      }
      
    } catch {
      print("Error fetching all Albums")
    }
  }
  
  func fetchPredefinedTexts(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.PREDEFINED_TEXT_ENTITY)
    
    let sortDescriptor = NSSortDescriptor(key: Const.PREDEFINED_TEXT, ascending: true)
    request.sortDescriptors = [sortDescriptor]
    
    do {
      let predefinedTexts = try manageContext.fetch(request)
      for predefinedText in predefinedTexts as! [PredefinedTextData] {
        let new = PredefinedText(text: predefinedText.text!, selected: predefinedText.selected, key: predefinedText.key!)
        self.predefinedTexts.append(new)
      }
    } catch {
      print("Error fetching predeterminedText")
    }
  }
 
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    updateAImage()
  }

  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", imageKey)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! ImageData
      
      managedObject.setValue(predText.text!, forKey: Const.OWNER_TEXT)
      managedObject.setValue(UIImageJPEGRepresentation(photoView.image!, 1), forKey: Const.IMG_DATA)

      if textView.text! != "Add a description..." {
        managedObject.setValue(textView.text!, forKey: Const.LONG_DESCRIPTION)
      }
      
      do{
        try manageContext.save()
        
        updateAImageFB()
        print("Image updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
  }

  func updateAImageFB(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", image.key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      if let uid = Auth.auth().currentUser?.uid {
        let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(image.key)
        ref.updateChildValues(image.toDictionary())
      }
      
    } catch {
      print("error")
    }
  }
  
  func fetchImage() -> Image {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", imageKey)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
            let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      return image
      
    } catch {
      print("error")
    }
    return image
  }





}

extension EditImageTVC {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 6 {
      performSegue(withIdentifier: "goToAlbums", sender: self)
    } else if indexPath.row == 4 {
      performSegue(withIdentifier: "goToCategories", sender: self)
    } else if indexPath.row == 5 {
      performSegue(withIdentifier: "goToPredText", sender: self)
    } else if indexPath.row == 3 {
      performSegue(withIdentifier: "goToCoordinates", sender: self)
    }
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "goToAlbums" == segue.identifier {
      let DestVC = segue.destination as! selectAlbumTVC
      DestVC.albums = self.albums.map{$0.name}
      DestVC.key = image.key
    } else if "goToCategories" == segue.identifier {
      let Dest : selectCategoryTVC = segue.destination as! selectCategoryTVC
      Dest.key = image.key
    } else if "goToPredText" == segue.identifier {
      let Dest : SelectPredTextTVC = segue.destination as! SelectPredTextTVC
      Dest.key = image.key
      Dest.predTexts = self.predefinedTexts.map{$0.text}
    } else if "goToCoordinates" == segue.identifier {
      let destination : SelectCoordinatesTVC = segue.destination as! SelectCoordinatesTVC
      destination.key = image.key

    }
  }

//  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//    if indexPath.row == 8 {
//      categoryPicker.isHidden = true
//    }
//  }
}


//Edit photo methods
extension EditImageTVC {
  
  @IBAction func editPhoto(_ sender: UIButton) {
    let alertController = UIAlertController(title: "Select a Photo", message: "Please select a method to upload the photo.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "Take a Photo", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.takePhoto()
    })
    
    let kmlAction = UIAlertAction(title: "Choose from Library", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.chooseFromLibrary()
    })
    
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  func takePhoto(){
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .camera
    picker.cameraFlashMode = .off
    present(picker, animated: true, completion: nil)
    
  }
  
  
  func chooseFromLibrary(){
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .photoLibrary
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    photoView.image = (info[UIImagePickerControllerEditedImage] as? UIImage)!; dismiss(animated: true, completion: nil)
    updateAImage()

  }

}


