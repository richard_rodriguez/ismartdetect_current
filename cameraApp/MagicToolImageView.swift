//
//  MagicToolImageView.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/9/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class MagicToolImageView: UIImageView {

  
  override func awakeFromNib() {
    
    if DeviceType.IS_IPHONE_4_OR_LESS {
      if let image = UIImage(named: "magic-tool-small") {
        self.image = image
      }
    } else {
      if let image = UIImage(named: "magic-yellow") {
        self.image = image
      }
    }
  
  }

}
