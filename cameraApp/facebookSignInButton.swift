//
//  facebookSignInButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/3/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class facebookSignInButton: FBSDKLoginButton {

  override func awakeFromNib() {
    self.readPermissions = ["public_profile", "email", "user_friends","user_birthday"]
  }
    
    
}
