//
//  CameraButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/9/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class CameraButton: UIButton {
  
  override func awakeFromNib() {
    if DeviceType.IS_IPHONE_4_OR_LESS {
      if let image = UIImage(named: "camera-small") {
        self.setImage(image, for: .normal)
      }
    } else {
      if let image = UIImage(named: "camera") {
        self.setImage(image, for: .normal)
      }
    }
    
    
  }
  
}
