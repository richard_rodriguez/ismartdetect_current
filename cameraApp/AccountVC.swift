//  AccountVC.swift
//  Created by Richard Rodriguez on 11/30/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MBProgressHUD
import FBSDKLoginKit
import FirebaseStorage
import KeychainSwift


class AccountVC: UITableViewController {
  
  @IBOutlet weak var hiName: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var loginInfo: UILabel!
  @IBOutlet weak var profilePicture: UIImageView!
  
  var imagesHandle: UInt!
  var emailsHandle: UInt!
  var predefinedTextsHandle: UInt!
  var notesHandle: UInt!
  var albumsHandle: UInt!
  
  var imagesRef : DatabaseReference!
  var albumsRef : DatabaseReference!
  var predefinedTextsRef : DatabaseReference!
  var emailsRef : DatabaseReference!
  var notesRef : DatabaseReference!
  let keychain = KeychainSwift()

  
  let firstRunObserver = defaults.bool(forKey: "firstRunObserver")

  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    

    
    
    if firstRunObserver {
      defaults.set(true, forKey: "firstRunObserver")

      let albumsObserver = ObserversProvider.Instance.observer(child: Const.ALBUMS, option: .album)
      self.albumsRef = albumsObserver.ref
      self.albumsHandle = albumsObserver.handle
      //DBProvider.Instance.restoreSetting()

      let imagesObserver = ObserversProvider.Instance.observer(child: Const.IMAGES_DATA, option: .image)
      self.imagesRef = imagesObserver.ref
      self.imagesHandle = imagesObserver.handle

      let notesObserver = ObserversProvider.Instance.observer(child: Const.NOTES, option: .note)
      self.notesRef = notesObserver.ref
      self.notesHandle = notesObserver.handle

      let emailsObserver = ObserversProvider.Instance.observer(child: Const.EMAILS, option: .email)
      self.emailsRef = emailsObserver.ref
      self.emailsHandle = emailsObserver.handle

      let predefinedTextsObserver = ObserversProvider.Instance.observer(child: Const.PREDEFINED_TEXTS, option: .predefinedText)
      self.predefinedTextsRef = predefinedTextsObserver.ref
      self.predefinedTextsHandle = predefinedTextsObserver.handle
      DBProvider.Instance.restoreDefinedFinds()
  }
  
    setProfile()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

   
    navigationItem.title = "Account Setting"
    let index  = IndexPath(item: 0, section: 0)
    self.tableView.cellForRow(at: index)?.contentView.layer.borderWidth = 5.0
    self.tableView.cellForRow(at: index)?.contentView.layer.borderColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1).cgColor
    

  }
  
  override func viewWillDisappear(_ animated: Bool) {
    if firstRunObserver {
      imagesRef.removeObserver(withHandle: imagesHandle)
      emailsRef.removeObserver(withHandle: emailsHandle)
      notesRef.removeObserver(withHandle: notesHandle)
      predefinedTextsRef.removeObserver(withHandle: predefinedTextsHandle)
      albumsRef.removeObserver(withHandle: albumsHandle)
    }
  }
  
  //TODO: - Need work
  func assignUserNameToNavigation() {
    print("entró assignUserNameToNavigation")
    
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    if indexPath.section == 2 && indexPath.row == 0 {
      deleteAccount()
    }
  }
  
  
  @IBAction func logoutButton(_ sender: Any) {
    logout()
  }
  
  
  @IBAction func goToHomescreen(_ sender: Any) {
    performSegue(withIdentifier: "home", sender: self)
  }
  
  
  func logout(){
    if AuthProvider.Instance.logout() {
      
    Helpers.Instance.logoutFacebook()
      
      dismiss(animated: true, completion: nil)
    } else {
      let alert = Helpers.Instance.alertUser(title: "Error", message: "Error trying to log out, please try later.")
      present(alert, animated: true, completion: nil)
    }
  }
  

  func showAllDataDeleteMessage(message: String){
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .zoomIn
    loadingNotification.bezelView.color = UIColor.black
    loadingNotification.customView =  UIImageView(image: UIImage(named: "correct8"))
    loadingNotification.label.textColor = UIColor.white
    loadingNotification.label.text = message
    
    DispatchQueue.main.async(execute: { () -> Void in
      loadingNotification.hide(animated: true, afterDelay: 1.2)
    })
    
  }
  
  
  func setProfile(){
    
     let profileInfo = Helpers.Instance.getInitialProfile()
      
      if let email = profileInfo?.email {
          emailLabel.text = "(\(email))"
      } else {
        emailLabel.text = ""
      }
    
      if let name = profileInfo?.firstName {
        if !(name.isEmpty) {
          self.hiName.text = "Hi \(name)!"
        } else {
          hiName.text = "Hi there!"
        }
      } else {
        hiName.text = "Hi there!"
      }

      
      if let profileImageURL = profileInfo?.profileImageURL {
        profilePicture.downloadedFrom(link: profileImageURL) //sd_setImage(with: URL(string: profileImageURL))
        profilePicture.layer.borderWidth = 4.0
        profilePicture.layer.cornerRadius = 10.0
        profilePicture.layer.borderColor = UIColor.white.cgColor
      }
      
      
      if let firstName = profileInfo?.firstName {
        print(firstName)
      }
      
      if let providerInfo = profileInfo?.providerInfo {
        loginInfo.text  = ("Logged in using your \(providerInfo)")
      } else {
        loginInfo.text  = ("Logged In")
      }
  }
  
  
  func deleteAccount(){
    let alert = UIAlertController(title: "WARNING!", message: "Are you about to delete your Account? Do you want to continue?", preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: "Continue", style: .destructive, handler: { (action) in
      self.alertInside()
    }))
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    present(alert, animated: true, completion: nil)
  }
  
  
  func alertInside(){
    let alert = UIAlertController(title: "Delete your Account?", message: "Are you sure you want to delete your Account? All your data will be destroy.", preferredStyle: UIAlertControllerStyle.alert)
    
    alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {
      (action) -> Void in
      
      if let user = Auth.auth().currentUser {
        let uid = user.uid
        let storageRef =  Storage.storage().reference().child(uid)
        self.keychain.set("1", forKey: Const.APP_NAME)
        
        Database.database().reference().child(uid).removeValue(completionBlock: { (error, ref) in
          if error != nil {
            print("Error deleting Database info")
          }
          
          storageRef.delete(completion: { (error) in
            if error != nil {
            print("Error deleting Storage info: ", error!)
            return
            }
          })

          
        })
      
        user.delete { error in
          if error != nil {
            print("Error trying to deleting user: ", error!.localizedDescription) // An error happened.
            
            let alert = UIAlertController(title: "Log In Again", message: "For security reasons, please log in again.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
             
              // Create a reference to the file to delete
              // Delete the file
              Helpers.Instance.logoutFacebook()
              self.logout()
            })
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
          
            
          } else {
            defaults.set(nil, forKey: Const.LAST_EMAIL_USED)
            
            Helpers.Instance.logoutFacebook()
            Helpers.Instance.deleteAllCoreData()
            
            self.showAllDataDeleteMessage(message: "Account Deleted")
            defaults.set(false, forKey: "restored")
            defaults.set(false, forKey: "restoredEmailConf")
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.2){
              self.performSegue(withIdentifier: "home", sender: self)
            }
            
          }
        }
      }
      
    })
    )
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    present(alert, animated: true, completion: nil)

  }

  
  
  
}






