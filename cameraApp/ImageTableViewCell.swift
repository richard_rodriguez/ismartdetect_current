//
//  ImageTVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 2/24/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

  
  @IBOutlet weak var id: UILabel!
  @IBOutlet weak var date: UILabel!
  @IBOutlet weak var accuracy: UILabel!
  @IBOutlet weak var easting: UILabel!
  @IBOutlet weak var northing: UILabel!
  @IBOutlet weak var gZone: UILabel!
  @IBOutlet weak var category: UILabel!
  @IBOutlet weak var predText: UILabel!
  @IBOutlet weak var zoneLabel: UILabel!
  @IBOutlet weak var eastingLabel: UILabel!
  @IBOutlet weak var northingLabel: UILabel!
  @IBOutlet weak var longDescription: UITextView!
  
  
  @IBOutlet weak var thumbImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
