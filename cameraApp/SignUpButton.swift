//
//  SignUpButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/28/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class SignUpButton: UIButton {

 
  override func awakeFromNib() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = Const.RED
    frame.size.height = 40
    layer.borderColor = Const.RED.cgColor
    layer.cornerRadius = 0
    layer.borderWidth = 1
    layer.masksToBounds = true
    setTitleColor(.white, for: .normal)
  }
  
}
