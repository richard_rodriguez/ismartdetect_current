//
//  AddNoteViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/14/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import FirebaseDatabase

class AddNoteViewController : UIViewController {
  
  
  @IBOutlet weak var note: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    note.becomeFirstResponder()
    IQKeyboardManager.sharedManager().enableAutoToolbar = false
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
  }
   
override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    print(note.text)
    if !note.text!.isEmpty {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let newNote = NSEntityDescription.insertNewObject(forEntityName: Const.NOTE_ENTITY, into: manageContext)
  
      
      
      let text = note.text!
      let key = Database.database().reference().child(Const.NOTES).childByAutoId().key

      
      newNote.setValue(text, forKey: "text")
      newNote.setValue(key, forKey: "key")
    
      
      do{
        try manageContext.save()
        print("Note saved on CoreData.")
        DBProvider.Instance.add(new: Note(text: text, key: key))
        
      }catch {
        print("error")
      }
    }
  }
  
}
