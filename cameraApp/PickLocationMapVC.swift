//
//  PickLocationMapVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 7/26/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData


class PickLocationMapVC: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

  
    var key = String()
    var image : Image!
    var mapView: GMSMapView?
    var camera : GMSCameraPosition!
    var isWGS84_DK : Bool!
    var finalLatitude = String()
    var finalLongitude = String()
    var finalDescription = String()
    var finalZone = String()
    var initialLatitude = 0.0
    var initialLongitude = 0.0
  
  
    let locationManager = CLLocationManager()

  
  
    let coordinatesSytemsCode =   ["DMS-WGS84",
                                   "DM-WGS84",
                                   "DD-WGS84",
                                   "UTM-WGS84",
                                   "UTM-ETRS89",
                                   "UTM-WGS84",
                                   "UTM-UK-GREF"]
  
    let coordinateSystem = ["LAT/LON  DMS WGS84",
                            "LAT/LON  DM  WGS84",
                            "LAT/LON  DD  WGS84",
                            "UTM  WGS84 DK/N",
                            "UTM  ETRS89 DK East",
                            "UTM  WGS84",
                            "UTM  UK Grid Refence"]


    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      image = Helpers.Instance.fetchImage(from: key)
      navigationItem.title = "Pick a Location"
      
      
    
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
      

      NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
      NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
      
      
      image = Helpers.Instance.fetchImage(from: key)

      GMSServices.provideAPIKey("AIzaSyAMKn4fHA_rDCApJtGFSOgFgWHDMgMlQ8E")
      mapView?.delegate = self
     
      var marker = CSMarker(position: CLLocationCoordinate2D(latitude: initialLatitude , longitude: initialLongitude))
      var coordinate = CLLocationCoordinate2D(latitude: initialLatitude, longitude: initialLongitude)
      var snippetText = String()
      
      
      if image.latitude != "" {
        coordinate = CLLocationCoordinate2D(latitude: image.decimalLongitude, longitude: image.decimalLatitude)
        self.camera = GMSCameraPosition.camera(withLatitude: image.decimalLongitude, longitude: image.decimalLatitude, zoom: 15)
        marker = CSMarker(position: CLLocationCoordinate2D(latitude: image.decimalLongitude, longitude: image.decimalLatitude))
        
        let valuesBuilded = buildSnippet(coordinate: coordinate)
        snippetText = valuesBuilded.snippetText
      
      } else {
        coordinate = CLLocationCoordinate2D(latitude: initialLatitude, longitude: initialLongitude)
        self.camera = GMSCameraPosition.camera(withLatitude: initialLatitude, longitude: initialLongitude, zoom: 15)
        marker = CSMarker(position: CLLocationCoordinate2D(latitude: initialLatitude , longitude: initialLongitude))
        
        let valuesBuilded = buildSnippet(coordinate: coordinate)
        snippetText = valuesBuilded.snippetText
      }

      view = mapView
      mapView?.settings.compassButton = true
      mapView?.isMyLocationEnabled = true
      mapView?.settings.myLocationButton = true
      mapView = GMSMapView.map(withFrame: .zero, camera: self.camera)
      mapView?.mapType = .hybrid // kGMSTypeHybrid

      if marker.map == nil {
        marker.map = mapView
      }
      
      let valuesBuilded = buildSnippet(coordinate: coordinate)
      finalLatitude = valuesBuilded.latitude
      finalLongitude = valuesBuilded.longitude
      finalZone = valuesBuilded.zone
      finalDescription = Helpers.Instance.buildDescription(String(coordinate.latitude), String(coordinate.longitude))
      
      marker.snippet = snippetText
      marker.map = mapView
      mapView?.selectedMarker = marker
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
    
    updateAImage()
    
  }
  
  
  func setCenterPoint(){
  }

  func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    mapView.clear()
    let marker = GMSMarker(position: coordinate)
    marker.title = "\(coordinateSystem[ coordinatesSytemsCode.index(of: image.coordinateSystem)! ])"
    
    let valuesBuilded = buildSnippet(coordinate: coordinate)
    finalLatitude = valuesBuilded.latitude
    finalLongitude = valuesBuilded.longitude
    finalZone = valuesBuilded.zone
    finalDescription = Helpers.Instance.buildDescription(String(coordinate.latitude), String(coordinate.longitude))

    marker.snippet = valuesBuilded.snippetText
    marker.map = mapView
    mapView.selectedMarker = marker
    
  }
 
  func cancel(){
    print("Canceled")
    deleteAImage()
    dismiss(animated: true, completion: nil)
    
  }
  
  func buildSnippet(coordinate : CLLocationCoordinate2D ) -> (snippetText : String, latitude : String, longitude : String, zone : String) {
    
    var result = String()
    var latitude = String()
    var longitude = String()
    var finalZone = String()
    
    let coordinateSystem = image.coordinateSystem
    
    if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
      if coordinateSystem == Const.COOR_DD_WGS84 {
        
        latitude = String(round(coordinate.latitude*100000.0) / 100000.0)
        longitude = String(round(coordinate.longitude*100000.0) / 100000.0)
        finalZone = ""
        
        result = "Lat: \(latitude) \nLon: \(longitude)"
      } else if coordinateSystem == Const.COOR_DM_WGS84 {
        let DM = Helpers.Instance.DMStoDM(lat: coordinate.latitude, lon: coordinate.longitude)
        result = "Lat: \(DM.latitude) \nLon: \(DM.longitude)"
        
        latitude = DM.latitude
        longitude = DM.longitude
        finalZone = ""

        
      } else if coordinateSystem == Const.COOR_DMS_WGS84 {
        let DMS = Helpers.Instance.coordinateToDMS(lat: coordinate.latitude, lon: coordinate.longitude)
        
        print(Helpers.Instance.getDegMinSecOri(coord: DMS.latitude))

        latitude = DMS.latitude
        longitude = DMS.longitude
        finalZone = ""
        
        result = "Lat: \(DMS.latitude) \nLon: \(DMS.longitude)"

      }
    
    } else {
      
      if coordinateSystem == Const.COOR_UTM_WGS84 && isWGS84_DK == false {
        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesGrl: coordinate)
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(coordinate.latitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        let zone = String(gridZone) + subZone + " " + hemisphere
        
        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]
        finalZone = zone
        
        
        result = "E: \(longitude) \nN: \(latitude)\nZ: \(zone)"
      
      } else if  coordinateSystem == Const.COOR_UTM_WGS84 && isWGS84_DK == true {
        
        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinates: coordinate)
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(coordinate.latitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        let zone = String(gridZone) + subZone + " " + hemisphere
        
        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]
        finalZone = zone
        
        result = "E: \(longitude) \nN: \(latitude)\nZ: \(zone)"
        
      } else if coordinateSystem == Const.COOR_UTM_ETRS89 {
        let resultV =  GeodeticUTMConverter.latitudeAndLongitude(toUTMCoordinatesETRS: coordinate)
        let subZone = Helpers.Instance.UTMzdl(latDeg: Int(coordinate.latitude))
        let gridZone = resultV.gridZone
        let hemisphere = resultV.hemisphere.rawValue == 1 ? "S" : "N"
        let zone = String(gridZone) + subZone + " " + hemisphere
        
        latitude = String(resultV.northing).components(separatedBy: ".")[0]
        longitude = String(resultV.easting).components(separatedBy: ".")[0]
        finalZone = zone
        
        
        result = "E: \(longitude) \nN: \(latitude)\nZ: \(zone)"
      } else if coordinateSystem == Const.COOR_UTM_UK_GREF {
        let coord = Helpers.Instance.LLtoNE(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let gridReference = Helpers.Instance.NE2NGR(easting: Double(coord.eastring)!, northing: Double(coord.northing)!)
        
        
        let northing = coord.northing.components(separatedBy: ".")[0]
        let easting = coord.eastring.components(separatedBy: ".")[0]
        let gridRef = gridReference
        
        latitude = northing
        longitude = easting
        finalZone = gridRef
        
        result = "E: \(easting), \nN: \(northing)\nGrid Ref: \(gridRef)"
      }
      
     
    }
    
    return (snippetText : result, latitude : latitude, longitude : longitude, zone : finalZone)
  }
  
  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      if result.count > 0 {
        let managedObject = result[0] as! ImageData
        
        managedObject.setValue(finalLongitude, forKey: Const.LONGITUDE)
        managedObject.setValue(finalLatitude, forKey: Const.LATITUDE)
        managedObject.setValue(finalZone, forKey: Const.ZONE)
        managedObject.setValue(finalDescription, forKey: Const.DESC)
        managedObject.setValue(image.coordinateSystem, forKey: Const.COORDINATE_SYSTEM)

    
        do{
          try manageContext.save()
          
          print("Image updated.")
          
          
        }catch {
          print("Error trying to editing the note.")
        }
      }
    }
  }

  func deleteAImage(){
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        /*
         if let uid = Auth.auth().currentUser?.uid {
         let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
         ref.removeValue()
         print("Deleted Image Data on firebase: ", key)
         } else {
         print("Error trying to Image Data.")
         }
         */
      }catch {
        print("error")
      }
    }
    
  }
  
  
}

