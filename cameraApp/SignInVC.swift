//  Created by Richard Rodriguez on 11/29/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import FirebaseAuth
import CoreData
import FBSDKLoginKit
import FirebaseDatabase
import GoogleSignIn
import MBProgressHUD


class SignInVC: UIViewController, FBSDKLoginButtonDelegate, GIDSignInDelegate, GIDSignInUIDelegate {
  
  private let MAIN_VIEW_SEGUE = "mainSG"
  private let ACCOUNT_SEGUE = "accounSG"
  
  @IBOutlet weak var emailTF: UITextField!
  @IBOutlet weak var passwordTF: UITextField!
  
  @IBOutlet weak var googleButton: googleLogInButton!
  @IBOutlet weak var orLabel: UILabel!
  var counter = 0
  @IBOutlet weak var emailLoginButton: SignUpButton!
  
  var dbRef : DatabaseReference {
    return Database.database().reference()
  }
  
  weak var actionToEnable : UIAlertAction?
  
  @IBOutlet weak var loginView: FBSDKLoginButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    DispatchQueue.global(qos: .userInitiated).async {
      if let id = Auth.auth().currentUser?.uid {
        let ref = self.dbRef.child(id).child(Const.IMAGES_DATA)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
          defaults.set(snapshot.children.allObjects.count, forKey: "numberOfFiles")
        })
      }
    }
    
    self.navigationController?.navigationBar.tintColor = Const.RED
    
    if let lastEmailUsed = defaults.string(forKey: Const.LAST_EMAIL_USED) {
      emailTF.text = lastEmailUsed
    }
  
    checkIfUserIsVerified()

    
    GIDSignIn.sharedInstance().uiDelegate = self
    GIDSignIn.sharedInstance().delegate = self
    IQKeyboardManager.sharedManager().enableAutoToolbar = true
    
    loginView.delegate = self
    loginView.readPermissions = ["public_profile", "email", "user_friends","user_birthday"]
    loginView.frame.size.height = 50

  }
  
  //MARK: - Email/Pass Login
  @IBAction func signIn(_ sender: Any) {
    if emailTF.text! != "" && passwordTF.text! != "" {
      DispatchQueue.main.async {
        self.emailLoginButton.isEnabled = false
      }
      
      AuthProvider.Instance.login(email: emailTF.text!, password: passwordTF.text!, loginHandler: {(message) in
        
        if message != nil {
          let alert = Helpers.Instance.alertUser(title: "There was a problem.", message: message!)

          DispatchQueue.main.async {
            self.emailLoginButton.isEnabled = true
          }

          self.present(alert, animated: true, completion: nil)
        } else {
          
          if let userLoggedUsingEmail = defaults.string(forKey: Const.USER_LOGGED_USING_EMAIL) {
            Helpers.Instance.setDefaultsData(email: self.emailTF.text!, name: userLoggedUsingEmail, loginType: Const.EMAIL_LOGIN)
          } else {
            Helpers.Instance.setDefaultsData(email: self.emailTF.text!, name: "", loginType: Const.EMAIL_LOGIN)
          }

          
          defaults.set(self.emailTF.text!, forKey: Const.LAST_EMAIL_USED)
          let user = Auth.auth().currentUser
          
          
          if (user?.isEmailVerified)! {
            let restored = defaults.bool(forKey: "restored")
           

            if !restored {
              defaults.set(true, forKey: "restored")
              DispatchQueue.global(qos: .userInitiated).async {
                
                
                if let id = Auth.auth().currentUser?.uid {
                  let ref = self.dbRef.child(id).child(Const.IMAGES_DATA)
                  
                  ref.observeSingleEvent(of: .value, with: { (snapshot) in
                    defaults.set(snapshot.children.allObjects.count, forKey: "numberOfFiles")
                    
                    if snapshot.children.allObjects.count == 0 {
                      DBProvider.Instance.saveImagesData()
                      //DBProvider.Instance.saveSetting()
                      DBProvider.Instance.saveEmails()
                      DBProvider.Instance.savePredTexts()
                      DBProvider.Instance.saveNotes()
                      DBProvider.Instance.saveAlbums()
                      DBProvider.Instance.saveDefinedFinds()
                    } else {
                      DBProvider.Instance.saveImagesData()
                      //DBProvider.Instance.saveSetting()
                      DBProvider.Instance.saveEmails()
                      DBProvider.Instance.savePredTexts()
                      DBProvider.Instance.saveNotes()
                      DBProvider.Instance.saveAlbums()
                      DBProvider.Instance.saveDefinedFinds()
                      
                      DBProvider.Instance.restoreAlbums()
                      //DBProvider.Instance.restoreSetting()
                      DBProvider.Instance.restoreImagesData()
                      DBProvider.Instance.restoreNotes()
                      DBProvider.Instance.restoreEmails()
                      DBProvider.Instance.restorePredefinedTexts()
                      DBProvider.Instance.restoreDefinedFinds()
                    }
                  })
                }
              }
            } else {
              Helpers.Instance.deleteAllData(entity: Const.ALBUM_ENTITY)
              Helpers.Instance.deleteAllData(entity: Const.NOTE_ENTITY)
              Helpers.Instance.deleteAllData(entity: Const.PREDEFINED_TEXT_ENTITY)
              Helpers.Instance.deleteAllData(entity: Const.EMAIL_ENTITY)
              Helpers.Instance.deleteAllData(entity: Const.IMAGE_ENTITY)
              Helpers.Instance.saveDefinedFinds()
              
              DispatchQueue.global(qos: .userInitiated).async {
                DBProvider.Instance.restoreAlbums()
                //DBProvider.Instance.restoreSetting()
                DBProvider.Instance.restoreImagesData()
                DBProvider.Instance.restoreNotes()
                DBProvider.Instance.restoreEmails()
                DBProvider.Instance.restorePredefinedTexts()
                DBProvider.Instance.restoreDefinedFinds()
              }

            }

            DispatchQueue.main.async {
              self.emailLoginButton.isEnabled = true
              
            }
           
            
            let alert = UIAlertController(title: "Downloading...\nIn Progress", message: "It will take from 10 sec. to 5 min., all depends on your account storage.", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (alert) -> Void in
              self.performSegue(withIdentifier: self.ACCOUNT_SEGUE, sender: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
          } else {
            if AuthProvider.Instance.logout() {
              //print(user)
              //self.handleNoVerifiedEmail(user as Any)
              
              let alert = UIAlertController(title: "Confirmation Needed", message: "We sent you an email. Please, confirm you account before to continue.", preferredStyle: .alert)
              
              
              
              let resent = UIAlertAction(title: "Resent Email", style: .cancel) { (action) in
                
                
                user?.sendEmailVerification(completion: { (error) in
                  
                  if error != nil {
                    print("Error trying to sending the verification email, on SignUpVC email/pass login func line 65")
                    return
                  }
                  print("Verification Email sent.")
                  
                })
              }
              
              let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
              
              alert.addAction(resent)
              alert.addAction(ok)
              
              self.present(alert, animated: true, completion: nil)
              
              
              
              
            } else {
              print("Error trying loggin out.")
            }
          }
        }
        
        

      })
    } else {
      let alert = Helpers.Instance.alertUser(title: "Email and Password required.", message: "Please enter a Email and Password to log in.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  
  
//  func handleNoVerifiedEmail(_ user: Any){
//  }
  

 func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    print("Dig log out of Facebook")
    dismiss(animated: true, completion: nil)

 }

  
  //MARK: - Facebook Login
  func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
    loginButton.delegate = self
    loginView.frame.size.height = 40
    loginButton.readPermissions = ["public_profile", "email", "user_friends","user_birthday"]
    
    if error != nil {
      print(error)
      return
    }
    
    if result.isCancelled {
      return
    }
    
    defaults.set(nil, forKey: Const.LAST_EMAIL_USED)
    
    
    let accessToken = FBSDKAccessToken.current()
    let accessTokenString = accessToken?.tokenString!
    
    let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(250).height(250)"], tokenString: accessTokenString, version: "v2.4", httpMethod: "GET")
    
    _ = request?.start(completionHandler: { ( connection , result, error) -> Void in
      
      if error != nil {
        print("Failed to st art graph request ", error!)
       }
      
      
        let values = result as! NSDictionary
        print(values)
        let name =  (values.value(forKey: "name") as! String)
        let email = (values.value(forKey: "email") as! String)
        let picture = (values.value(forKey: "picture") as! NSDictionary)
        let data = picture.value(forKey: "data") as! NSDictionary
        let profileURL = data.value(forKey: "url") as! String
 
      
        print("Email: ", email)
        
        Helpers.Instance.setDefaultsData(email: email, name: name, loginType: Const.FACEBOOK, profileURL: profileURL)
     

        self.loginWithFB(result: result, accessToken: accessToken! )

      
    })
  }

  func loginWithFB(result: Any?, accessToken: FBSDKAccessToken ) {
    let credentials = FacebookAuthProvider.credential(withAccessToken: (accessToken.tokenString!))
    
    
    
    
    Auth.auth().signIn(with: credentials, completion: { (user, error) in

      
      if error != nil {
        print("somentthing happen: ", error!.localizedDescription)
        
        if error?.localizedDescription == "The email address is already in use by another account." {
          let alert = Helpers.Instance.alertUser(title: "NOTICE!", message: "The email of this account has been used for Sign Up using another Sign Up method (Google or Email).")
          self.present(alert, animated: true, completion: nil)
        }
        
        
        
        Helpers.Instance.logoutFacebook()
        return
      } else {
        
        print("Logged in usign Facebook.")

        let values = result as! NSDictionary
        let id = user!.uid
        let name =  (values.value(forKey: "name") as! String)
        let email =  (values.value(forKey: "email") as! String)
        print(name)
        print(email)
        
//        let credentials = FacebookAuthProvider.credential(withAccessToken: (accessToken.tokenString)!)
//        
//        
//        user?.link(with: credentials, completion: { (user, error) in
//          if let error = error {
//            print("Error: ", error)
//          }
//         })

        self.handleData()

          defaults.set(true, forKey: "saveData")
          DBProvider.Instance.saveUser(id: id, name: name, email: email)
        
        self.performSegue(withIdentifier: self.ACCOUNT_SEGUE, sender: self)

      }
    })
  }
  
  
  //MARK: - Google Login
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    self.googleButton.isEnabled = false
    if let err = error {
      print("error trying to log in Google: ", err.localizedDescription)
      return 
    }
    
    defaults.set(nil, forKey: Const.LAST_EMAIL_USED)

    guard let idToken = user.authentication.idToken else { return}
    guard let accessToken = user.authentication.accessToken else { return}
    
    
    let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
    
    
    Auth.auth().signIn(with: credentials, completion: {(user, error) in
      if let err = error {
        print("Failed to create a Firebase User with Google account: ", err)
        return
      }
      
      self.handleData()
      
      guard let uid = user?.uid else { return }
      print("Successfully logged into Firebase with Google", uid)
      
      let profileURL =  String(describing: (user?.photoURL!)!) + "?sz=250"
      Helpers.Instance.setDefaultsData(email: (user?.email!)!, name: (user?.displayName)!, loginType: Const.GOOGLE, profileURL: profileURL)

   
      let firstRun = defaults.bool(forKey: "saveData")
      if !firstRun {
        defaults.set(true, forKey: "saveData")
        DBProvider.Instance.saveUser(id: uid, name: (user?.displayName)!, email: (user?.email)!)
      }
      self.googleButton.isEnabled = false
      self.performSegue(withIdentifier: self.ACCOUNT_SEGUE, sender: self)
    })
   
   counter = 0

  }

  
  
  @IBAction func forgotButton(_ sender: UIButton) {
    let titleStr = "Reset Password"
    let messageStr = "Enter email associated to your account."
    var actionIsTrue : Bool = false
    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
    
    let placeholderStr =  "Email of your account"
    
    alert.addTextField(configurationHandler: {(textField: UITextField) in
      textField.placeholder = placeholderStr
      let previousEmail = self.emailTF.text!

      if previousEmail == "" {
        alert.textFields!.first!.placeholder = placeholderStr
      } else {
        alert.textFields!.first!.text = previousEmail
        actionIsTrue = self.validate(YourEMailAddress: previousEmail)
      }
      textField.addTarget(self, action: #selector(self.textChanged(_:)), for: .editingChanged)
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (_) -> Void in
      
    })
    
    let action = UIAlertAction(title: "Send Email", style: UIAlertActionStyle.default, handler: { (_) -> Void in
      let textfield = alert.textFields!.first!
      self.sendResetPassord(email: textfield.text!)
    
    })
    alert.addAction(cancel)
    alert.addAction(action)
    
    self.actionToEnable = action
    
    if actionIsTrue {
      action.isEnabled = true
    } else {
      action.isEnabled = false
    }
    self.present(alert, animated: true, completion: nil)
  }
  
  
  
  @objc func textChanged(_ sender:UITextField) {
    self.actionToEnable?.isEnabled  = validate(YourEMailAddress: sender.text!)
  }
  
  
  
  func validate(YourEMailAddress: String) -> Bool {
    defaults.set(YourEMailAddress, forKey: "email")
    //print(email)
    let REGEX: String
    REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
  }
  
  
  
  func sendResetPassord(email: String){
    Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
      if error != nil {
        let alert = Helpers.Instance.alertUser(title: "There was a problem!", message: "Check if your email is correct, or an Internet problem.")
        self.present(alert, animated: true, completion: nil)
      }
      self.showSentEmailMessage()
    })
  }
  

  
  func showSentEmailMessage(){
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .zoomIn
    loadingNotification.bezelView.color = UIColor.black
    loadingNotification.customView =  UIImageView(image: UIImage(named: "correct8"))
    loadingNotification.label.textColor = UIColor.white
    loadingNotification.label.text = "Sent. Check your inbox."
    
    DispatchQueue.main.async(execute: { () -> Void in
     loadingNotification.hide(animated: true, afterDelay: 1.2)
    })
  }
  
  
  func handleData(){
    let restored = defaults.bool(forKey: "restored")
    
    if !restored {
    
      defaults.set(true, forKey: "restored")
      DBProvider.Instance.saveAlbums()
      DBProvider.Instance.saveImagesData()
      //DBProvider.Instance.saveSetting()
      DBProvider.Instance.saveEmails()
      DBProvider.Instance.savePredTexts()
      DBProvider.Instance.saveNotes()
    
    } else {
      
      Helpers.Instance.deleteAllData(entity: Const.ALBUM_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.NOTE_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.PREDEFINED_TEXT_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.EMAIL_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.IMAGE_ENTITY)
      
      DBProvider.Instance.restoreAlbums()
      //DBProvider.Instance.restoreSetting()
      DBProvider.Instance.restoreImagesData()
      DBProvider.Instance.restoreNotes()
      DBProvider.Instance.restoreEmails()
      DBProvider.Instance.restorePredefinedTexts()
      DBProvider.Instance.restoreDefinedFinds()
      }
    
  }
  


  @IBAction func goBackToView(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
  }


  func checkIfUserIsVerified() {
    if let user = Auth.auth().currentUser {
      if user.isEmailVerified {
          print("Email is verified");
        }
        else {
          print("Email is not verified");
        }
      }
    }
  
  
}






