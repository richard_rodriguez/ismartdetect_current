//
//  Album.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/27/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct Album {

  let name : String
  let key : String
  let owner : String?
  var createdAt : Double
  var updatedAt : Double
  

  init(name: String, key: String, owner: String?, createdAt: Double, updatedAt: Double) {
    self.name = name
    self.key = key
    self.owner = owner
    self.createdAt = createdAt
    self.updatedAt = updatedAt
  }

  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
    self.name = result![Const.NAME]! as! String
    self.key = result![Const.KEY]! as! String
    self.createdAt = result![Const.CREATED_AT]! as! Double
    self.updatedAt = result![Const.UPDATED_AT]! as! Double
    if let owner = result![Const.OWNER] {
      self.owner = owner as? String
    } else {
      self.owner = "" 
    }
    
  }

  
  func toDictionary() -> Dictionary<String, Any> {
    return [Const.NAME : name, Const.KEY : key, Const.OWNER : owner ?? "", Const.CREATED_AT : createdAt, Const.UPDATED_AT : updatedAt]

  }
  
}


