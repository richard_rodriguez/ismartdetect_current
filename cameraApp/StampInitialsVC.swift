//  StampInitialsVC.swift
//  Created by Richard Rodriguez on 12/19/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import KeychainSwift

class StampInitialsVC: UIViewController, UITextFieldDelegate {

  
  @IBOutlet var fullName: UITextField!
  @IBOutlet weak var nameInitials: UITextField!
  let keychain = KeychainSwift()
  var letters = [String]()

  
  
  override func viewDidLoad() {
        super.viewDidLoad()
    IQKeyboardManager.sharedManager().enable = false
    IQKeyboardManager.sharedManager().enableAutoToolbar = true
    
    self.navigationItem.title = "Stamp Initials"
    nameInitials.text = keychain.get(Const.INITIALS) ?? ""
    fullName.text = keychain.get(Const.INITIALS_NAME) ?? ""
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)

    let name = self.fullName.text!
    let initials = self.nameInitials?.text!
    
    keychain.set(initials!, forKey: Const.INITIALS)
    keychain.set(name, forKey: Const.INITIALS_NAME)
   
    }
  }
  



