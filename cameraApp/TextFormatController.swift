//  TextFormatController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 8/27/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation


class TextFormatController: UITableViewController {
  
  @IBOutlet weak var shadowSwitch: UISwitch!
  
  let defaults = UserDefaults.standard
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    shadowSwitch.setOn(defaults.bool(forKey: "textShadow"), animated: true)
  }
  

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
  }
  

  @IBAction func switchPressed(_ sender: AnyObject) {
    if shadowSwitch.isOn {
      defaults.set(true, forKey: "textShadow")
    } else {
      defaults.set(false, forKey: "textShadow")
    }
  
    //DBProvider.Instance.saveSetting()
  }
  
}
