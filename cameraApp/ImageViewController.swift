//  ImageViewController.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 7/18/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import Photos
import MessageUI
import MBProgressHUD
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import CoreData
import AlamofireImage
import KeychainSwift




@objc class ImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  
  
  @IBOutlet weak var imageTaken: UIImageView!
  @IBOutlet weak var doneButton: UIButton!
  @IBOutlet weak var cameraButton: UIButton!
  @IBOutlet weak var otherLabel: UILabel!
  @IBOutlet weak var doneLabel: UILabel!
  @IBOutlet weak var ownTextField: UITextField!
  @IBOutlet weak var shareByEmail: UIButton!
  @IBOutlet weak var selectPicker: UIPickerView!
  @IBOutlet weak var definedFindPicker: UIPickerView!
  @IBOutlet weak var addPredTextButton: UIButton!
  @IBOutlet weak var segmentControl: UISegmentedControl!
  @IBOutlet weak var addTextSelector: UIButton!
  @IBOutlet weak var definedFindSelector: UIButton!
  @IBOutlet weak var definedFind: UILabel!
  @IBOutlet weak var symbolImage: UIImageView!
  var symbolText : String!
  var symbolLetter : String!
  var showAlert = false
  
  let picker = UIImagePickerController()
  
  var pixelsByLetter : Double!
  var limitedChar : Int = 2
  var imagePassed: UIImage!
  var secondImage: UIImage!
  var finalImage: UIImage!
  var gpsInfo: [String : String] = [:]
  var passFullAddress: [String] = []
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = true
  var twoPhoto : Bool = false
  var isPredTextAdded : Bool = false
  var ownText : String = ""
  var imageWidthSize : Double!
  let keychain = KeychainSwift()
  
  var isDefinedFind : Bool = false
  
  let defaults = UserDefaults.standard
  var albumName : String!
  var predTexts = [String]()
  var definedFindsDict = [[String : String]]()
  var definedFinds = [String : String]()
  var selectedEmails = [String]()
  
  let CONST = 1242.0
  let TEXT_FORMAT = [0:(64.0/1242.0), 1:(92/1242.0), 2:(120.0/1242.0), 3:(148.0/1242.0)]
  let DATA_FORMAT = [0 : "DMS-WGS84", 1 : "DM-WGS84", 2: "DD-WGS84", 3: "UTM-WGS84", 4: "UTM-ETRS89", 5: "UTM-WGS84", 6: "UTM-UK-GREF"]
  let TRANS = [0:1.0, 1:0.8, 2:0.6, 3:0.4, 4:0.2]
  
  var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
  
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    firstSetup()
  }
  
  
  
  func firstSetup(){
    albumName = defaults.string(forKey: "albumName")
    
    definedFinds = [:]
    fetchDefinedFind()
    
    showDistanceAlert()
    fetchSelectedPredefinedTexts()
    predTexts.append(albumName)
    predTexts.append("No Predefined Text.")
    
    if let predOwnTex = defaults.string(forKey: albumName! + "predText") {
      ownText = predOwnTex
      if !predTexts.contains(predOwnTex) {
        defaults.set("", forKey: albumName! + "predText")
        ownText = ""
      }
    } else {
      ownText = ""
    }
    
    selectPicker.isHidden = true
    definedFindPicker.isHidden = true
    
    self.ownTextField.delegate = self;
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ImageViewController.dismissKeyboard)))
    
    imageWidthSize = Double(imagePassed.size.width)
    
    ownTextField.isHidden = true
    addPredTextButton.isHidden = true
    finalImage = imagePassed!
    
    if defaults.bool(forKey: "emailOptionActive") {
      shareByEmail.isHidden = false
    } else {
      shareByEmail.isHidden = true
    }
    
    imageTaken.image = textToImage(inImage: imagePassed!, drawText: gpsInfo as [String:String])
    
    symbolText = definedFinds[returnLetter(0)]
    symbolLetter = returnLetter(0)
    
    definedFind.isUserInteractionEnabled = true
    
    let aSelector : Selector = #selector(ImageViewController.didDefinedFindPressed(_:))
    let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
    tapGesture.numberOfTapsRequired = 1
    
    definedFind.addGestureRecognizer(tapGesture)
  }
  
  
  
  
  @IBAction func addPredText(_ sender: UIButton) {
    addPredTexts()
  }
  
  func addPredTexts(){
    isPredTextAdded = true
    doneButton.isHidden = true
    doneLabel.isHidden = true
    otherLabel.isHidden = true
    cameraButton.isHidden = true
    ownTextField.resignFirstResponder()
    
    
    
    if !isDefinedFind {
      selectPicker.isHidden = false
      var indexPickerSelected = 0
      //let predTexts = defaults.array(forKey: "selectedPredTexts") as! [String]
      
      print(predTexts)
      if var predOwnTex = defaults.string(forKey: albumName! + "predText") {
        if predOwnTex == "" {
          predOwnTex = "No Predefined Text."
          indexPickerSelected = (predTexts.index(of: predOwnTex))!
        } else {
          ownText = predOwnTex
          ownTextField.text = predOwnTex
          indexPickerSelected = (predTexts.index(of: predOwnTex))!
        }
        
      } else {
        ownText = ""
        indexPickerSelected = 0
      }
      
      selectPicker.isHidden = false
      selectPicker.selectRow(indexPickerSelected, inComponent: 0, animated: false)
    } else {
      selectPicker.isHidden = true
      definedFindPicker.isHidden = false
    }
  }
  
  
  
  
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  
  @IBAction func doneButtonAction(_ sender: UIButton) {
    
    
    let newImage: UIImage = textToImage(inImage: finalImage, drawText: gpsInfo as [String:String])
    
    var photoIndex = Int(keychain.get(albumName!) ?? "1")
    
    if photoIndex == 1 {
      let coordinate = gpsInfo["LatitudeOr"]! + "," + gpsInfo["LongitudeOr"]!
      keychain.set(coordinate, forKey: albumName! + "firstLocation")
      defaults.set([Double(gpsInfo["LatitudeOr"]!), Double(gpsInfo["LongitudeOr"]!)], forKey: albumName! + "firstLocation")
      
      let now = Date()
      defaults.set(now, forKey: albumName! + "firstLocation" + "date")
      
      let nowString = String(now.timeIntervalSince1970) //.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .medium)
      keychain.set(nowString, forKey: albumName! + "firstLocation" + "date")
    }
    
    
    photoIndex = photoIndex! + 1
    let newIndex = String(photoIndex!)
    keychain.set(newIndex, forKey: albumName!)
    
    if defaults.bool(forKey: "saveOriginal") {
      saveImageToCustomAlbum(image: finalImage)
    }
    saveImageToCustomAlbum(image: newImage)
    saveData(imageData: finalImage)
    
    
  }
  
  func alertAfterSave(){
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
    }
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .zoomIn
    loadingNotification.bezelView.color = UIColor.black
    loadingNotification.customView =  UIImageView(image: UIImage(named: "correct8"))
    loadingNotification.label.textColor = UIColor.white
    loadingNotification.label.text = "Photo saved."
    loadingNotification.hide(animated: true, afterDelay: 1.1)
    
    doneButton.isEnabled = true
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.2){
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  @IBAction func cameraButtonAction(_ sender: UIButton) {
    if (ownText == "" || ownText == "\n") {
      print("Empty")
    } else {
      print("//editButton.isHidden = false")
    }
    
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .camera
    twoPhoto = true
    otherLabel.text = "RETAKE"
    present(picker, animated: true, completion: nil)
  }
  
  
  @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    secondImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!; dismiss(animated: true, completion: nil)
    
    let secondImageNew = Helpers.Instance.resizeImage(image: UIImage(data: UIImageJPEGRepresentation(secondImage!, 0.5)!)!,  withSize: CGSize(width: imagePassed.size.width, height: imagePassed.size.height))
    
    
    let topImage = imagePassed!
    let bottomImage = secondImageNew
    
    //    let topImage = UIImage(data: UIImageJPEGRepresentation(imagePassed!, 0.6)!)!
    //    let bottomImage = UIImage(data: UIImageJPEGRepresentation(secondImage!, 0.6)!)!
    
    
    
    let size = CGSize(width: (topImage.size.width + topImage.size.width), height: (topImage.size.height) )
    
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    topImage.draw(in: CGRect(origin: CGPoint(x:0, y:0), size: CGSize(width: topImage.size.width, height: topImage.size.height) ))
    bottomImage.draw(in: CGRect(origin: CGPoint(x:topImage.size.width, y:0), size: CGSize(width: bottomImage.size.width, height:size.height)))
    
    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    
   
    imageTaken.image = textToImage(inImage: newImage, drawText: gpsInfo as [String:String])
    
    
    finalImage = newImage


    
    //finalImage = newImage
    //imageTaken.image = textToImage(inImage: newImage, drawText: gpsInfo as [String:String])
    
    
    
    
  }
  
  @objc func textToImage(inImage: UIImage, drawText: [String:String])-> UIImage {
    // Setup the font specific variables
    let temp = UIImage(data: UIImageJPEGRepresentation(inImage, 0.4)!)!
    
    imageWidthSize = Double(inImage.size.width)
    
    var fontSize = (TEXT_FORMAT[defaults.integer(forKey: "fontSize")]!) * imageWidthSize
    print(fontSize)
    
    
    if !twoPhoto {
      fontSize = fontSize * 0.5
    }
    
    let TRES = CGFloat( (3/CONST)*imageWidthSize )
    let DIEZ = CGFloat( (10/CONST)*imageWidthSize )
    let CINCO = CGFloat( (5/CONST) * imageWidthSize )
    let QUINCE = CGFloat( (15/CONST)*imageWidthSize )
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    let transLevel = TRANS[defaults.integer(forKey: "trans")]!
    let textColor: UIColor = UIColor.white.withAlphaComponent(CGFloat(transLevel))
    let textFont: UIFont = UIFont(name: "Helvetica Bold", size: CGFloat( fontSize ))!
    var shadowSize : Double!
    var shadowRadiusSize : Double!
    let shadowActive = defaults.bool(forKey: "textShadow")
    var dateStr = ""
    let albumStr = defaults.string(forKey: "albumName")!
    
    if shadowActive == true {
      shadowSize = fontSize / 9.6
      shadowRadiusSize = fontSize / 8
    } else {
      shadowSize = 0
      shadowRadiusSize = 0
    }
    
    
    let shadow : NSShadow = NSShadow()
    shadow.shadowOffset = CGSize(width: shadowSize, height: shadowSize)
    shadow.shadowColor = UIColor.black
    shadow.shadowBlurRadius = CGFloat(shadowRadiusSize)
    
    
    //Setup the image context using the passed image.
    let scale = UIScreen.main.scale
    UIGraphicsBeginImageContextWithOptions(temp.size, false, scale)
    
    
    //Setups up the font attributes that will be later used to dictate how the text should be drawn
    let textFontAttributes = [
      NSAttributedStringKey.font: textFont,
      NSAttributedStringKey.foregroundColor: textColor,
      NSAttributedStringKey.shadow : shadow,
      ]
    
    
    //Put the image into a rectangle as large as the original image.
    temp.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: temp.size.width, height: temp.size.height)) )
    
    
    //texts to add in the images
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 || datum == 6 {
      //inverted to put East up and Nor down
      latStr = "N: \(gpsInfo["Latitude"]!)"
      lonStr = "E: \(gpsInfo["Longitude"]!)"
      zoneStr = "\(gpsInfo["Zone"]!)"
    }else {
      latStr = "Lat: \(gpsInfo["Latitude"]!)"
      lonStr = "Lon: \(gpsInfo["Longitude"]!)"
      zoneStr = ""
    }
    
    
    let accuStr  = zoneStr + "   " + "+/-  \(gpsInfo["HAcc"]!)m"
    let datumStr = "\(DATA_FORMAT[defaults.integer(forKey: "dataFormat")]!)"
    
    var id = (keychain.get(albumName!) ?? "1")!
    
    if Int(id) == 0 {
      id  = "1"
      keychain.set("1", forKey: albumName!)
    }
    
    let isDateTimeOnly = defaults.bool(forKey: "dateTimeOnly")
    
    
    if isDateTimeOnly {
      dateStr = "\(gpsInfo["Timestamp"]!)"
    } else {
      if let initial = keychain.get(Const.INITIALS) {
        dateStr = "\(gpsInfo["Timestamp"]!)" + "  /  " + initial + " - No " + id
      } else {
        dateStr = "\(gpsInfo["Timestamp"]!)" + "  /  No " + id
      }
    }
    
    
    
    //size texts
    //let coorStrSize = (coorStr as NSString).size(attributes: textFontAttributes)
    //Por hacer El tamaño de las letras HEIGHT es el emismo para las todas las letras
    //Por hacer refactor cod
    let dateStrSize = (dateStr as NSString).size(withAttributes: textFontAttributes)
    let accuStrSize  = (accuStr as NSString).size(withAttributes: textFontAttributes)
    let datumStrSize = (datumStr as NSString).size(withAttributes: textFontAttributes)
    let ownTextSize = (ownText as NSString).size(withAttributes: textFontAttributes)
    let albumTextSize = (albumStr as NSString).size(withAttributes: textFontAttributes)
    let standardHeightSize = dateStrSize.height
    
    
    var yDatePosition = temp.size.height - (standardHeightSize*2) - TRES - CINCO
    
    if ownText == "" || ownText == "\n" {
      yDatePosition = temp.size.height - (standardHeightSize) - CINCO
    } else {
      let ownTextRect = CGRect(origin: CGPoint(x:(temp.size.width/2) - (ownTextSize.width/2) + CINCO, y:temp.size.height - (standardHeightSize) - CINCO), size: CGSize(width: temp.size.width - DIEZ, height: temp.size.height))
      ownText.draw(in: ownTextRect, withAttributes: textFontAttributes)
    }
    
    
    // Creating a point within the space that is as bit as the image.
    if isDateTimeOnly {
      let dateRect = CGRect(origin: CGPoint(x:(temp.size.width/2) - (dateStrSize.width/2), y:yDatePosition), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      dateStr.draw(in: dateRect, withAttributes: textFontAttributes)
      
      if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
        let albumRect = CGRect(origin: CGPoint(x:((temp.size.width/2) - (albumTextSize.width/2)), y: (yDatePosition - standardHeightSize )), size: CGSize(width: temp.size.width - QUINCE, height: temp.size.height))
        albumStr.draw(in: albumRect, withAttributes: textFontAttributes)
      }
      
    }else {
      let lonRect = CGRect(origin: CGPoint(x:CINCO, y:CINCO), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      let latRect = CGRect(origin: CGPoint(x:CINCO, y:CINCO + standardHeightSize + TRES), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      let datumRect = CGRect(origin: CGPoint(x:temp.size.width - datumStrSize.width - CINCO, y:CINCO), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      let accuRect = CGRect(origin: CGPoint(x:temp.size.width - accuStrSize.width - CINCO, y:CINCO + accuStrSize.height + TRES ), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      let dateRect = CGRect(origin: CGPoint(x:(temp.size.width/2) - (dateStrSize.width/2), y:yDatePosition), size: CGSize(width: temp.size.width - QUINCE , height: temp.size.height))
      
      if defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME) {
        let albumRect = CGRect(origin: CGPoint(x:((temp.size.width/2) - (albumTextSize.width/2)), y: (yDatePosition - standardHeightSize )), size: CGSize(width: temp.size.width - QUINCE, height: temp.size.height))
        albumStr.draw(in: albumRect, withAttributes: textFontAttributes)
      }
      
      //Now Draw the text into an image.
      latStr.draw(in: latRect, withAttributes: textFontAttributes)
      lonStr.draw(in: lonRect, withAttributes: textFontAttributes)
      dateStr.draw(in: dateRect, withAttributes: textFontAttributes)
      accuStr.draw(in: accuRect, withAttributes: textFontAttributes)
      datumStr.draw(in: datumRect, withAttributes: textFontAttributes)
      
    }
    
    
    // Create a new image out of the images we have created
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    
    
    // End the context now that we have the image we need
    UIGraphicsEndImageContext()
    //And pass it back up to the caller.
    //twoPhoto = false

    
    var compressedImage : UIImage!

    if (newImage.size.width != newImage.size.height) {
        compressedImage = Helpers.Instance.resizeImage(image: newImage, withSize: CGSize(width: 2000, height: 1000))
    } else {
      compressedImage = Helpers.Instance.resizeImage(image: newImage, withSize: CGSize(width: 1000, height: 1000))
    }
    
    
    return compressedImage
    
  }
  
  func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
    image.draw(in: CGRect(origin: CGPoint(x:0, y:0), size: CGSize(width: newSize.width, height: newSize.height) ))
    
    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return newImage
  }
  
  func saveImageToCustomAlbum(image: UIImage) {
    //Check if the folder exists, if not, create it
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
    
    let listAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
    let albumsSize: Int = listAlbums.count
    
    if albumsSize < 2 {
      UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
      return
    }
    
    if let first_Obj : AnyObject = collection.firstObject{
      //found the album
      self.albumFound = true
      self.assetCollection = first_Obj as! PHAssetCollection
      
      
      //save the image in a specific folder (album)
      PHPhotoLibrary.shared().performChanges({
        let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
        assetChangeRequest.location = CLLocation(latitude: Double(self.gpsInfo["LatitudeOr"]!)!, longitude: Double(self.gpsInfo["LongitudeOr"]!)!)
        let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
        let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
        
        let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
        albumChangeRequest!.addAssets(fastEnumeration)
        
      }, completionHandler: nil)
    }else{
      
      //Album placeholder for the asset collection, used to reference collection in completion handler
      var albumPlaceholder : PHObjectPlaceholder!
      //create the folder
      NSLog("\nFolder \"%@\" does not exist\nCreating now...", albumName)
      PHPhotoLibrary.shared().performChanges({
        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.albumName)
        albumPlaceholder = request.placeholderForCreatedAssetCollection
      },
                                             completionHandler: {(success:Bool, error:Error?)in
                                              if(success){
                                                print("Successfully created folder")
                                                self.defaults.set(0, forKey: "albumIndex")
                                                self.albumFound = true
                                                let collection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumPlaceholder.localIdentifier], options: nil)
                                                self.assetCollection = collection.firstObject! as PHAssetCollection
                                                
                                                //save the image in a specific folder (album)
                                                PHPhotoLibrary.shared().performChanges({
                                                  let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                                                  assetChangeRequest.location = CLLocation(latitude: Double(self.gpsInfo["LatitudeOr"]!)!, longitude: Double(self.gpsInfo["LongitudeOr"]!)!)
                                                  let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
                                                  
                                                  let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                                                  
                                                  let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
                                                  albumChangeRequest!.addAssets(fastEnumeration)
                                                }, completionHandler: nil)
                                                
                                              }else{
                                                print("Error creating folder")
                                                self.albumFound = false
                                              }
      })
    }
  }
  
  func dismissKeyboard() {
    ownTextField.resignFirstResponder()
    handleReturnKeyAndClickOutside()
    print(imageWidthSize)
  }
  
  // for hitting return
  private func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  
  
  
  @IBAction func addTextButton(_ sender: UIButton) {
    addTexts()
  }
  
  func addTexts() {
    //self.ownTextField.becomeFirstResponder()
    isPredTextAdded = false
    ownTextField.isHidden = false
    addPredTextButton.isHidden = false
    
    if defaults.bool(forKey: "activePredifinedTexts") {
      addPredTextButton.isEnabled = true
      addPredTextButton.tintColor = UIColor.white
    } else {
      addPredTextButton.isHidden = true
      
      ownTextField.translatesAutoresizingMaskIntoConstraints = false
      view.addConstraint(NSLayoutConstraint(item: ownTextField, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
      
      ownTextField.becomeFirstResponder()
    }
    
    addPredTextButton.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    ownTextField.tintColor = UIColor.white
    ownTextField.placeholder = "Insert text here"
    ownTextField.autocapitalizationType = .sentences
    ownTextField.backgroundColor = UIColor.black.withAlphaComponent(0.8)
  }
  
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    if textField == ownTextField {
      textField.resignFirstResponder()
      handleReturnKeyAndClickOutside()
      return false
    }
    return true
  }
  
  func handleReturnKeyAndClickOutside(){
    if !isPredTextAdded {
      ownText = ownTextField.text!
    }
    
    
    
    addPredTextButton.isHidden = true
    ownTextField.isHidden = true
    doneButton.isHidden = false
    doneLabel.isHidden = false
    otherLabel.isHidden = false
    cameraButton.isHidden = false
    selectPicker.isHidden = true
    definedFindPicker.isHidden = true
    imageTaken.image = textToImage(inImage: finalImage, drawText: gpsInfo as [String:String])
    
    if (ownText == "" || ownText == "\n") {
      print("Empty")
    } else {
      print("No Empty")
    }
  }
  
  
  @IBAction func editButtonAction(_ sender: UIButton) {
    ownTextField.isHidden = false
    
    ownTextField.textColor = UIColor.white
    ownTextField.text = ownText
    ownTextField.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    imageTaken.image = textToImage(inImage: finalImage, drawText: gpsInfo as [String:String])
    
  }
  
  
  @IBAction func textFieldChanged(_ textField: UITextField) {
    print(textField.text!)
    ownText = textField.text!
    var fontSize = (TEXT_FORMAT[defaults.integer(forKey: "fontSize")]!) * Double(finalImage.size.height)
    
    if !twoPhoto {
      fontSize = fontSize * 0.50
    }
    
    let transLevel = TRANS[defaults.integer(forKey: "trans")]!
    let textColor: UIColor = UIColor.white.withAlphaComponent(CGFloat(transLevel))
    let textFont: UIFont = UIFont(name: "Helvetica Bold", size: CGFloat( fontSize ))!
    var shadowSize : Double!
    var shadowRadiusSize : Double!
    let shadowActive = defaults.bool(forKey: "textShadow")
    
    
    if shadowActive == true {
      shadowSize = fontSize / 9.6
      shadowRadiusSize = fontSize / 8
    } else {
      shadowSize = 0
      shadowRadiusSize = 0
    }
    
    
    let shadow : NSShadow = NSShadow()
    shadow.shadowOffset = CGSize(width: shadowSize, height: shadowSize)
    shadow.shadowColor = UIColor.black
    shadow.shadowBlurRadius = CGFloat(shadowRadiusSize)
    
    
    //Setups up the font attributes that will be later used to dictate how the text should be drawn
    let textFontAttributes = [
      NSAttributedStringKey.font: textFont,
      NSAttributedStringKey.foregroundColor: textColor,
      NSAttributedStringKey.shadow : shadow,
      ]
    
    let fontLeng = Double(textField.text!.count)
    let fontStrSize = (textField.text! as NSString).size(withAttributes: textFontAttributes)
    
    if fontLeng != 0 {
      pixelsByLetter = Double(fontStrSize.width) / fontLeng
    } else {
      self.pixelsByLetter = 1
    }
    
    limitedChar = Int(Double(finalImage.size.width - 20) / pixelsByLetter)
    
  }
  
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    guard let text = textField.text else { return true }
    let newLength = text.count + string.count - range.length
    
    return newLength <= limitedChar - 1
  }
  
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    print("entró: textFieldDidBeginEditing ")
    textFieldChanged(textField)
  }
  
  
  
  @IBAction func shareButton(_ sender: UIButton) {
    if MFMailComposeViewController.canSendMail() {
      
      let composeVC = MFMailComposeViewController()
      composeVC.mailComposeDelegate = self
      
      let appName = defaults.string(forKey: "appName")!
      let content = gpsInfoStr(texts: gpsInfo as [String:String])
      
      let timestamp = "\(gpsInfo["Timestamp"]!)"
      let filename = matches(for: "[0-9]", in: timestamp).joined(separator: "")
      
      fetchSelectedEmails()
      
      if selectedEmails.count == 0 {
        selectedEmails = [""]
      }
      
      composeVC.setToRecipients(selectedEmails)
      composeVC.setSubject("\(albumName!) - by \(appName)")
      let neutral = UIImageJPEGRepresentation(finalImage, 0.5)
      let edited = UIImageJPEGRepresentation(textToImage(inImage: finalImage, drawText: gpsInfo as [String:String]), 0.1)
      composeVC.addAttachmentData(edited!, mimeType: "image/jpeg", fileName: filename + "0.jpg")
      composeVC.addAttachmentData(neutral!, mimeType: "image/jpeg", fileName: filename + "1.jpg")
      
      
      
      composeVC.setMessageBody(content, isHTML: true)
      
      present(composeVC, animated: true)
    } else {
      let alert = Helpers.Instance.alertUser(title: "ERROR!", message: "Please make sure you have the Mail App installed on your phone.")
      present(alert, animated: true, completion: nil)
    }
    
  }
  
  
  
  func gpsInfoStr(texts: [String:String]) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    let accuStr  = "<strong>Accuracy:</strong> ± \(gpsInfo["HAcc"]!)m"
    let datumStr = "<strong>CoordSys: </strong>\(DATA_FORMAT[defaults.integer(forKey: "dataFormat")]!)"
    let category = "<strong>Category: </strong>\(self.symbolText!)"
    
    let dateStr = "\(gpsInfo["Timestamp"]!)"
    
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 {
      //inverted to put East up and Nor down
      lonStr = "<strong>Northing:</strong> \(gpsInfo["Latitude"]!)"
      latStr = "<strong>Easting:</strong> \(gpsInfo["Longitude"]!)"
      zoneStr = "<strong>\(datum == 6 ? "Grid Ref" : "Zone:" )</strong> \(gpsInfo["Zone"]!)</strong>"
      
      result =  "<strong>" + dateStr + "</strong></br></br>" +
        "<strong>Project Name:</strong> \(albumName!) </br>" +
        accuStr + "</br>" +
        latStr + "</br>" +
        lonStr + "</br>" +
        zoneStr + "</br>" +
        category + "</br>" +
        datumStr + "</br>" +
        (ownText.isEmpty ? "" : "<strong>Text added: </strong> \(ownText)") +
      "</br></br><h6><i>Created by iSmartDetect</br>Find us on App Store.</i></h6>"
      
    } else {
      latStr = "<strong>Latitude:</strong> \(gpsInfo["Latitude"]!)"
      lonStr = "<strong>Longitude:</strong> \(gpsInfo["Longitude"]!)"
      zoneStr = ""
      
      
      result =  "<strong>" + dateStr + "</strong></br></br>" +
        "<strong>Project Name:</strong> \(albumName!) </br>" +
        accuStr + "</br>" +
        latStr + "</br>" +
        lonStr + "</br>" +
        category + "</br>" +
        datumStr + "</br>" +
        (ownText.isEmpty ? "" : "<strong>Text added: </strong> \(ownText)") +
      "</br></br><h6><i>Created by iSmartDetect</br>Find us on App Store.</i></h6>"
      
    }
    
    return result
  }
  
  
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    switch result {
    case .cancelled:
      print("Mail cancelled")
    case .saved:
      print("Mail saved")
    case .sent:
      print("Mail sent")
    case .failed:
      print("Mail sent failure: \(error!.localizedDescription)")
    }
    controller.dismiss(animated: true, completion: nil)
    
  }
  
  func matches(for regex: String, in text: String) -> [String] {
    
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let nsString = text as NSString
      let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
      return results.map { nsString.substring(with: $0.range)}
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return []
    }
  }
  
  
  
  func saveData(imageData: UIImage){
    doneButton.isEnabled = false
    DispatchQueue.main.async {
      self.activityIndicator.center = self.view.center
      self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
      self.activityIndicator.startAnimating()
      self.view.addSubview(self.activityIndicator)
    }
    
    
    //resizeImage(image: imageData, withSize: CGSize(witdh: , height: ))
    //let compressedImage = Helpers.Instance.resizeImage(image: imageData, withSize: CGSize(width: imageData.size.width*0.3333333, height: imageData.size.height*0.3333333))
    let compressedImage = Helpers.Instance.resizeImage(image: imageData, withSize: CGSize(width: imageData.size.width*0.5, height: imageData.size.height*0.5))

    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let newImageData = NSEntityDescription.insertNewObject(forEntityName: "ImageData", into: manageContext)
    
    
    let mydateFormatter = DateFormatter()
    mydateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
    let id = Int(keychain.get(albumName!) ?? "1")! - 1
    let coorSys = DATA_FORMAT[defaults.integer(forKey: "dataFormat")]!
    let initials = keychain.get(Const.INITIALS)
    
    let key = Database.database().reference().child(Const.IMAGES_DATA).childByAutoId().key
    
    var editedImage : UIImage!
    var compressionValue : CGFloat!
    
    if imageData.size.width != imageData.size.height {
      editedImage = imageData.compressTo(1)
      compressionValue = 0.8
    } else {
      editedImage = imageData
      compressionValue = 0.8
    }
    
    
    if self.albumName == Const.APP_NAME {
      if !Helpers.Instance.checkFirstAlbumExist(albumName: Const.APP_NAME) {
        Helpers.Instance.createFirstAlbum()
      }
    }
    
    
    let date = Date().timeIntervalSince1970
    
    
    let image = Image(id: id, latitude: self.gpsInfo["Latitude"]!, longitude: self.gpsInfo["Longitude"]!, zone: self.gpsInfo["Zone"]!, accuracy: (self.gpsInfo["HAcc"]!), timestamp: self.gpsInfo["Timestamp"]!, albumName: self.albumName, coordinateSystem: coorSys, ownerText: self.ownText, key: key, desc : self.gpsInfo["Description"]!, url: "", initials: initials, symbolLetter : symbolLetter, symbolText : symbolText, createdAt: date, updatedAt : date, imgData: UIImageJPEGRepresentation(compressedImage, 0.7)! as NSData, longDesc : "")
    
    newImageData.setValue(image.id, forKey: "id")
    newImageData.setValue(image.latitude, forKey: "latitude")
    newImageData.setValue(image.longitude, forKey: "longitude")
    newImageData.setValue(image.zone, forKey: "gZone")
    newImageData.setValue(image.accuracy, forKey: "acurracy")
    newImageData.setValue(image.timestamp, forKey: "timestamp")
    newImageData.setValue(image.albumName, forKey: "albumName")
    newImageData.setValue(image.coordinateSystem, forKey: "coordinateSystem")
    newImageData.setValue(image.ownerText, forKey: "note")
    newImageData.setValue(image.key, forKey: "key")
    newImageData.setValue(image.desc, forKey: "desc")
    newImageData.setValue(image.url, forKey: "url")
    newImageData.setValue(image.initials, forKey: "initials")
    newImageData.setValue(image.symbolLetter, forKey: "symbolLetter")
    newImageData.setValue(image.symbolText, forKey: "symbolText")
    newImageData.setValue(image.imgData, forKey: "imgData")
    
    do{
      try manageContext.save()
      DBProvider.Instance.addImageDataToChild(image : image)
      self.alertAfterSave()
      print("data saved.")
    }catch {
      print("error")
    }
    //}
  }
  
  
  @IBAction func goBackHomeScreen(_ sender: Any) {
    let alert = UIAlertController(title: "Delete Photo?", message: "If you go back you will lose your progress. Are you sure you want to continue?", preferredStyle: .alert)
    
    let ok = UIAlertAction(title: "Ok", style: .default) { (alert) in
      self.dismiss(animated: true, completion: nil)
    }
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    alert.addAction(ok)
    alert.addAction(cancel)
    
    present(alert, animated: false, completion: nil)
  }
  
  
  func showDistanceAlert(){
    
    let ID = Int((keychain.get(albumName!) ?? "1")!)
    
    print(ID!)
    if ID! > 1 {
      var firstDate : Date?
      var firstLocationSaved = [String]()
      
      if let firstLocation = keychain.get(albumName! + "firstLocation") {
        firstLocationSaved = firstLocation.components(separatedBy: ",")
      } else {
        firstLocationSaved = [gpsInfo["LatitudeOr"]!, gpsInfo["LongitudeOr"]!]
        
        let coordinate = gpsInfo["LatitudeOr"]! + "," + gpsInfo["LongitudeOr"]!
        keychain.set(coordinate, forKey: albumName! + "firstLocation")
      }
      
      if let firstDateString = keychain.get( (albumName! + "firstLocation" + "date") ) {
        
        if let interval = Double(firstDateString) {
          firstDate = Date(timeIntervalSince1970: interval)
        } else {
          firstDate = Date()
        }
        
      } else {
        firstDate = Date()
      }
      
      
      
      let firstLocation = CLLocation(latitude: Double(firstLocationSaved[0])!, longitude: Double(firstLocationSaved[1])! )
      let secondLocation = CLLocation(latitude: Double(gpsInfo["LatitudeOr"]!)!, longitude: Double(gpsInfo["LongitudeOr"]!)!)
      let distance = secondLocation.distance(from: firstLocation)
      
      let daysPassed = Date().hours(from: firstDate!)
      let showAlert = keychain.getBool((albumName! + "dontShowMore")) ?? false
      
      print("dayPassed")
      print(daysPassed)
      //show alert
      if !distance.isLess(than: 500)  {
        if daysPassed > 7 || !showAlert {
          DispatchQueue.main.async(execute: { () -> Void in
            print("entró")
            let error = UIAlertController(title: "WARNING!", message: "These GPS coordinates do not look like to be perfect for this Project. You are \(Int(distance)) meters away from the first position.", preferredStyle: UIAlertControllerStyle.alert)
            error.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.cancel, handler: nil))
            error.addAction(UIAlertAction(title: "Dismiss for 8 hours", style: UIAlertActionStyle.default, handler: { (result) in
              
              self.keychain.set(true, forKey: self.albumName! + "dontShowMore")
              let dateString = Date().timeIntervalSince1970.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .medium)
              self.keychain.set(dateString, forKey: self.albumName! + "firstLocation" + "date")
              
              self.defaults.set(true, forKey: self.albumName! + "dontShowMore")
              self.defaults.set(Date(), forKey: self.albumName! + "firstLocation" + "date")
            }))
            
            self.present(error, animated: true, completion: nil)
          })
        } else {
          print("no more than 500 or dismissed")
        }
      }
    }
  }
  
  
  func fetchSelectedEmails(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.EMAIL_ENTITY)
    
    request.predicate = NSPredicate(format: "selected = YES")
    
    let emails = try? manageContext.fetch(request)
    for email in emails as! [EmailData]{
      if email.selected == true {
        let new = email.email!
        self.selectedEmails.append(new)
      }
    }
    
  }
  
  func fetchSelectedPredefinedTexts(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PredefinedTextData")
    
    request.predicate = NSPredicate(format: "selected = YES")
    
    let predefinedTexts = try? manageContext.fetch(request)
    for predefinedText in predefinedTexts as! [PredefinedTextData]{
      let new = predefinedText.text!
      self.predTexts.append(new)
    }
  }
  
  
  
  func fetchDefinedFind() {
    definedFind.text = defaults.string(forKey: "x")!
    symbolImage.image = UIImage(named: "x")
    symbolText = defaults.string(forKey: "x")!
    symbolLetter = "x"
    
    self.definedFinds = ["x" : defaults.string(forKey: "x")!,
                         "a" : defaults.string(forKey: "a")!,
                         "b" : defaults.string(forKey: "b")!,
                         "c" : defaults.string(forKey: "c")!,
                         "d" : defaults.string(forKey: "d")!,
                         "e" : defaults.string(forKey: "e")!,
                         "f" : defaults.string(forKey: "f")!,
                         "o" : defaults.string(forKey: "o")!,
                         "v" : defaults.string(forKey: "v")!]
    
  }
  
  
  //  @IBAction func editCoordinates(_ sender: UIButton) {
  //    imageTaken.image = finalImage
  //  }
  
  
  
  
  
}




