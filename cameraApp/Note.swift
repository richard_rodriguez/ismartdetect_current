//
//  Note.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/9/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth


struct Note {
  var text : String
  let key : String
  
  init(text: String, key: String){
    self.text = text
    self.key = key
  }
  
  init(snapshot: DataSnapshot){
    let result = snapshot.value! as? [String: AnyObject]
    self.text = result!["text"] as! String
    self.key = result!["key"] as! String
  }
  
  func toDictionary() -> Dictionary<String, Any> {
    return [Const.NOTE : text, Const.KEY: key]
  }

  
}
