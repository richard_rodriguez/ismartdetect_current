//  Created by Richard Rodriguez on 11/29/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn
import MBProgressHUD


class SignUpVC: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

  
  @IBOutlet weak var fullNameTF: UITextField!
  @IBOutlet weak var emailTF: UITextField!
  @IBOutlet weak var passwordTF: TFSignUp!
  @IBOutlet weak var loginView: FBSDKLoginButton!

  let SignIn = SignInVC()
  
  private let ACCOUNT_SG = "accounSG"
  private let SIGNIG_SG = "signinSG"
  
  var email : String!
  var password : String!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
      
      
      if (FBSDKAccessToken.current() != nil){
        performSegue(withIdentifier: self.ACCOUNT_SG, sender: self)
      } else {
        self.loginView.delegate = self
        loginView.readPermissions = ["public_profile", "email", "user_friends"]
      }

    }

  
  //MARK: - Sign In using Email
  @IBAction func signUp(_ sender: Any) {
    if emailTF.text! != "" && passwordTF.text! != "" {
      AuthProvider.Instance.signUp(name: fullNameTF.text!, email: emailTF.text!, password: passwordTF.text!, loginHandler: {(message) in
        self.email = self.emailTF.text!
        self.password = self.passwordTF.text!
        
        if message != nil {
          let alert = Helpers.Instance.alertUser(title: "Problem creating your user.", message: message!)
          
          if message == "This email is in use, if you already Signed Up, please go back and Sign In using your email, if not use another email." {
            let alert = UIAlertController(title: "NOTICE!", message: "This email is in use, if you already Signed Up, please Log In using the Log In button, if not use another email.", preferredStyle: .alert)
            
            
            let login = UIAlertAction(title: "Log In", style: .destructive, handler: { (action) in
              self.performSegue(withIdentifier: self.SIGNIG_SG, sender: self)
            })

            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(ok)
            alert.addAction(login)
            //let alert = Helpers.Instance.alertUser(title: "NOTICE!", message: "The email of this account has been used for Sign Up using another Sign Up method (Google or Email).")
            self.present(alert, animated: true, completion: nil)
          }
          
          self.present(alert, animated: true, completion: nil)
          print(message!)
          return
        }

        print("USER CREATED")
        Helpers.Instance.setDefaultsData(email: self.emailTF.text!, name: self.fullNameTF.text!, loginType: Const.EMAIL_LOGIN)
        defaults.set(self.emailTF.text!, forKey: Const.LAST_EMAIL_USED)
        
          if message == nil {
            if AuthProvider.Instance.logout() {
              self.showSentEmailMessage()
              DispatchQueue.main.asyncAfter(deadline: .now() + 1.5){
                self.performSegue(withIdentifier: self.SIGNIG_SG, sender: self)
              }
          }
        }

      })

      
      
    } else {
      let alert = Helpers.Instance.alertUser(title: "Email and Password required.", message: "Please enter a Email and Password to sign in.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  
  
  func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    print("Dig log out of Facebook")
    dismiss(animated: true, completion: nil)
  }
  
  
  //MARK: - Facebook Login
  func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
    loginButton.delegate = self
    loginButton.readPermissions = ["public_profile", "email", "user_friends","user_birthday"]
    
    if error != nil {
      print(error)
      return
    }
    
    if result.isCancelled {
      return
    }
    
    defaults.set(nil, forKey: Const.LAST_EMAIL_USED)
    
    
    let accessToken = FBSDKAccessToken.current()
    let accessTokenString = accessToken?.tokenString!
    
    let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(250).height(250)"], tokenString: accessTokenString, version: "v2.4", httpMethod: "GET")
   
     _ = request?.start(completionHandler: { (connection , result, error) -> Void in
      

      if error != nil {
        print("Failed to st art graph request ", error!)
      }
      
      let values = result as! NSDictionary
      print(values)
      let name =  (values.value(forKey: "name") as! String)
      let email = (values.value(forKey: "email") as! String)
      let picture = (values.value(forKey: "picture") as! NSDictionary)
      let data = picture.value(forKey: "data") as! NSDictionary
      let profileURL = data.value(forKey: "url") as! String
      
      Helpers.Instance.setDefaultsData(email: email, name: name, loginType: Const.FACEBOOK, profileURL: profileURL)
      
      self.loginWithFB(result: result)
      self.performSegue(withIdentifier: self.ACCOUNT_SG, sender: self)
      
      
    })
  }
  
  func loginWithFB(result: Any?) {
    let accessToken = FBSDKAccessToken.current()
    let credentials = FacebookAuthProvider.credential(withAccessToken: (accessToken?.tokenString!)!)
    
    Auth.auth().signIn(with: credentials, completion: { (user, error) in
      if error != nil {
        print("somentthing happen: ", error!)
        return
      } else {
        
        print("Logged in usign Facebook.")
        
        let values = result as! NSDictionary
        let id = user!.uid
        let name =  (values.value(forKey: "name") as! String)
        let email =  (values.value(forKey: "email") as! String)
     
        
        DBProvider.Instance.saveUser(id: id, name: name, email: email)
        
        self.handleData()
      }
    })
  }

  
  
  //MARK: - Sign In using Google
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    if let err = error {
      print("error trying to log in Google: ", err.localizedDescription)
      return
    }
    
    defaults.set(nil, forKey: Const.LAST_EMAIL_USED)
    
    guard let idToken = user.authentication.idToken else { return}
    guard let accessToken = user.authentication.accessToken else { return}
    
    let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
    
    Auth.auth().signIn(with: credentials, completion: {(user, error) in
      if let err = error {
        print("Failed to create a Firebase User with Google account: ", err)
        return
      }
      guard let uid = user?.uid else { return }
      
      let profileURL =  String(describing: (user?.photoURL!)!) + "?sz=250"
     
      Helpers.Instance.setDefaultsData(email: (user?.email!)!, name: (user?.displayName)!, loginType: Const.GOOGLE, profileURL: profileURL )
      DBProvider.Instance.saveUser(id: uid, name: (user?.displayName)!, email: (user?.email)!)
      self.handleData()
      
      self.performSegue(withIdentifier: self.ACCOUNT_SG, sender: self)
    })
  }

  
  func handleData(){
    let restored = defaults.bool(forKey: "restored")
    
    if restored {
      Helpers.Instance.deleteAllData(entity: Const.ALBUM_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.NOTE_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.PREDEFINED_TEXT_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.EMAIL_ENTITY)
      Helpers.Instance.deleteAllData(entity: Const.IMAGE_ENTITY)
     
      DBProvider.Instance.restoreAlbums()
      //DBProvider.Instance.restoreSetting()
      DBProvider.Instance.restoreImagesData()
      DBProvider.Instance.restoreNotes()
      DBProvider.Instance.restoreEmails()
      DBProvider.Instance.restorePredefinedTexts()
    }
    
    if !restored {
      defaults.set(true, forKey: "restored")
      DBProvider.Instance.saveAlbums()
      DBProvider.Instance.saveImagesData()
      //DBProvider.Instance.saveSetting()
      DBProvider.Instance.saveEmails()
      DBProvider.Instance.savePredTexts()
      DBProvider.Instance.saveNotes()
    }
  }

  func showSentEmailMessage(){
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.customView
    loadingNotification.animationType = .zoomIn
    loadingNotification.bezelView.color = UIColor.black
    loadingNotification.label.textColor = UIColor.white
    loadingNotification.label.text = "Account Created \n Confirm your Account."
    loadingNotification.label.numberOfLines = 2
    
    DispatchQueue.main.async(execute: { () -> Void in
      loadingNotification.hide(animated: true, afterDelay: 1.8)
    })
  }
  
  
}

