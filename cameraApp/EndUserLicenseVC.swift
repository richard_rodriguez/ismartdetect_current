//
//  EndUserLicenseVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 4/27/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class EndUserLicenseVC: UIViewController {

  @IBOutlet weak var text: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let htmlString = "<div style=\"font-size: 18px;font-family: 'HelveticaNeue-Light'\">" +
      "<p><strong>End-User License Agreement</strong> </p>" +
      "<p>iVirtual Games Aps  </p>" +
      "<p>End-User License Agreement (\"Agreement\") Last updated: April 26, 2017 </p>" +
      "Please read this End-User License Agreement (\"Agreement\") carefully before clicking the \"I Agree\" button, downloading or using iSmartDetect (\"Application\"). By clicking the \"I Agree\" button, downloading or using the Application, you are agreeing to be bound by the terms and conditions of this Agreement. This Agreement is a legal agreement between you (either an individual or individual member of a single entity) and iVirtual Games Aps and it governs your use of the iSmartDetect Application made available to you by iVirtual Games Aps. If you do not agree to the terms of this Agreement, do not click on the \"I Agree\" button and do not download or use the Application. The Application is licensed, not sold, to you by iVirtual Games Aps for use strictly in accordance with the terms of this Agreement.</p>" +
    "<p><strong>License</strong></p>" +
    "<p><strong>Limited Commercial License</strong></p>" +
    "<p>Upon purchase of the Application, iVirtual Games Aps grants a single end user, revocable, non-transferable, limited license to download, install and use the Application for commercial purposes strictly in accordance with the terms of this Agreement.</strong></p>" +
    "<p><strong>Restrictions</strong></p>" +
    "<p>You agree not to, and you will not permit others to: License, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or otherwise exploit the Application or make the Application available to any unlicensed third party. Copy or use the Application for any purpose other than as permitted under the above applicable part of the section 'License'. Modify, make derivative works of, disassemble, decrypt, reverse compile or reverse engineer or attempt to reconstruct, identify, or discover any source code, underlying ideas, underlying user interface techniques, in any part of the Application.  Except to the extent the foregoing restriction is prohibited by applicable law. Remove, alter or obscure any proprietary notice (including any notice of copyright or trademark) of iVirtual Games Aps or its affiliates, partners, suppliers or the licensors of the Application. </p>" +
      "<p><strong>Intellectual Property </strong></p>" +
      "<p>The Application, including without limitation all copyrights, patents, trademarks, trade secrets and other intellectual property rights are, and shall remain, the sole and exclusive property of iVirtual Games Aps. </p>" +
      "<p><strong>Your Suggestions</strong></p>" +
      "<p>Any feedback, comments, ideas, improvements or suggestions (collectively, \"Suggestions\") provided by you to iVirtual Games Aps with respect to the Application shall remain the sole and exclusive property of iVirtual Games Aps. iVirtual Games Aps shall be free to use, copy, modify, publish, or redistribute the Suggestions for any purpose and in any way without any credit or any compensation to you. </p>" +
      "<p><strong>Modifications to Application</strong></p>" +
      "<p>iVirtual Games Aps reserves the right to modify, suspend or discontinue, temporarily or permanently, the Application or any service to which it connects, with or without notice and without liability to you. </p>" +
      "<p><strong>Updates to Application</strong></p>" +
      "<p>iVirtual Games Aps may from time to time provide enhancements or improvements to the features/functionality of the Application, which may include patches, bug fixes, updates, upgrades and other modifications (\"Updates\"). Updates may modify or delete certain features and/or functionalities of the Application. You agree that iVirtual Games Aps has no obligation to (i) provide any Updates, or (ii) continue to provide or enable any particular features and/or functionalities of the Application to you. You further agree that all Updates will be (i) deemed to constitute an integral part of the Application, and (ii) subject to the terms and conditions of this Agreement. </p>" +
      "<p><strong>Third-Party Services</strong></p>" +
      "<p>The Application may display, include or make available third-party content (including data, information, applications and other products services) or provide links to third-party websites or services (\"Third-Party Services\"). </p>" +
      "<p>You acknowledge and agree that iVirtual Games Aps shall not be responsible for any Third-Party Services, including their accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality or any other aspect thereof. iVirtual Games Aps does not assume and shall not have any liability or responsibility to you or any other person or entity for any Third-Party Services. </p>" +
      "<p>Third-Party Services and links thereto are provided solely as a convenience to you and you access and use them entirely at your own risk and subject to such third parties' terms and conditions. </p>" +
      "Disclaimer of Warranty. THE SOFTWARE AND SERVICES ARE PROVIDED “AS IS” AND “WITH ALL FAULTS” AND THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE AND SERVICES IS WITH YOU. SHOULD THE SOFTWARE OR SERVICES PROVE DEFECTIVE, IVIRTUAL GAMES APS DOES NOT HAVE ANY LIABILITY FOR THE SERVICING AND/OR REPAIR OF YOUR DEVICE OR THE SOFTWARE. IVIRTUAL GAMES APS AND ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES WITH RESPECT TO THE SOFTWARE AND SERVICES, EXPRESS OR IMPLIED, INCLUDING ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR USE OR PURPOSE. IVIRTUAL GAMES APS AND ITS SUPPLIERS DO NOT WARRANT THAT THE SOFTWARE OR SERVICES WILL MEET YOUR REQUIREMENTS IN ANY RESPECT, OR THAT THE OPERATION OF THE SOFTWARE OR SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE SOFTWARE OR SERVICES OR NONCONFORMITY TO ITS OR THEIR DOCUMENTATION CAN OR WILL BE CORRECTED. YOU ACKNOWLEDGE THAT THE PROVISIONS OF THIS SECTION ARE A MATERIAL INDUCEMENT AND CONSIDERATION TO IVIRTUAL GAMES APS AND ITS SUPPLIERS TO GRANT THE LICENSE CONTAINED IN THIS EULA AND TO PROVIDE YOU WITH ACCESS TO THE SERVICES.  </p>" +
      "Use of iSmartDetect is at the User's Own Risk. THE SOFTWARE AND SERVICES ARE NOT FAA CERTIFIED AND MUST NOT BE USED FOR PRIMARY NAVIGATION. THE SOFTWARE AND SERVICES MAY NOT BE USED FOR SAFETY OF LIFE APPLICATIONS, OR FOR ANY OTHER APPLICATION IN WHICH THE ACCURACY OR RELIABILITY OF THE SOFTWARE OR SERVICES COULD CREATE A SITUATION WHERE PERSONAL INJURY OR DEATH MAY OCCUR. DO NOT ATTEMPT TO CONFIGURE THE SOFTWARE OR INPUT INFORMATION WHILE DRIVING; FAILURE TO PAY FULL ATTENTION TO THE OPERATION OF YOUR VEHICLE COULD RESULT IN DEATH, SERIOUS INJURY, OR PROPERTY DAMAGE. BY USING THE SOFTWARE, YOU ASSUME ALL RESPONSIBILITY AND RISK. YOU ACKNOWLEDGE THAT THE ACTIVITIES WITH WHICH YOU USE THE SOFTWARE AND SERVICES (INCLUDING BUT NOT LIMITED TO DRIVING AND ATHLETIC ACTIVITIES) MAY HAVE CERTAIN INHERENT AND SIGNIFICANT RISKS OF PROPERTY DAMAGE, BODILY INJURY OR DEATH. BY USING ANY OF THE SOFTWARE OR SERVICES, YOU ASSUME ALL RESPONSIBILITY AND ALL KNOWN AND UNKNOWN RISKS OF SUCH USE AND OF THE ACTIVITIES WITH WHICH YOU USE THE SOFTWARE AND SERVICES, EVEN IF CAUSED IN WHOLE OR PART BY THE ACTION, INACTION, OR NEGLIGENCE OF IVIRTUAL GAMES APS OR ITS SUPPLIERS.</p>" +
    "<p style=\"color:red;\"><strong>Read the full Terms and Condition going to \"Settings / Terms and Conditions\" here on the app. </strong></p>" +
      "<p><strong>Privacy Policy</strong></p>" +
      "<p>iVirtual Games Aps collects, stores, maintains, and shares information about you in accordance with its Privacy Policy, which is available at http://ismartdetect.com/privacy-policy. By accepting this Agreement, you acknowledge that you hereby agree and consent to the terms and conditions of our Privacy Policy. </p>" +
      "<p><strong>Term and Termination</strong></p>" +
      "<p>This Agreement shall remain in effect until terminated by you or iVirtual Games Aps. </p>" +
      "<p>iVirtual Games Aps may, in its sole discretion, at any time and for any or no reason, suspend, modify or terminate this Agreement with or without prior notice. </p>" +
      "<p>This Agreement will terminate immediately, without prior notice from iVirtual Games Aps, in the event that you fail to comply with any provision of this Agreement. You may also terminate this Agreement by deleting the Application and all copies thereof from your mobile device or from your computer. </p>" +
      "<p>Upon termination of this Agreement, you shall cease all use of the Application and delete all copies of the Application from your mobile device or from your computer. </p>" +
      "<p>Termination of this Agreement will not limit any of iVirtual Games Aps's rights or remedies at law or in equity in case of breach by you (during the term of this Agreement) of any of your obligations under the present Agreement. </p>" +
      "<p><strong>Indemnification</strong></p>" +
      "<p>You agree to indemnify and hold iVirtual Games Aps and its parents, subsidiaries, affiliates, officers, employees, agents, partners and licensors (if any) harmless from any claim or demand, including reasonable attorneys' fees, due to or arising out of your: (a) use of the Application; (b) violation of this Agreement or any law or regulation; or (c) violation of any right of a third party. </p>" +
      "<p><strong>No Warranties</strong></p>" +
      "The Application is provided to you \"AS IS\" and \"AS AVAILABLE\" and with all faults and defects without warranty of any kind. To the maximum extent permitted under applicable law, iVirtual Games Aps , on its own behalf and on behalf of its affiliates and its respective licensors and service providers, expressly disclaims all warranties, whether express, implied, statutory or otherwise, with respect to the Application, including all implied warranties of merchantability, fitness for a particular purpose, title and non-infringement, and warranties that may arise out of course of dealing, course of performance, usage or trade practice. Without limitation to the foregoing, iVirtual Games Aps provides no warranty or undertaking, and makes no representation of any kind that the Application will meet your requirements, achieve any intended results, be compatible or work with any other software, applications, systems or services, operate without interruption, meet any performance or reliability standards or be error free or that any errors or defects can or will be corrected. </p>" +
      "Without limiting the foregoing, neither iVirtual Games Aps nor any iVirtual Games Aps 's provider makes any representation or warranty of any kind, express or implied: (i) as to the operation or availability of the Application, or the information, content, and materials or products included thereon; (ii) that the Application will be uninterrupted or error-free; (iii) as to the accuracy, reliability, or currency of any information or content provided through the Application; or (iv) that the Application, its servers, the content, or e-mails sent from or on behalf of iVirtual Games Aps are free of viruses, scripts, trojan horses, worms, malware, timebombs or other harmful components. </p>" +
      "Exclusions. Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. Accordingly, some of the above limitations and disclaimers may not apply to you. To the extent that iVirtual Games Aps may not, as a matter of applicable law, disclaim any implied warranty or limit its liabilities, the scope and duration of such warranty and the extent of the iVirtual Games Aps Parties’ liability shall be the minimum permitted under such applicable law. </p>" +
      "<p><strong>Limitation of Liability</strong></p>" +
      "<p>Notwithstanding any damages that you might incur, the entire liability of iVirtual Games Aps and any of its suppliers under any provision of this Agreement and your exclusive remedy for all of the foregoing shall be limited to the amount actually paid by you for the Application. </p>" +
      "To the maximum extent permitted by applicable law, in no event shall iVirtual Games Aps or its suppliers be liable for any special, incidental, indirect, or consequential damages whatsoever (including, but not limited to, damages for loss of profits, for loss of data or other information, for business interruption, for personal injury, for loss of privacy arising out of or in any way related to the use of or inability to use the Application, third-party software and/or third-party hardware used with the Application, or otherwise in connection with any provision of this Agreement), even if iVirtual Games Aps or any supplier has been advised of the possibility of such damages and even if the remedy fails its essential purpose. </p>" +
      "<strong>Exclusions</strong>. Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. Accordingly, some of the above limitations and disclaimers may not apply to you. To the extent that iVirtual Games Aps may not, as a matter of applicable law, disclaim any implied warranty or limit its liabilities, the scope and duration of such warranty and the extent of the iVirtual Games Aps Parties’ liability shall be the minimum permitted under such applicable law. </p>" +
      "<p><strong>Severability</strong></p>" +
      "<p>If any provision of this Agreement is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions will continue in full force and effect. </p>" +
      "<p><strong>Waiver</strong></p>" +
      "<p>Except as provided herein, the failure to exercise a right or to require performance of an obligation under this Agreement shall not effect a party's ability to exercise such right or require such performance at any time thereafter nor shall be the waiver of a breach constitute waiver of any subsequent breach. </p>" +
      "<p><strong>Amendments to this Agreement</strong></p>" +
      "<p>iVirtual Games Aps reserves the right, at its sole discretion, to modify or replace this Agreement at any time. If a revision is material iVirtual Games Aps will provide at least 15 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion. </p>" +
      "<p>By continuing to access or use our Application after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Application. </p>" +
      "<p><strong>Governing Law</strong></p>" +
      "<p>The laws of Denmark, excluding its conflicts of law rules, shall govern this Agreement and your use of the Application. Your use of the Application may also be subject to other local, state, national, or international laws. </p>" +
      "<p><strong>Contact Information</strong></p>" +
      "<p>If you have any questions about this Agreement, please contact us.</p>" +
      "<p><strong>Entire Agreement</strong></p>" +
      "<p>The Agreement constitutes the entire agreement between you and iVirtual Games Aps regarding your use of the Application and supersedes all prior and contemporaneous written or oral agreements between you and iVirtual Games Aps. </p>" +
      "<p>You may be subject to additional terms and conditions that apply when you use or purchase other iVirtual Games Aps's services, which iVirtual Games Aps will provide to you at the time of such use or purchase. </p>" +
    "<p>EULA of iSmartDetect https://termsfeed.com/eula/59ae9dd5cf8b3d024f2b23a9d51e787a </p></div>"
    
    
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
    let attributedString = try! NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    //let attributedString = try! NSAttributedString(data: htmlData!, options: [NSAttributedString.DocumentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
    
    text.attributedText = attributedString
    
  }

}
