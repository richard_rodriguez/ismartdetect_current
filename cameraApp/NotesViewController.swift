//  NotesViewController.swift
//  iSmartDetect
//  Created by Richard Rodriguez on 11/14/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation
import UIKit
import CoreData
import FirebaseDatabase
import FirebaseAuth

class NotesViewController : UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
  
  var notes = [Note]()
  var noteTexts = [String]()
  var filteredNotes = [String]()
  var noteToPass : Note!
  var indexToDelete : Int?
  var updateSecond : Timer!
  @IBOutlet var tableV: UITableView!
  
  var searchController : UISearchController!
  let resultController = UITableViewController()
  
  var handle: UInt!
  var ref : DatabaseReference!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    if let _ = Auth.auth().currentUser?.uid {
      let responde = ObserversProvider.Instance.observer(child: Const.NOTES, option: .note)
      handle = responde.handle
      ref = responde.ref
    }
    
    notes = []
    noteTexts = []
    fetchNotes()
    self.tableView.reloadData()
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    notes = []
    noteTexts = []
    fetchNotes()
    
    startTimer()

    self.resultController.tableView.delegate = self
    self.resultController.tableView.dataSource = self
    
    self.searchController = UISearchController(searchResultsController: self.resultController)
    self.tableView.tableHeaderView = self.searchController.searchBar
    self.searchController.searchResultsUpdater = self
    definesPresentationContext = true
    tableView.delaysContentTouches = false
  }
  

  func fetching(){
    notes = []
    noteTexts = []
    fetchNotes()
    self.tableView.reloadData()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    stopTimer()
    
    if let _ = Auth.auth().currentUser?.uid {
      ref.removeObserver(withHandle: handle!)
    }
    
  }

 
  
  func updateSearchResults(for searchController: UISearchController) {
    searchController.searchBar.delegate = self
    print(self.searchController.searchBar.text!)
    self.filteredNotes = self.noteTexts.filter({ (note: String) -> Bool in
      if note.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
        return true
      } else {
        return false
      }
    })
    print(filteredNotes)
    self.resultController.tableView.reloadData()
  }
  
  // MARK: - SearchBar Delegate Methods
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
    print("entró: searchBarCancelButtonClicked ")
    notes = []
    noteTexts = []
    fetchNotes()
    self.tableView.reloadData()
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
    notes = []
    noteTexts = []
    fetchNotes()
    self.tableView.reloadData()
  }

  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableView {
      return self.noteTexts.count
    } else {
      return self.filteredNotes.count
    }
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "Note")! as UITableViewCell
    
    //TODO: - refacoring
    if tableView == self.tableView {
      let text = noteTexts[indexPath.row]
      if text.count > 40 {
        let str = text
        let startIndex = str.index(str.startIndex, offsetBy: 40)
        cell.textLabel!.text = str.substring(to: startIndex) + "..."
        cell.detailTextLabel!.textColor = UIColor.lightGray
        cell.detailTextLabel!.text = str.substring(from: startIndex)
      } else {
        cell.textLabel!.text = text
        cell.detailTextLabel!.text = ""
      }

    } else {
      let text = filteredNotes[indexPath.row]
      if text.count > 40 {
        let str = text
        let startIndex = str.index(str.startIndex, offsetBy: 40)
        cell.textLabel!.text = str.substring(to: startIndex) + "..."
        cell.detailTextLabel!.textColor = UIColor.lightGray
        cell.detailTextLabel!.text = str.substring(from: startIndex)
      } else {
        cell.textLabel!.text = text
        cell.detailTextLabel!.text = ""
      }
    }
    
    cell.tintColor = Const.RED
    
    return cell
    
  }
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    noteToPass = notes[indexPath.row]
    performSegue(withIdentifier: "editNote", sender: self)
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "editNote" == segue.identifier {
      let EditVC : EditNoteViewController = segue.destination as! EditNoteViewController
      EditVC.notePassed = noteToPass
    }
  }
  
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
    let realIndex : Int?

    
    if tableView == self.tableView {
      indexToDelete = indexPath.row
      realIndex = indexPath.row
    } else {
      indexToDelete = noteTexts.index(of: filteredNotes[indexPath.row])
      realIndex = indexPath.row
    }
    
        if editingStyle == .delete {
          
          
            request.predicate = NSPredicate(format: "key = %@", notes[indexToDelete!].key)
          
          if let result = try? manageContext.fetch(request){
            let object = result[0]
              manageContext.delete(object as! NSManagedObject)
              
              do{
                try manageContext.save()
                
                
                if let uid = Auth.auth().currentUser?.uid {
                    let key = notes[indexToDelete!].key
                    let ref = Database.database().reference().child(uid).child(Const.NOTES).child(key)
                    ref.removeValue()
                } else {
                  print("Error trying to deleting Email.")
                }

                print("Note deleted.")

                let indexFinal = IndexPath(row: realIndex!, section: 0)
                
                if tableView == self.tableView {
                  noteTexts.remove(at: indexToDelete!)
                } else {
                  filteredNotes.remove(at: indexPath.row)
                }
                
                tableView.deleteRows(at: [indexFinal], with: UITableViewRowAnimation.automatic)
                
                notes = []
                noteTexts = []
                fetchNotes()
                self.tableView.reloadData()
                

              }catch {
                print("Error deleting Note on CoreData")
              }
          }
      }
     self.tableView.reloadData()
  }
  
  
  func fetchNotes(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.NOTE_ENTITY)
    
    do {
      let notes = try manageContext.fetch(request)
      print(notes.count)
      for note in notes as! [Notes]{
        print(note.text ?? "")
        let new = Note(text: note.text!, key: note.key!)
        self.noteTexts.append(new.text)
        self.notes.append(new)
      }
      
    } catch {
      print("Error fetching all Notes")
    }
  }

  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(NotesViewController.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }

}


