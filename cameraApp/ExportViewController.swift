//  ExportViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/3/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation
import CoreData
import FirebaseAuth
import FirebaseDatabase
import MessageUI
import FirebaseStorage
import MBProgressHUD
import Photos
import SDWebImage
import SimplePDF
import KeychainSwift
import DHSmartScreenshot


class ExportViewController : UITableViewController, UINavigationBarDelegate, UISearchResultsUpdating, UISearchBarDelegate, MFMailComposeViewControllerDelegate {
  
  var albumName : String!
  var albumNameList = [String]()
  var responde = ""
  var albumNameListFiltered = [String]()
  var selectedEmails = [String]()
  let keychain = KeychainSwift()
  var updateSecond : Timer!
  var selectButtonPressed : Bool = false
  var donwloadPressed : Bool = false
  let imagePlaceholderData = UIImageJPEGRepresentation(UIImage(named: "placeholderImage")!, 1)

  
  var fetcher : SDWebImagePrefetcher!
  
  @IBOutlet weak var thumbImage: UIImageView!
  @IBOutlet weak var cellTitle: UILabel!
  @IBOutlet weak var cellSubtitle: UILabel!
  
  let defaults = UserDefaults.standard
  var searchController : UISearchController!
  let resultController = UITableViewController()
  var indexToDelete : Int?
  var appName : String?
  //var imagesIDs = [String]()
  var images = [Image]()
  var image : Image?
  var activeSelection : Bool = true
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = true
  
  
  var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.navigationBar.tintColor = Const.RED

    
    self.images = []
    _ = fetchData()
    self.tableView.reloadData()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    _ = fetchData()
    print(albumName)
    
    startTimer()
    
    appName = defaults.string(forKey: "appName")!
    //navigationItem.title = albumName!
    
    let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
    label.backgroundColor = UIColor.clear
    label.numberOfLines = 2
    label.font = UIFont.boldSystemFont(ofSize: 16.0)
    label.textAlignment = .center
    label.textColor = UIColor.black
    label.text = albumName!
    self.navigationItem.titleView = label
    
    self.resultController.tableView.delegate = self
    self.resultController.tableView.dataSource = self
    navigationController?.setToolbarHidden(true, animated: false)
    
    setSelectButton()
    
  }
  
  
  func setSelectButton(){
    let select = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(selectImages))
    let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(goToAddImage))
    navigationItem.rightBarButtonItems = [select, add]
  }
  
  func goToAddImage(){
      performSegue(withIdentifier: "goToAddImage", sender: self)
  }
  
  
  
  func selectImages(){
    let select = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(selectImages))
    
    if selectButtonPressed == false {
      select.title = "Done"
      activeSelection = false
      navigationItem.rightBarButtonItem = select
      tableView.allowsSelectionDuringEditing = false
      tableView.allowsMultipleSelectionDuringEditing = true
      tableView.setEditing(true, animated: true)
      
      let selectAllButton = UIBarButtonItem(title: "Select All", style: .plain, target: self, action: #selector(selectAllCell) )
      navigationItem.leftBarButtonItem = selectAllButton
      
      selectButtonPressed = true
      stopTimer()
      setToolBarButton()
    } else {
      select.title = "Select"
      navigationItem.title = "\(self.albumName!)"
      activeSelection = true
      navigationItem.rightBarButtonItem = select
      navigationItem.leftBarButtonItem = nil
      tableView.allowsSelectionDuringEditing = false
      tableView.allowsMultipleSelectionDuringEditing = false
      tableView.setEditing(false, animated: true)
      selectButtonPressed = false
      
      navigationController?.setToolbarHidden(true, animated: true)
      startTimer()
    }
  }
  
  
  func selectAllCell(){
    for row in (0...images.count-1) {
      let index = IndexPath(item: row , section: 0)
      tableView.selectRow(at: index, animated: true, scrollPosition: .middle)
    }
    
    if tableView.indexPathsForSelectedRows?.count == images.count {
      let deselectAll = UIBarButtonItem(title: "Deselect All", style: .plain, target: self, action: #selector(deselectAllCell))
      navigationItem.title = "\(images.count) photos selected"
      navigationItem.leftBarButtonItem = deselectAll
    }
    
  }
  
  
  func deselectAllCell(){
    for row in (0...images.count-1) {
      let index = IndexPath(item: row , section: 0)
      tableView.deselectRow(at: index, animated: true)
    }
    let selectAllButton = UIBarButtonItem(title: "Select All", style: .plain, target: self, action: #selector(self.selectAllCell) )
    navigationItem.title = "\(self.albumName!)"
    navigationItem.leftBarButtonItem = selectAllButton
    
  }
  
  
  func setToolBarButton(){
    
    let downloadButtonImage = UIButton(frame: CGRect(x: 0, y: 0, width: 37, height: 27))
    let donwloadImage = UIImage(named: "download-red")
    downloadButtonImage.setImage(donwloadImage!, for: .normal)
    downloadButtonImage.addTarget(self, action: #selector(newDownloadImages), for: .touchUpInside)
    let downloadButton = UIBarButtonItem(customView: downloadButtonImage)
    
    let emailButtonImage = UIButton(frame: CGRect(x: 0, y: 0, width: 31, height: 25))
    let emailImage = UIImage(named: "email-blank")
    emailButtonImage.setImage(emailImage!, for: .normal)
    emailButtonImage.addTarget(self, action: #selector(beforeSendEmail), for: .touchUpInside)
    let mailButton = UIBarButtonItem(customView: emailButtonImage)
    
    let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteImages))
    deleteButton.tintColor = Const.RED
    
    let exportButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(export))
    exportButton.tintColor = Const.RED
    
    let PDFButton = UIBarButtonItem(title: "PDF", style: .plain, target: self, action: #selector(beforeGeneratePDF))
    PDFButton.tintColor = Const.RED
    
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
    //if let _ = Auth.auth().currentUser {
      toolbarItems = [exportButton, spacer, mailButton, spacer, downloadButton, spacer, deleteButton]
    //} else {
      //toolbarItems = [exportButton, spacer, deleteButton]
    //}
    
    navigationController?.setToolbarHidden(false, animated: true)
    
    
  }
  
  
  func export(){
    if let list = tableView.indexPathsForSelectedRows {
      var imgs = [Image]()
      
      for index in list{
        imgs.append(self.images[index.row])
      }
      exportFiles(images: imgs)
    }
  }
  
  
  func deleteImages(){
    if let list = tableView.indexPathsForSelectedRows  {
      let alert = UIAlertController(title: "Delete Photos", message: "Are you sure you want to delete \(list.count == 1 ? "this photo" : "these \(list.count) photos")? This also will delete your \(list.count == 1 ? "photo" : "photos") and all \(list.count == 1 ? "its" : "their") data.", preferredStyle: .alert)
      
      let delete = UIAlertAction(title: "Delete", style: .destructive) { (action) in
        for index in list {
          self.handleDeleteImage(image: self.images[index.row])
        }
        
        self.images = []
        _ = self.fetchData()
        self.tableView.reloadData()
        
        
        if self.images.count == 0 {
          self.dismiss(animated: true, completion: nil)
        }
      }
      
      let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
      
      alert.addAction(delete)
      alert.addAction(cancel)
      
      present(alert, animated: true, completion: nil)
    }
  }
  
  
  
  
    func newDownloadImages(){
      if let list = tableView.indexPathsForSelectedRows  {
        let images = list.map{self.images[$0.row]}
        let urls = images.map{URL(string: $0.url!) }.flatMap {$0}
        
        var counter = 0
        
        if images.count > 0 {
          let alert = UIAlertController(title: "", message: "(\(counter)/\(urls.count)) photos downloaded.", preferredStyle: .alert)
          present(alert, animated: true, completion: nil)
          
          for index in 0...(images.count - 1){
            //if urls[index].absoluteString != "" {
            if images[index].url != "" {
              SDWebImageDownloader.shared().downloadImage(with: URL(string: images[index].url!), options: .continueInBackground, progress: nil, completed: { (image, data, error, finished) in
                self.saveImageToCustomAlbum(image: (Helpers.Instance.textToImage(inImage: image!, image: images[index])), imageModel: images[index])
                alert.message = "(\(counter)/\(images.count)) photos downloaded."
                print("finished: ", finished)
              })
            } else {
              alert.message = "(\(counter)/\(images.count)) photos downloaded."
              saveImageToCustomAlbum(image: (Helpers.Instance.textToImage(inImage: UIImage(data: images[index].imgData! as Data)!, image: images[index])), imageModel: images[index])
            }
            
            counter += 1
          
            if counter == images.count {
              alert.dismiss(animated: true, completion: nil)
            }

          }
        }
      }
    
    
    
    
    
    
    //inside function
    func handleDownloads(for list : [IndexPath]){
      var urls = [URL]()
      
      for index in list {
        urls.append(URL(string: images[index.row].url!)!)
      }
    }
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    SDWebImageManager.shared().imageCache?.clearMemory()
    stopTimer()
  }
  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
  
  func fetching(){
    albumNameList = []
    //imagesIDs = []
    self.images = []
    _ = fetchData()
    self.tableView.reloadData()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableView {
      return self.images.count
    } else {
      return self.albumNameListFiltered.count
    }
  }
  
  
  func updateSearchResults(for searchController: UISearchController) {
    searchController.searchBar.delegate = self
    
    self.albumNameListFiltered = self.albumNameList.filter({ (album: String) -> Bool in
      if album.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
        return true
      } else {
        return false
      }
    })
    self.resultController.tableView.reloadData()
  }
  
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
    albumNameList = []
    //imagesIDs = []
    _ = fetchData()
    self.tableView.reloadData()
  }
  
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
    albumNameList = []
   // imagesIDs = []
    _ = fetchData()
    
    self.tableView.reloadData()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    print(self.images[indexPath.row].desc)
    
    if activeSelection {
        performSegue(withIdentifier: "editImage", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    } else {
      if let numberOfSelection = tableView.indexPathsForSelectedRows?.count {
        navigationItem.title = "\( numberOfSelection == 1 ? "\(numberOfSelection) photo selected" : "\(numberOfSelection) photos selected") "
      } else {
        navigationItem.title = "\(self.albumName!)"
      }
      
    }
  }
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if !activeSelection {
      if let numberOfSelection = tableView.indexPathsForSelectedRows?.count {
        navigationItem.title = "\( numberOfSelection == 1 ? "\(numberOfSelection) photo selected" : "\(numberOfSelection) photos selected") "
      } else {
        navigationItem.title = "\(self.albumName!)"
      }
    }
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as! ImageTableViewCell
    
    if tableView == self.tableView {
      
      let imageData = self.images[indexPath.row]
      var imageURL = imageData.url!
      
      if imageURL == "" {
        imageURL = Const.PLACEHOLDER_URL
      }
      
      DispatchQueue.global(qos: .userInteractive).async {
        if self.images[indexPath.row].url != "" {
          cell.thumbImage.sd_setShowActivityIndicatorView(true)
          cell.thumbImage.sd_setIndicatorStyle(.gray)
          cell.thumbImage.sd_setImage(with: URL(string: imageURL)!) { (image, error, SDImageCacheType, url) in
            if error != nil {
              return
            } else {
              
              DispatchQueue.main.async() { () -> Void in
                let coordinateSystem = imageData.coordinateSystem
                
                if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
                  cell.eastingLabel.text = "Longitude:"
                  cell.northingLabel.text = "Latitude:"
                  cell.zoneLabel.isHidden = true
                  cell.gZone.isHidden = true
                  
                } else {
                  cell.eastingLabel.text = "Easting:"
                  cell.northingLabel.text = "Northing:"
                  cell.zoneLabel.isHidden = false
                  if coordinateSystem == Const.COOR_UTM_UK_GREF {
                    cell.gZone.isHidden = false
                    cell.zoneLabel.text = "Pos:"
                  } else {
                    cell.gZone.isHidden = false
                  }
                  
                }
                
                cell.id.text = imageData.initialsStamp
                cell.date.text = imageData.timestamp
                cell.accuracy.text = imageData.accuracyValue
                cell.easting.text = imageData.longitude
                cell.northing.text = imageData.latitude
                cell.gZone.text = imageData.datumAndZone
                cell.category.text = imageData.symbolText
                cell.predText.text = imageData.ownerText
                cell.longDescription.text = imageData.longDesc
              }
            }
          }
        } else {
          if self.images[indexPath.row].imgData != nil {
            DispatchQueue.main.async() { () -> Void in
              let coordinateSystem = imageData.coordinateSystem
              cell.thumbImage.image = UIImage(data: self.images[indexPath.row].imgData! as Data)
              if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
                cell.eastingLabel.text = "Longitude:"
                cell.northingLabel.text = "Latitude:"
                cell.zoneLabel.isHidden = true
                cell.gZone.isHidden = true
                
              } else {
                cell.eastingLabel.text = "Easting:"
                cell.northingLabel.text = "Northing:"
                cell.zoneLabel.isHidden = false
                if coordinateSystem == Const.COOR_UTM_UK_GREF {
                  cell.gZone.isHidden = false
                  cell.zoneLabel.text = "Pos:"
                } else {
                  cell.gZone.isHidden = false
                }
                
              }
              
              cell.id.text = imageData.initialsStamp
              cell.date.text = imageData.timestamp
              cell.accuracy.text = imageData.accuracyValue
              cell.easting.text = imageData.longitude
              cell.northing.text = imageData.latitude
              cell.gZone.text = imageData.datumAndZone
              cell.category.text = imageData.symbolText
              cell.predText.text = imageData.ownerText
              cell.longDescription.text = imageData.longDesc
            }
          } else {
            DispatchQueue.main.async() { () -> Void in
              let coordinateSystem = imageData.coordinateSystem
              cell.thumbImage.image = UIImage(data: self.imagePlaceholderData!)
              if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
                cell.eastingLabel.text = "Longitude:"
                cell.northingLabel.text = "Latitude:"
                cell.zoneLabel.isHidden = true
                cell.gZone.isHidden = true
                
              } else {
                cell.eastingLabel.text = "Easting:"
                cell.northingLabel.text = "Northing:"
                cell.zoneLabel.isHidden = false
                if coordinateSystem == Const.COOR_UTM_UK_GREF {
                  cell.gZone.isHidden = false
                  cell.zoneLabel.text = "Pos:"
                } else {
                  cell.gZone.isHidden = false
                }
                
              }
              
              cell.id.text = imageData.initialsStamp
              cell.date.text = imageData.timestamp
              cell.accuracy.text = imageData.accuracyValue
              cell.easting.text = imageData.longitude
              cell.northing.text = imageData.latitude
              cell.gZone.text = imageData.datumAndZone
              cell.category.text = imageData.symbolText
              cell.predText.text = imageData.ownerText
              cell.longDescription.text = imageData.longDesc
            }

          }
        }
      
      }
      
    }
    
    cell.tintColor = Const.RED
    return cell
  }
  
  

  //function deleted
  
  
  func fetchData() -> String{
    responde = ""
    
    let datum = defaults.integer(forKey: "dataFormat")
    if datum == 3 || datum == 4 || datum == 5 {
      responde = responde + Const.EASTING_NORTHING_CVS_HEADER + "\n"
    } else {
      responde = responde + Const.LATITUDE_LONGITUDE_CVS_HEADER + "\n"
    }
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", albumName)
    request.predicate = predicate
    
    
    
    let sortDescriptor1 = NSSortDescriptor(key: Const.INITIALS, ascending: true)
     let sortDescriptor2 = NSSortDescriptor(key: Const.CREATED_AT, ascending: true)
    request.sortDescriptors = [sortDescriptor1, sortDescriptor2]
    
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      if results.count > 0 {
        for result in results as! [ImageData]{
          
          var imageDataTemp : NSData!
          
          if result.imgData != nil {
            imageDataTemp = result.imgData
          } else {
            imageDataTemp = imagePlaceholderData! as NSData
          }
          
          let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: imageDataTemp, longDesc : result.longDesc)
          print(result.desc!)
          print(image.id)
          
          images.append(image)
          responde = responde + Helpers.Instance.respondInCommasForm(for: result)
          
          //imagesIDs.append(result.key!)
          
          let datum = defaults.integer(forKey: "dataFormat")
          if datum == 3 || datum == 4 || datum == 5 {
            if (result.note!.isEmpty) {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOnwerTextUTM(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          } else {
            if result.note!.isEmpty {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOwnerTextLL(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          }
        }
      } else{
        stopTimer()
        print("print something...")
        dismiss(animated: true, completion: nil)
      }
      
    } catch {
      print("error")
    }
    

    
    images.sort { (img1, img2) -> Bool in
      return Date.init(timeIntervalSince1970: img1.createdAt) < Date.init(timeIntervalSince1970: img2.createdAt)
    }
    
    return responde
    
  }
  
  
  func exportFiles(images : [Image]) {
    let alertController = UIAlertController(title: "Export Files", message: "Please select the files you want to export.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "CSV File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      
      self.exportDatabase(images: images)
    })
    
    let kmlAction = UIAlertAction(title: "KML File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportKMLDB(images: images)
    })
    
    let kmlcvsAction = UIAlertAction(title: "CSV/KML Files", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportCSVandKML(images: images)
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(kmlcvsAction)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "editImage" == segue.identifier {
      let Dest : EditImageTVC = segue.destination as! EditImageTVC
      Dest.imageKey = self.images[(tableView.indexPathForSelectedRow?.row)!].key
    } else if "goToAddImage" == segue.identifier {
      let Dest : AddImageTVC = segue.destination as! AddImageTVC
      Dest.albumName = self.albumName
    }
  }
  
  
  func gpsInfoStr(texts: Image) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""

    var accuStr = ""
    
    if texts.accuracy == "1" {
      accuStr  = "<b>Accuracy:</b> Added"
    } else {
      accuStr  = "<b>Accuracy:</b> ± \(texts.accuracy)m"
    }
    
    let datumStr = "<b>CoordSys:</b> \(texts.coordinateSystem)"
    
    var idInfo : String!
    
    if let initial = keychain.get(Const.INITIALS) {
      idInfo = initial + " " + String( texts.id )
    } else {
      idInfo = String( texts.id )
    }
    
    
    
    let dateStr = "<b>\(texts.timestamp)</b>"
    let symbolText = "<b>Category:</b> \(texts.symbolText!)"
    
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 || datum == 6 {
      //inverted to put East up and Nor down
      lonStr = "<b>Northing:</b> \(texts.latitude)"
      latStr = "<b>Easting:</b> \(texts.longitude)"
      zoneStr = "<b>\(datum == 6 ? "Grid Ref:" : "Zone:")</b> \(texts.zone)"
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(albumName!)<br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        zoneStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.ownerText.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.ownerText)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
    } else {
      latStr = "<b>Latitude:</b> \(texts.latitude)"
      lonStr = "<b>Longitude: </b>\(texts.longitude)"
      zoneStr = ""
      
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(albumName!) <br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.ownerText.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.ownerText)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
    }
    
    
    return result
  }
  
  
  func exportDatabase(images : [Image]) {
    let exportString = retrieveCSV(images: images)
    let file = saveAndExportGeneral(exportString: exportString, ext: "csv")
    presentActivity(files: [file])
    
  }
  
  
  
  func exportKMLDB(images : [Image]){
    let file = saveAndExportGeneral(exportString: exportKMLFunction(images : images ), ext: "kml")
    presentActivity(files: [file])
  }
  
  
  func exportKMLFunction(images : [Image]) -> String{
    var respondeKML : String = ""
    
    var header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n" +
      "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + "\n" +
      "<Document>" + "\n" +
      "<name>\(albumName!)</name>\"" + "\n"
    
    
    header += iconStyle()
    
    
    print(images.count)
    for result in images {
      let idInfo : String!
      
      
      let first = result.decimalLatitude
      let second = result.decimalLongitude
      
      if let initial = keychain.get(Const.INITIALS) {
        idInfo = initial + " " + String( result.id )
      } else {
        idInfo = String( result.id )
      }
      
      
      respondeKML =  respondeKML + "<Placemark>" + "\n" +
        "<name>\(idInfo!)</name>" + "\n" +
        "<styleUrl>#\(result.symbolLetter!)StyleMap</styleUrl>" + "\n" +
        "<description> <![CDATA[ \n \(gpsInfoStr(texts: result)) \n]]></description>" + "\n" +
        "<Point><coordinates>\(first),\(second)</coordinates></Point>" + "\n" +
        "</Placemark>" + "\n"
    }
    
    
    let footer = "</Document>" + "\n" +
    "</kml>"
    
    return (header + respondeKML + footer)
  }
  
  
  func iconStyle() -> String {
    var style = ""
    
    let symbols = ["x",
                   "a",
                   "b",
                   "c",
                   "d",
                   "e",
                   "f",
                   "o",
                   "v"]
    
    
    for symbol in symbols {
      style += "<Style id=\"\(symbol)Icon\">" + "\n" +
        "<IconStyle>" + "\n" +
        "<Icon>" + "\n" +
        "<href>http://ismartdetect.com/wp-content/uploads/2017/01/\(symbol)@1.5x.png</href>" + "\n" +
        "</Icon>" + "\n" +
        "</IconStyle>" + "\n" +
        "</Style>" + "\n" + "\n" +
        "<StyleMap id=\"\(symbol)StyleMap\">" + "\n" +
        "<Pair>" + "\n" +
        "<key>normal</key>" + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>" + "\n" +
        "</Pair>" + "\n" +
        "<Pair>"  + "\n" +
        "<key>highlight</key>"  + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>"  + "\n" +
        "</Pair>"  + "\n" +
        "</StyleMap>" + "\n"
    }
    
    
    return style
  }
  
  
  
  func exportCSVandKML(images : [Image]){
    let firstFile = saveAndExportGeneral(exportString: exportKMLFunction(images : images), ext: "kml" )
    let secondFile = saveAndExportGeneral(exportString: retrieveCSV(images: images), ext: "csv")
    presentActivity(files: [firstFile, secondFile])
    
  }
  
  
  
  func saveAndExportGeneral(exportString: String, ext: String) -> NSURL {
    var firstActivityItem : NSURL!
    
    let exportFilePath = NSTemporaryDirectory() + "\(albumName!.slugify())-\(appName!.slugify()).\(ext)"
    let exportFileURL = NSURL(fileURLWithPath: exportFilePath)
    
    FileManager.default.createFile(atPath: exportFilePath, contents: NSData() as Data, attributes: nil)
    //var fileHandleError: NSError? = nil
    var fileHandle: FileHandle? = nil
    do {
      fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
    } catch {
      print("Error with fileHandle")
    }
    
    if fileHandle != nil {
      fileHandle!.seekToEndOfFile()
      let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
      fileHandle!.write(csvData!)
      
      fileHandle!.closeFile()
      
      firstActivityItem = NSURL(fileURLWithPath: exportFilePath)
    }
    return firstActivityItem
    
  }
  
  
  func presentActivity(files: [NSURL]){
    let activityViewController : UIActivityViewController = UIActivityViewController(
      activityItems: files, applicationActivities: nil)
    
    activityViewController.excludedActivityTypes = [
      UIActivityType.assignToContact,
      UIActivityType.saveToCameraRoll,
      UIActivityType.postToFlickr,
      UIActivityType.postToVimeo,
      UIActivityType.postToTencentWeibo
    ]
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
      activityViewController.popoverPresentationController?.sourceView = self.view
      self.present(activityViewController, animated: true, completion: nil)
    } else{
      self.present(activityViewController, animated: true, completion: nil)
    }
    
  }
  
  
  
  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ExportViewController.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }
  
}



extension ExportViewController {
  func handleDeleteImage(image : Image){
    let key = (image.key)
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = key
          let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
          ref.removeValue()
          print("Deleted Image Data on firebase: ", key)
          
        } else {
          print("Error trying to Image Data.")
        }
        
      }catch {
        print("error")
      }
    }
    
  }
  
  
  
  func download(a image: Image){
    let httpsReference = Storage.storage().reference(forURL: image.url!)
    
    httpsReference.getData(maxSize: 1*1024*1024) { (data, error) in
      if (error != nil){
        print("something happend: func download at ShowPointInfo.swift line 135")
        return
      } else {
        let imageData = UIImage(data: data!)
        self.saveImageToCustomAlbum(image: (Helpers.Instance.textToImage(inImage: imageData!, image: image)), imageModel: image)
      }
    }
  }
  
  
  
  func saveImageToCustomAlbum(image: UIImage, imageModel: Image) {
    //Check if the folder exists, if not, create it
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", self.albumName)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
    
    let listAlbums: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
    let albumsSize: Int = listAlbums.count
    
    if albumsSize < 2 {
      UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
      return
    }
    
    if let first_Obj : AnyObject = collection.firstObject{
      //found the album
      self.albumFound = true
      self.assetCollection = first_Obj as! PHAssetCollection
      
      
      //save the image in a specific folder (album)
      PHPhotoLibrary.shared().performChanges({
        let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
        //Inverted Longitude / Latitude
        assetChangeRequest.location = CLLocation(latitude: imageModel.decimalLongitude, longitude: imageModel.decimalLatitude)
        let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
        let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
        
        let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
        albumChangeRequest!.addAssets(fastEnumeration)
        
      }, completionHandler: nil)
    }else{
      
      //Album placeholder for the asset collection, used to reference collection in completion handler
      var albumPlaceholder : PHObjectPlaceholder!
      //create the folder
      NSLog("\nFolder \"%@\" does not exist\nCreating now...", self.albumName)
      PHPhotoLibrary.shared().performChanges({
        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.albumName)
        albumPlaceholder = request.placeholderForCreatedAssetCollection
      },
                                             completionHandler: {(success:Bool, error:Error?)in
                                              if(success){
                                                print("Successfully created folder")
                                                self.defaults.set(0, forKey: "albumIndex")
                                                self.albumFound = true
                                                let collection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumPlaceholder.localIdentifier], options: nil)
                                                self.assetCollection = collection.firstObject! as PHAssetCollection
                                                
                                                //save the image in a specific folder (album)
                                                PHPhotoLibrary.shared().performChanges({
                                                  let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                                                  //Inverted Longitude / Latitude
                                                  assetChangeRequest.location = CLLocation(latitude: imageModel.decimalLongitude, longitude: imageModel.decimalLatitude)
                                                  //assetChangeRequest.
                                                  let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
                                                  
                                                  let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                                                  
                                                  let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
                                                  albumChangeRequest!.addAssets(fastEnumeration)
                                                }, completionHandler: nil)
                                                
                                              }else{
                                                print("Error creating folder")
                                                self.albumFound = false
                                              }
      })
    }
  }
  
  
  func retrieveCSV(images : [Image]) -> String {
    responde = ""
    
    let datum = defaults.integer(forKey: "dataFormat")
    if datum == 3 || datum == 4 || datum == 5 {
      responde = responde + Const.EASTING_NORTHING_CVS_HEADER + "\n"
    } else {
      responde = responde + Const.LATITUDE_LONGITUDE_CVS_HEADER + "\n"
    }
    
    if images.count > 0 {
      for image in images {
        
        responde = responde + respondInCommasForm(for: image)
        
        let datum = defaults.integer(forKey: "dataFormat")
        if datum == 3 || datum == 4 || datum == 5 {
          if image.ownerText.isEmpty {
            self.albumNameList.append(subtitleInfoNoOnwerTextUTM(for: image))
          } else {
            albumNameList.append(subtitleInfoOnwerTextUTM(for: image))
          }
        } else {
          if image.ownerText.isEmpty {
            albumNameList.append(subtitleInfoNoOwnerTextLL(for: image))
          } else {
            albumNameList.append(subtitleInfoOnwerTextUTM(for: image))
          }
        }
      }
    } else{
      stopTimer()
      print("print something...")
      self.dismiss(animated: true, completion: nil)
    }
    
    return responde
    
  }
  
  
  func respondInCommasForm(for image: Image) -> String {
    let initialsStamp = image.initials! + String(image.id)
    
    var longDescription = ""
    
    if image.longDesc != "Add a description..." {
      longDescription = image.longDesc!
    }
    
    return ("\(initialsStamp), \(image.longitude), \(image.latitude), \(image.zone), \(image.coordinateSystem), \(image.accuracy), \(image.timestamp), \(escapeCommas(text: image.albumName)), \(image.ownerText), \(image.symbolText!), \(longDescription)\n")
  }
  
  func escapeCommas(text: String) -> String{
    return text.replacingOccurrences(of: ",", with: " ")
  }
  
  func subtitleInfoNoOnwerTextUTM(for image: Image) -> String {
    let initialsStamp = image.initials! + String(image.id)
    return "E:\(image.longitude), N:\(image.latitude) - \(image.accuracy)m" + "\nID: " + initialsStamp + " | Date: \(image.timestamp)"
  }
  
  func subtitleInfoOnwerTextUTM(for image: Image) -> String {
    let initialsStamp = image.initials! + String(image.id)
    return "E:\(image.longitude), N:\(image.latitude) - \(image.accuracy)m" + " | \(image.ownerText)" + "\nID: " + initialsStamp + " | Date: \(image.timestamp)"
  }
  
  func subtitleInfoNoOwnerTextLL(for image: Image) -> String {
    let initialsStamp = image.initials! + String(image.id)
    return "Lon:\(image.longitude), Lat:\(image.latitude) - \(image.accuracy)m" + "\nID: " + initialsStamp + " | Date: \(image.timestamp)"
  }
  
  func subtitleInfoOwnerTextLL(for image: Image)  -> String {
    let initialsStamp = image.initials! + String(image.id)
    return  "Lon:\(image.longitude), Lat:\(image.latitude) - \(image.accuracy)m" + " | \(image.ownerText)" + "\nID: " + initialsStamp + " | Date: \(image.timestamp)"
  }
  
}


extension ExportViewController {
  
  func beforeSendEmail(){
    if let elements = tableView.indexPathsForSelectedRows {
      if elements.count > 10 {
        let alert = Helpers.Instance.alertUser(title: "Maximum Limit", message: "You can only send 10 photos at the same time using Email, because of Email limitation. Please just select 10 photos and try again.")
        present(alert, animated: true, completion: nil)
      } else {
        sendEmail()
      }
    }
    
  }
  
  
  func sendEmail(){
    if let list = tableView.indexPathsForSelectedRows  {
      let images = list.map{self.images[$0.row]}
      let urls = images.map{URL(string: $0.url!) }.flatMap{$0}
      var counter = 0
      var photos = [UIImage]()
      var filenames = [String]()
      
      if images.count > 0 {
        let alert = UIAlertController(title: "", message: "(\(counter)/\(images.count)) photos processed.", preferredStyle: .alert)
        //let alert = UIAlertController(title: "", message: "Processing...", preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
        
        for index in 0...(images.count - 1) {
          if images[index].url != "" {
            
            SDWebImageDownloader.shared().downloadImage(with: URL(string: images[index].url!), options: .continueInBackground, progress: nil, completed: { (image, data, error, finished) in
              photos.append(Helpers.Instance.textToImage(inImage: image!, image: images[index]))
              filenames.append("\(self.albumName.slugify())-\(images[index].initialsStamp.slugify())")
              alert.message = "(\(counter)/\(images.count)) photos processed."
              
              counter += 1

              if counter == images.count {
                alert.dismiss(animated: true, completion: nil)
                self.handleEmail(imagesData: photos, filenames : filenames)
              }
            })
          
          } else {
            photos.append((Helpers.Instance.textToImage(inImage: UIImage(data: (images[index].imgData! as Data))!, image: images[index])))
            filenames.append("\(self.albumName.slugify())-\(images[index].initialsStamp.slugify())")
            //alert.message = "(\(counter)/\(images.count)) photos processed."
            
            counter += 1

            if counter == images.count {
              //alert.dismiss(animated: true, completion: nil)
              alert.dismiss(animated: false, completion: nil)
              handleEmail(imagesData: photos, filenames : filenames)
            }

          }
          
         
        }
      }
    }
  
  }
  
  func beforeGeneratePDF(){
    if let elements = tableView.indexPathsForSelectedRows {
      
      if (elements.count) >= 30 {
        let alert = Helpers.Instance.alertUser(title: "Maximum Limit", message: "You can only generate a PDF for 30 photos at the same time. Please just select 30 photos and try again.")
        present(alert, animated: true, completion: nil)
      } else {
        if let list = tableView.indexPathsForSelectedRows  {
          let indexes = list.map{$0.row}
          var photos = [UIImage]()
          
          let select = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(selectImages))
          
          select.title = "Select"
          navigationItem.title = "\(self.albumName!)"
          activeSelection = true
          navigationItem.rightBarButtonItem = select
          navigationItem.leftBarButtonItem = nil
          tableView.allowsSelectionDuringEditing = false
          tableView.allowsMultipleSelectionDuringEditing = false
          tableView.setEditing(false, animated: true)
          selectButtonPressed = false
          
          navigationController?.setToolbarHidden(true, animated: true)
          
          if indexes.count > 0 {
            for indexPath in list {
              photos.append(self.tableView.screenshotOfCell(at: indexPath))
              usleep(2000000)
            }
            
            generatePDF(images: photos, info: images)
          }
        }
      }
    }
  }
  
  func generatePDF(images : [UIImage], info : [Image]){
    
    let A4paperSize = CGSize(width: 595, height: 842)
    let pdf = SimplePDF(pageSize: A4paperSize)
    
    pdf.addText("Project: \(self.albumName!)")
    
    for index in 0...(images.count - 1) {
      
      pdf.setContentAlignment(.center)
      pdf.addImage(images[index])
      
      pdf.addLineSpace(1)
      
      if mod((index + 1), 5) == 0 {
        pdf.beginNewPage()
      }
      
    }
    
    
    let pdfData = pdf.generatePDFdata()
    
    sendPDF(file: pdfData)
    
  }
  
  func mod(_ a: Int, _ n: Int) -> Int {
    precondition(n > 0, "modulus must be positive")
    let r = a % n
    return r >= 0 ? r : r + n
  }
  
  
  func sendPDF(file : Data) {
    if MFMailComposeViewController.canSendMail() {
      
      let composeVC = MFMailComposeViewController()
      composeVC.mailComposeDelegate = self
      
      fetchSelectedEmails()
      
      if selectedEmails.count == 0 {
        selectedEmails = [""]
      }
      
      composeVC.setToRecipients(selectedEmails)
      composeVC.setSubject("\(albumName!) - by \(appName!)")
      
      
      let filename = NSUUID()
      
      composeVC.addAttachmentData(file, mimeType: "application/pdf", fileName: "\(filename)")
      
      composeVC.setMessageBody("Photos Info", isHTML: true)
      
      present(composeVC, animated: true)
    } else {
      let alert = Helpers.Instance.alertUser(title: "ERROR!", message: "Please make sure you have the Mail App installed and configured on your phone.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  
  
  
  func withBigImage(completionHandler handler: @escaping (_ image: [UIImage]) -> Void){
    if let list = tableView.indexPathsForSelectedRows {
      var array = [UIImage]()
      
      for index in list {
        let httpsReference = Storage.storage().reference(forURL: self.images[index.row].url!)
        
        httpsReference.getData(maxSize: 1*1024*1024) { (data, error) in
          if (error != nil){
            print("something happend: func download at ShowPointInfo.swift line 135")
            return
          } else {
            let imageData = UIImage(data: data!)
            array.append(Helpers.Instance.textToImage(inImage: imageData!, image: self.images[index.row]))
          }
          
        }
      }
      
      handler(array)
      
    }
  }
  
  func handleEmail(imagesData : [UIImage], filenames: [String]) {
    if MFMailComposeViewController.canSendMail() {
      
      let composeVC = MFMailComposeViewController()
      composeVC.mailComposeDelegate = self
      
      fetchSelectedEmails()
      
      if selectedEmails.count == 0 {
        selectedEmails = [""]
      }
      
      composeVC.setToRecipients(selectedEmails)
      composeVC.setSubject("\(albumName!) - by \(appName!)")
      
      
      //let filename = NSUUID()
      
      for (index, image) in imagesData.enumerated() {
        let finalImage = UIImageJPEGRepresentation(image, 1)
        composeVC.addAttachmentData(finalImage!, mimeType: "image/jpeg", fileName : "\(filenames[index]).jpg" )
      }
      
      composeVC.setMessageBody("Photos Info", isHTML: true)
      
      present(composeVC, animated: true)
    } else {
      let alert = Helpers.Instance.alertUser(title: "ERROR!", message: "Please make sure you have the Mail App installed and configured on your phone.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    switch result {
    case .cancelled:
      print("Mail cancelled")
    case .saved:
      print("Mail saved")
    case .sent:
      print("Mail sent")
    case .failed:
      print("Mail sent failure: \(error!.localizedDescription)")
    }
    controller.dismiss(animated: true, completion: nil)
  }
  
  
  func fetchSelectedEmails(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.EMAIL_ENTITY)
    
    request.predicate = NSPredicate(format: "selected = YES")
    
    let emails = try? manageContext.fetch(request)
    for email in emails as! [EmailData]{
      if email.selected == true {
        let new = email.email!
        self.selectedEmails.append(new)
      }
    }
  }
  
  
  
}
