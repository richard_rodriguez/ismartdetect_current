//
//  CustomInfoWindow.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 2/2/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation

class CustomInfoWindow: UIView {
  
  
  @IBOutlet var predefinedText: UILabel!
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var accuracy: UILabel!
  @IBOutlet weak var date: UILabel!
  @IBOutlet weak var longitude: UILabel!
  @IBOutlet weak var definedFind: UILabel!
  @IBOutlet weak var gZone: UILabel!
  @IBOutlet weak var latitude: UILabel!
  @IBOutlet weak var id: UILabel!
  @IBOutlet weak var placeHolderImage: UIImageView!
  
  @IBOutlet weak var noPhotoUploadedLabel: UILabel!
  @IBOutlet weak var eastingLabel: UILabel!
  @IBOutlet weak var northingLabel: UILabel!
  @IBOutlet weak var zoneLabel: UILabel!
  
  @IBInspectable var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set(newValue) {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set(newValue) {
      layer.cornerRadius = newValue
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    get {
      if let color = layer.borderColor {
        return UIColor(cgColor: color)
      }
      return nil
    }
    set(newValue) {
      layer.borderColor = newValue?.cgColor
    }
  }
  
  
}
