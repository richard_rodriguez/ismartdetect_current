//  Constants.swift
//  Created by Richard Rodriguez on 11/30/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation

class Const {
  
  static let APP_VERSION            = "1.0.15"
  //App Name
  static let APP_NAME               = "iSmartDetect"
  
  static let SECURE_DIGITS          = "secure_digits"
  
  //Placeholder images
  static let PLACEHOLDER_URL        = "https://placeholdit.imgix.net/~text?txtsize=23&bg=A2A2A2&txtclr=000000&txt=NO%20PHOTO%20UPLOADED&w=450&h=450"
  
  //colors
  static let RED = UIColor(red: 234/255, green: 32/255, blue: 52/255, alpha: 1)
  
  //CSV Heading  
  static let EASTING_NORTHING_CVS_HEADER = "\"sep=,\"\n ID, EAST / LAT, NORTH / LONG, ZONE / GREF, COORDS SYS / DATUM, ACC +/- M, DATE / TIME, PROJECT NAME, STAMPED TEXT, CATEGORY, DESCRIPTION"
  static let LATITUDE_LONGITUDE_CVS_HEADER = "\"sep=,\"\n ID, EAST / LAT, NORTH / LONG, ZONE / GREF, COORDS SYS / DATUM, ACC +/- M, DATE / TIME, PROJECT NAME, STAMPED TEXT, CATEGORY, DESCRIPTION"
  
  //general
  static let SELECTED               = "selected"
  static let EMAIL                  = "email"
  static let KEY                    = "key"
  static let TEXT                   = "text"

  //Personal Info
  static let PERSONAL_INFO          = "personal_info"
  static let NAME                   = "name"
  static let PASSWORD               = "password"
  static let NICKNAME               = "nickname"
  static let PROFILE_URL            = "profileURL"

  
  //Setting Options
  static let SETTINGS                   = "settings"
  static let DATA_FORMAT                = "data_format"
  static let HOUR_FORMAT                = "hour_format"
  static let DATE_FORMAT                = "date_format"
  static let SHADOW                     = "shadow"
  static let FONT_SIZE                  = "font_size"
  static let TRANSPARENCY               = "transparency"
  static let SAVE_ORIGINAL              = "save_original"
  static let ONLY_DATE_ON_IMAGE         = "only_date_on_image"
  static let ACTIVE_PREDEFINED_TEXT     = "active_predefined_text"
  static let ACTIVE_EMAIL_OPTION        = "active_email_option"
  static let ACTIVE_STAMP_PROJECT_NAME  = "active_stamp_project_name"
  static let SYNC_IMAGE_DATA            = "sync_image_data"
  
  //Emails 
  static let EMAILS                 = "emails"
  
  //Albums
  static let ALBUMS               = "albums"
  static let CREATED_AT           = "createdAt"
  static let UPDATED_AT           = "updatedAt"
  static let OWNER                = "owner"
  
  
  //Predefined Texts
  static let PREDEFINED_TEXTS  = "predefined_texts"
  static let PREDEFINED_TEXT   = "text"
  
  //Notes
  static let NOTES             = "notes"
  static let NOTE              = "text"
  
  
  //DefinedFind
  static let DEFINED_FINDS = "defined_finds"
  static let INDEX = "index"
  static let LETTER = "letter"
  
  //Image Data
  static let IMAGES_DATA       = "images_data"
  static let ID                = "id"
  static let LATITUDE          = "latitude"
  static let LONGITUDE         = "longitude"
  static let ZONE              = "gZone"
  static let ACCURRARY         = "acurracy"
  static let COORDINATE_SYSTEM = "coordinateSystem"
  static let TIMESTAMP         = "timestamp"
  static let ALBUM_NAME        = "albumName"
  static let OWNER_TEXT        = "note"
  static let DESC              = "desc"
  static let URL               = "url"
  static let INITIALS          = "initials"
  static let INITIALS_NAME     = "initialsName"
  static let SYMBOL_TEXT       = "symbolText"
  static let SYMBOL_LETTER     = "symbolLetter"
  static let IMG_DATA          = "imgData"
  static let LONG_DESCRIPTION  = "longDesc"
  
  //Login Variable
  static let LOGIN_TYPE = "loginType"
  static let FACEBOOK = "Facebook Account"
  static let GOOGLE = "Google Account"
  static let EMAIL_LOGIN = "Email Account"
  static let LOGIN_USER_NAME = "loginUserName"
  static let LAST_EMAIL_USED = "lastEmailUsed"
  static let EMAIL_USED_FOR_LOGIN = "emailUsedForLogin"
  static let USER_LOGGED_USING_EMAIL = "userLoggedUsingEmail"
  
  
  //Entity Name
  static let EMAIL_ENTITY = "EmailData"
  static let IMAGE_ENTITY = "ImageData"
  static let NOTE_ENTITY = "Notes"
  static let PREDEFINED_TEXT_ENTITY = "PredefinedTextData"
  static let DEFINED_FIND_ENTITY = "DefinedFindData"
  static let ALBUM_ENTITY = "AlbumData"
  
  //Coordinate Systems
  static let COOR_DMS_WGS84 = "DMS-WGS84"
  static let COOR_DM_WGS84  = "DM-WGS84"
  static let COOR_DD_WGS84  = "DD-WGS84"
  static let COOR_UTM_WGS84 = "UTM-WGS84"
  static let COOR_UTM_ETRS89 = "UTM-ETRS89"
  static let COOR_UTM_UK_GREF = "UTM-UK-GREF"
  
  
}
