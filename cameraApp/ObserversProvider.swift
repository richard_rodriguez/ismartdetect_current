//
//  ObserversProvider.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/12/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import CoreData

typealias EmailCreation = Email
typealias NoteCreation = Note
typealias PredefinedTextCreation = PredefinedText
typealias ImageDataCreation = ImageData


class ObserversProvider {
  private static let _instance = ObserversProvider()
  var handle: UInt!
  
  static var Instance : ObserversProvider {
    return _instance
  }
  
  
  func observer(child: String, option: Option) -> (handle: UInt?, ref: DatabaseReference?) {
    var ref : DatabaseReference!
    
    if let id = Auth.auth().currentUser?.uid {
      ref = Database.database().reference().child(id).child(child)
      ref.keepSynced(true)
      
      handle = ref.observe(.value, with: { (snapshot) in
        let enumerator = snapshot.children
        while let rest = enumerator.nextObject() as? DataSnapshot {
          switch option {
            case .note:
              print(rest)
              let object = Note(snapshot: rest)
              self.add(new: object, option: .note)
            case .email:
              let object = Email(snapshot: rest)
              self.add(new: object, option: .email)
            case .predefinedText:
              let object = PredefinedText(snapshot: rest)
              print(object.text)
              self.add(new: object, option: .predefinedText)
            case .image:
              let object = Image(snapshot: rest)
              self.add(new: object, option: .image)
            case .album:
              let object = Album(snapshot: rest)
              print(object)
              self.add(new: object, option: .album)
          }
        }

      })
    } else {
      print("User not log in.")
    }
    
    if let id = Auth.auth().currentUser?.uid {
      let ref = Database.database().reference().child(id).child(child)
      ref.observe(.childRemoved, with: { (snapshotRemoved) in
        print("removed child")
        
        switch option {
        case .note:
          self.delete(this: snapshotRemoved.key, option: .note)
        case .email:
          self.delete(this: snapshotRemoved.key, option: .email)
        case .predefinedText:
          self.delete(this: snapshotRemoved.key, option: .predefinedText)
        case .image:
          self.delete(this: snapshotRemoved.key, option: .image)
        case .album:
          self.delete(this: snapshotRemoved.key, option: .album)
        }
      })
    }
  
    return (handle, ref)
  }


  func add(new object: Any, option: Option){
    var dictionary : Dictionary<String, Any>
    
    switch option {
      case .note:
        let model = object as! Note
        dictionary = model.toDictionary()
      
      case .email:
        let model = object as! Email
        dictionary = model.toDictionary()
      
      case .predefinedText:
         let model = object as! PredefinedText
         dictionary = model.toDictionary()
      
      case .image:
        let model = object as! Image
        dictionary = model.toCoreDataDictionary()
      
      case .album:
        let model = object as! Album
        dictionary = model.toDictionary()
    }

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: option.entity() )
    
    
    request.predicate = NSPredicate(format: "key = %@", dictionary["key"] as! String )
    
    if let result = try? manageContext.fetch(request){
      if result.count == 0 {
        
        let new = NSEntityDescription.insertNewObject(forEntityName: option.entity(), into: manageContext)

        new.setValuesForKeys(dictionary)
        
        do{
            try manageContext.save()
            print("Data saved from Observer. Add(new Object...) ObserversProvider")
        }catch {
          print("Error trying to saving data from the Observer")
        }
      } else {
        //un-comment
        //print("Already in the database")
        
        switch option {
          case .note:
            let managedObject = result[0] as! Notes
            managedObject.setValuesForKeys(dictionary)
          case .email:
            let managedObject = result[0] as! EmailData
            managedObject.setValuesForKeys(dictionary)
          case .predefinedText:
            let managedObject = result[0] as! PredefinedTextData
            print(dictionary["text"] as! String)
            managedObject.setValuesForKeys(dictionary)
          case .image:
            let managedObject = result[0] as! ImageData
            managedObject.setValuesForKeys(dictionary)
          case .album:
            let managedObject = result[0] as! AlbumData
            managedObject.setValuesForKeys(dictionary)
        }
        
        
        do {
            try manageContext.save()
            //print("Updated data from Observer on CoreData")
        } catch {
          print("Error trying data from Obsever on CoreData")
        }
      }
    }
  }

  func delete(this key: String, option: Option){
    var keyToDelete : String

    switch option {
      case .note:
        keyToDelete = key
      case .email:
        keyToDelete = key
      case .predefinedText:
        keyToDelete = key
      case .image:
        keyToDelete = key
      case .album:
        keyToDelete = key

      
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: option.entity() )
    
    request.predicate = NSPredicate(format: "key = %@", keyToDelete )

      if let resultElement = try? manageContext.fetch(request){
        if resultElement.count == 0 {
          print("was deleted")
        } else {
          let element = resultElement[0]

          
          manageContext.delete(element as! NSManagedObject)
          
          do{
            try manageContext.save()
            
            print("Delete data from Observer in CoreData")
          }catch {
            print("Error deleting data from Observer in CoreData")
          }

        }
        
      }
    
  }

  
  func observerDefinedFind() -> (handle: UInt?, ref: DatabaseReference?) {
    var ref : DatabaseReference!
    if let id = Auth.auth().currentUser?.uid {
      ref = Database.database().reference().child(id).child(Const.DEFINED_FINDS)
      print("run this")
      handle = ref.observe(.value, with: { (snapshot) in
        print(snapshot)
        if let setting = snapshot.value as? [String: AnyObject] {
          let x = self.castNil(setting["x"])
          let a = self.castNil(setting["a"])
          let b = self.castNil(setting["b"])
          let c = self.castNil(setting["c"])
          let d = self.castNil(setting["d"])
          let e = self.castNil(setting["e"])
          let f = self.castNil(setting["f"])
          let o = self.castNil(setting["o"])
          let v = self.castNil(setting["v"])
          
          defaults.set(x, forKey: "x")
          defaults.set(a, forKey: "a")
          defaults.set(b, forKey: "b")
          defaults.set(c, forKey: "c")
          defaults.set(d, forKey: "d")
          defaults.set(e, forKey: "e")
          defaults.set(f, forKey: "f")
          defaults.set(o, forKey: "o")
          defaults.set(v, forKey: "v")
        }
      })
    } else {
      print("User not log in.")
    }
  return (handle, ref)
  
  }

  func castNil(_ value : AnyObject?) -> String{
    if value == nil {
      return "undefined"
    } else {
      return  (value as! String)
  }
  }

}

