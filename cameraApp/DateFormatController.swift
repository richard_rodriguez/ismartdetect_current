//
//  DateFormatController.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 8/14/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import UIKit
import Photos

class DateFormatController: UITableViewController {
  
  let defaults = UserDefaults.standard
  
  override func viewWillAppear(_ animated: Bool) {
   selectRow(index: (defaults.integer(forKey: "dateFormat")))
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    //DBProvider.Instance.saveSetting()
  }
  
 
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    
      if let cell = tableView.cellForRow(at: indexPath) {
          cell.tintColor = Const.RED
          cell.accessoryType = .checkmark
      }
    
    defaults.set(indexPath.row, forKey: "dateFormat")
  }
  

  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

          if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
         }
  }
  
  func selectRow(index: Int) {
    let rowToSelect:IndexPath = IndexPath(row: index, section: 0);  //slecting 0th row with 0th section
    self.tableView.selectRow(at: rowToSelect, animated: false, scrollPosition: UITableViewScrollPosition.middle);
    
    if let cell = tableView.cellForRow(at: rowToSelect) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
  }
  

  
}

