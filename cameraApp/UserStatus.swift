//
//  UserStatus.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/15/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

class UserStatus: UIImageView{

  
  override func awakeFromNib() {
    self.isUserInteractionEnabled = true
    
    if Auth.auth().currentUser?.uid == nil {
      image = UIImage(named: "offline")
    } else {

      if let url = defaults.string(forKey: Const.PROFILE_URL) {
        downloadedFrom(link: url)
        layer.cornerRadius = self.frame.size.width / 2;
        layer.borderWidth = 4.0
        layer.cornerRadius = 10.0
        layer.borderColor = UIColor.white.cgColor
      } else {
        image = UIImage(named: "online")
      }
    }
  }

}



