//
//  ImageCollectionViewCell.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/25/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var easting: UILabel!
  @IBOutlet weak var northing: UILabel!
  @IBOutlet weak var coordinateSystem: UILabel!
  @IBOutlet weak var zoneAccuracy: UILabel!
  @IBOutlet weak var line1: UILabel!
  @IBOutlet weak var line2: UILabel!
  @IBOutlet weak var line3: UILabel!
  
  
}
