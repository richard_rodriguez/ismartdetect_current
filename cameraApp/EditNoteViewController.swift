//
//    
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/15/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth
import FirebaseDatabase

@objc class EditNoteViewController: UIViewController {
  
    @IBOutlet weak var note: UITextView!
  
    var notePassed : Note!
    var tempNote : Note!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        tempNote = notePassed
        note.text = notePassed.text
      
      let editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(goToEdit))
      editButton.tintColor = Const.RED
      navigationItem.rightBarButtonItem = editButton
      //navigationController?.setToolbarHidden(false, animated: false)
      
      
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    
    navigationController?.setToolbarHidden(false, animated: false)
    
    saveANote()

  }
  
  func saveANote(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.NOTE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", tempNote.key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! Notes
      tempNote.text = note.text!
      managedObject.setValue(note.text!, forKey: "text")
      
      do{
        try manageContext.save()
        edit(note: tempNote)
        print("Note updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
  }
  
  func edit(note : Note){
    if let uid = Auth.auth().currentUser?.uid {
      let ref = Database.database().reference().child(uid).child(Const.NOTES).child(note.key)
      ref.updateChildValues(Note(text: note.text, key: note.key).toDictionary())
    }
  }
  
  
  func goToEdit(){
    let saveButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(goToSave))
    saveButton.tintColor = Const.RED
    navigationItem.rightBarButtonItem = saveButton
    
    note.isEditable = true
  }
  
  
  @objc func goToSave(){
    saveANote()
    
    let editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(goToEdit))
    editButton.tintColor = Const.RED
    navigationItem.rightBarButtonItem = editButton
    
    note.isEditable = false

  }
  
  @IBAction func editPressed(_ sender: Any) {
    note.isEditable = true
  }
  
  

}
