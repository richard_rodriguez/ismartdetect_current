//
//  BackgroundLaunchScreen.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 9/20/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class BackgroundLaunchScreen: UIImageView {

  
    override func draw(_ rect: CGRect) {
      let screenshot = defaults.integer(forKey: "backgroundColor")
      
      switch screenshot {
      case 0:
        self.image = #imageLiteral(resourceName: "Rectangle 0")
      case 1:
        self.image = #imageLiteral(resourceName: "Rectangle 1")
      case 2:
        self.image = #imageLiteral(resourceName: "Rectangle 2")
      case 3:
        self.image = #imageLiteral(resourceName: "Rectangle 3")
      default:
        self.image = #imageLiteral(resourceName: "Rectangle 0")
      }
    }
  

}
