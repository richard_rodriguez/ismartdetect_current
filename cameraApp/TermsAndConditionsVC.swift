//
//  TermsAndConditionsVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 4/14/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class TermsAndConditionsVC: UIViewController {

  @IBOutlet weak var text: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let htmlString = "<div style=\"font-size: 18px;font-family: 'HelveticaNeue-Light'\">" +
      "<h3>Terms and Conditions</h3>" +
      "<p>Last updated: March 22, 2017</p>" +
    "<p>These Terms and Conditions (\"Terms\", \"Terms and Conditions\") govern your relationship with iSmartDetect mobile application (the \"Service\") operated by iVirtual Games Aps (\"us\", \"we\", or \"our\").</p>" +
    "<p>Please read these Terms and Conditions carefully before using our iSmartDetect mobile application (the \"Service\").</p>" +
      "<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>" +
      "<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>" +
      "<p><strong>Purchases</strong></p>" +
    "<p>If you wish to purchase any product or service made available through the Service (\"Purchase\"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information.</p>" +
      "<p>You represent and warrant that: (i) you have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete.</p>" +
      "<p>By submitting such information, you grant us the right to provide the information to third parties for purposes of facilitating the completion of Purchases.</p>" +
      "<p>We reserve the right to refuse or cancel your order at any time for certain reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons.</p>" +
      "<p>We reserve the right to refuse or cancel your order if fraud or an unauthorised or illegal transaction is suspected.</p>" +
      "<p><strong>Availability, Errors and Inaccuracies</strong></p>" +
      "<p>We are constantly updating our offerings of products and services on the Service. The products or services available on our Service may be mispriced, described inaccurately, or unavailable, and we may experience delays in updating information on the Service and in our advertising on other web sites.</p>" +
      "<p>We cannot and do not guarantee the accuracy or completeness of any information, including prices, product images, specifications, availability, and services. We reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.</p>" +
      "<p><strong>Subscriptions</strong></p>" +
    "<p>Some parts of the Service are billed on a subscription basis (\"Subscription(s)\"). You will be billed in advance on a recurring and periodic basis (\"Billing Cycle\"). Billing cycles are set either on a monthly or annual basis, depending on the type of subscription plan you select when purchasing a Subscription.</p>" +
      "<p>At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless you cancel it or iVirtual Games Aps cancels it. You may cancel your Subscription renewal either through your online account management page or by contacting iVirtual Games Aps customer support team.</p>" +
      "<p>A valid payment method, including credit card, is required to process the payment for your Subscription. You shall provide iVirtual Games Aps with accurate and complete billing information including full name, address, state, zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize iVirtual Games Aps to charge all Subscription fees incurred through your account to any such payment instruments.</p>" +
      "<p>Should automatic billing fail to occur for any reason, iVirtual Games Aps will issue an electronic invoice indicating that you must proceed manually, within a certain deadline date, with the full payment corresponding to the billing period as indicated on the invoice.</p>" +
      "<p><strong>Free Trial</strong></p>" +
    "<p>iVirtual Games Aps may, at its sole discretion, offer a Subscription with a free trial for a limited period of time (\"Free Trial\").</p>" +
      "<p>You may be required to enter your billing information in order to sign up for the Free Trial.</p>" +
      "<p>If you do enter your billing information when signing up for the Free Trial, you will not be charged by iVirtual Games Aps until the Free Trial has expired. On the last day of the Free Trial period, unless you cancelled your Subscription, you will be automatically charged the applicable Subscription fees for the type of Subscription you have selected.</p>" +
      "<p>At any time and without notice, iVirtual Games Aps reserves the right to (i) modify the terms and conditions of the Free Trial offer, or (ii) cancel such Free Trial offer.</p>" +
      "<p><strong>Fee Changes</strong></p>" +
      "<p>iVirtual Games Aps, in its sole discretion and at any time, may modify the Subscription fees for the Subscriptions. Any Subscription fee change will become effective at the end of the then-current Billing Cycle.</p>" +
      "<p>iVirtual Games Aps will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective.</p>" +
      "<p>Your continued use of the Service after the Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount.</p>" +
      "<p><strong>Refunds</strong></p>" +
      "<p>Except when required by law, paid Subscription fees are non-refundable.</p>" +
      "<p><strong>Content</strong></p>" +
    "<p>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material (\"Content\"). You are responsible for the Content that you post to the Service, including its legality, reliability, and appropriateness.</p>" +
      "<p> <span style=\"color: red;\">By posting Content to the Service, you grant us the right and license to use, modify, perform, display, reproduce, and distribute such Content on and through the Service. </span>You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights.</p>" +
      "<p>You represent and warrant that: (i) the Content is yours (you own it) or you have the right to use it and grant us the rights and license as provided in these Terms, and (ii) the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person.</p>" +
      "<p>We use Advanced Encryption Standard (AES)128, and all stored GPS data is hosted at Google Firebase. There is absolutely no way we can read your GPS coordinates, they are all encrypted with Advanced Encryption Standard (AES)128.</p>" +
      "<p><strong>Accounts</strong></p>" +
      "<p>When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.</p>" +
      "<p>You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.</p>" +
      "<p>You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>" +
      "<p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>" +
      "<p><strong>Intellectual Property</strong></p>" +
      "<p>The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of iVirtual Games Aps and its licensors. The Service is protected by copyright, trademark, and other laws of both the Denmark and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of iVirtual Games Aps.</p>" +
      "<p><strong>Links To Other Web Sites</strong></p>" +
      "<p>Our Service may contain links to third-party web sites or services that are not owned or controlled by iVirtual Games Aps.</p>" +
      "<p>iVirtual Games Aps has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that iVirtual Games Aps shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>" +
      "<p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>" +
      "<p><strong>Termination</strong></p>" +
      "<p>We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>" +
      "<p>Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>" +
      "<p><strong>Limitation Of Liability</strong></p>" +
    "In no event shall iVirtual Games Aps, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) \"<p>unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>" +
    "<p><strong>Disclaimer</strong></p>" +
    "<p>Your use of the Service is at your sole risk. The Service is provided on an \"AS IS\" and \"AS AVAILABLE\" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.</p>" +
     "<p><strong>THE SOFTWARE AND SERVICES ARE PROVIDED “AS IS” AND “WITH ALL FAULTS” AND THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE AND SERVICES IS WITH YOU. SHOULD THE SOFTWARE OR SERVICES PROVE DEFECTIVE, IVIRTUAL GAMES APS DOES NOT HAVE ANY LIABILITY FOR THE SERVICING AND/OR REPAIR OF YOUR DEVICE OR THE SOFTWARE. IVIRTUAL GAMES APS AND ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES WITH RESPECT TO THE SOFTWARE AND SERVICES, EXPRESS OR IMPLIED, INCLUDING ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR USE OR PURPOSE. IVIRTUAL GAMES APS AND ITS SUPPLIERS DO NOT WARRANT THAT THE SOFTWARE OR SERVICES WILL MEET YOUR REQUIREMENTS IN ANY RESPECT, OR THAT THE OPERATION OF THE SOFTWARE OR SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE SOFTWARE OR SERVICES OR NONCONFORMITY TO ITS OR THEIR DOCUMENTATION CAN OR WILL BE CORRECTED. YOU ACKNOWLEDGE THAT THE PROVISIONS OF THIS SECTION ARE A MATERIAL INDUCEMENT AND CONSIDERATION TO FULLPOWER AND ITS SUPPLIERS TO GRANT THE LICENSE CONTAINED IN THIS EULA AND TO PROVIDE YOU WITH ACCESS TO THE SERVICES. </p>" +
        "<p>Use of iSmartDetect is at the User's Own Risk. THE SOFTWARE AND SERVICES ARE NOT FAA CERTIFIED AND MUST NOT BE USED FOR PRIMARY NAVIGATION. THE SOFTWARE AND SERVICES MAY NOT BE USED FOR SAFETY OF LIFE APPLICATIONS, OR FOR ANY OTHER APPLICATION IN WHICH THE ACCURACY OR RELIABILITY OF THE SOFTWARE OR SERVICES COULD CREATE A SITUATION WHERE PERSONAL INJURY OR DEATH MAY OCCUR. DO NOT ATTEMPT TO CONFIGURE THE SOFTWARE OR INPUT INFORMATION WHILE DRIVING; FAILURE TO PAY FULL ATTENTION TO THE OPERATION OF YOUR VEHICLE COULD RESULT IN DEATH, SERIOUS INJURY, OR PROPERTY DAMAGE. BY USING THE SOFTWARE, YOU ASSUME ALL RESPONSIBILITY AND RISK. YOU ACKNOWLEDGE THAT THE ACTIVITIES WITH WHICH YOU USE THE SOFTWARE AND SERVICES (INCLUDING BUT NOT LIMITED TO DRIVING AND ATHLETIC ACTIVITIES) MAY HAVE CERTAIN INHERENT AND SIGNIFICANT RISKS OF PROPERTY DAMAGE, BODILY INJURY OR DEATH. BY USING ANY OF THE SOFTWARE OR SERVICES, YOU ASSUME ALL RESPONSIBILITY AND ALL KNOWN AND UNKNOWN RISKS OF SUCH USE AND OF \"<p>THE ACTIVITIES WITH WHICH YOU USE THE SOFTWARE AND SERVICES, EVEN IF CAUSED IN WHOLE OR PART BY THE ACTION, INACTION, OR NEGLIGENCE OF IVIRTUAL GAMES APS OR ITS SUPPLIERS</strong></p>" +
    "<p>iVirtual Games Aps its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.</p>" +
    "<p><strong>Governing Law</strong></p>" +
    "<p>These Terms shall be governed and construed in accordance with the laws of Denmark, without regard to its conflict of law provisions.</p>" +
    "<p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>" +
    "<p><strong>Changes</strong></p>" +
    "<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 15 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>" +
    "<p>By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>" +
    "<p><strong>Contact Us</strong></p>" +
    "<p>If you have any questions about these Terms, please contact us.</p>" +
    "<p></p></div>"

    
    
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
    
    let attributedString = try! NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)

    text.attributedText = attributedString
    
  }


   
}
