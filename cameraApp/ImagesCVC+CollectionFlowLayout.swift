//
//  ImagesCVC+CollectionFlowLayout.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/6/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation

extension ImagesCVC {


  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let screenSize = UIScreen.main.bounds
    let screenWidth = screenSize.width
    let cellSquareSize: CGFloat = screenWidth
    
    return CGSize(width: cellSquareSize, height: cellSquareSize)
  }
  
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(0, 0, 0.0, 0.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 10.0
  }
  

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 10.0
  }
  
  
  
}
