//
//  User.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/16/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation



struct User {
  var name: String
  var email: String
  var profileImageURL: String
  var provider : String
  
  var providerInfo : String {
    if provider == "google.com" {
      return Const.GOOGLE
    } else if provider == "facebook.com" {
      return Const.FACEBOOK
    } else if provider == "password" {
      return Const.EMAIL_LOGIN
    } else {
      return "Logged In"
    }
  }
  
  var firstName : String {
    return self.name.components(separatedBy: " ")[0]
  }

  
  
}

