import UIKit
import CoreLocation
import Photos
import FirebaseAuth
import CoreData
import FirebaseDatabase
import KeychainSwift
import SwiftyStoreKit
import CryptoSwift


@objc class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {

  struct SegueTo {
    static let ACCOUNT = "AccontSG"
    static let SIGNIN = "SignInSG"
    static let ZOOM = "ZoomSG"
    static let SUSCRIBE = "Suscribe"
  }
  
  struct ErrorMessage {
    static let NO_GPS = "Failed to find user's location. Check if your Location Services is active, or if this app has access to use your location on your Phone Settings."
  }

  @IBOutlet weak var cameraButton: UIButton!
  @IBOutlet weak var gpsDetailsLabel: UITextView!
  @IBOutlet weak var albumNameLabel: UILabel!
  @IBOutlet weak var currentProjectLabel: UILabel!
  @IBOutlet weak var pauseButton: UIButton!
  @IBOutlet weak var appNameLabel: UILabel!
  @IBOutlet weak var settingButton: UIButton!
  @IBOutlet weak var holdGPSButton: UILabel!
  @IBOutlet weak var takeAPictureButton: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var hourLabel: UILabel!
  @IBOutlet weak var userStatusImage: UIImageView!
  @IBOutlet weak var magicLabel: UILabel!
  @IBOutlet weak var magicToolImage: MagicToolImageView!
  
  
  
  let keychain = KeychainSwift()

  
  
  //New 1.0.18
  @IBOutlet weak var lastImage: UIImageView!
  @IBOutlet weak var lastPhotoLabel: UILabel!
  var handleImages: UInt!
  var refImages : DatabaseReference!
  var handle: UInt!
  
  @IBOutlet weak var backgroundImage: UIImageView!
  
  var picker = UIImagePickerController()
  weak var tapGestureZoom = UITapGestureRecognizer()

  var cameraAllowed : Bool = false
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = false
  var pausedButtonPressed : Bool = true
  let locationManager = CLLocationManager()
  var address: [String : String] = [:]
  var full_address: [String] = []
  var full_address_str: String = ""
  var full_address_str_bigger: String = ""
  var updateSecond : Timer!
  var time : Timer!
  var hasLaunched : Bool = true
  var errorMessage : String?
  let defaults = UserDefaults.standard
  var imageTaken: UIImage!
  let DATA_FORMAT = [0 : "DMS-WGS84", 1 : "DM-WGS84", 2: "DD-WGS84", 3: "UTM-WGS84", 4: "UTM-ETRS89", 5: "UTM-WGS84", 6: "UTM-UK-GREF"]
  let GPS_UPDATE_TIME = [0:1.0, 1:5.0, 2:10.0, 3:20.0, 4:30.0, 5:60.0]
  let DATE_FORM = [0: "dd.MM.yyyy", 1: "MM/dd/yyyy", 2: "dd/MM/yyyy", 3: "yyyy/MM/dd"]
  let TIME_FORM = [0: "h:mm:ss a", 1: "HH:mm:ss"]
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    //locationManager.startUpdatingLocation()
   changeBackgroundColor()

    keychain.set(true, forKey: "purchased")

    
    
    let one = "String"
    let two = [1, 2, 3]
    
    print("a string: ", checkType(varible: one as AnyObject))
    print("a integer: ", checkType(varible: two as AnyObject))
    
    initializeLocationManager()

    navigationController?.setToolbarHidden(true, animated: false)

    updateTime()
    startTimer()
    setUserStatus()
    
    startObservers()
   
    // 1.0.18
    lastPhotoSettings()

    
    if let image = UIImage(named: "pause14") {
      pauseButton.setImage(image, for: .normal)
    }
    
  }
  
  func checkType(varible: AnyObject) -> String{
     if varible is String {
      return "yes is a String"
     } else if (varible is Array<UInt8>) {
      return "yes is a Array"
     }
    
    return ""
  }
  
  func changeBackgroundColor(){
    let screenshot = defaults.integer(forKey: "backgroundColor")
    
    switch screenshot {
    case 0:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 0")
    case 1:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 1")
    case 2:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 2")
    case 3:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 3")
    default:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 0")
    }
    
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    //let encryptedValue: String = "YkBYAvZ7f5F5wcBu4dv8eYJVP32B5B9lj3gFnYWA/yc="
    //let key: NSString = "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw"
    //let iv: NSString = "gqLOHUioQ0QjhuvI"
    
    //decryption
//    do {
//    
//      let aes = try AES(key: "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw", iv: "gqLOHUioQ0QjhuvI", blockMode: .PCBC, padding: PKCS7()) // aes128
//      let ciphertext = try aes.decrypt(Array<UInt8>(hex: encryptedValue))
//      print("ciphertext")
//      let str = String(data: Data(ciphertext), encoding: String.Encoding.utf8)
//      print("string: ", str)
//      print(ciphertext.toHexString())
//      
//    } catch {
//      print(error)
//    }
    
    //if AuthProvider.Instance.logout() {
    //  Helpers.Instance.logoutFacebook()
    //}
    
    if let id = Auth.auth().currentUser?.uid {
      let ref = Database.database().reference().child(id).child(Const.IMAGES_DATA)
      ref.keepSynced(true)
    }
    

    self.navigationController?.navigationBar.tintColor = Const.RED
    self.navigationController?.setNavigationBarHidden(true, animated: true)
    magicLabel.isHidden = true
    magicToolImage.isHidden = true
    
    
    hasLaunched = defaults.bool(forKey: "HasLaunched")
    if hasLaunched {
      albumNameLabel.text = getSelectedAlbumName()
      gpsDetailsLabel.text = (full_address_str).components(separatedBy: "|").joined(separator: "\n")
    }

    cameraButton.isHidden = false
    pauseButton.isHidden = false
    
    errorMessage = "No Internet or Server connection"
    full_address_str = "Loading..."
    
    firstRun()
    
    startTimer()
    gpsDetailsLabel.text = "Loading..."
    gpsDetailsLabel.isUserInteractionEnabled = true
    gpsDetailsLabel.isSelectable = true
    
    let aSelector : Selector = #selector(ViewController.TapFunc)
    let tapGestureZoom = UITapGestureRecognizer(target: self, action: aSelector)
    tapGestureZoom.numberOfTapsRequired = 2
    gpsDetailsLabel.addGestureRecognizer(tapGestureZoom)
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  
 
  
  func initializeLocationManager() {
    locationManager.delegate = self
    locationManager.requestLocation()
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    locationManager.stopUpdatingLocation()
    stopObservers()

  }
  
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    print("Country: ")

    CLGeocoder().reverseGeocodeLocation(location!, completionHandler: {(placemarks, error) -> Void in
      if error != nil {
        
        if self.defaults.string(forKey: "countryCode") != nil {
          self.setMagicToolText(location: location!)
        }
        
        print("Reverse geocoder failed with error: " + (error?.localizedDescription)!)
        return
      }
      
      if (placemarks?.count)! > 0 {
        let pm = (placemarks?[0])! as CLPlacemark
        self.defaults.set(pm.isoCountryCode ?? "", forKey: "countryCode")
      }
      else {
        print("Problem with the data received from geocoder")
      }
    })
    
    setMagicToolText(location: location!)
  }
  
  func setMagicToolText(location : CLLocation){
    handlerCoords(location: location)
    let countryCode = defaults.string(forKey: "countryCode")
    if countryCode == "DK" {
      magicLabel.isHidden = false
      magicToolImage.isHidden = false
      Helpers.Instance.getMagicToolText((location.coordinate.latitude), (location.coordinate.longitude)) { (text) in
        DispatchQueue.main.async(execute: {
          self.magicLabel.text = text
        })
        
      }
    } else{
      magicLabel.isHidden = true
      magicToolImage.isHidden = true
    }

  }
  
  
  @IBAction func goToSetting(_ sender: Any) {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: "setting", sender: self)
    }
  }

  @IBAction func goToTools(_ sender: Any) {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: "tools", sender: self)
    }
    
  }
  
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    DispatchQueue.main.async(execute: { () -> Void in
      self.gpsDetailsLabel.text = ErrorMessage.NO_GPS
    })
    print("Failed to find user's location: ", error.localizedDescription)
  }
  
  
  func setUserStatus(){
    userStatusImage.isUserInteractionEnabled = true

    if Auth.auth().currentUser == nil {
      if (keychain.getBool("purchased") ?? false) {
        userStatusImage.isHidden = false
        userStatusImage.image = UIImage(named: "offline")
        userStatusImage.isUserInteractionEnabled = true
        let aSelector : Selector = #selector(ViewController.goToSignInVC)
        let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
        tapGesture.numberOfTapsRequired = 1
        userStatusImage.addGestureRecognizer(tapGesture)
      } else {
        userStatusImage.isHidden = true
      }
      
    } else {
      userStatusImage.isUserInteractionEnabled = true
      let aSelector : Selector = #selector(ViewController.goToAccountVC)
      let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
      tapGesture.numberOfTapsRequired = 1
      userStatusImage.addGestureRecognizer(tapGesture)
      
      if let url = Helpers.Instance.getInitialProfile()?.profileImageURL {
        if url != "" {
          userStatusImage.downloadedFrom(link: url) //sd_setImage(with: URL(string: url))
          userStatusImage.layer.cornerRadius = userStatusImage.frame.size.width / 2.0
          userStatusImage.clipsToBounds = true
        } else {
          userStatusImage.image = UIImage(named: "online")
        }
      } else {
        userStatusImage.image = UIImage(named: "online")
      }
      userStatusImage.isHidden = false
    }
  }
  
  
  @objc func goToAccountVC() {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: SegueTo.ACCOUNT, sender: self)
    }
  }
  
  @objc func goToSignInVC() {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
    }
  }
  
  @objc func TapFunc() {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: SegueTo.ZOOM, sender: self)
    }
  }
  
  @objc func goToSuscribeVC() {
    DispatchQueue.main.async {
      self.performSegue(withIdentifier: SegueTo.SUSCRIBE, sender: self)
    }
  }
  

  func firstRun(){
    if !hasLaunched {
      DBProvider.Instance.restore()
      Helpers.Instance.setFirstDefaults()
      
      defaults.set(Helpers.Instance.countFiles(), forKey: "numberOfFiles")
      
      let firstAlbumName = defaults.string(forKey: "appName")!
      keychain.set("1", forKey: firstAlbumName)
    
      if let _ = Auth.auth().currentUser?.uid {
        defaults.set(true, forKey: "restored")
        DBProvider.Instance.restoreDefinedFinds()
        print("logueado")
      } else {
        Helpers.Instance.saveDefinedFinds()
      }
     
    }
  }
  
  
  func createFirstAlbum() {
    let albumName = "iSmartDetect"
    albumNameLabel.text = albumName
    defaults.set(albumName, forKey: "albumName")
  }
  
  @objc func updateTime(){
    print(defaults.string(forKey: "countryCode") ?? "")
    
    let currentDate = Date()
    let dateFormatter = DateFormatter()
    lastPhotoSettings()
    
    dateFormatter.dateFormat = dateFormat()
    let convertedDate = dateFormatter.string(from: currentDate as Date)
    
    address["Timestamp"] = convertedDate
    
    let dateArray = convertedDate.components(separatedBy: " - ")
    
    dateLabel.text = dateArray[0]
    hourLabel.text = dateArray[1]
    
    locationManager.startUpdatingLocation()
    
    gpsDetailsLabel.text = (full_address_str).components(separatedBy: "|").joined(separator: "\n")
    
    let currentAlbum = getSelectedAlbumName() // 
    let allAlbums = Helpers.Instance.albumList() //albumList().list
    defaults.set(allAlbums, forKey: "allAlbums")
    
    if currentAlbumWasDeleted(currentAlbumName: currentAlbum) {
      let defaultAlbum = Const.APP_NAME
      defaults.set(defaultAlbum, forKey: "albumName")
      if !Helpers.Instance.defaulfAlbumExist() {
        print("Don't nothing")
      } else {
        print("iSmartDetect already exist. Do not nothing.")
      }
    }
    albumNameLabel.text = getSelectedAlbumName()
  }
  

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "ImageViewSG" == segue.identifier {
      let DestViewController : ImageViewController = segue.destination as! ImageViewController
      DestViewController.imagePassed = imageTaken
      DestViewController.gpsInfo = address
      
    } else if "ZoomSG" == segue.identifier {
      let ZoomController : ZoomController = segue.destination as! ZoomController
      ZoomController.textP = full_address_str_bigger.components(separatedBy: "|").joined(separator: "\n")
    } else if "viewPhotos" == segue.identifier {
      let nav = segue.destination as! UINavigationController
      nav.navigationBar.backItem?.title = "Back"
      //let GoogleMapsVC = nav.topViewController as! GoogleMapVC
      //GoogleMapsVC.album = getSelectedAlbumName()
      
      let DestVC = nav.topViewController as! ImagesCVC
      DestVC.album = getSelectedAlbumName()
      DestVC.appName = Const.APP_NAME
      
    }

  
  }
  

  @IBAction func pausePressed(_ sender: UIButton) {
    if pausedButtonPressed {
      stopTimer()
      locationManager.stopUpdatingLocation()
      pausedButtonPressed = false
      
      if DeviceType.IS_IPHONE_4_OR_LESS {
        if let image = UIImage(named: "refresh-small") {
          pauseButton.setImage(image, for: .normal)
        }
      } else {
        if let image = UIImage(named: "refresh36") {
          pauseButton.setImage(image, for: .normal)
        }
      }
      
    } else {
      
      if DeviceType.IS_IPHONE_4_OR_LESS {
        if let image = UIImage(named: "pause-small") {
          pauseButton.setImage(image, for: .normal)
        }
      } else {
        if let image = UIImage(named: "pause14") {
          pauseButton.setImage(image, for: .normal)
        }
      }
      
      locationManager.startUpdatingLocation()
      pausedButtonPressed = true
      
      startTimer()
    }
  }
  
  @IBAction func cameraButtonAction(_ sender: UIButton) {
    let status = PHPhotoLibrary.authorizationStatus()
    switch status {
    case .authorized:
      self.showNotLoginAlert()
      print(".authorized")
    case .denied, .restricted:
      self.showPhotoLibraryAlert()
      print(".denied, .restricted")
    case .notDetermined:
      print("notDetermined")
      PHPhotoLibrary.requestAuthorization() { status in
        switch status {
        case .authorized:
          self.showNotLoginAlert()
        case .denied, .restricted:
          self.showPhotoLibraryAlert()
          print(".denied, .restricted after request")
        case .notDetermined:
          break
          // won't happen
        }
      }
    }
  }
  
  
  func cameraPicker() {
    stopTimer()
    //stopObservers()
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .camera
    picker.cameraFlashMode = .off
    checkCamera()
    DispatchQueue.main.async {
      self.present(self.picker, animated: true, completion: nil)
    }
  }
  
  
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }
  
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    self.imageTaken = (info[UIImagePickerControllerEditedImage] as? UIImage)!; dismiss(animated: true, completion: nil)
    performSegue(withIdentifier: "ImageViewSG", sender: self)
  }

  
  func checkFirstAlbumExist(albumName: String) -> Bool {
    return Helpers.Instance.albumList().contains(albumName) ? true : false
  }
  
  
  func currentAlbumWasDeleted(currentAlbumName: String) -> Bool {
    let allAlbums = Helpers.Instance.albumList()
    // false primero, porque no lo contiene
    return allAlbums.contains(currentAlbumName) ? false : true
  }
  
  
  func checkCamera() {
    let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
      switch authStatus {
      case .authorized: break // Do your stuff here i.e. allowScanning()
      case .denied: alertToEncourageCameraAccessInitially()
      case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
      default: alertToEncourageCameraAccessInitially()
    }
  }
  
  func alertToEncourageCameraAccessInitially() {
    let alert = UIAlertController(
      title: "Action Required!",
      message: "Camera access required for this app main function.",
      preferredStyle: UIAlertControllerStyle.alert
    )
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "Allow Camera", style: .default, handler: { (alert) -> Void in
      UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
    }))
    present(alert, animated: true, completion: nil)
  }
  
  
  func alertPromptToAllowCameraAccessViaSetting() {
    let alert = UIAlertController(
      title: "Camera Access",
      message: "Please allow camera access for this app, in the following Message Box.",
      preferredStyle: UIAlertControllerStyle.alert
    )
    alert.addAction(UIAlertAction(title: "Ok", style: .cancel) { alert in
      if AVCaptureDevice.devices(for: AVMediaType.video).count > 0 {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
          DispatchQueue.main.async() {
            self.checkCamera() } }
      }
      }
    )
    present(alert, animated: true, completion: nil)
  }

  
  func showPhotoLibraryAlert(){
    let alert = UIAlertController(
      title: "Action Required!",
      message: "Photo Library access required for this app.",
      preferredStyle: UIAlertControllerStyle.alert
    )
       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) -> Void in
      UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
    }))

    present(alert, animated: true, completion: nil)
  }


  
  
func LLtoNE(latitude : Double, longitude: Double) -> (northing : String, eastring: String){
  let deg2rad = Double.pi / 180.0

  let WGS84_AXIS = 6378137;
  let WGS84_ECCENTRIC = 0.00669438037928458;
  let OSGB_AXIS = 6377563.396;
  let OSGB_ECCENTRIC = 0.0066705397616;
  let nLat = latitude;  // 51.5 degrees north
  let nLon = longitude;  // 0.5 degrees west (negative for west)
  let phip = nLat * deg2rad;
  let lambdap = nLon * deg2rad;
  
  let geo = transform(phip, lambdap, Double(WGS84_AXIS), WGS84_ECCENTRIC, 0, OSGB_AXIS, OSGB_ECCENTRIC, -446.448, 125.157, -542.06, -0.1502, -0.247, -0.8421, 20.4894)
  
  
  let phi = geo.latitude  // * deg2rad      // convert latitude to radians
  let lam = geo.longitude //  * deg2rad   // convert longitude to radians
  let a = 6377563.396       // OSGB semi-major axis
  let b = 6356256.909   // OSGB semi-minor axis
  let e0 = 400000.0           // OSGB easting of false origin
  let n0 = -100000          // OSGB northing of false origin
  let f0 = 0.9996012717     // OSGB scale factor on central meridian
  let e2 = 1 - (b*b)/(a*a) //0.0066705397616  // OSGB eccentricity squared
  let lam0 = -2 * deg2rad  // OSGB false east
  let phi0 = 49 * deg2rad   // OSGB false north
  let  af0 = a * f0
  let  bf0 = b * f0
  
  // easting
  let slat2 = sin(phi) * sin(phi)
  let nu = af0 / sqrt(1 - e2 * slat2)
  let rho = (af0 * (1 - e2)) / pow((1 - e2 * slat2), 1.5)
  let eta2 = (nu / rho) - 1
  
  let clat3 = cos(phi) * cos(phi) * cos(phi) //pow(cos(phi),3)
  let tlat2 = tan(phi) * tan(phi)
  let clat5 = cos(phi) * cos(phi) * cos(phi) * cos(phi) * cos(phi) //pow(cos(phi), 5)
  let tlat4 = tan(phi) * tan(phi) * tan(phi) * tan(phi) //pow(tan(phi), 4)
  
  
  // northing
  let n = (a - b) / (a + b)
  let M = Marc(bf0, n, phi0, phi)
  
  let I = M + Double(n0)
  let II = (nu / 2.0) * sin(phi) * cos(phi)
  let III = ((nu / 24.0) * sin(phi) * clat3 * (5 - tlat2 + (9 * eta2)))
  let IIIA = ((nu / 720.0) * sin(phi) * clat5) * (61 - (58 * tlat2) + tlat4)
  let IV = nu * cos(phi)
  let V = (nu / 6.0) * clat3 * ((nu / rho) - tlat2)
  let VI = (nu / 120.0) * clat5 * ((5.0 - 18.0 * tlat2 + tlat4 + (14.0 * eta2) - (58.0 * tlat2 * eta2)))

  let p = lam - lam0
  let north = I + ((p * p) * II) + (pow(p, 4) * III) + (pow(p, 6) * IIIA)
  let east = e0 + (p * IV) + (pow(p, 3) * V) + (pow(p, 5) * VI)
  
  let northing = String(north);      // convert to string
  let easthing = String(east);       // ditto
  
  print(M)
  
  
  return (northing, easthing)
  
}
  
func Marc(_ bf0: Double, _ n: Double, _ phi0 : Double, _ phi : Double) -> Double {
  let Ma = ((1 + n + ((5.0 / 4.0) * (n * n)) + ((5.0 / 4.0) * (n * n * n))) * (phi - phi0))
  let Mb = (((3 * n) + (3 * (n * n)) + ((21.0 / 8.0) * (n * n * n))) * (sin(phi - phi0)) * (cos(phi + phi0)))
  let Mc = ((((15.0 / 8.0) * (n * n)) + ((15.0 / 8.0) * (n * n * n))) * (sin(2 * (phi - phi0))) * (cos(2 * (phi + phi0))))
  let Md = (((35.0 / 24.0) * (n * n * n)) * (sin(3 * (phi - phi0))) * (cos(3 * (phi + phi0))))
  
  let Marc = bf0 * (Ma - Mb + Mc - Md)
  
  return Marc
}

func NE2NGR(easting: Double, northing: Double) -> String {
  var eX = easting / 500000.0
  var nX = northing / 500000.0
  var tmp = floor(eX)-5.0 * floor(nX)+17.0
  nX = 5 * (nX - floor(nX))
  eX = 20.0 - 5.0 * floor(nX) + floor(5.0 * (eX - floor(eX)))

  if (eX > 7.5) {
    eX = eX + 1
  }
  
  if (tmp > 7.5) {
    tmp = tmp + 1
  }
 
  let eing = String(Int(easting))
  let ning = String(Int(northing))

  let resultEing = eing.substring(from:eing.index(eing.endIndex, offsetBy: -5))
  let resultNing = ning.substring(from:ning.index(ning.endIndex, offsetBy: -5))

  let ngr = String(describing: UnicodeScalar(Int(tmp) + 65)!) + String(describing: UnicodeScalar(Int(eX) + 65)!) + " " + String(describing: resultEing) + " " + String(resultNing)
  return ngr

  }
    
  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateTime), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }

}

extension ViewController {
  
  func getSelectedAlbumName() -> String {
    if let albumName = defaults.string(forKey: "albumName") {
      return albumName
    } else {
      return Const.APP_NAME
    }
  }
  

  func saveDefinedFind(index: Int, text: String, letter: String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let newDefinedFind = NSEntityDescription.insertNewObject(forEntityName: Const.DEFINED_FIND_ENTITY, into: manageContext)
    
    
    let key = Database.database().reference().child(Const.NOTES).childByAutoId().key
    let definedFind = DefinedFind(index: index, letter: letter, text: text, key: key)
    
    newDefinedFind.setValuesForKeys(definedFind.toDictionary())
    
    
    do{
      try manageContext.save()
      print("DefinedFind saved on CoreData.")
      
    }catch {
      print("error")
    }
  }


  func randomString(length: Int) -> String {
    
    let letters : NSString = "0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
      let rand = arc4random_uniform(len)
      var nextChar = letters.character(at: Int(rand))
      randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
  }
  
}

extension ViewController {
  func transform(_ lat : Double, _ lon : Double, _ a : Double, _ e : Double, _ h : Double, _ a2 : Double, _ e2 : Double, _ xp : Double, _ yp : Double, _ zp : Double, _ xr : Double, _ yr : Double, _ zr : Double, _ s : Double) -> (latitude: Double, longitude: Double, height: Double) {
    // convert to cartesian; lat, lon are radians
    let deg2rad = Double.pi / 180.0
    
    let sf = s * 0.000001;
    var v = a / (sqrt(1 - (e * sin(lat) * sin(lat) )));
    let x = (v + h) * cos(lat) * cos(lon);
    let y = (v + h) * cos(lat) * sin(lon);
    let z = ((1 - e) * v + h) * sin(lat);
    // transform cartesian
    let xrot = (xr / 3600) * deg2rad;
    let yrot = (yr / 3600) * deg2rad;
    let zrot = (zr / 3600) * deg2rad;
    let hx = x + (x * sf) - (y * zrot) + (z * yrot) + xp;
    let hy = (x * zrot) + y + (y * sf) - (z * xrot) + yp;
    let hz = (-1 * x * yrot) + (y * xrot) + z + (z * sf) + zp;
    
    // Convert back to lat, lon
    let lon = atan(hy / hx);
    let p = sqrt((hx * hx) + (hy * hy));
    var lat = atan(hz / (p * (1 - e2)));
    v = a2 / (sqrt(1 - e2 * (sin(lat) * sin(lat))));
    var errvalue = 1.0;
    var lat0 = 0.0;
    
    while (errvalue > 0.001){
      lat0 = Double(atan((hz + e2 * v * sin(lat)) / p));
      errvalue = abs(lat0 - lat);
      lat = lat0;
    }
    
    let h = p / cos(lat) - v;
    //var geo = { latitude: lat, longitude: lon, height: h };  // object to hold lat and lon
    return (lat, lon, h);
  }

}

// 1.0.18
extension ViewController {
  func lastImageURL() -> String {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", getSelectedAlbumName() )
    request.predicate = predicate
    
    
    do {
      let results = try manageContext.fetch(request)
      let last = (results as! [ImageData]).last?.url
      return String(last!)
      
    } catch {
      print("error")
    }
    return ""
  }
  
  //new method
  func lastPhoto() -> UIImage? {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", getSelectedAlbumName() )
    request.predicate = predicate
    
    
    do {
      
      if let results = try? manageContext.fetch(request) {
        if results.count > 0 {
          if let last = (results as! [ImageData]).last?.imgData {
            return UIImage(data: last as Data)!
          }
        }
      }
      
    } catch {
      print("Error")
    }
    
    return nil
  }

  
  
//  func setLastPhoto(){
//    DispatchQueue.main.async(execute: {
//      self.lastImage.sd_setShowActivityIndicatorView(true)
//      if self.lastImageURL() != "" {
//        self.lastPhotoLabel.isHidden = false
//        self.lastImage.image = self.lastPhoto()
//        //self.lastImage.sd_setImage(with: URL(string: self.lastImageURL()))
//      } else {
//        self.lastPhotoLabel.isHidden = true
//      }
//      
//      self.lastImage.sd_setShowActivityIndicatorView(false)
//      self.lastImage.layer.cornerRadius = 80 / 2.0
//      self.lastImage.clipsToBounds = false
//      
//      self.lastImage.layer.masksToBounds = false
//      self.lastImage.layer.cornerRadius = 8
//      self.lastImage.layer.shadowOffset = CGSize(width: 3, height: 3)
//      self.lastImage.layer.shadowColor = Const.RED.cgColor
//      self.lastImage.layer.shadowRadius = 5
//      self.lastImage.layer.shadowOpacity = 0.5
//      
//      
//      self.lastImage.isUserInteractionEnabled = true
//      let aSelector : Selector = #selector(self.goToGoogleMaps)
//      let tapGestureZoom = UITapGestureRecognizer(target: self, action: aSelector)
//      tapGestureZoom.numberOfTapsRequired = 1
//      self.lastImage.addGestureRecognizer(tapGestureZoom)
//    })
//    
//  }
  
  func setLastPhoto(){
    DispatchQueue.main.async(execute: {
      self.lastImage.sd_setShowActivityIndicatorView(true)
      if Helpers.Instance.records(for: self.getSelectedAlbumName()) > 0  {
        self.lastPhotoLabel.isHidden = false
        
        
        if self.lastImageURL() != "" {
          self.lastImage.sd_setImage(with: URL(string: self.lastImageURL()))
        } else {
          self.lastImage.image = self.lastPhoto()
        }
        
       
        
        self.lastImage.sd_setShowActivityIndicatorView(false)
        self.lastImage.layer.cornerRadius = 80 / 2.0
        self.lastImage.clipsToBounds = false
        
        self.lastImage.layer.masksToBounds = false
        self.lastImage.layer.cornerRadius = 8
        self.lastImage.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.lastImage.layer.shadowColor = UIColor.black.cgColor
        self.lastImage.layer.shadowRadius = 5
        self.lastImage.layer.shadowOpacity = 0.5
        
        
        self.lastImage.isUserInteractionEnabled = true
        let aSelector : Selector = #selector(self.goToGoogleMaps)
        let tapGestureZoom = UITapGestureRecognizer(target: self, action: aSelector)
        tapGestureZoom.numberOfTapsRequired = 1
        self.lastImage.addGestureRecognizer(tapGestureZoom)
        
        
      } else if Helpers.Instance.records(for: self.getSelectedAlbumName()) == 0 || self.lastPhoto() == nil || self.lastImageURL() == "" {
        self.lastPhotoLabel.isHidden = true
        self.lastImage.isHidden = true
      }
      
    })
    
  }

  
  
  func goToGoogleMaps(){
    print("viewPhotos")
    performSegue(withIdentifier: "viewPhotos", sender: self)
  }
  


  func lastPhotoSettings(){
    if Helpers.Instance.records(for: getSelectedAlbumName()) > 0 {
      lastPhotoLabel.isHidden = false
      lastImage.isHidden = false
      setLastPhoto()
    } else {
      lastPhotoLabel.isHidden = true
      lastImage.isHidden = true
    }
  }
  
  func startObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
        let respondeImages = self.observerImages()
        self.handleImages = respondeImages.handle
        self.refImages = respondeImages.ref
        
        //        let respondeImages = ObserversProvider.Instance.observer(child: Const.IMAGES_DATA, option: .image)
      //        self.handleImages = respondeImages.handle
      //        self.refImages = respondeImages.ref
      }
    }
  }
  
  func stopObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
        self.refImages.removeObserver(withHandle: self.handleImages!)
      }
    }
  }

  
}

//1.0.20
extension ViewController {
  func observerImages() -> (handle: UInt?, ref: DatabaseReference?) {
    var ref : DatabaseReference!
    
    if let id = Auth.auth().currentUser?.uid {
      ref = Database.database().reference().child(id).child(Const.IMAGES_DATA)
      ref.keepSynced(true)
      
      handle = ref.queryOrderedByValue().queryLimited(toLast: 5).observe(.childAdded, with: { (snapshot) in
        let image = Image(snapshot: snapshot)
        ObserversProvider.Instance.add(new: image, option: .image)
      }, withCancel: nil)
    } else {
      print("User not log in.")
    }
    
    return (handle, ref)
  }
}

extension ViewController {
  func showNotLoginAlert() {
    var firstDate = Date()
    
    if let initialDate = defaults.object(forKey: "initialDate") {
      firstDate = initialDate as! Date
    } else {
      firstDate = Date()
    }
    
    defaults.string(forKey: "dontShowAlert" )
    let loggedOut = Auth.auth().currentUser == nil ? false : true
    
    let purchased = keychain.getBool("purchased") ?? false
    let daysPassed = Date().seconds(from: firstDate)
    let showAlert = defaults.bool(forKey: "dontShowAlert")
    let notShowAlertForever = defaults.bool(forKey: "dontShowAlertForever")
    
    print("showAlert: ", showAlert)
    print("loggedOut: ", loggedOut)
    print("daysPassed: ", daysPassed)
    
    if notShowAlertForever {
      cameraPicker()
    }
    
    if !loggedOut && purchased {
      if defaults.bool(forKey: "dontShowAlert" ) && daysPassed < 23 {
        cameraPicker()
      }
      
      if daysPassed > 23 || !showAlert || !notShowAlertForever {
        DispatchQueue.main.async(execute: { () -> Void in
          let error = UIAlertController(title: "WARNING!", message: "You are not log in, photos when you are log out will not sync after you log in again, to avoid lose photos please log in.", preferredStyle: UIAlertControllerStyle.alert)
          
          error.addAction(UIAlertAction(title: "Login", style: .default, handler: { (alert) in
            self.performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
          }))
          
          error.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (result) in
            self.defaults.set(false, forKey: "dontShowAlert" )
            self.cameraPicker()
          }))
          
          
          error.addAction(UIAlertAction(title: "Dismiss for a day", style: .default, handler: { (result) in
            
            self.defaults.set(true, forKey: "dontShowAlert" )
            self.defaults.set(Date(), forKey: "initialDate" )
            self.cameraPicker()
          }))
          
          error.addAction(UIAlertAction(title: "Dismiss forever", style: .default, handler: { (result) in
            //self.defaults.set(true, forKey: "dontShowAlert" )
            self.defaults.set(true, forKey: "dontShowAlertForever")
            self.cameraPicker()
          }))
          
          self.present(error, animated: true, completion: nil)
        })
      } else {
      
        print("Nothing")
      }
    } else {
      cameraPicker()
    }
  }
  
  
  
}



