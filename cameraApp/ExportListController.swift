//  ExportListController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/2/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.


import Foundation
import Photos
import CoreData
import FirebaseDatabase
import FirebaseAuth
import KeychainSwift


@objc class ExportListController : UITableViewController, UISearchResultsUpdating {
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = false
  var updateSecond : Timer!
  var activeSelection : Bool = true
  let keychain = KeychainSwift()

  var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
  
  let defaults = UserDefaults.standard
  var names = [String]()
  var albumName : String!
  var arrayResult = [String]()
  var responde = ""
  var albumFiltered = [String]()
  
  var searchController : UISearchController!
  let resultController = UITableViewController()
  
  
  var handleImages: UInt!
  var refImages : DatabaseReference!
  var handleAlbums : UInt!
  var refAlbums : DatabaseReference!
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    names = []
    fetchAlbums()
    
    self.tableView.reloadData()

    navigationController?.setToolbarHidden(true, animated: true)
  }
  

//  func startObservers(){
//    DispatchQueue.global(qos: .userInteractive).async {
//      if let _ = Auth.auth().currentUser?.uid {
//        let respondeImages = ObserversProvider.Instance.observer(child: Const.IMAGES_DATA, option: .image)
//        let respondeAlbums = ObserversProvider.Instance.observer(child: Const.ALBUMS, option: .album)
//        self.handleImages = respondeImages.handle
//        self.refImages = respondeImages.ref
//        self.handleAlbums = respondeAlbums.handle
//        self.refAlbums = respondeAlbums.ref
//      }
//    }
//  }
//
//
//  func stopObservers(){
//    DispatchQueue.global(qos: .background).async {
//      if let _ = Auth.auth().currentUser?.uid {
//        self.refImages.removeObserver(withHandle: self.handleImages!)
//        self.refAlbums.removeObserver(withHandle: self.handleAlbums!)
//      }
//    }
//  }

  func startObservers(){
    DispatchQueue.global(qos: .userInteractive).async {
      if let _ = Auth.auth().currentUser?.uid {
        
        let resultAlbum = self.observerAlbums()
        self.handleAlbums = resultAlbum.handle
        self.refAlbums = resultAlbum.ref
      }
    }
    
  }
  
  
  func stopObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
        // self.refImages.removeObserver(withHandle: self.handleImages!)
        self.refAlbums.removeObserver(withHandle: self.handleAlbums!)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //startObservers()
    
    startTimer()
    
    self.resultController.tableView.delegate = self
    self.resultController.tableView.dataSource = self
    
    self.searchController = UISearchController(searchResultsController: self.resultController)
    self.tableView.tableHeaderView = self.searchController.searchBar
    self.searchController.searchResultsUpdater = self
    definesPresentationContext = true
    
    setSelectButton()
    
    startObservers()
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    stopTimer()
    stopObservers()
    
  }
  
  
 @objc func fetching(){
    names = []
    fetchAlbums()
    self.tableView.reloadData()
  }
  
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableView {
      return self.names.count
    } else {
      return self.albumFiltered.count
    }

  }
  
  
  func updateSearchResults(for searchController: UISearchController) {
    self.albumFiltered = self.names.filter({ (album: String) -> Bool in
      if album.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
        return true
      } else {
        return false
     }
    })
    
    self.resultController.tableView.reloadData()
  }
  

  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell
    
    if tableView == self.tableView {
      let albumName = self.names[indexPath.row]
      cell.textLabel!.text = albumName
      cell.detailTextLabel!.textColor = UIColor.lightGray
      cell.detailTextLabel!.text = "\(records(for: albumName)) files"
    } else {
      let albumName = self.albumFiltered[indexPath.row]
      cell.textLabel!.text = albumName
      cell.detailTextLabel!.textColor = UIColor.lightGray
      cell.detailTextLabel?.text = "\(records(for: albumName)) files"
    }
    
   cell.tintColor = Const.RED
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if activeSelection{
      self.tableView.deselectRow(at: indexPath, animated: true)
      if tableView == self.tableView {
        self.albumName = self.names[indexPath.row]
        performSegue(withIdentifier: "exportDetails", sender: self)
      } else {
        self.albumName = self.albumFiltered[indexPath.row]
        performSegue(withIdentifier: "exportDetails", sender: self)
      }
    } else {
      let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
      let optimizeButton = UIBarButtonItem(title: "Optimize", style: .plain, target: self, action: #selector(optimizeAlbum))
      optimizeButton.tintColor = Const.RED
      
      if tableView.indexPathsForSelectedRows?.count == 1 {
        let mergeButton = UIBarButtonItem(title: "Duplicate Project", style: .plain, target: self, action: #selector(merge))
        mergeButton.tintColor = Const.RED
        
        toolbarItems = [mergeButton]
        navigationController?.setToolbarHidden(false, animated: true)
      } else {
        let mergeButton = UIBarButtonItem(title: "Combine Projects", style: .plain, target: self, action: #selector(merge))
        mergeButton.tintColor = Const.RED
        toolbarItems = [mergeButton]
        navigationController?.setToolbarHidden(false, animated: true)
      }
    }
  }



  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "exportDetails" == segue.identifier {
          let ExportTV : GoogleMapVC = segue.destination as! GoogleMapVC
          ExportTV.album = albumName
    }
  }
  

  
  
  
  func albumIndex(name: String) -> Int {
    return albumArray().list.index(of: name)!
  }
  
  func albumArray() -> (list: [String], size: Int) {
    let albumList : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
    let albumSize : Int = albumList.count
    var tempArray : [String] = []
    
    for index in 1..<albumSize {
      tempArray.append([albumList[index].localizedTitle!][0])
    }
    return (tempArray, albumSize)
  }
  

  
  func fetchData() -> String{
    responde = ""
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 {
      responde = responde + "id, easting, northing, zone, accuracy, coorSys, date, albumName, predefinedText" + "\n"
    } else {
      responde = responde + "id, longitude, latitude, zone, accuracy, coorSys, date, albumName, predefinedText" + "\n"
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageData")
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", albumName)
    request.predicate = predicate
    
    
    do {
      let results = try manageContext.fetch(request)
      for result in results as! [ImageData]{
        responde = responde + Helpers.Instance.respondInCommasForm(for: result)
      }
  
    } catch {
      print("error")
    }
    return responde
    
  }

  
  // MARK: CSV export function
  func gpsInfoStr(texts: ImageData) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    
    var accuStr = ""
    
    if texts.acurracy == "1" {
      accuStr  = "<b>Accuracy:</b> Added"
    } else {
      accuStr  = "<b>Accuracy:</b> ± \(texts.acurracy!)m"
    }
    
    let datumStr = "CoordSys: \(texts.coordinateSystem!)"
    
    let dateStr = "\(texts.timestamp!)"
    let symbolText = "Defined Find: \(texts.symbolText!)"
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 || datum == 6 {
      //inverted to put East up and Nor down
      lonStr = "Northing: \(texts.latitude!)"
      latStr = "Easting: \(texts.longitude!)"
      zoneStr = "Zone: \(texts.gZone!)"
      
      result =  "" + dateStr + "\n\n" +
        "Project Name: \(albumName!) \n" +
        accuStr + "\n" +
        latStr + "\n" +
        lonStr + "\n" +
        zoneStr + "\n" +
        datumStr + "\n" +
        symbolText + "\n" +
        (texts.note!.isEmpty ? "" : "Predefined Text:  \(texts.note!)")
      
    } else {
      latStr = "Latitude: \(texts.latitude!)"
      lonStr = "Longitude: \(texts.longitude!)"
      zoneStr = ""
      
      
      result =  "" + dateStr + "\n\n" +
        "Project Name: \(albumName!) \n" +
        accuStr + "\n" +
        latStr + "\n" +
        lonStr + "\n" +
        datumStr + "\n" +
        symbolText + "\n" +
        (texts.note!.isEmpty ? "" : "Predefined Text:  \(texts.note!)")
      
    }
    
    return result
  }

  
  func hasRecords(albumName: String) -> Bool {
    var hasRecord : Bool!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", albumName)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      if results.count > 0 {
        hasRecord = true
      } else{
        hasRecord =  false
      }
    } catch {
      print("error")
    }
    return hasRecord
  }

  
  func records(for album: String) -> Int {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      return results.count
    } catch {
      print("error")
    }
    return 0
  }
  
  
  func fetchAlbums() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    
    let sortDescriptor = NSSortDescriptor(key: Const.UPDATED_AT, ascending: false)
    request.sortDescriptors = [sortDescriptor]
    
    
    do {
      let albums = try manageContext.fetch(request)
      for album in albums as! [AlbumData]{
        let newAlbum = Album(name: album.name!, key: album.key!, owner: album.owner, createdAt: album.createdAt, updatedAt: album.updatedAt)
        
        let name = newAlbum.name

        if hasRecords(albumName: name) {
          names.append(name)
        }
      }
      
    } catch {
      print("Error fetching all Notes")
    }
  }

  
  //Timers functions
  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ExportListController.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }
  
  
  
}

extension ExportListController {
  func setSelectButton(){
    let selectButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(didDelect))
    selectButton.tintColor = Const.RED
    navigationItem.rightBarButtonItem = selectButton
  }
  
  
  @objc func didDelect(){
    stopTimer()
    
    let mergeButton = UIBarButtonItem(title: "Select Project(s)", style: .plain, target: self, action: #selector(merge))
    mergeButton.tintColor = Const.RED
    toolbarItems = [mergeButton]
    navigationController?.setToolbarHidden(false, animated: true)
    
    let select = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(done))
    select.tintColor = Const.RED
    activeSelection = false
    navigationItem.rightBarButtonItem = select
    
    tableView.allowsSelectionDuringEditing = true
    tableView.allowsMultipleSelectionDuringEditing = true
    tableView.setEditing(true, animated: true)
  }
  
  
  func done(){
    startTimer()
    navigationController?.setToolbarHidden(true, animated: true)
    let selectButton = UIBarButtonItem(title: "Select", style: .done, target: self, action: #selector(didDelect))
    selectButton.tintColor = Const.RED
    navigationItem.rightBarButtonItem = selectButton
    activeSelection = true
    
    tableView.allowsSelectionDuringEditing = false
    tableView.allowsMultipleSelectionDuringEditing = false
    tableView.setEditing(false, animated: true)
    navigationController?.setToolbarHidden(true, animated: true)
    
  }
  
  
  @objc func merge(){
    if let list = tableView.indexPathsForSelectedRows {
      DispatchQueue.main.async {
        self.activityIndicator.center = self.view.center
        self.activityIndicator.color = .black
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)
      }

      
      var name = [String]()
      var indexes = [Int]()
      
      var joinedName = ""
      
      if list.count == 1 {
        joinedName = self.names[(list[0].row)] + " copy"
        indexes.append((list[0].row))
      } else {
        for index in list {
          name.append(self.names[index.row].components(separatedBy: " ")[0])
          indexes.append(index.row)
        }
        joinedName = name.joined(separator: "-")
      }
      var alert = UIAlertController()
      
      if list.count == 1 {
        alert = UIAlertController(title: "Duplicate Project", message: "Please specific a name for the new project or just accept this one.", preferredStyle: .alert)

      } else {
        alert = UIAlertController(title: "Combined Project", message: "Please specific a name for the new project or just accept this one.", preferredStyle: .alert)
      }
      
      alert.addTextField { (textField) in
        textField.text = joinedName
      }
      
      let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
        
        self.stopObservers()
        
        let key = Database.database().reference().child(Const.ALBUMS).childByAutoId().key
        let date = Date().timeIntervalSince1970
        var owner = self.keychain.get("secure_digits") ?? ""
        
        if owner == "" {
          owner = "0000"
        }
        
        let albumName = alert.textFields!.first?.text!
        self.keychain.set("1", forKey: albumName!)
        
        let album = Album(name: albumName!, key: key, owner: owner, createdAt: date, updatedAt: date)
        
        Helpers.Instance.add(new: album)
        self.mergeAlbums(indexes, albumName!)

        DispatchQueue.main.async {
          self.tableView.allowsSelectionDuringEditing = false
          self.tableView.allowsMultipleSelectionDuringEditing = false
          self.tableView.setEditing(false, animated: true)
          self.navigationController?.setToolbarHidden(true, animated: true)
          self.fetching()
        }
        
        self.done()
        self.startObservers()
        
      }
      
      let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        DispatchQueue.main.async {
          self.activityIndicator.stopAnimating()
        }
      })
      
      alert.addAction(cancel)
      alert.addAction(ok)
 
      present(alert, animated: true, completion: nil)
    }
  }
  
  

  
  func mergeAlbums(_ indexes : [Int], _ albumName: String) {
    
    for row in indexes {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
      request.returnsObjectsAsFaults = false
      
      let predicate = NSPredicate(format: "albumName = %@", names[row])
      request.predicate = predicate
      
      
      do {
        let results = try manageContext.fetch(request)

        for result in results as! [ImageData]{
          create(new: result, albumName: albumName)
        }
        
      } catch {
        print("error")
      }
    }
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
    }

  }
  

  func create(new image : ImageData, albumName: String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let newImageData = NSEntityDescription.insertNewObject(forEntityName: Const.IMAGE_ENTITY, into: manageContext)
    
    let key = Database.database().reference().child(Const.IMAGES_DATA).childByAutoId().key
    
    let image = Image(id: Int(image.id), latitude: image.latitude!, longitude: image.longitude!, zone: image.gZone!, accuracy: image.acurracy!, timestamp: image.timestamp!, albumName: albumName, coordinateSystem: image.coordinateSystem!, ownerText: image.note!, key: key, desc : image.desc!, url: image.url!, initials: image.initials!, symbolLetter : image.symbolLetter!, symbolText : image.symbolText, createdAt: image.createdAt, updatedAt: image.updatedAt, imgData: image.imgData!, longDesc : image.longDesc )
    
    newImageData.setValuesForKeys(image.toCoreDataDictionary())
    
    do{
      try manageContext.save()
      DBProvider.Instance.addImageDataToChild(image: image)
      
      print("data saved.")
    }catch {
      print("error")
    }
    
  }
  


  
}

extension ExportListController {
  @objc func optimizeAlbum(){
    if let list = tableView.indexPathsForSelectedRows {
     
     
      
      
      
      let indexes = list.map{$0.row}
      for row in indexes {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let manageContext = appDelegate.managedObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "albumName = %@", names[row])
        request.predicate = predicate
        
        
        DispatchQueue.main.async(execute: {
          self.activityIndicator.center = self.view.center
          self.activityIndicator.color = .black
          self.activityIndicator.startAnimating()
          self.view.addSubview(self.activityIndicator)
        })
        
        do {
          let results = try manageContext.fetch(request)
          
          for result in results as! [ImageData]{
            let image = UIImage(data: result.imgData! as Data)
            
            var compressedImage : UIImage!
            if (image?.size.width != image?.size.height) {
              if Double((image?.size.height)!) <= 1500.0 {
                compressedImage = Helpers.Instance.resizeImage(image: UIImage(data: UIImageJPEGRepresentation(image!, 0.4)!)!,  withSize: CGSize(width: 2000, height: 1000))
              } else {
                compressedImage = Helpers.Instance.resizeImage(image: image!, withSize: CGSize(width: 2000, height: 1000))
              }
              
            } else {
              if Double((image?.size.height)!) <= 1500.0 {
                compressedImage = Helpers.Instance.resizeImage(image: UIImage(data: UIImageJPEGRepresentation(image!, 0.4)!)!, withSize: CGSize(width: 1000, height: 1000))
              } else {
                compressedImage = Helpers.Instance.resizeImage(image: image!, withSize: CGSize(width: 1000, height: 1000))
              }
            }
            update(with: result.key!, and: compressedImage)
            
          }
          
        } catch {
          print("error")
        }
      }
    }
  
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
      self.done()
    }
  }
  
  func update(with key: String, and image: UIImage){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! ImageData
      
      managedObject.setValue(UIImageJPEGRepresentation(image, 1), forKey: Const.IMG_DATA)
      //managedObject.setValue(UIImageJPEGRepresentation(photoView.image!, 1), forKey: Const.IMG_DATA)
      
      
      
      do{
        try manageContext.save()
        
        updateAImageAtFB(with: key)
        print("Image updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
    
   
  
  }
  
  func updateAImageAtFB(with key: String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      if let uid = Auth.auth().currentUser?.uid {
        let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(image.key)
        ref.updateChildValues(image.toDictionary())
      }
      
    } catch {
      print("error")
    }
  }
}

extension ExportListController {
  func observerAlbums() -> (handle: UInt?, ref: DatabaseReference?) {
    var ref : DatabaseReference!
    
    if let id = Auth.auth().currentUser?.uid {
      ref = Database.database().reference().child(id).child(Const.ALBUMS)
      ref.keepSynced(true)
      
      handleAlbums = ref.observe(.childAdded, with: { (snapshot) in
        let album = Album(snapshot: snapshot)
        ObserversProvider.Instance.add(new: album, option: .album)
      }, withCancel: nil)
    } else {
      print("User not log in.")
    }
    
    return (handleAlbums, ref)
  }
}
