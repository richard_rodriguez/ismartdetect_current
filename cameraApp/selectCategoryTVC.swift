//
//  selectCategoryTVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 7/6/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth
import FirebaseDatabase

class selectCategoryTVC: UITableViewController {

  var categories = [String : String]()
  var key = String()
  var categoryText = String()
  var categorySymbol = String()
 

  
  @IBOutlet weak var x: UITextField!
  @IBOutlet weak var a: UITextField!
  @IBOutlet weak var b: UITextField!
  @IBOutlet weak var c: UITextField!
  @IBOutlet weak var d: UITextField!
  @IBOutlet weak var e: UITextField!
  @IBOutlet weak var f: UITextField!
  @IBOutlet weak var o: UITextField!
  @IBOutlet weak var v: UITextField!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    fetchDefinedFind()
    updateTextFields()
    print("key from category")
    print(key)

  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  
  func fetchDefinedFind() {
    
    self.categories =   ["x" : defaults.string(forKey: "x")!,
                         "a" : defaults.string(forKey: "a")!,
                         "b" : defaults.string(forKey: "b")!,
                         "c" : defaults.string(forKey: "c")!,
                         "d" : defaults.string(forKey: "d")!,
                         "e" : defaults.string(forKey: "e")!,
                         "f" : defaults.string(forKey: "f")!,
                         "o" : defaults.string(forKey: "o")!,
                         "v" : defaults.string(forKey: "v")!]
    
  }

  
  func updateTextFields(){
    x.text = categories["x"]
    a.text = categories["a"]
    b.text = categories["b"]
    c.text = categories["c"]
    d.text = categories["d"]
    e.text = categories["e"]
    f.text = categories["f"]
    o.text = categories["o"]
    v.text = categories["v"]
  }

  


  override func numberOfSections(in tableView: UITableView) -> Int {
      return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 9
  }


  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 70
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .checkmark
      categorySymbol = returnLetter(indexPath.row)
      categoryText = categories[categorySymbol]!
      updateAImage()
      navigationController?.popViewController(animated: true)
    }
  }
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .none
    }
  }
  
  
  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! ImageData
      
      managedObject.setValue(categoryText, forKey: "symbolText")
      managedObject.setValue(categorySymbol, forKey: "symbolLetter")
      
      do{
        try manageContext.save()
        
        print("Image updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
  }
  


  
  func returnLetter(_ num: Int) -> String {
    switch num {
    case 0:
      return "x"
    case 1:
      return "a"
    case 2:
      return "b"
    case 3:
      return "c"
    case 4:
      return "d"
    case 5:
      return "e"
    case 6:
      return "f"
    case 7:
      return "o"
    case 8:
      return "v"
    default:
      return ""
    }
  }

  
}
