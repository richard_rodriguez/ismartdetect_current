//
//  ProfilePicture.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/15/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class ProfilePicture: UIImageView {

  
  override func awakeFromNib() {
    self.layoutIfNeeded()
    layer.borderWidth = 3.0
    layer.cornerRadius = 10.0
    layer.borderColor = UIColor.white.cgColor
    layer.masksToBounds = true
  }

  
  
}
