//
//  TableViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 6/30/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import Photos
import FirebaseAuth
import FirebaseDatabase
import KeychainSwift

@objc class AddImageTVC: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
  
  
  var id : Int!
  var buttonIsPressed = false
  var image : Image!
  var key = String()
  var albumName = String()
  let keychain = KeychainSwift()
  var photo = UIImage(named: "tinySquare")
  @IBOutlet weak var textView: UITextView!
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  let defaults = UserDefaults.standard

  let DATA_FORMAT = [0 : "DMS-WGS84", 1 : "DM-WGS84", 2: "DD-WGS84", 3: "UTM-WGS84", 4: "UTM-ETRS89", 5: "UTM-WGS84", 6: "UTM-UK-GREF"]
  let DATE_FORM = [0: "dd.MM.yyyy", 1: "MM/dd/yyyy", 2: "dd/MM/yyyy", 3: "yyyy/MM/dd"]
  let TIME_FORM = [0: "h:mm:ss a", 1: "HH:mm:ss"]
  
  
  var predefinedTexts = [PredefinedText]()
  var albums = [Album]()
  var definedFinds = [String : String]()
  
  var createdAt = Double()
  var updatedAt = Double()
  

  
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var photoView: UIImageView!
  @IBOutlet weak var date: UITextField!
  @IBOutlet weak var accuracy: UITextField!
  @IBOutlet weak var coordinate: UITextField!
  @IBOutlet weak var category: UITextField!
  @IBOutlet weak var predText: UITextField!
  @IBOutlet weak var project: UITextField!
  var picker = UIImagePickerController()
  @IBOutlet weak var cameraButton: UIButton!
  
  var datePickerView  : UIDatePicker = UIDatePicker()

  
  override func viewWillAppear(_ animated: Bool) {
    navigationItem.title = "Add New Photo"
    
    createToolbar()
    
    
    if cameraButton.isHidden {
      editButton.isHidden = false
    } else {
      editButton.isHidden = true
    }
    
    textView.delegate = self

    let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
    date.inputAccessoryView = toolBar
    datePickerView.datePickerMode = .date
    date.inputView = datePickerView
    
    
    albums = []
    predefinedTexts = []
    fetchAlbums()
    fetchPredefinedTexts()
    fetchDefinedFind()
   
    if image != nil {
      image = fetchImage()
    } else {
      key = Database.database().reference().child(Const.IMAGES_DATA).childByAutoId().key
      createEmptyPhoto()
    }
    
    updateTextFields()
    setInitialDate()
    
  }
  

  @objc func dismissPicker() {
    print("print something")
    view.endEditing(true)
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
    NotificationCenter.default.addObserver(self, selector: #selector(self.cancel), name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
    
    
    let saveButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(AddImageTVC.back(sender:)))
    saveButton.tintColor = Const.RED
    navigationItem.rightBarButtonItem = saveButton

    cameraButton.isHidden = false
    datePickerView.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
    
    
    self.navigationItem.hidesBackButton = true
    let newBackButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancel) )
    self.navigationItem.leftBarButtonItem = newBackButton

  }

  
  @objc func cancel(){
    print("Canceled")
    deleteAImage()
    _ = navigationController?.popViewController(animated: true)
  }
  
  
  @objc func back(sender: UIBarButtonItem) {
    
    if allFieldsCompleted().status {
      updateAImage()
      updateFirebase()
      print(image)
      _ = navigationController?.popViewController(animated: true)

    } else {
      let alert = UIAlertController(title: "Something is missing.", message: "Make sure all fields are completed. Some fields are missing: \n \(allFieldsCompleted().message)", preferredStyle: .alert)
      
      let ok = UIAlertAction(title: "Continue", style: .default, handler: nil)
      let cancel = UIAlertAction(title: "Exit", style: .cancel, handler: { (alert) in
        self.deleteAImage()
        _ = self.navigationController?.popViewController(animated: true)

      })
      
      alert.addAction(ok)
      alert.addAction(cancel)
      present(alert, animated: true, completion: nil)
    }
  }

  func deleteAImage(){
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        /*
        if let uid = Auth.auth().currentUser?.uid {
          let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
          ref.removeValue()
          print("Deleted Image Data on firebase: ", key)
        } else {
          print("Error trying to Image Data.")
        }
        */
      }catch {
        print("error")
      }
    }
    
  }

  
  
  override func viewWillDisappear(_ animated: Bool) {
    //super.viewWillDisappear(animated)
    //if allFieldsCompleted().status {
      //updateAImage()
    //} else {
    //  deleteAImage()
    //}
    
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillTerminate, object: UIApplication.shared)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)

  }
  
  func allFieldsCompleted() -> (status : Bool, message: String) {
    if photoView.image == UIImage(named: "tinySquare") || coordinate.text == "" {
      if photoView.image == UIImage(named: "tinySquare") && coordinate.text != "" {
        return (false, "- Photo")
      } else if photoView.image != UIImage(named: "tinySquare") && coordinate.text != ""  {
        return (false, "- Coordinates")
      } else {
        return (false, "- Photo \n- Coordinates")
      }
    } else {
    return (true, "")
    }
  }
  
  
  
  func updateTextFields(){
    if image != nil {
      photoView.image = photo
      date.text = image.timestamp
      category.text = image.symbolText!
      predText.text = image.ownerText
      project.text = image.albumName 
      textView.text = image.longDesc
      if image.longitude != "" {
        if image.coordinateSystem == Const.COOR_DD_WGS84 || image.coordinateSystem == Const.COOR_DMS_WGS84 || image.coordinateSystem == Const.COOR_DM_WGS84 {
          coordinate.text = "LO: \(image.longitude), LA: \(image.latitude)"
        } else {
          
          if image.coordinateSystem == Const.COOR_UTM_UK_GREF {
            coordinate.text = "E: \(image.longitude), N: \(image.latitude), GRef: \(image.zone)"
          } else {
            coordinate.text = "E: \(image.longitude), N: \(image.latitude), Z: \(image.zone)"
          }
        
        }
      }
    }
  }
  
  func setInitialDate(){
    let currentDate = Date()
    let dateFormatter = DateFormatter()
    
    createdAt = currentDate.timeIntervalSince1970
    updatedAt = currentDate.timeIntervalSince1970
    
    dateFormatter.dateFormat = dateFormat()
    let convertedDate = dateFormatter.string(from: currentDate as Date)
    date.text = convertedDate
  }
  
  
  func dateChanged(_ sender: UIDatePicker) {
    let currentDate = sender.date
    let dateFormatter = DateFormatter()
    createdAt = sender.date.timeIntervalSince1970
    updatedAt = sender.date.timeIntervalSince1970
    
    dateFormatter.dateFormat = dateFormat()
    let convertedDate = dateFormatter.string(from: currentDate as Date)
    date.text = convertedDate
  }
  
  func dateFormat() -> String {
    if defaults.bool(forKey: "dateFormatSwitch") {
      return DATE_FORM[defaults.integer(forKey: "dateFormat")]! + " - " + TIME_FORM[1]!
    } else {
      return DATE_FORM[defaults.integer(forKey: "dateFormat")]! + " - " + TIME_FORM[0]!
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 6 {
      updateAImage()
      performSegue(withIdentifier: "goToAlbums", sender: self)
    } else if indexPath.row == 5 {
      updateAImage()
      performSegue(withIdentifier: "goToCategories", sender: self)
    } else if indexPath.row == 4 {
      updateAImage()
      performSegue(withIdentifier: "goToPredText", sender: self)
    } else if indexPath.row == 3 {
      updateAImage()
      performSegue(withIdentifier: "goToCoordinates", sender: self)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "goToAlbums" == segue.identifier {
      let DestVC = segue.destination as! selectAlbumTVC
      DestVC.albums = self.albums.map{$0.name}
      DestVC.key = image.key
    } else if "goToCategories" == segue.identifier {
      let Dest : selectCategoryTVC = segue.destination as! selectCategoryTVC
      Dest.key = image.key
    } else if "goToPredText" == segue.identifier {
      let Dest : SelectPredTextTVC = segue.destination as! SelectPredTextTVC
      Dest.key = image.key
      Dest.predTexts = self.predefinedTexts.map{$0.text}
    } else if "goToCoordinates" == segue.identifier {
      let destination : SelectCoordinatesTVC = segue.destination as! SelectCoordinatesTVC
      destination.key = image.key
      
    }
  }


  func createEmptyPhoto(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.IMAGE_ENTITY, into: manageContext)
    
    id = Int(keychain.get(albumName) ?? "1")!
    
    let symbolLetter = "x"
    let symbolText = definedFinds[symbolLetter]
    
    let coorSys = DATA_FORMAT[defaults.integer(forKey: "dataFormat")]!
    
    var initials = ""
    
    if keychain.get(Const.INITIALS) != nil {
      initials =  keychain.get(Const.INITIALS)!
    }
    
    createdAt = Date().timeIntervalSince1970
    updatedAt = Date().timeIntervalSince1970
    
    image = Image(id: id, latitude: "", longitude: "", zone: "", accuracy: "1", timestamp: "", albumName: defaults.string(forKey: "albumName")!, coordinateSystem: coorSys, ownerText: "", key: key, desc: "", url: "", initials : initials, symbolLetter : symbolLetter, symbolText : symbolText, createdAt: createdAt, updatedAt: updatedAt, imgData: UIImageJPEGRepresentation(photo!, 1)! as NSData, longDesc: "")

    
    new.setValue(id, forKey: "id")
    new.setValue(image.latitude, forKey: "latitude")
    new.setValue(image.longitude, forKey: "longitude")
    new.setValue(image.zone, forKey: "gZone")
    new.setValue(image.accuracy, forKey: "acurracy")
    new.setValue(image.timestamp, forKey: "timestamp")
    new.setValue(image.albumName, forKey: "albumName")
    new.setValue(image.coordinateSystem, forKey: "coordinateSystem")
    new.setValue(image.ownerText, forKey: "note")
    new.setValue(key, forKey: "key")
    new.setValue(image.desc, forKey: "desc")
    new.setValue(image.url, forKey: "url")
    new.setValue(image.initials, forKey: "initials")
    new.setValue(image.symbolLetter, forKey: "symbolLetter")
    new.setValue(image.symbolText, forKey: "symbolText")
    new.setValue(image.imgData, forKey: "imgData")

    do{
      try manageContext.save()
      //DBProvider.Instance.addImageDataToChild(image : image)
      
      let newIndex = String(id + 1 )
      keychain.set(newIndex, forKey: albumName)
      
      
      print("data saved.")
    }catch {
      print("error")
    }
    
    
  }
  
  
  @IBAction func takeAPhoto(_ sender: Any) {
    let alertController = UIAlertController(title: "Select a Photo", message: "Please select a method to upload the photo.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "Take a Photo", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.takePhoto()
    })
    
    let kmlAction = UIAlertAction(title: "Choose from Library", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.chooseFromLibrary()
    })
    
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)

  }
  
  func takePhoto(){
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .camera
    picker.cameraFlashMode = .off
    present(picker, animated: true, completion: nil)

  }
  
  
  func chooseFromLibrary(){
    picker.delegate = self
    picker.allowsEditing = true
    picker.sourceType = .photoLibrary
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    cameraButton.isHidden = true
    photo = (info[UIImagePickerControllerEditedImage] as? UIImage)!; dismiss(animated: true, completion: nil)
    photoView.image = photo
    updateAImage()
  }
  
  
}

extension AddImageTVC{
  
  func fetchDefinedFind() {
    
    self.definedFinds = ["x" : defaults.string(forKey: "x")!,
                         "a" : defaults.string(forKey: "a")!,
                         "b" : defaults.string(forKey: "b")!,
                         "c" : defaults.string(forKey: "c")!,
                         "d" : defaults.string(forKey: "d")!,
                         "e" : defaults.string(forKey: "e")!,
                         "f" : defaults.string(forKey: "f")!,
                         "o" : defaults.string(forKey: "o")!,
                         "v" : defaults.string(forKey: "v")!]
    
  }
  
  func fetchAlbums(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    
    let sortDescriptor = NSSortDescriptor(key: Const.UPDATED_AT, ascending: false)
    request.sortDescriptors = [sortDescriptor]
    
    
    do {
      let albums = try manageContext.fetch(request)
      for album in albums as! [AlbumData]{
        let newAlbum = Album(name: album.name!, key: album.key!, owner: album.owner, createdAt: album.createdAt, updatedAt: album.updatedAt)
        
        self.albums.append(newAlbum)
      }
      
    } catch {
      print("Error fetching all Albums")
    }
  }
  
  func fetchPredefinedTexts(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.PREDEFINED_TEXT_ENTITY)
    
    let sortDescriptor = NSSortDescriptor(key: Const.PREDEFINED_TEXT, ascending: true)
    request.sortDescriptors = [sortDescriptor]
    
    do {
      let predefinedTexts = try manageContext.fetch(request)
      for predefinedText in predefinedTexts as! [PredefinedTextData] {
        let new = PredefinedText(text: predefinedText.text!, selected: predefinedText.selected, key: predefinedText.key!)
        self.predefinedTexts.append(new)
      }
    } catch {
      print("Error fetching predeterminedText")
    }
  }
  

  
  
  
  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", image.key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! ImageData
      
      //let coorSys = DATA_FORMAT[defaults.integer(forKey: "dataFormat")]!
      let initials = keychain.get(Const.INITIALS)

      
      
      managedObject.setValue(id, forKey: Const.ID)
      managedObject.setValue("1", forKey: Const.ACCURRARY)
      managedObject.setValue(date.text!, forKey: Const.TIMESTAMP)
      managedObject.setValue(project.text!, forKey: Const.ALBUM_NAME)
      //Need to change
      managedObject.setValue(predText.text!, forKey: Const.OWNER_TEXT)
      managedObject.setValue(key, forKey: Const.KEY)
      managedObject.setValue("", forKey: Const.URL)
      managedObject.setValue(initials, forKey: Const.INITIALS)
      //symbols
      managedObject.setValue(createdAt, forKey: Const.CREATED_AT)
      managedObject.setValue(updatedAt, forKey: Const.UPDATED_AT)
      managedObject.setValue(UIImageJPEGRepresentation(photoView.image!, 1), forKey: Const.IMG_DATA)
      managedObject.setValue(textView.text!, forKey: Const.LONG_DESCRIPTION)
      
      do{
        try manageContext.save()
        
        //updateAImageFB()
        print("Image updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
  }
  
  
  
  
  
  func fetchImage() -> Image {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      
      let result = results[0] as! ImageData
      
      
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      return image
      
    } catch {
      print("error")
    }
    return image
  }

  
  func buildDescription(_ lon: String, _ lat: String) -> String {
    return "<\(lat),\(lon)>"
  }
  
  
  @IBAction func editPhoto(_ sender: Any) {
    let alertController = UIAlertController(title: "Select a Photo", message: "Please select a method to upload the photo.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "Take a Photo", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.takePhoto()
    })
    
    let kmlAction = UIAlertAction(title: "Choose from Library", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.chooseFromLibrary()
    })
    
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)

  }
  
  
  
  func updateFirebase() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "key = %@", image.key)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      let result = results[0] as! ImageData
      let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData!, longDesc: result.longDesc )
      
      if let uid = Auth.auth().currentUser?.uid {
        let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(image.key)
        ref.updateChildValues(image.toDictionary())
      }
      
    } catch {
      print("error")
    }
  }

  
}

extension AddImageTVC {
  func createToolbar(){
    
    let toolbar = UIToolbar()
    toolbar.barStyle = .default
    toolbar.sizeToFit()
    
    let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClicked))
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClicked))
    let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    
    toolbar.setItems([cancelButton, space, doneButton], animated: false)
    date.inputAccessoryView = toolbar
  }
  
  @objc func doneClicked(){
    print("doneClicked")
  }
  
  @objc func cancelClicked(){
    print("cancelClicked")
    
  }
}


