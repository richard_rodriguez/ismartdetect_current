//  FontSizeViewController.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 9/15/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation

class FontSizeViewController: UITableViewController {
  
  let defaults = UserDefaults.standard
  let TEXT_FORMAT = [0:64.0, 1:94.0, 2:124.0, 3:154.0]

  
  override func viewWillAppear(_ animated: Bool) {
    selectRow(index: (defaults.integer(forKey: "fontSize") ))
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    //DBProvider.Instance.saveSetting()
  }
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
    
    defaults.set(indexPath.row, forKey: "fontSize")
    defaults.set(TEXT_FORMAT[indexPath.row], forKey: "fontSizeValue")
  
  }
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .none
    }
  }
  
  func selectRow(index: Int) {
    let rowToSelect:IndexPath = IndexPath(row: index, section: 0);  //slecting 0th row with 0th section
    self.tableView.selectRow(at: rowToSelect, animated: false, scrollPosition: UITableViewScrollPosition.middle);
    
    if let cell = tableView.cellForRow(at: rowToSelect) {
      cell.tintColor = Const.RED
      cell.accessoryType = .checkmark
    }
  }
  
  
  
}
