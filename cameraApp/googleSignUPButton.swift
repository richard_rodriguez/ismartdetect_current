//
//  googleSignUPButton.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/3/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//
import UIKit
import GoogleSignIn


class googleSignUpButton : UIButton {
  
  
  override func awakeFromNib() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
    tintColor = UIColor.red
    layer.cornerRadius = 3
    layer.shadowColor = UIColor.darkGray.cgColor
    layer.shadowOffset = CGSize(width: 1, height: 1)
    layer.shadowOpacity = 0.5
    self.addTarget(self, action: #selector(handlerCustomButton), for: .touchUpInside)
    let icon = UIImage(named: "google@1x")
    setTitle("Continue with Google", for: .normal)
    setImage(icon, for: .normal)
    imageEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
    titleEdgeInsets = UIEdgeInsets(top: 5, left: 30, bottom: 5, right: 10)  }
  
  func handlerCustomButton(){
    GIDSignIn.sharedInstance().signIn()
  }
  
}

