//
//  TFSignUp.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 11/28/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import UIKit

class TFSignUp: UITextField {

  override func awakeFromNib() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = .clear
    frame.size.height = 40
    layer.borderColor = Const.RED.cgColor
    layer.borderWidth = 0.8
    layer.masksToBounds = true
  }
}
