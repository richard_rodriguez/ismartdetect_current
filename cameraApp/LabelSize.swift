//
//  AlbumNameLabel.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 3/9/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class LabelSize: UILabel {

  override func awakeFromNib() {
    var multiplier : CGFloat!
    let fontSize = font.pointSize
    
    if DeviceType.IS_IPHONE_4_OR_LESS {
      multiplier = 0.65
    } else {
      multiplier = 1
    }
    
    font = UIFont(name: "Helvetica Bold", size: fontSize * multiplier)
  }

}
