//  SubscriptionVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 4/11/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.

import UIKit
import SwiftyStoreKit
import StoreKit
import FirebaseAuth
import KeychainSwift

var sharedSecret = "1614a6c9bf9d497fa725a0a795d9aa63"
let receiptData = SwiftyStoreKit.localReceiptData
let receiptString = receiptData?.base64EncodedString(options: [])
let appleValidator = AppleReceiptValidator(service: .production)

struct SegueTo {
  static let SIGNUP = "SignUpSG"
  static let SIGNIN = "SignInSG"
}

enum RegisteredPurchase : String {
  case premiumPlan = "premiumplan"
}


class NetworkActivityIndicatorManager : NSObject {
  
  private static var loadingCount = 0
  
  class func NetworkOperationStarted() {
    if loadingCount == 0 {
      
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    loadingCount += 1
  }
  class func networkOperationFinished(){
    if loadingCount > 0 {
      loadingCount -= 1
      
    }
    
    if loadingCount == 0 {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      
    }
  }
}


class SubscriptionVC: UIViewController {
  
  let bundleID = "com.ngintermarketingaps.ismartdetect"
  var premiumPlan = RegisteredPurchase.premiumPlan
  
  var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
  let keychain = KeychainSwift()



  @IBOutlet weak var unlockLabel: UILabel!
  @IBOutlet weak var unlimitedLabel: UILabel!
  @IBOutlet weak var suscribeButton: SuscribeButton!
  @IBOutlet weak var noticeLabel: UILabel!
  @IBOutlet weak var monthlyLabel: UILabel!
  @IBOutlet weak var freeTimeLabel: UILabel!
  @IBOutlet weak var termsEndUserButton: UIButton!
  @IBOutlet weak var byClickingLabel: UILabel!
  @IBOutlet weak var loginButton: SuscribeButton!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    
    if (keychain.getBool("purchased") ?? false) {
      unlockLabel.text = "YOU ARE UNLOCKED"
      unlimitedLabel.text = "You have access to all tools and updates. Sign up or Log in for more options."
      
      monthlyLabel.isHidden = true
      freeTimeLabel.isHidden = true
      
      if let _ = Auth.auth().currentUser {
        suscribeButton.isHidden = true
        loginButton.isHidden = true
        termsEndUserButton.isHidden = true
        byClickingLabel.isHidden = true
      } else {
        termsEndUserButton.isHidden = false
        byClickingLabel.isHidden = false
        suscribeButton.isHidden = false
        loginButton.isHidden = false
        suscribeButton.setTitle("SIGN UP", for: .normal)
        suscribeButton.addTarget(self, action: #selector(goToSignUp), for: .touchUpInside)
        loginButton.setTitle("LOG IN", for: .normal)
        loginButton.addTarget(self, action: #selector(goToLogin), for: .touchUpInside)
      }
      
      noticeLabel.text = "iSmartDetect automatically renews at the end of its duration unless you cancel. To cancel your subscription go to AppStore App."
    } else {
      //loginButton.setTitle("RESTORE SUBSCRIPTION", for: .normal)
      unlockLabel.text = "Unlock These Benefits"
      monthlyLabel.isHidden = false
      freeTimeLabel.isHidden = false
      unlimitedLabel.text = "Unlimited access to more tools and updates."
      
      noticeLabel.text = "If you already subscribed just press RESTORE SUSBSCRIPTION, it will not charge again, just restore your subscription."
      
      termsEndUserButton.isHidden = true
      byClickingLabel.isHidden = true
    }

    
  }
  
 
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
   
    
    self.navigationItem.titleView?.tintColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0)

  }
  
  func goToLogin(){
    performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
  }
  
  
  @IBAction func restore(_ sender: UIButton) {
    restorePurchases()
    //performSegue(withIdentifier: SegueTo.SIGNIN, sender: self)
  }
  
  
  func goToSignUp(){
    performSegue(withIdentifier: SegueTo.SIGNUP, sender: self)
  }
  
  @IBAction func suscription(_ sender: Any) {
    
    if (keychain.getBool("purchased") ?? false) {
      suscribeButton.addTarget(self, action: #selector(goToSignUp), for: .touchUpInside)
    } else {
      newPurchase(purchase: .premiumPlan)
      //purchase(purchase: .premiumPlan)
    }
  }
  
  func getInfo(purchase : RegisteredPurchase) {
    NetworkActivityIndicatorManager.NetworkOperationStarted()
    SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue], completion: {
      result in
      NetworkActivityIndicatorManager.networkOperationFinished()
      self.showAlert(alert: self.alertForProductRetrievalInfo(result: result))
    })
  }
  
  func newPurchase(purchase : RegisteredPurchase) {
    NetworkActivityIndicatorManager.NetworkOperationStarted()
    SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue], completion: {
      result in
      NetworkActivityIndicatorManager.networkOperationFinished()
      
      if let product = result.retrievedProducts.first {
        let priceString = product.localizedPrice!
        let alert = UIAlertController(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let ok = UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
          self.purchase(purchase: .premiumPlan)
        })
        
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
      
      }
        
      else if let invalidProductID = result.invalidProductIDs.first {
        let alert = UIAlertController(title: "Could not retreive product info", message: "Invalid product identifier: \(invalidProductID)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
      }
        
      else {
        let errorString = result.error?.localizedDescription ?? "Unknown Error. Please Contact Support"
        let alert = UIAlertController(title: "Could not retreive product info", message: errorString, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
      }

      
      
      self.showAlert(alert: self.alertForProductRetrievalInfo(result: result))
    })
  }
  
  
  func purchase(purchase : RegisteredPurchase) {
    //getInfo(purchase: .premiumPlan)


    DispatchQueue.main.async {
      self.activityIndicator.center = self.view.center
      self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
      self.activityIndicator.startAnimating()
      self.view.addSubview(self.activityIndicator)
    }
    
    NetworkActivityIndicatorManager.NetworkOperationStarted()

    
    SwiftyStoreKit.purchaseProduct(bundleID + "." + purchase.rawValue, atomically: true) {
      result in
      NetworkActivityIndicatorManager.networkOperationFinished()
      
      DispatchQueue.main.async {
        self.activityIndicator.stopAnimating()
      }

      
      if case .success(let product) = result {
        if product.needsFinishTransaction {
          SwiftyStoreKit.finishTransaction(product.transaction)
        }
      }
      
      if let alert = self.alertForPurchaseResult(result: result) {
        self.showAlert(alert: alert)
      }
    }
    
  }
  
  func restorePurchases() {
    NetworkActivityIndicatorManager.NetworkOperationStarted()
    DispatchQueue.main.async {
      self.activityIndicator.center = self.view.center
      self.activityIndicator.activityIndicatorViewStyle = .whiteLarge
      self.activityIndicator.startAnimating()
      self.view.addSubview(self.activityIndicator)
    }
    
    SwiftyStoreKit.restorePurchases(atomically: true, completion: {
      result in
      NetworkActivityIndicatorManager.networkOperationFinished()
      
      DispatchQueue.main.async {
        self.activityIndicator.stopAnimating()
      }
      
      for product in result.restoredPurchases {
        if product.needsFinishTransaction {
          SwiftyStoreKit.finishTransaction(product.transaction)
        }
      }
      
      self.showAlert(alert: self.alertForRestorePurchases(result: result))
      
    })
  }
  
  func verifyReceipt() {
    NetworkActivityIndicatorManager.NetworkOperationStarted()
    SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: {
      result in
      NetworkActivityIndicatorManager.networkOperationFinished()
      
      self.showAlert(alert: self.alertForVerifyReceipt(result: result))
      
      if case .error(let error) = result {
        if case .noReceiptData = error {
          print("Error: noReceiptData ")
          
        }
      }
      
    })
    
  }
  
  func verifyPurcahse(product : RegisteredPurchase) {
    NetworkActivityIndicatorManager.NetworkOperationStarted()
    SwiftyStoreKit.verifyReceipt(using: appleValidator, password: sharedSecret) { (result) in
      NetworkActivityIndicatorManager.networkOperationFinished()
      
      switch result{
      case .success(let receipt):
        
        let productID = self.bundleID + "." + product.rawValue
        
        if product == .premiumPlan {
          let purchaseResult = SwiftyStoreKit.verifySubscription(type: .autoRenewable, productId: productID, inReceipt: receipt, validUntil: Date())
          
          self.showAlert(alert: self.alertForVerifySubscription(result: purchaseResult))
          
          
        }
        else {
          let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productID, inReceipt: receipt)
          self.showAlert(alert: self.alertForVerifyPurchase(result: purchaseResult))
          
        }
      case .error(let error):
        self.showAlert(alert: self.alertForVerifyReceipt(result: result))
        if case .noReceiptData = error {
          //self.refreshReceipt()
          print("Receipt verification failed: \(error)")

          
        }
        
      }
      
      
    }
    
  }
  
  
//  func refreshReceipt() {
//
////    SwiftyStoreKit.refreshReceipt(completion: {
////      result in
////
////      self.showAlert(alert: self.alertForRefreshRecepit(result: result))
////
////    })
////
// }
  
}


extension SubscriptionVC {
  
  func alertWithTitle(title : String, message : String) -> UIAlertController {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    return alert
    
  }
  
  func showAlert(alert : UIAlertController) {
    guard let _ = self.presentedViewController else {
      self.present(alert, animated: true, completion: nil)
      return
    }
    
  }
  
  func alertForProductRetrievalInfo(result : RetrieveResults) -> UIAlertController {
    if let product = result.retrievedProducts.first {
      let priceString = product.localizedPrice!
      return alertWithTitle(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
      
    }
    else if let invalidProductID = result.invalidProductIDs.first {
      return alertWithTitle(title: "Could not retreive product info", message: "Invalid product identifier: \(invalidProductID)")
    }
    else {
      let errorString = result.error?.localizedDescription ?? "Unknown Error. Please Contact Support"
      return alertWithTitle(title: "Could not retreive product info" , message: errorString)
      
    }
    
  }
  
  func alertForPurchaseResult(result : PurchaseResult) -> UIAlertController? {
    switch result {
      case .success(let product):
        print("Purchase Succesful: \(product.productId)")
        successfulPurchase()
        return alertWithTitle(title: "Thank You", message: "Purchase completed")
      

      case .error(let error):
        print("Purchase Failed: \(error)")
        switch error.code {
          case .unknown:
            return alertWithTitle(title: "Purchase Failed", message: "Unknown error. Please contact support")
          case .clientInvalid:
            return alertWithTitle(title: "Purchase Failed", message: "Not allowed to make the payment")
          case .paymentCancelled: break
          case .paymentInvalid:
            return alertWithTitle(title: "Purchase Failed", message:"The purchase identifier was invalid")
          case .paymentNotAllowed:
            return alertWithTitle(title: "Purchase Failed", message: "The device is not allowed to make the payment")
          case .storeProductNotAvailable:
            return alertWithTitle(title: "Purchase Failed", message: "The product is not available in the current storefront")
          case .cloudServicePermissionDenied:
            return alertWithTitle(title: "Purchase Failed", message: "Access to cloud service information is not allowed")
          case .cloudServiceNetworkConnectionFailed:
            return alertWithTitle(title: "Purchase Failed", message: "Could not connect to the network")
          case .cloudServiceRevoked:
            return alertWithTitle(title: "Purchase Failed", message: "Cloud Service Revoked")
          case .privacyAcknowledgementRequired:
            return alertWithTitle(title: "Purchase Failed", message: "Privacy Acknowledgement Required")
          case .unauthorizedRequestData:
            return alertWithTitle(title: "Purchase Failed", message: "Unauthorized Request Data")
          case .missingOfferParams:
            return alertWithTitle(title: "Purchase Failed", message: "Missing Offer Params")
          case .invalidOfferIdentifier:
            return alertWithTitle(title: "Purchase Failed", message: "Cloud Service Revoked")
          case .invalidSignature:
          return alertWithTitle(title: "Purchase Failed", message: "Invalid Signature")
            case .invalidOfferPrice:
          return alertWithTitle(title: "Purchase Failed", message: "Invalid Offer Price")
 
          
          
          
          }
      }
    return alertWithTitle(title: "Purchase Failed", message: "Could not connect to the network")

  }
  
  
  func successfulPurchase(){
    unlockLabel.text = "All Benefits Unlocked"
    unlimitedLabel.text = "You have access to all tools and updates. Sign up for more options."
    suscribeButton.isHidden = true
    termsEndUserButton.isHidden = false
    byClickingLabel.isHidden = false
    monthlyLabel.isHidden = true
    freeTimeLabel.isHidden = true
    
    if let _ = Auth.auth().currentUser {
      suscribeButton.isHidden = true
      loginButton.isHidden = true
      termsEndUserButton.isHidden = true
      byClickingLabel.isHidden = true
    } else {
      termsEndUserButton.isHidden = false
      byClickingLabel.isHidden = false
      suscribeButton.isHidden = false
      loginButton.isHidden = false
      suscribeButton.setTitle("SIGN UP", for: .normal)
      suscribeButton.addTarget(self, action: #selector(goToSignUp), for: .touchUpInside)
      loginButton.setTitle("LOG IN", for: .normal)
      loginButton.addTarget(self, action: #selector(goToLogin), for: .touchUpInside)
    }
    
    noticeLabel.text = "iSmartDetect automatically renews at the end of its duration unless you cancel. To cancel your subscription go to AppStore App."
    
    keychain.set(true, forKey: "purchased")

  }
  
  func alertForRestorePurchases(result : RestoreResults) -> UIAlertController {
    if result.restoreFailedPurchases.count > 0 {
      print("Restore Failed: \(result.restoreFailedPurchases)")
      return alertWithTitle(title: "Restore Failed", message: "Unknown Error. Please Contact Support")
    }
    else if result.restoredPurchases.count > 0 {
      keychain.set(true, forKey: "purchased")

      successfulPurchase()
      return alertWithTitle(title: "Subscription Restored", message: "Your subscription have been restored.")
      
    }
    else {
      return alertWithTitle(title: "Nothing To Restore", message: "No previous purchases were made.")
    }
    
  }
  func alertForVerifyReceipt(result: VerifyReceiptResult) -> UIAlertController {
    
    switch result {
    case.success(let succ):
      print(succ)
      return alertWithTitle(title: "Receipt Verified", message: "Receipt Verified Remotely")
    case .error(let error):
      switch error {
      case .noReceiptData:
        return alertWithTitle(title: "Receipt Verification", message: "No receipt data found, application will try to get a new one. Try Again.")
      default:
        return alertWithTitle(title: "Receipt verification", message: "Receipt Verification failed")
      }
    }
  }
  func alertForVerifySubscription(result: VerifySubscriptionResult) -> UIAlertController {
    switch result {
    case .purchased(let expiryDate):
      return alertWithTitle(title: "Product is Purchased", message: "Product is valid until \(expiryDate)")
    case .notPurchased:
      return alertWithTitle(title: "Not purchased", message: "This product has never been purchased")
    case .expired(let expiryDate):
      
      return alertWithTitle(title: "Product Expired", message: "Product is expired since \(expiryDate)")
    }
  }
  func alertForVerifyPurchase(result : VerifyPurchaseResult) -> UIAlertController {
    switch result {
    case .purchased:
      return alertWithTitle(title: "Product is Purchased", message: "Product will not expire")
    case .notPurchased:
      
      return alertWithTitle(title: "Product not purchased", message: "Product has never been purchased")
      
      
    }
    
  }
//  func alertForRefreshRecepit(result : RefreshReceiptResult) -> UIAlertController {
//
//    switch result {
//    case .success(let receiptData):
//      print(receiptData)
//      return alertWithTitle(title: "Receipt Refreshed", message: "Receipt refreshed successfully")
//    case .error(let error):
//      print(error)
//      return alertWithTitle(title: "Receipt refresh failed", message: "Receipt refresh failed")
//    }
//  }
  
}
