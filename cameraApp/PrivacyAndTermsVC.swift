//
//  PrivacyAndTermsVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 2/17/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class PrivacyAndTermsVC: UIViewController, UIWebViewDelegate {

  @IBOutlet weak var text: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let htmlString =  "<div style=\"font-size: 18px;font-family: 'HelveticaNeue-Light'\"><p><strong>Privacy Policy for iSmartDetect app&nbsp;</strong>Last updated: April 7, 2017</p><p>iVirtual Games Aps (“us”, “we”, or “our”) operates the iSmartDetect mobile application (the “Service”).</p><p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p><p>We will not use or share your information with anyone except as described in this Privacy Policy.</p><p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.</p><p><strong>Information Collection And Use</strong></p><p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your email address, name (“Personal Information”).</p><p><strong>Log Data and&nbsp;</strong><strong>Location information</strong></p><p>We use Advanced Encryption Standard (AES)128, and all stored GPS data is hosted at Google Firebase. There is absolutely no way we can read your GPS coordinates, they are all encrypted with Advanced Encryption Standard (AES)128.</p><p><strong>Cookies</strong></p><p>We don’t use cookies on this app.</p><p><strong>Service Providers</strong></p><p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p><p>These third parties have access to your Personal Information email address, name (“Personal Information”) but only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p><p><strong>Communications</strong></p><p>We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.</p><p><strong>Compliance With Laws</strong></p><p>We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p><p><strong>Business Transaction</strong></p><p>If iVirtual Games Aps is involved in a merger, acquisition or asset sale, your Personal Information may be transferred. We will provide notice before your Personal Information is transferred and becomes subject to a different Privacy Policy.</p><p><strong>Security</strong></p><p>The security of your Personal Information is very important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p><p><strong>International Transfer</strong></p><p>Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p><p>If you are located outside Denmark and choose to provide information to us, please note that we transfer the information, including Personal Information, to Denmark and process it there.</p><p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p><p><strong>Links To Other Sites</strong></p><p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party’s site. We strongly advise you to review the Privacy Policy of every site you visit.</p><p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p><p><strong>Children’s Privacy</strong></p><p>Our Service does not address anyone under the age of 13 (“Children”).</p><p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you are aware that your Children has provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a children under age 13 without verification of parental consent, we take steps to remove that information from our servers.</p><p><strong>Changes To This Privacy Policy</strong></p><p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p><p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p><p><strong>Contact Us</strong></p><p>If you have any questions about this Privacy Policy, please contact us.</p></div> "
    
    
    let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
    
    let attributedString = try! NSAttributedString(data: htmlData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)

    text.attributedText = attributedString
    
  }
  
  
}

