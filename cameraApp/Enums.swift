//
//  Enums.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/28/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation


struct ScreenSize
{
  static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
  static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
  static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
  static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
  static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
  static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
  static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
  static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
  static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}


enum Option {
  case email
  case note
  case predefinedText
  case image
  case album
  
  func child() -> String {
    switch self {
      case .email:
        return Const.EMAILS
      case .album:
        return Const.ALBUMS
      case .image:
        return Const.IMAGES_DATA
      case .note:
        return Const.NOTES
      case .predefinedText:
        return Const.PREDEFINED_TEXTS
          }
  }
  
  func entity() -> String {
    switch self {
      case .email:
        return Const.EMAIL_ENTITY
      case .album:
        return Const.ALBUM_ENTITY
      case .image:
        return Const.IMAGE_ENTITY
      case .note:
        return Const.NOTE_ENTITY
      case .predefinedText:
        return Const.PREDEFINED_TEXT_ENTITY
      
      }
  }
  
}
