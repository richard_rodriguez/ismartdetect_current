//
//  ImageViewController+Segments.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 1/23/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation
import CoreData

extension ImageViewController {
  
  
  
  @IBAction func didAddTextPressed(_ sender: Any) {
    isDefinedFind = false
    definedFindPicker.isHidden = true
    
    fetchSelectedPredefinedTexts()
    addTexts()
  }
  
  
  @IBAction func didDefinedFindPressed(_ sender: Any) {
    isDefinedFind = true
    definedFind.isHidden = false
    definedFind.text = definedFinds[returnLetter(0)]
    symbolImage.image = UIImage(named: returnLetter(0))
    selectPicker.isHidden = true
    definedFindPicker.isHidden = false
    addPredTexts()
  }
  
}

//Pickers
extension ImageViewController {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView.tag == 1 && !isDefinedFind {
      return predTexts.count
    } else {
      return 9
    }
  }
  
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let indexLastElement = predTexts.count - 1
    
    if !isDefinedFind && pickerView.tag == 1 {
      let predTextSeleted =  indexLastElement == row ? "" : (predTexts[row] as String?)!
      ownText = predTextSeleted
      ownTextField.text = predTextSeleted
      
      defaults.set(predTextSeleted, forKey: albumName! + "predText")
      imageTaken.image = textToImage(inImage: finalImage, drawText: gpsInfo as [String:String])
    } else {
      symbolText = definedFinds[returnLetter(row)]
      symbolLetter = returnLetter(row)
      
      symbolImage.image = UIImage(named: returnLetter(row))
      definedFind.text = definedFinds[returnLetter(row)]

    }
  }
  
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    if pickerView.tag == 1 && !isDefinedFind {
      return predTexts[row] as String?
    } else {
      return definedFinds[returnLetter(row)] as String?
    }
  }
  
  
  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
      let indexLastElement = predTexts.count - 1
      var titleData : String!

      var myTitle : NSAttributedString!
      
      var pickerLabel = view as! UILabel!
      if view == nil {
        pickerLabel = UILabel()
      }
    
    
      if pickerView.tag == 1 && !isDefinedFind {
        titleData = predTexts[row]
      } else {
        titleData = definedFinds[returnLetter(row)]
      }
    
      if (row == indexLastElement) && (pickerView.tag == 1 && !isDefinedFind) {
        myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.font:UIFont(name: "Helvetica Light", size: 22.0)!,NSAttributedStringKey.foregroundColor:UIColor.red])
      } else {
        myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.font:UIFont(name: "Helvetica Light", size: 22.0)!,NSAttributedStringKey.foregroundColor:UIColor.white])
      }
    
      pickerLabel!.attributedText = myTitle
      pickerLabel!.textAlignment = .center
      return pickerLabel!
  }
  
  
  func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 24.0
  }
  
  
  func returnLetter(_ num: Int) -> String {
    switch num {
      case 0:
        return "x"
      case 1:
        return "a"
      case 2:
        return "b"
      case 3:
        return "c"
      case 4:
        return "d"
      case 5:
        return "e"
      case 6:
        return "f"
      case 7:
        return "o"
      case 8:
        return "v"
      default:
        return ""
    }
  }


}




