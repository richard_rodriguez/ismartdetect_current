//
//  Extensions.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 12/25/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
//

import Foundation
import SystemConfiguration


protocol Utilities {
}


private var kAssociationKeyMaxLength: Int = 0


extension UIToolbar {
  
  func ToolbarPiker(mySelect : Selector) -> UIToolbar {
    
    let toolBar = UIToolbar()
    
    toolBar.barStyle = UIBarStyle.default
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor.black
    toolBar.sizeToFit()
    
    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    
    toolBar.setItems([ spaceButton, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
    
    return toolBar
  }
  
}


extension String {
  func convertCoordinatesToInteger() -> Double {
    let array = self.components(separatedBy: " ")
    let sign = (array[2] == "W" || array[2] == "S") ? -1.0 : 1.0
    let deg = Double(array[0])!
    let min = Double(array[1])!
    let sec = Double(array[2])!
    return (deg + (min + sec/60.0)/60.0) * sign
  }
}


extension NSObject:Utilities{
  
  
  enum ReachabilityStatus {
    case notReachable
    case reachableViaWWAN
    case reachableViaWiFi
  }
  
  var currentReachabilityStatus: ReachabilityStatus {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
        SCNetworkReachabilityCreateWithAddress(nil, $0)
      }
    }) else {
      return .notReachable
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
      return .notReachable
    }
    
    if flags.contains(.reachable) == false {
      // The target host is not reachable.
      return .notReachable
    }
    else if flags.contains(.isWWAN) == true {
      // WWAN connections are OK if the calling application is using the CFNetwork APIs.
      return .reachableViaWWAN
    }
    else if flags.contains(.connectionRequired) == false {
      // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
      return .reachableViaWiFi
    }
    else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
      // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
      return .reachableViaWiFi
    }
    else {
      return .notReachable
    }
  }
  
}



func getPlacemarkFromLocation(location:CLLocation)->CLPlacemark?{
  let g = CLGeocoder()
  var p:CLPlacemark?
  g.reverseGeocodeLocation(location, completionHandler: {
    (placemarks, error) in
    let pm = placemarks!
    if (pm.count > 0){
      p = placemarks![0]
    }
  })
  return p
}

extension Int {
  init(_ range: Range<Int> ) {
    let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
    let min = UInt32(range.lowerBound + delta)
    let max = UInt32(range.upperBound   + delta)
    self.init(Int(min + arc4random_uniform(max - min)) - delta)
  }
}

extension Double {
  func getDateStringFromUnixTime(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = dateStyle
    dateFormatter.timeStyle = timeStyle
    return dateFormatter.string(from: Date(timeIntervalSince1970: self))
  }
}


extension UITextField {
  
  @IBInspectable var maxLength: Int {
    get {
      if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
        return length
      } else {
        return Int.max
      }
    }
    set {
      objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
      addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
      checkMaxLength()
    }
  }
  
  func checkMaxLength() {
    if let textCurrent = self.text, textCurrent.count > maxLength {
      let cursorPosition = selectedTextRange
      let index = textCurrent.index(textCurrent.startIndex, offsetBy: 1)
      self.text = textCurrent.substring(to: index)
      selectedTextRange = cursorPosition
    }
  }
}


extension UIImage {
  // MARK: - UIImage+Resize
  func compressTo(_ expectedSizeInMb:Int) -> UIImage? {
    let sizeInBytes = expectedSizeInMb * 1024 * 1024
    var needCompress:Bool = true
    var imgData:Data?
    var compressingValue:CGFloat = 1.0
    while (needCompress) {
      if let data:Data = UIImageJPEGRepresentation(self, compressingValue) {
        if data.count < sizeInBytes {
          needCompress = false
          imgData = data
        } else {
          compressingValue -= 0.1
        }
      }
    }
    
    if let data = imgData {
      return UIImage(data: data)
    }
    return nil
  }
}



extension UIImage {
  
  func scaleImage(toSize newSize: CGSize) -> UIImage? {
    let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
    if let context = UIGraphicsGetCurrentContext() {
      context.interpolationQuality = .high
      let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
      context.concatenate(flipVertical)
      context.draw(self.cgImage!, in: newRect)
      let newImage = UIImage(cgImage: context.makeImage()!)
      UIGraphicsEndImageContext()
      return newImage
    }
    return nil
  }
}


extension UIImageView {
  func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
    contentMode = mode
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard
        let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
        let data = data, error == nil,
        let image = UIImage(data: data)
        else { return }
      DispatchQueue.main.async() { () -> Void in
        self.image = image
      }
      }.resume()
  }
  func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
    guard let url = URL(string: link) else { return }
    downloadedFrom(url: url, contentMode: mode)
  }
}

extension String {
  private static let allowedCharacters = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-")
  
  public func slugify() -> String {
    let cocoaString = NSMutableString(string: self) as CFMutableString
    CFStringTransform(cocoaString, nil, kCFStringTransformToLatin, false)
    CFStringTransform(cocoaString, nil, kCFStringTransformStripCombiningMarks, false)
    CFStringLowercase(cocoaString, .none)
    
    return String(cocoaString)
      .components(separatedBy: String.allowedCharacters.inverted)
      .filter { $0 != "" }
      .joined(separator: "-")
  }
  
  }


extension Date {
  func years(from date: Date) -> Int {
    return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
  }
  func months(from date: Date) -> Int {
    return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
  }
  func weeks(from date: Date) -> Int {
    return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
  }
  func days(from date: Date) -> Int {
    return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
  }
  func hours(from date: Date) -> Int {
    return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
  }
  func minutes(from date: Date) -> Int {
    return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
  }
  func seconds(from date: Date) -> Int {
    return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
  }
  
  func offset(from date: Date) -> String {
    if years(from: date)   > 0 { return "\(years(from: date))y"   }
    if months(from: date)  > 0 { return "\(months(from: date))M"  }
    if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
    if days(from: date)    > 0 { return "\(days(from: date))d"    }
    if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
    if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
    if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
    return ""
  }
}


