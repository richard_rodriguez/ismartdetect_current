//  DBProvider.swift
//  Created by Richard Rodriguez on 11/30/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.

import Foundation
import FirebaseDatabase
import FirebaseAuth
import CoreData
import CryptoSwift
import MBProgressHUD

let defaults = UserDefaults.standard

class DBProvider {
  private static let _instance = DBProvider()
  
  static var Instance : DBProvider {
    return _instance
  }

  var dbRef : DatabaseReference {
    return Database.database().reference()
  }

  var personalInfoRef : DatabaseReference {
    return dbRef.child(Const.PERSONAL_INFO)
  }
  
  
  
  func saveUser(id: String, name: String, email: String){
    let data : Dictionary<String, Any> = [Const.NAME: name, Const.EMAIL: email, "default_album_created": true]
      dbRef.child(id).child(Const.PERSONAL_INFO).setValue(data)
  }

  
  func saveSetting(){
    if let id = Auth.auth().currentUser?.uid {
      let settingRef = dbRef.child(id).child(Const.SETTINGS)
      let DATA_FORMAT = defaults.integer(forKey: "dataFormat")
      let HOUR_FORMAT = defaults.bool(forKey: "dateFormatSwitch")
      let DATE_FORMAT = defaults.integer(forKey: "dateFormat")
      let SHADOW = defaults.bool(forKey: "textShadow")
      let FONT_SIZE = defaults.integer(forKey: "fontSize")
      let TRANSPARENCY = defaults.integer(forKey: "trans")
      let SAVE_ORIGINAL =  defaults.bool(forKey: "saveOriginal")
      let ONLY_DATE_ON_IMAGE = defaults.bool(forKey: "dateTimeOnly")
      let ACTIVE_PREDEFINED_TEXT = defaults.bool(forKey: "activePredifinedTexts")
      let ACTIVE_EMAIL_OPTION = defaults.bool(forKey: "emailOptionActive")
      let ACTIVE_STAMP_PROJECT_NAME = defaults.bool(forKey: Const.ACTIVE_STAMP_PROJECT_NAME)
      
      
      let data : Dictionary<String, Any> = [Const.DATA_FORMAT : DATA_FORMAT,
                                            Const.HOUR_FORMAT : HOUR_FORMAT,
                                            Const.DATE_FORMAT : DATE_FORMAT,
                                            Const.SHADOW : SHADOW,
                                            Const.FONT_SIZE : FONT_SIZE,
                                            Const.TRANSPARENCY : TRANSPARENCY,
                                            Const.SAVE_ORIGINAL : SAVE_ORIGINAL,
                                            Const.ONLY_DATE_ON_IMAGE : ONLY_DATE_ON_IMAGE,
                                            Const.ACTIVE_PREDEFINED_TEXT: ACTIVE_PREDEFINED_TEXT,
                                            Const.ACTIVE_EMAIL_OPTION: ACTIVE_EMAIL_OPTION,
                                            Const.ACTIVE_STAMP_PROJECT_NAME : ACTIVE_STAMP_PROJECT_NAME]
      
      settingRef.setValue(data)
    } else {
      return
    }
  }
  
  
  
  func saveDefinedFinds(){
    if let id = Auth.auth().currentUser?.uid {
      let definedFindRef = dbRef.child(id).child(Const.DEFINED_FINDS)
      let x = defaults.string(forKey: "x")!
      let a = defaults.string(forKey: "a")!
      let b = defaults.string(forKey: "b")!
      let c = defaults.string(forKey: "c")!
      let d = defaults.string(forKey: "d")!
      let e = defaults.string(forKey: "e")!
      let f = defaults.string(forKey: "f")!
      let o = defaults.string(forKey: "o")!
      let v = defaults.string(forKey: "v")!
      
      
      
      let data : Dictionary<String, Any> = ["x" : x,
                                            "a" : a,
                                            "b" : b,
                                            "c" : c,
                                            "d" : d,
                                            "e" : e,
                                            "f" : f,
                                            "o" : o,
                                            "v" : v]
                                         
      
      definedFindRef.setValue(data)
    } else {
      return
    }
  }

  
  
  func saveEmails(){
    if let id = Auth.auth().currentUser?.uid {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EmailData")
     
      
      do {
        let emails = try manageContext.fetch(request)

        for email in emails as! [EmailData]{
          let emailRef = dbRef.child(id).child(Const.EMAILS).child(email.key!)
          let data: Dictionary<String, Any> = [Const.EMAIL : email.email!, Const.SELECTED : email.selected, Const.KEY : email.key!]
          
          emailRef.setValue(data)
        }
      } catch {
        print("error")
      }
    }
  }

  func saveAlbums(){
    if let id = Auth.auth().currentUser?.uid {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
      
      
      do {
        let albums = try manageContext.fetch(request)
        
        for album in albums as! [AlbumData]{
          if album.name! != Const.APP_NAME {
            let albumRef = dbRef.child(id).child(Const.ALBUMS).child(album.key!)
            let data : Dictionary<String, Any> = [Const.NAME : album.name!,
                                                  Const.KEY : album.key!,
                                                  Const.CREATED_AT : album.createdAt,
                                                  Const.UPDATED_AT : album.updatedAt]
   
            
            albumRef.setValue(data)
          }
        }
      } catch {
        print("error")
      }
    }
  }

  

  
  func savePredTexts(){
    if let id = Auth.auth().currentUser?.uid {
      let predTextsRef = dbRef.child(id).child(Const.PREDEFINED_TEXTS)
      
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.PREDEFINED_TEXT_ENTITY)
      
      
      do {
        let predTexts = try manageContext.fetch(request)
        
        for predText in predTexts as! [PredefinedTextData] {
          let data: Dictionary<String, Any> = [Const.PREDEFINED_TEXT : predText.text!,
                                               Const.KEY : predText.key!,
                                               Const.SELECTED : predText.selected]
          
          let key = predText.key!
          //notesRef.childByAutoId().setValue(data)
          predTextsRef.child(key).setValue(data)
        }
      } catch {
        print("error")
      }
    }

  }
  
  
  func saveNotes(){
    if let id = Auth.auth().currentUser?.uid {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.NOTE_ENTITY)
      let notesRef = dbRef.child(id).child(Const.NOTES)

      do {
        let notes = try manageContext.fetch(request)
        print(notes.count)
        for note in notes as! [Notes]{
          let data: Dictionary<String, Any> = [Const.NOTE : note.text!,
                                               Const.KEY : note.key!]
          
          let key = note.key!
          notesRef.child(key).setValue(data)
        }
      } catch {
        print("error")
      }
    }
  }
  

  
  func saveDefinedFindsToCoreData(){
     if let id = Auth.auth().currentUser?.uid {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.DEFINED_FIND_ENTITY)
      let definedFindRef = dbRef.child(id).child(Const.DEFINED_FINDS)
      
      do {
        let definedFinds = try manageContext.fetch(request)
        print(definedFinds.count)
        for definedFind in definedFinds as! [DefinedFindData]{
          
          let data : Dictionary<String, Any> = [Const.INDEX : definedFind.index,
                                                Const.LETTER : definedFind.letter!,
                                                Const.TEXT : definedFind.text!,
                                                Const.KEY : definedFind.key!]
          
          
          definedFindRef.child(definedFind.key!).setValue(data)
        }
      } catch {
        print("Error saving data on Firebase")
      }
    }
  }
  
  func saveImagesData(){
    if let id = Auth.auth().currentUser?.uid {
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let manageContext = appDelegate.managedObjectContext
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
      let imagesDatasRef = dbRef.child(id).child(Const.IMAGES_DATA)
      
      do {
        let images = try manageContext.fetch(request)
        for result in images as! [ImageData]{

        let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: result.imgData, longDesc : result.longDesc)
          
//           let ID = image.id
//           let LATITUDE = Helpers.Instance.encryptString(text: image.latitude!)
//           let LONGITUDE = Helpers.Instance.encryptString(text: image.longitude!)
//           let ZONE = image.gZone!
//           let ACCURRARY = image.acurracy!
//           let COORDINATE_SYSTEM = image.coordinateSystem!
//           let TIMESTAMP = image.timestamp!
//           let ALBUM_NAME = image.albumName!
//           let OWNER_TEXT = image.note!
//           let KEY = image.key!
//           let DESC = Helpers.Instance.encryptString(text: image.desc!)
//           let URL = image.url!
//           let INITIALS = image.initials!
//           let SYMBOL_TEXT = image.symbolText!
//           let SYMBOL_LETTER = image.symbolLetter!

          
          
          let data = image.toDictionary()
//           let data: Dictionary<String, Any> = [Const.ID :ID,
//                                               Const.LATITUDE : LATITUDE,
//                                               Const.LONGITUDE : LONGITUDE,
//                                               Const.ZONE : ZONE,
//                                               Const.ACCURRARY : ACCURRARY,
//                                               Const.COORDINATE_SYSTEM : COORDINATE_SYSTEM,
//                                               Const.TIMESTAMP : TIMESTAMP,
//                                               Const.ALBUM_NAME : ALBUM_NAME,
//                                               Const.OWNER_TEXT : OWNER_TEXT,
//                                               Const.KEY : KEY,
//                                               Const.DESC: DESC,
//                                               Const.URL : URL,
//                                               Const.INITIALS : INITIALS,
//                                               Const.SYMBOL_TEXT : SYMBOL_TEXT,
//                                               Const.SYMBOL_LETTER : SYMBOL_LETTER]
          

          imagesDatasRef.child(result.key!).setValue(data)
          
        }
      } catch {
        print("error")
      }
    }
  }



  
  /// This function add every new Image Data to the External Database.
  func addImageDataToChild(image : Image){
    if let id = Auth.auth().currentUser?.uid {
      let imageDataRef = dbRef.child(id).child(Const.IMAGES_DATA).child(image.key)
      
      imageDataRef.updateChildValues(image.toDictionary())
      
    } else {
      print("Error saving ImageData on the Server")
      return
    }
  }
  
  //MARK: - Add Elements Functions
  ///This function add every new normal element to the External Database.
  func add(new email: Email){
    if let id = Auth.auth().currentUser?.uid {
      let emailRef = dbRef.child(id).child(Const.EMAILS).child(email.key)
      emailRef.updateChildValues(email.toDictionary())
      print("Email saved on the Server.")
    } else {
      print("Error saving email on the Server")
      return
    }
  }
  
  
  func add<T>(new element: T, model: Option){
    if let id = Auth.auth().currentUser?.uid {
      var dict = [String : Any]()
      
      switch model {
      case .album:
        let elem = element as! Album
        dict = elem.toDictionary()
          default:
        break
      }
      
        let ref = dbRef.child(id).child(model.child()).child(dict["key"] as! String)
        ref.updateChildValues(dict)
        print("Element saved on the Firebase from: DBProvider, line 223.")
      
    } else {
      print("Error saving Element on the Firebase from: DBProvider, line 223.")
      return
    }
  }
  
  func add(new predefinedText: PredefinedText){
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.PREDEFINED_TEXTS).child(predefinedText.key)
      ref.updateChildValues(predefinedText.toDictionary())
      print("Predifined text saved on the Server.")
    } else {
      print("Error saving predifined text on the Server")
      return
    }
  }
  
  
  func add(new note: Note){
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.NOTES).child(note.key)
      ref.updateChildValues(note.toDictionary())
      print("Note saved on the Server.")
    } else {
      print("Error saving note on the Server")
      return
    }
  }
  
   
  //MARK: - Fetching function
  func restore() {
    restoreAlbums()
    restoreSetting()
    restoreImagesData()
    restoreNotes()
    restoreEmails()
    restorePredefinedTexts()
   
  }
  
  
  func restoreSetting(){
    if let id = Auth.auth().currentUser?.uid {
      let settingRef = dbRef.child(id).child(Const.SETTINGS)
      
        settingRef.observeSingleEvent(of: .value, with: { (snapshot) in
          print(snapshot)
          if let setting = snapshot.value as? [String: AnyObject] {
            let activeEmailOption = (setting["active_email_option"] as! Bool)
            let activePredefinedText = (setting["active_predefined_text"] as! Bool)
            let dataFormat = (setting["data_format"] as! Int)
            let dateFormat = (setting["date_format"] as! Int)
            let fontSize = (setting["font_size"] as! Int)
            let hourFormat = (setting["hour_format"] as! Bool)
            let onlyDateOnImage = (setting["only_date_on_image"] as! Bool)
            let saveOriginal = (setting["save_original"] as! Bool)
            let shadow = (setting["shadow"] as! Bool)
            let transparency = (setting["transparency"]! as! Int)
            let activeStampProjectName = (setting[Const.ACTIVE_STAMP_PROJECT_NAME] as! Bool)
           
            defaults.set(activeStampProjectName, forKey: Const.ACTIVE_STAMP_PROJECT_NAME)
            defaults.set(activePredefinedText, forKey: "activePredifinedTexts")
            defaults.set(activeEmailOption, forKey: "emailOptionActive")
            defaults.set(dataFormat, forKey: "dataFormat")
            defaults.set(dateFormat, forKey: "dateFormat")
            defaults.set(fontSize, forKey: "fontSize")
            defaults.set(hourFormat, forKey: "dateFormatSwitch")
            defaults.set(onlyDateOnImage, forKey: "dateTimeOnly")
            defaults.set(saveOriginal, forKey: "saveOriginal")
            defaults.set(shadow, forKey: "textShadow")
            defaults.set(transparency, forKey: "trans")
          }
        })
    }
  }
  
  
  func restoreDefinedFinds() {
    
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.DEFINED_FINDS)

      
      ref.observeSingleEvent(of: .value, with: { (snapshot) in
        print(snapshot)
        if let definedFind = snapshot.value as? [String: AnyObject] {
          let x = self.castNil(definedFind["x"])
          let a = self.castNil(definedFind["a"])
          let b = self.castNil(definedFind["b"])
          let c = self.castNil(definedFind["c"])
          let d = self.castNil(definedFind["d"])
          let e = self.castNil(definedFind["e"])
          let f = self.castNil(definedFind["f"])
          let o = self.castNil(definedFind["o"])
          let v = self.castNil(definedFind["v"])
          
          defaults.set(x, forKey: "x")
          defaults.set(a, forKey: "a")
          defaults.set(b, forKey: "b")
          defaults.set(c, forKey: "c")
          defaults.set(d, forKey: "d")
          defaults.set(e, forKey: "e")
          defaults.set(f, forKey: "f")
          defaults.set(o, forKey: "o")
          defaults.set(v, forKey: "v")
        }
      })
    }
  
  
  }
  
  func castNil(_ value : AnyObject?) -> String{
    if value == nil {
      return "undefined"
    } else {
      return  (value as! String)
    }
  }
  
  
  
  //MARK: - Restore Emails functions.
  func restoreEmails(){
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.EMAILS)
      print("entró: restoreEmails()")
      ref.observeSingleEvent(of: .value, with: { (snapshot) in
        //print(snapshot.childrenCount)
        //let enumerator = snapshot.children
        
        for rest in snapshot.children {
          let email =  Email(snapshot: rest as! DataSnapshot)
          self.addNewEmailToServer(email: email)
        }
        
        
//        while let rest = enumerator.nextObject() as? DataSnapshot {
//
//          let result = rest.value! as? [String: AnyObject]
//          let emailValue = result!["email"] as! String
//          let selected = result!["selected"] as! Bool
//          let key = rest.key
//          let email = Email(email: emailValue, selected: selected, key: key )
//          print(email)
//          self.addNewEmailToServer(email: email)
//        }
      })
    } else {
      print("User not log in.")
    }
  }
  
  func addNewEmailToServer(email: Email){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.EMAIL_ENTITY, into: manageContext)
    print(email.email)
    print("entró: add(new email()")
    new.setValue(email.email, forKey: Const.EMAIL)
    new.setValue(email.selected, forKey: Const.SELECTED)
    new.setValue(email.key, forKey: Const.KEY)
    
    do{
      try manageContext.save()
      print("data saved.")
      
    }catch {
      print("error")
    }
  }

  
  //MARK: - Restore PredefinedTexts functions.
  func restorePredefinedTexts(){
    print("Run : restorePredefinedTexts ")
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.PREDEFINED_TEXTS)
  
      ref.observeSingleEvent(of: .value, with: { (snapshot) in
        //print(snapshot.childrenCount) // I got the expected number of items
        //let enumerator = snapshot.children
        
        for rest in snapshot.children {
          let predText =  PredefinedText(snapshot: rest as! DataSnapshot)
          self.addNewPredefinedTextToServer(predefinedText: predText)
        }
        
        
//        while let rest = enumerator.nextObject() as? DataSnapshot {
//
//          let result = rest.value! as? [String: AnyObject]
//
//          let text = result!["text"] as! String
//          let selected = result!["selected"] as! Bool
//          let key = rest.key
//
//          self.addNewPredefinedTextToServer(predefinedText: PredefinedText(text: text, selected: selected, key: key ))
//        }
      })
    } else {
      print("User not log in.")
    }
  }
  
  
  func addNewPredefinedTextToServer(predefinedText: PredefinedText){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: "PredefinedTextData", into: manageContext)
   
    new.setValue(predefinedText.text, forKey: Const.TEXT)
    new.setValue(predefinedText.selected, forKey: Const.SELECTED)
    new.setValue(predefinedText.key, forKey: Const.KEY)
    
    do{
      try manageContext.save()
      print("Run : PredefinedText saved on CoreData. Func call after RestorePredtext")
      
    }catch {
      print("Error saving PredefinedText on CoreData")
    }
  }

  //MARK: - Restore Notes functions.
  func restoreNotes(){
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.NOTES)

      
      ref.observeSingleEvent(of: .value, with: { (snapshot) in
        //let enumerator = snapshot.children
        
        for rest in snapshot.children {
          let note = Note(snapshot: rest as! DataSnapshot)
          self.addNoteToServer(note: note)
        }
        
//        while let rest = enumerator.nextObject() as? DataSnapshot {
//          let note = Note(snapshot: rest)
//
//          self.addNoteToServer(note: note)
//        }
      })
    } else {
      print("User not log in.")
    }
  }

  
  func addNoteToServer(note: Note) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.NOTE_ENTITY, into: manageContext)
    
    new.setValue(note.text, forKey: Const.TEXT)
    new.setValue(note.key, forKey: Const.KEY)
    
    do{
      try manageContext.save()
      print("Note saved in CoreData.")
      
    }catch {
      print("Error saving note in CoreData")
    }
  }
  
  
  func restoreAlbums(){
    if let id = Auth.auth().currentUser?.uid {
      let ref = dbRef.child(id).child(Const.ALBUMS)
      
      ref.observeSingleEvent(of: .value, with: { (snapshot) in
        //let enumerator = snapshot.children
        
        for rest in snapshot.children {
          let album = Album(snapshot: rest as! DataSnapshot)
          if album.name != Const.APP_NAME {
            self.addAlbumToServer(album: album)
            print("Album restored from Firebase. From restoreAlbums, DBProvider")
          }
          
        }
        
        
//        while let rest = enumerator.nextObject() as? DataSnapshot {
//          let album = Album(snapshot: rest)
//          if album.name != Const.APP_NAME {
//            self.addAlbumToServer(album: album)
//            print("Album restored from Firebase. From restoreAlbums, DBProvider")
//          }
//        }
      })
    } else {
      print("User not log in.")
    }
  }
  
  
  
  func addAlbumToServer(album: Album) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.ALBUM_ENTITY, into: manageContext)
    
    new.setValuesForKeys(album.toDictionary())
    
    
    do{
      try manageContext.save()
      print("Album saved in CoreData. From addAlbumToServer, DBProvider")
      
    }catch {
      print("Error saving album in CoreData, From addAlbumToServer, DBProvider")
    }
  }
  
  
  
  func adddefinedFindToServer(_ definedFind: DefinedFind) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.DEFINED_FIND_ENTITY, into: manageContext)
    
    new.setValuesForKeys(definedFind.toDictionary())
    
    
    do{
      try manageContext.save()
      print("DefinedFind saved in CoreData. From addDefinedFindToServer, DBProvider")
      
    }catch {
      print("Error saving album in CoreData, From addDefinedFindToServer, DBProvider")
    }
  }

  
  
  
  
  //MARK: - Restore Image Data functions
  func restoreImagesData(){
    
    DispatchQueue.global(qos: .userInitiated).async {
      
     
      if let id = Auth.auth().currentUser?.uid {
        let ref = self.dbRef.child(id).child(Const.IMAGES_DATA)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
          let enumerator = snapshot.children
          print("total de imagenes: ", enumerator.allObjects.count)
          defaults.set(snapshot.children.allObjects.count, forKey: "numberOfFiles")
          
          var counter = 0
          
          for rest in snapshot.children {
            let image = Image(snapshot: rest as! DataSnapshot)
            counter = counter + 1
            print("Counter: ", counter)
            self.addImageDataToServer(image: image)
          }
          
  //        while let rest = enumerator.nextObject() as? DataSnapshot {
  //          let image = Image(snapshot: rest)
  //          counter = counter + 1
  //          print("Counter: ", counter)
  //          self.addImageDataToServer(image: image)
  //
  //        }
        })
      }   else {
        print("User not log in.")
      }
    }
  }
    
  func addImageDataToServer(image : Image){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.IMAGE_ENTITY, into: manageContext)
    
    new.setValuesForKeys(image.toCoreDataDictionary())

    do{
      try manageContext.save()
      print("Image Data saved in CoreData.")
      
    }catch {
      print("Error saving Image Data in CoreData")
    }
  }
}

