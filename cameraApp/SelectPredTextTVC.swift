//
//  SelectPredTextTVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 7/10/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth
import FirebaseDatabase

class SelectPredTextTVC: UITableViewController {

  var selectPredText : String = ""
  var predTexts = [String]()
  var key = String()
  

  
  override func viewDidLoad() {
    super.viewDidLoad()
    print("key from predtext")
    print(key)
  }
//

//  
  // MARK: - Table view data source
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return predTexts.count
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
    cell.textLabel?.text = predTexts[indexPath.row]
    return cell
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if section == 0 {
      return "Select a Predefined Stamp Text"
    }
    
    return ""
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 70
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectPredText = predTexts[indexPath.row]
    updateAImage()
    navigationController?.popViewController(animated: true)
  }
  
  
  func updateAImage(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let managedObject = result[0] as! ImageData
      
      
      managedObject.setValue(selectPredText, forKey: "note")
      
      do{
        try manageContext.save()
        
        print("Image updated.")
        
        
      }catch {
        print("Error trying to editing the note.")
      }
    }
  }
  


}
