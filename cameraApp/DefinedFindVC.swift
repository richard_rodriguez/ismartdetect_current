//
//  DefinedFindVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 1/25/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import Foundation
import CoreData
import FirebaseDatabase
import FirebaseAuth
import NotificationCenter


class DefinedFindVC : UITableViewController, UITextFieldDelegate {
  
  
  @IBOutlet weak var x: UITextField!
  @IBOutlet weak var a: UITextField!
  @IBOutlet weak var b: UITextField!
  @IBOutlet weak var c: UITextField!
  @IBOutlet weak var d: UITextField!
  @IBOutlet weak var e: UITextField!
  @IBOutlet weak var f: UITextField!
  @IBOutlet weak var o: UITextField!
  @IBOutlet weak var v: UITextField!

  var updateSecond : Timer!
  var definedFinds = [DefinedFind]()
  var activeTextField = UITextField()

  
  var handle: UInt!
  var ref : DatabaseReference!
  
  override func viewWillAppear(_ animated: Bool) {
    if let _ = Auth.auth().currentUser?.uid {
      let responde = ObserversProvider.Instance.observerDefinedFind()
      handle = responde.handle
      ref = responde.ref
    }
  
    x.delegate = self
    a.delegate = self
    b.delegate = self
    c.delegate = self
    d.delegate = self
    e.delegate = self
    f.delegate = self
    o.delegate = self
    v.delegate = self
    
    let notificationCenter = NotificationCenter.default
    notificationCenter.addObserver(self, selector: #selector(DefinedFindVC.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    notificationCenter.addObserver(self, selector: #selector(DefinedFindVC.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    
    updateTextFields()
    
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startTimer()
    IQKeyboardManager.sharedManager().enable = false
    IQKeyboardManager.sharedManager().enableAutoToolbar = true
    
    
    
  }
  
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    stopTimer()
    let notificationCenter = NotificationCenter.default
    notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    
    let x = self.x.text!
    let a = self.a.text!
    let b = self.b.text!
    let c = self.c.text!
    let d = self.d.text!
    let e = self.e.text!
    let f = self.f.text!
    let o = self.o.text!
    let v = self.v.text!

    defaults.set(x, forKey: "x")
    defaults.set(a, forKey: "a")
    defaults.set(b, forKey: "b")
    defaults.set(c, forKey: "c")
    defaults.set(d, forKey: "d")
    defaults.set(e, forKey: "e")
    defaults.set(f, forKey: "f")
    defaults.set(o, forKey: "o")
    defaults.set(v, forKey: "v")

    DBProvider.Instance.saveDefinedFinds()
    
    
    if let _ = Auth.auth().currentUser?.uid {
      ref.removeObserver(withHandle: handle!)
    }
    
  }
  
  func updateTextFields(){
    x.text = defaults.string(forKey: "x")
    a.text = defaults.string(forKey: "a")
    b.text = defaults.string(forKey: "b")
    c.text = defaults.string(forKey: "c")
    d.text = defaults.string(forKey: "d")
    e.text = defaults.string(forKey: "e")
    f.text = defaults.string(forKey: "f")
    o.text = defaults.string(forKey: "o")
    v.text = defaults.string(forKey: "v")
  }
  
  
  func keyboardWillShow(notification: NSNotification) {
    let userInfo = notification.userInfo!
    let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        
    UIView.animate(withDuration: 0.3) {
      if ( self.activeTextField != self.x && self.activeTextField != self.a && self.activeTextField != self.b  ) {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
      }
    }
    self.view.layoutIfNeeded();

    
    print(keyboardHeight);
  }
  
  func keyboardWillHide(notification: NSNotification) {
    let userInfo = notification.userInfo!
    let keyboardHeight = (-1 * ((userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height))

    UIView.animate(withDuration: 0.3) {
      
      self.tableView.contentInset = UIEdgeInsetsMake((-1 * keyboardHeight) * 0.3, 0, (2 * keyboardHeight), 0)
    }
    self.view.layoutIfNeeded();
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
      self.activeTextField = textField
      stopTimer()
  }
  
  


  
  func fetching(){
    updateTextFields()
    self.tableView.reloadData()
  }
  

  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(DefinedFindVC.fetching), userInfo: nil, repeats: true)
    }
  }
  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }
  
  
  
  
}



















