//
//  LaunchScreenVC.swift
//  cameraApp
//
//  Created by Richard Rodriguez on 9/20/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit

class LaunchScreenVC: UIViewController {
  @IBOutlet weak var backgroundImage: UIImageView!
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    changeBackgroundColor()
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }

  
  func changeBackgroundColor(){
    let screenshot = defaults.integer(forKey: "backgroundColor")
    
    switch screenshot {
    case 0:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 0")
    case 1:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 1")
    case 2:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 2")
    case 3:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 3")
    default:
      backgroundImage.image = #imageLiteral(resourceName: "Rectangle 0")
    }
    
  }

}
