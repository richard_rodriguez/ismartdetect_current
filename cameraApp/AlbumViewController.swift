//  AlbumViewController.swift
//  Created by Richard Rodriguez on 7/30/16.
//  Copyright © 2016 Richard Rodriguez. All rights reserved.
import Foundation
import Photos
import FirebaseAuth
import FirebaseDatabase
import CoreData
import CoreLocation
import KeychainSwift



class AlbumViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate {
  var assetCollection: PHAssetCollection!
  var photoAsset : PHFetchResult<AnyObject>!
  var albumFound : Bool = false
  var albumFiltered = [String]()
  var searchController : UISearchController!
  let resultController = UITableViewController()
  let defaults = UserDefaults.standard
  var updateSecond : Timer!
  let locationManager = CLLocationManager()
  let keychain = KeychainSwift()

  var magicAlbumName : String!
  @IBOutlet weak var magicToolButton: UIBarButtonItem!
  @IBOutlet weak var navigationTitle: UINavigationItem!
  @IBOutlet weak var addAlbumButton: UIBarButtonItem!
  
  
  //1.0.20
  var handle: UInt!
  var ref : DatabaseReference!

  var acc = "1.0"
  
  var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()

  var names = [String]()
  var albums = [Album]()
  
  var handleImages: UInt!
  var refImages : DatabaseReference!
  var handleAlbums : UInt!
  var refAlbums : DatabaseReference!
  

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let countryCode = defaults.string(forKey: "countryCode")
    if countryCode == "DK" {
      initializeLocationManager()
    }
    
    names = []
    albums = []
    fetchAlbums()
    
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    startTimer()

   
    
    startObservers()
    
    self.resultController.tableView.delegate = self
    self.resultController.tableView.dataSource = self
    
    self.searchController = UISearchController(searchResultsController: self.resultController)
    self.tableView.tableHeaderView = self.searchController.searchBar
    self.searchController.searchResultsUpdater = self
    definesPresentationContext = true
  }

  
  func startObservers(){
    DispatchQueue.global(qos: .userInteractive).async {
      if let _ = Auth.auth().currentUser?.uid {
        let resultAlbum = self.observerAlbums()
        self.handleAlbums = resultAlbum.handle
        self.refAlbums = resultAlbum.ref
      }
    }

  }

  
  func stopObservers(){
    DispatchQueue.global(qos: .background).async {
      if let _ = Auth.auth().currentUser?.uid {
       // self.refImages.removeObserver(withHandle: self.handleImages!)
        self.refAlbums.removeObserver(withHandle: self.handleAlbums!)
      }
    }
  }
  
  
  func initializeLocationManager(){
    locationManager.delegate = self
    locationManager.requestLocation()
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
  }
  
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    
    if let value = location?.horizontalAccuracy {
      acc = String(describing: value)
    }
    
    let countryCode = defaults.string(forKey: "countryCode")
    if countryCode == "DK" {
      let heartButton = UIBarButtonItem(title: "\(Helpers.Instance.heart(acc))", style: .done, target: self, action: nil)
      heartButton.isEnabled = false
      self.navigationItem.rightBarButtonItems = [addAlbumButton, magicToolButton, heartButton]
    } else {
      self.navigationItem.rightBarButtonItems = [addAlbumButton]
    }
    
    Helpers.Instance.getMagicToolText((location?.coordinate.latitude)!, (location?.coordinate.longitude)!) { (text) in
      self.magicAlbumName = text
    }
    
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Failed to find user's location: \(error.localizedDescription)")
  }
  
  func updateSearchResults(for searchController: UISearchController) {
    print(self.searchController.searchBar.text!)
    self.albumFiltered = self.names.filter({ (album: String) -> Bool in
      if album.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
        return true
      } else {
        return false
      }
    })
    self.resultController.tableView.reloadData()
  }

  
  func fetching(){
    names = []
    albums = []
    fetchAlbums()
    self.tableView.reloadData()
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    stopTimer()
    
    let countryCode = defaults.string(forKey: "countryCode")
    if countryCode == "DK" {
      locationManager.stopUpdatingLocation()
    }
    
//    DispatchQueue.global(qos: .userInteractive).async {
//      if let _ = Auth.auth().currentUser?.uid {
//        self.refImages.removeObserver(withHandle: self.handleImages!)
//        self.refAlbums.removeObserver(withHandle: self.handleAlbums!)
//      }
//    }
    
        DispatchQueue.global(qos: .userInteractive).async {
          if let _ = Auth.auth().currentUser?.uid {
            //self.refImages.removeObserver(withHandle: self.handleImages!)
            self.refAlbums.removeObserver(withHandle: self.handleAlbums!)
          }
        }

    
  }
  
  
  override func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
    stopTimer()
  }
  
 
  @IBAction func newAlbumBtn(_ sender: UIBarButtonItem) {
    let status = PHPhotoLibrary.authorizationStatus()
    
    switch status {
    case .authorized:
       self.showNewAlbumAlertView()
       print(".authorized")
    case .denied, .restricted:
      self.showPhotoLibraryAlert()
      print(".denied, .restricted")
    case .notDetermined:
      print("notDetermined")
      PHPhotoLibrary.requestAuthorization() { status in
        switch status {
        case .authorized:
          self.showNewAlbumAlertView()
        case .denied, .restricted:
          self.showPhotoLibraryAlert()
          print(".denied, .restricted after request")
        case .notDetermined:
          break
          // won't happen
        }
      }
    }

  }
  
  func showNewAlbumAlertView() {
    let alert = UIAlertController(title: "New Project", message: "Enter a name for this Project", preferredStyle: UIAlertControllerStyle.alert)
    
    alert.addTextField { (textField) in
      textField.placeholder = "Title"
      textField.autocapitalizationType = .sentences
    }
    
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
      (action) -> Void in
      let textf = (alert.textFields?[0])! as UITextField
      self.newAlbum(albumName: textf.text!)
    })
    )
    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
    
    present(alert, animated: true, completion: nil)
  }
  
  

  
  
  func newAlbum(albumName: String) {
    //Check if the folder exists, if not, create it
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
    let collection:PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: fetchOptions)
    
    if let first_Obj:AnyObject = collection.firstObject{
      //found the album
      presentAlert(albumName: albumName)
      self.albumFound = true
      self.assetCollection = first_Obj as! PHAssetCollection
    }else{
      //Album placeholder for the asset collection, used to reference collection in completion handler
      var albumPlaceholder:PHObjectPlaceholder!
      //create the folder
      NSLog("\nFolder \"%@\" does not exist\nCreating now ...", albumName)
      PHPhotoLibrary.shared().performChanges({
        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName)
        albumPlaceholder = request.placeholderForCreatedAssetCollection
        },
           completionHandler: {(success:Bool, error:Error?)in
            if(success){
              print("Successfully created folder")
              self.albumFound = true
              let collection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumPlaceholder.localIdentifier], options: nil)
              self.assetCollection = collection.firstObject! as PHAssetCollection
              
              DispatchQueue.main.async(execute: { () -> Void in
                self.cleanAllRow()
                
                //Calling function to save in coredata
                let key = Database.database().reference().child(Const.ALBUMS).childByAutoId().key
                let date = Date().timeIntervalSince1970
                var owner = self.keychain.get("secure_digits") ?? "0000"
                
                if owner == "" {
                  owner = "0000"
                }
                
                let album = Album(name: albumName, key: key, owner: owner, createdAt: date, updatedAt: date)
                
                self.add(new: album)
                self.keychain.set("1", forKey: albumName)
                self.keychain.set(albumName, forKey: "albumName")
              })
              
            }else{
              print("Error creating folder")
              self.albumFound = false
            }
      })
    }
  }
  
  
  func cleanAllRow(){
    let numberOfAlbum = Helpers.Instance.albumList().count //albumList().size
     for index in 0..<numberOfAlbum {
       let rowToSelect : IndexPath = IndexPath(row: index, section: 0)  //slecting 0th row with 0th section
       if let cell = tableView.cellForRow(at: rowToSelect) {
         cell.accessoryType = .none
      }
    }
  }
  

  func selectRow(index: Int) {
    let rowToSelect:IndexPath = IndexPath(row: index, section: 0);  //slecting 0th row with 0th section
    
    if let cell = tableView.cellForRow(at: rowToSelect) {
      cell.accessoryType = .checkmark
    }
  }
  
  
  func albumIndex(name: String) -> Int {
    return albumArray().list.index(of: name)!
  }
  
  
  func albumArray() -> (list: [String], size: Int) {
    let albumList : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
    let albumSize : Int = albumList.count
    var tempArray : [String] = []
    
    for index in 1..<albumSize {
      tempArray.append([albumList[index].localizedTitle!][0])
    }
    return (tempArray, albumSize)
  }

  
  func selectAlbum(name: String) {
    let indexToSelect = albumList().list.index(of: name)!
    defaults.set(indexToSelect, forKey: "albumIndex")
  }
  
  
  func albumList() -> (list: [String], size: Int) {
    var albumList : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
    var albumSize : Int = albumList.count
    var tempArray : [String] = []
    
    if albumSize == 0 {
      newAlbum(albumName:  defaults.string(forKey: "appName")!)
      albumList = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
      albumSize  = albumList.count
    }
    
    for index in 1..<albumSize {
      tempArray.append([albumList[index].localizedTitle!][0])
    }
    
    return (tempArray, albumSize)
  }


  func startTimer(){
    if updateSecond == nil {
      updateSecond = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(AlbumViewController.fetching), userInfo: nil, repeats: true)
    }
  }

  
  func stopTimer(){
    if updateSecond != nil {
      updateSecond!.invalidate()
      updateSecond = nil
    }
  }

  
}



extension AlbumViewController {
 //tableview functions
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.tableView {
      return self.names.count
    } else {
      return self.albumFiltered.count
    }
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell
    
    if tableView == self.tableView {
      cell.textLabel!.text = self.names[indexPath.row]
      let numberOfFiles = records(for: self.names[indexPath.row])
      cell.detailTextLabel!.text = String(numberOfFiles) + "\(numberOfFiles == 1 ? " File" : " Files")"
      cell.selectionStyle = UITableViewCellSelectionStyle.none
      
      let albumSelected = defaults.string(forKey: "albumName")!
      let index = names.index(of: albumSelected)
      
      if index == indexPath.row {
        cell.accessoryType = .checkmark
      } else {
        cell.accessoryType = .none
      }

    } else {
      cell.textLabel!.text = self.albumFiltered[indexPath.row]
      let numberOfFiles = records(for: self.albumFiltered[indexPath.row])
      cell.detailTextLabel!.text = String(numberOfFiles) + "\(numberOfFiles == 1 ? " File" : " Files")"
      cell.selectionStyle = UITableViewCellSelectionStyle.none
      let index = self.names.index(of: self.albumFiltered[indexPath.row])
      
      if index == indexPath.row {
        cell.accessoryType = .checkmark
        
      } else {
        cell.accessoryType = .none
      }
    }
    
    cell.tintColor = Const.RED
    return cell
  }
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if tableView == self.tableView {
      
      defaults.set(indexPath.row, forKey: "albumIndex")
      defaults.set(names[indexPath.row], forKey: "albumName")
      print("You selected album: \(defaults.integer(forKey: "albumIndex"))")

      cleanAllRow()

      let album = albums[indexPath.row]
      tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
      edit(album)
      
    } else {
      cleanAllRow()
      
      let index = names.index(of: albumFiltered[indexPath.row])
      let album = albums[index!]
      
      tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
      edit(album)
      
      defaults.set(albumIndex(name: albumFiltered[indexPath.row]), forKey: "albumIndex")
      defaults.set(albumFiltered[indexPath.row], forKey: "albumName")
      print("You selected album: \(defaults.integer(forKey: "albumIndex"))")
    }
    
  }
  
  
  override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath) {
      cell.accessoryType = .none

    }

  }

  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
     
      
      if keychain.get("secure_digits") == self.albums[indexPath.row].owner {
        handleDelete(indexPath)
      } else {
        let alert = UIAlertController(title: "NOTICE!", message: "Sorry, you can not delete this album unless you have the owner code, because you did not create it.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let enterCode = UIAlertAction(title: "Enter Code", style: .destructive, handler: { (action) in
          self.handleEnterCode(indexPath)
        })
        
        alert.addAction(ok)
        alert.addAction(enterCode)
        
        present(alert, animated: true, completion: nil)
      }
    }

  }

  
  func handleEnterCode(_ indexPath : IndexPath){
    let alert  = UIAlertController(title: "SECURE CODE", message: "Enter the device code for this album", preferredStyle: .alert)
    
    alert.addTextField { (textfield) in
      textfield.placeholder = "Enter 4 digits code"
      textfield.keyboardType = .decimalPad
      textfield.delegate = self
      textfield.tag = 100
    }
    
    let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
      if alert.textFields?[0].text == self.albums[indexPath.row].owner! {
        self.handleDelete(indexPath)
      } else {
        alert.textFields?[0].text = ""
        alert.title = "WRONG PASSWORD!"
        alert.message = "Try again"
        self.present(alert, animated: true, completion: nil)
      }
    })
    
    alert.addAction(delete)
  
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    present(alert, animated: true, completion: nil)
  }
  
  
  func handleDelete(_ indexPath : IndexPath){
    let alert = UIAlertController(title: "WARNING!", message: "You are about to delete '\(names[indexPath.row])' album. This action will not delete your local album on your Photos app, your photos in it either. ", preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: "Continue", style: .destructive, handler: { (action) in
      //self.stopObservers()
      self.names.remove(at: indexPath.row)
      self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
      DispatchQueue.global(qos: .background).async {
        self.deletePhotosBy(self.albums[indexPath.row].name)
        self.delete(album: self.albums[indexPath.row])
        
        DispatchQueue.main.async { [unowned self] in
          self.a()
        }
      }
    
      
      
    }))
    
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    present(alert, animated: true, completion: nil)

  }
  
  
  func a() {
    DispatchQueue.main.async { [unowned self] in
      self.fetching()
      self.tableView.reloadData()
    }
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if textField.tag == 100 {
      if (textField.text?.count)! >= 4 && range.length == 0 {
        return false
      }
    }
    return true
  }
  
  
}


extension AlbumViewController {
//coredata and firebase functions
  
  func fetchAlbums(){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    
    let sortDescriptor = NSSortDescriptor(key: Const.UPDATED_AT, ascending: false)
    request.sortDescriptors = [sortDescriptor]
    
    
    do {
      let albums = try manageContext.fetch(request)
      for album in albums as! [AlbumData]{
        let newAlbum = Album(name: album.name!, key: album.key!, owner: album.owner, createdAt: album.createdAt, updatedAt: album.updatedAt)
                
        let name = newAlbum.name
        names.append(name)
        self.albums.append(newAlbum)
      }
      
    } catch {
      print("Error fetching all Notes")
    }
  }

  

  func edit(_ album: Album){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", album.key)
    if let result = try? manageContext.fetch(request){
      if result.count > 0 {
        let managedObject = result[0] as! AlbumData
        let time = Date().timeIntervalSince1970
        managedObject.setValue(time, forKey: "updatedAt")
        
        do{
          try manageContext.save()
          
          if let uid = Auth.auth().currentUser?.uid {
            let key = album.key
            let ref = Database.database().reference().child(uid).child(Const.ALBUMS).child(key)
            let new : Dictionary<String, Any> = [Const.NAME : album.name,
                                                 Const.KEY : album.key,
                                                 Const.CREATED_AT : album.createdAt,
                                                 Const.UPDATED_AT : time]
            ref.updateChildValues(new)
            print("Album updated on Firebase.")
          }

          print("Album updated on CoreData.")
        }catch {
          print("error")
        }
      }
    }
  }
  
  
  func add(new album: Album) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let new = NSEntityDescription.insertNewObject(forEntityName: Const.ALBUM_ENTITY, into: manageContext)
    new.setValuesForKeys(album.toDictionary())
   
    do{
      try manageContext.save()
      print("Album saved on CoreData.")
      
      DBProvider.Instance.add(new: album, model: .album)
      
    }catch {
      print("Error trying to saving Album on Coredata")
    }
  }
  
  
  func delete(album: Album){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.ALBUM_ENTITY)
    request.predicate = NSPredicate(format: "key = %@", album.key)
    
    if let result = try? manageContext.fetch(request){
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        if let id = Auth.auth().currentUser?.uid {
          let ref = Database.database().reference().child(id).child(Const.ALBUMS).child(album.key)
          ref.removeValue()
        }
        print("Album deleted from AlbumVC, func delete(album)  line 381.")
      }catch {
        print("Error trying to delete Album from AlbumVC, func delete(album)  line 381")
      }
    }
  }

  func presentAlert(albumName : String){
    let alert = UIAlertController(title: "NOTICE", message: "This album is already in your Photos Albums. Do you want to use this album?", preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
      self.cleanAllRow()
      let key = Database.database().reference().child(Const.ALBUMS).childByAutoId().key
      let date = Date().timeIntervalSince1970
      var owner = self.keychain.get("secure_digits") ?? ""
      
      if owner == "" {
        owner = "0000"
      }
      
      let album = Album(name: albumName, key: key, owner: owner, createdAt: date, updatedAt: date)
      self.add(new: album)
      self.defaults.set(albumName, forKey: "albumName")

      DispatchQueue.main.async {
        self.tableView.reloadData()
     }

    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
    present(alert, animated: true, completion: nil)
  }
  
  
  func showPhotoLibraryAlert(){
    let alert = UIAlertController(
      title: "Action Required!",
      message: "Photo Library access required for this app.",
      preferredStyle: UIAlertControllerStyle.alert
    )
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
    
    alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) -> Void in
       UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
    }))
    present(alert, animated: true, completion: nil)
  }
  
  func records(for album: String) -> Int {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album)
    request.predicate = predicate
    
    do {
      let results = try manageContext.fetch(request)
      return results.count
    } catch {
      print("error")
    }
    return 0
  }
  
  

}



extension AlbumViewController {

  @IBAction func addMagicAlbum(_ sender: Any) {
    let alert = UIAlertController(title: "New Project", message: "Name magically generated. Edit it as you want.", preferredStyle: .alert)

    alert.addTextField(configurationHandler: {(textField: UITextField) in
      textField.text = self.magicAlbumName
    })
    
    
    let save = UIAlertAction(title: "Save", style: .default) { (action) in
      self.newAlbum(albumName: (alert.textFields!.first?.text)!)
    }
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    alert.addAction(cancel)
    alert.addAction(save)
    
    present(alert, animated: true, completion: nil)

  }

}


extension AlbumViewController {
 
  func deletePhotosBy(_ album : String) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album)
    request.predicate = predicate
    
      if let results = try? manageContext.fetch(request) {
        for result in results as! [ImageData]{
          deletePhoto(by: result.key!)
        }
      }
  }

  func deletePhoto(by key : String){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.predicate = NSPredicate(format: "key = %@", key)
    
    if let result = try? manageContext.fetch(request){
      let object = result[0]
      manageContext.delete(object as! NSManagedObject)
      
      do{
        try manageContext.save()
        print("data deleted.")
        
        if let uid = Auth.auth().currentUser?.uid {
          let key = key
          let ref = Database.database().reference().child(uid).child(Const.IMAGES_DATA).child(key)
          ref.removeValue()
          print("Deleted Image Data on firebase: ", key)
          
        } else {
          print("Error trying to Image Data.")
        }
        
      }catch {
        print("error")
      }
    }
  }
  
  
}

//1.0.20
extension AlbumViewController {
//  func observerImages() -> (handle: UInt?, ref: DatabaseReference?) {
//    var ref : DatabaseReference!
//    
//    if let id = Auth.auth().currentUser?.uid {
//      ref = Database.database().reference().child(id).child(Const.IMAGES_DATA)
//      ref.keepSynced(true)
//      
//      handle = ref.observe(.childAdded, with: { (snapshot) in
//            let image = Image(snapshot: snapshot)
//            //print(image)
//            ObserversProvider.Instance.add(new: image, option: .image)
//      }, withCancel: nil)
//    } else {
//      print("User not log in.")
//    }
//    
//    return (handle, ref)
//  }
  
  
  func observerAlbums() -> (handle: UInt?, ref: DatabaseReference?) {
    var ref : DatabaseReference!
    
    if let id = Auth.auth().currentUser?.uid {
      ref = Database.database().reference().child(id).child(Const.ALBUMS)
      ref.keepSynced(true)

      handle = ref.observe(.childAdded, with: { (snapshot) in
      let album = Album(snapshot: snapshot)
      ObserversProvider.Instance.add(new: album, option: .album)
      }, withCancel: nil)
    } else {
      print("User not log in.")
    }
    
    return (handle, ref)
  }
  

  
}
