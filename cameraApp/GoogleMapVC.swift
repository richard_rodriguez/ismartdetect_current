//
//  GoogleMapVC.swift
//  iSmartDetect
//
//  Created by Richard Rodriguez on 2/1/17.
//  Copyright © 2017 Richard Rodriguez. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData
import AlamofireImage
import Alamofire
import FirebaseAuth
import FirebaseDatabase
import SDWebImage
import KeychainSwift
import SimplePDF
import MessageUI

@objc
class GoogleMapVC : UIViewController, GMSMapViewDelegate, MFMailComposeViewControllerDelegate {

  
  var album : String?
  var albumNameList = [String]()
  var responde = ""
  var albumNameListFiltered = [String]()
  var appName : String?
  var imagesIDs = [String]()
  var image : Image?
  var mapView: GMSMapView?
  var images = [Image]()
  let keychain = KeychainSwift()
  var index : Int!
  var markers = [CSMarker]()
  @IBOutlet weak var viewPhotosButton: UIBarButtonItem!
  var camera : GMSCameraPosition!
  
  let imagePlaceholderData = UIImageJPEGRepresentation(UIImage(named: "placeholderImage")!, 1)
  
  var tempCurrentIndex : Int = 0
  var numberOfImages : Int = 0
  
  override func viewWillAppear(_ animated: Bool) {
   super.viewWillAppear(animated)

    self.navigationController?.navigationBar.tintColor = Const.RED
   
    
   newMethod()
   newMethod()
    
  }
  
  func cleanMarkers(){
    for marker in markers {
      marker.map = nil
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    newMethod()
    
  }

  
  func captureScreen() -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
    view.layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  
  func newMethod(){
    GMSServices.provideAPIKey("AIzaSyAMKn4fHA_rDCApJtGFSOgFgWHDMgMlQ8E")
    navigationItem.backBarButtonItem?.title = "Back"
    mapView?.delegate = self
    self.images = []
    self.markers = []
    

    
   
    appName = defaults.string(forKey: "appName")!
    self.navigationItem.title = "Waypoints"
    
    let exportButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(GoogleMapVC.exportData(_:)))
    //deletePoints is called like that because that was its first duty, now it is a tool download and delete.
    let listingButtonImage = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 27))
    let listingImage = UIImage(named: "listing-bold")
    listingButtonImage.setImage(listingImage!, for: .normal)
    listingButtonImage.addTarget(self, action: #selector(GoogleMapVC.deletePoints(_:)), for: .touchUpInside)
    let deleteButton = UIBarButtonItem(customView: listingButtonImage)
    
    
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 21))
    label.textAlignment = .center
    label.text = album!

    let albumName = UIBarButtonItem(customView: label)
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
   
    exportButton.tintColor  = Const.RED
    deleteButton.tintColor = UIColor.red
    
    toolbarItems = [exportButton, spacer, albumName, spacer, deleteButton]
    
    navigationController?.setToolbarHidden(false, animated: true)
    
    if let nav_height = self.navigationController?.navigationBar.frame.height{
      let status_height = UIApplication.shared.statusBarFrame.size.height
      mapView?.padding = UIEdgeInsetsMake (nav_height+status_height,0,nav_height,0);
    }
    
    
    setMapView()

  }
  
  
  func setMapView(){
    
    
    _ = fetchData()
    
    image = images[0]
    
    if let _ =  defaults.string(forKey: "latitude" + self.album!) {
      let latitude = defaults.string(forKey: "latitude" + self.album!)
      let longitude = defaults.string(forKey: "longitude" + self.album!)
      let zoom = defaults.string(forKey: "zoom" + self.album!)
      self.camera = GMSCameraPosition.camera(withLatitude: Double(latitude!)!, longitude: Double(longitude!)!, zoom: Float(zoom!)!)
    } else {
      self.camera = GMSCameraPosition.camera(withLatitude: (self.image?.decimalLongitude)!, longitude: (self.image?.decimalLatitude)!, zoom: 15)
    }
   
    tempCurrentIndex = 0
    numberOfImages = markers.count
    
    view = mapView
    mapView?.settings.compassButton = true
    mapView?.isMyLocationEnabled = true
    mapView?.settings.myLocationButton = true
    mapView = GMSMapView.map(withFrame: .zero, camera: self.camera)
    mapView?.mapType = .hybrid // kGMSTypeHybrid
    
    for marker in markers {
      draw(marker)
    }
  }
  
  
  
  
  @IBAction func previousPoint(_ sender: Any) {
    backImage()
  }
  
  @IBAction func nextPoint(_ sender: Any) {
    nextImage()
  }
  
  func backImage() {
    let id = mod((tempCurrentIndex - 1), numberOfImages)
    tempCurrentIndex = Int(id)
    updateCamera(marker: markers[id])
  }
  
  func nextImage() {
    let id = (tempCurrentIndex + 1) % numberOfImages
    tempCurrentIndex = id
    updateCamera(marker: markers[id])
  }
  
  func updateCamera(marker : CSMarker){

    view = mapView
    
    mapView?.settings.compassButton = true
    mapView?.isMyLocationEnabled = true
    mapView?.settings.myLocationButton = true

    
    if let nav_height = self.navigationController?.navigationBar.frame.height{
      let status_height = UIApplication.shared.statusBarFrame.size.height
      mapView?.padding = UIEdgeInsetsMake (nav_height+status_height,0,nav_height,0);
    }
    
 
    CATransaction.begin()
    CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
    

    
    let newPosition = GMSCameraUpdate.setTarget(marker.position)
    mapView?.animate(with: newPosition)
    

    marker.map = mapView
    mapView?.selectedMarker = marker
    
    view = mapView
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    SDWebImageManager.shared().imageCache?.clearMemory()
    navigationController?.setToolbarHidden(true, animated: true)
  }
  
  @IBAction func viewPhotos(_ sender: Any) {
    performSegue(withIdentifier: "viewPhotos", sender: self)
  }
  
  @IBAction func deletePoints(_ sender: Any) {
    deletePoints()
  }
  
  @IBAction func exportPoints(_ sender: Any) {
    exportData(sender as! UIButton)
  }
  
  
  func draw(_ marker : CSMarker){
    if marker.map == nil {
      marker.map = mapView
    }
  }
  

  func setMarker(for image: Image){
    let currentLocation = CLLocationCoordinate2DMake(image.decimalLongitude, image.decimalLatitude)
    
    let infoWindow = Bundle.main.loadNibNamed("InfoWindow", owner: self, options: nil)?.first as! CustomInfoWindow

    
    infoWindow.placeHolderImage.sd_setShowActivityIndicatorView(true)
    infoWindow.placeHolderImage.sd_setIndicatorStyle(.gray)
    
    if image.url! != "" {
      infoWindow.placeHolderImage.sd_setImage(with: URL(string: image.url!))
    } else {
      infoWindow.placeHolderImage.image = UIImage(data: image.imgData! as Data)
    }

    
    var initialInfo = "" + String(image.id)
    
    if let letter = image.initials?.characters.first {
      initialInfo = String(letter) + String(image.id)
    }
    
    let iconV = Bundle.main.loadNibNamed("CustomMarkerView", owner: self, options: nil)?.first as! CustomMarkerViewV
    iconV.initialID.text = initialInfo
    iconV.iconImage.image = UIImage(named: image.symbolLetter!)
    
    let marker = CSMarker(position: currentLocation)
    marker.iconView = iconV //getIcon(image: image) //UIImage(named: image.symbolLetter!)
//    marker.ico
    marker.userData = image
    marker.map = mapView

  }
  
  func getIcon(image: Image) -> UIImage{
    // image
    //24 * 43
    let iconView = UIImageView(frame: CGRect(x: 0, y:0, width: 30, height: 50))
    iconView.image = UIImage(named: image.symbolLetter!)!
    // text
    let label = UILabel(frame: CGRect(x: 0, y:-20, width: 45, height: 50))
    var firstLettr = ""
    
    if let letter = image.initials?.characters.first {
      firstLettr = String(letter)
    }
    
    label.text = ("\(firstLettr)\(image.id)")
    label.textColor = .white
    iconView.addSubview(label)
    // grab it
    UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)
    iconView.layer.render(in: UIGraphicsGetCurrentContext()!)
    let icon = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return icon!
  }
  

  func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
    
   
      let infoWindow = Bundle.main.loadNibNamed("InfoWindow", owner: self, options: nil)?.first as! CustomInfoWindow
    
       infoWindow.noPhotoUploadedLabel.isHidden = true
    
    
      self.image = (marker.userData as! Image)
    
      let accuracy = (marker.userData as! Image).accuracyValue
      let lon = (marker.userData as! Image).longitude
      let lat = (marker.userData as! Image).latitude
      let predefinedText = (marker.userData as! Image).ownerText
      let definedFined = (marker.userData as! Image).symbolText
      let date = (marker.userData as! Image).timestamp
      let id = (marker.userData as! Image).initialsStamp
      let gZone = (marker.userData as! Image).zone
    
      let coordinateSystem = (marker.userData as! Image).coordinateSystem
    
    if coordinateSystem == Const.COOR_DD_WGS84 || coordinateSystem == Const.COOR_DM_WGS84 || coordinateSystem == Const.COOR_DMS_WGS84 {
        infoWindow.eastingLabel.text = "Longitude:"
        infoWindow.northingLabel.text = "Latitude:"
        infoWindow.zoneLabel.isHidden = true
        infoWindow.gZone.isHidden = true
        
      } else {
        infoWindow.eastingLabel.text = "Easting:"
        infoWindow.northingLabel.text = "Northing:"
        infoWindow.zoneLabel.isHidden = false
      
      if coordinateSystem == Const.COOR_UTM_UK_GREF {
          infoWindow.gZone.isHidden = false
          infoWindow.zoneLabel.text = "Grid Ref:"
        } else {
          infoWindow.gZone.isHidden = false
        }
      
    }

      infoWindow.accuracy.text = accuracy
      infoWindow.gZone.text = gZone
      infoWindow.longitude.text = lon
      infoWindow.latitude.text = lat
      infoWindow.definedFind.text = definedFined!
      infoWindow.date.text = date
      infoWindow.id.text = id
      infoWindow.predefinedText.text = predefinedText
      infoWindow.image.sd_setShowActivityIndicatorView(true)
      infoWindow.image.sd_setIndicatorStyle(.gray)
    
    
      //old
      //infoWindow.image.sd_setImage(with: urlInfo)
    
      //new
      if image?.url! != "" {
        infoWindow.image.sd_setImage(with: URL(string: (image?.url!)!))
      } else {
        if image?.imgData != nil {
          infoWindow.image.image = UIImage(data: ((image?.imgData)! as Data))
        } else {
          infoWindow.image.image = UIImage(data: self.imagePlaceholderData!)
        }
      }
    
      infoWindow.frame = CGRect(x: 50, y: 100, width: 250, height: 305)
      infoWindow.center.y -= 80
    
    
    return infoWindow
  }
  
  func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
      performSegue(withIdentifier: "showInfo", sender: self)
  }
  
  func tapFunc(){
      performSegue(withIdentifier: "showInfo", sender: self)
  }
  

  func deletePoints() {
     performSegue(withIdentifier: "deletePoints", sender: self)
  }
  
  
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
     
  
      mapView.animate(toLocation: marker.position)
      mapView.selectedMarker = marker
  
      var point = mapView.projection.point(for: marker.position)
      if let nav_height = self.navigationController?.navigationBar.frame.height{
        point.y = point.y - nav_height
      }
  
      let newPoint = mapView.projection.coordinate(for: point)
      let camera = GMSCameraUpdate.setTarget(newPoint)
      mapView.animate(with: camera)
  
      return true
  
    }

  
  
  @objc func exportData(_ sender: UIButton){
    exportHandler(sender)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if "deletePoints" == segue.identifier {
      let DestViewController : ExportViewController = segue.destination as! ExportViewController
      DestViewController.albumName = album
    } else if "showInfo" == segue.identifier {
      let DestVC : ShowPointInfo = segue.destination as! ShowPointInfo
      DestVC.image = self.image
      DestVC.images = self.images
      DestVC.album = self.album
    } else if "viewPhotos" == segue.identifier {
      let nav = segue.destination as! UINavigationController
      let DestVC = nav.topViewController as! ImagesCVC
      DestVC.album = self.album!
      DestVC.appName = self.appName!
    }
    
  }
  

  func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    defaults.set(String(Double((mapView.camera.target.latitude))), forKey: "latitude" + self.album!)
    defaults.set(String(Double((mapView.camera.target.longitude))), forKey: "longitude" + self.album!)
    defaults.set(String(Float((mapView.camera.zoom))), forKey: "zoom" + self.album!)
  }
  
  
  func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
    if let nav_height = self.navigationController?.navigationBar.frame.height{
      let status_height = UIApplication.shared.statusBarFrame.size.height
      mapView.padding = UIEdgeInsetsMake (nav_height+status_height,0,nav_height,0);
    }
  }
  
  
  
  
  func createMarkers(image: Image) -> CSMarker {
    let infoWindow = Bundle.main.loadNibNamed("InfoWindow", owner: self, options: nil)?.first as! CustomInfoWindow
    
    if image.url! != "" {
      infoWindow.placeHolderImage.sd_setImage(with: URL(string: image.url!)!)
    } else {
      if image.imgData == nil {
        infoWindow.placeHolderImage.image = UIImage(named: "placeholderImage")
      } else {
        infoWindow.placeHolderImage.image = UIImage(data: image.imgData! as Data)
      }
    }
    
    

    let currentLocation = CLLocationCoordinate2DMake(image.decimalLongitude, image.decimalLatitude)
    let marker = CSMarker(position: currentLocation)
    
    var initialInfo = "" + String(image.id)
    
    if let letter = image.initials?.characters.first {
      initialInfo = String(letter) + String(image.id)
    }
    
    let iconV = Bundle.main.loadNibNamed("CustomMarkerView", owner: self, options: nil)?.first as! CustomMarkerViewV
    iconV.initialID.text = initialInfo
    iconV.iconImage.image = UIImage(named: image.symbolLetter!)
    
    
    marker.iconView = iconV //getIcon(image: image)//UIImage(named: image.symbolLetter!)
    marker.userData = image
    marker.map = nil
  
    return marker
  }
  

  func mod(_ a: Int, _ n: Int) -> Int {
    precondition(n > 0, "modulus must be positive")
    let r = a % n
    return r >= 0 ? r : r + n
  }


  
  
  
  

}







extension GoogleMapVC {
  
  func fetchData() -> String{
    responde = ""
    
    let datum = defaults.integer(forKey: "dataFormat")
    if datum == 3 || datum == 4 || datum == 5 {
        responde = responde + Const.EASTING_NORTHING_CVS_HEADER + "\n"
    } else {
      responde = responde + Const.LATITUDE_LONGITUDE_CVS_HEADER + "\n"
    }
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: Const.IMAGE_ENTITY)
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", self.album!)
    request.predicate = predicate

    
    let sortDescriptor1 = NSSortDescriptor(key: Const.INITIALS, ascending: true)
    let sortDescriptor2 = NSSortDescriptor(key: Const.CREATED_AT, ascending: true)
    request.sortDescriptors = [sortDescriptor1, sortDescriptor2]
    
    
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      if results.count > 0 {
        for result in results as! [ImageData]{
          var imageDataTemp : NSData!
          
          if result.imgData != nil {
            imageDataTemp = result.imgData
          } else {
            imageDataTemp = imagePlaceholderData! as NSData
          }
          
          let image = Image(id: Int(result.id), latitude: result.latitude!, longitude: result.longitude!, zone: result.gZone!, accuracy: result.acurracy!, timestamp: result.timestamp!, albumName: result.albumName!, coordinateSystem: result.coordinateSystem!, ownerText: result.note!, key: result.key!, desc: result.desc!, url: result.url!, initials: result.initials!, symbolLetter: result.symbolLetter!, symbolText: result.symbolText!, createdAt: result.createdAt, updatedAt: result.updatedAt, imgData: imageDataTemp, longDesc : result.longDesc  )
          
          markers.append(createMarkers(image: image))
          
          self.images.append(image)
          responde = responde + Helpers.Instance.respondInCommasForm(for: result)
          
          imagesIDs.append(result.key!)
          
          let datum = defaults.integer(forKey: "dataFormat")
          if datum == 3 || datum == 4 || datum == 5 {
            if (result.note!.isEmpty) {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOnwerTextUTM(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          } else {
            if result.note!.isEmpty {
              albumNameList.append(Helpers.Instance.subtitleInfoNoOwnerTextLL(for: result))
            } else {
              albumNameList.append(Helpers.Instance.subtitleInfoOnwerTextUTM(for: result))
            }
          }
        }
      } else{
        let error = UIAlertController(title: "Hey!", message: "This Project has not more records.", preferredStyle: UIAlertControllerStyle.alert)
        error.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
          (action) -> Void in
          
          self.dismiss(animated: true, completion: nil)
          
        }))
        present(error, animated: true, completion: nil)
      }
      
    } catch {
      print("error")
    }
    
    print(responde)
    return responde
    
  }

  func exportHandler(_ sender: UIButton) {
    let alertController = UIAlertController(title: "Export Files", message: "Please select the files you want to export.", preferredStyle: .alert)
    
    let csvAction = UIAlertAction(title: "CSV File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      
      self.exportDatabase()
    })
    
    let kmlAction = UIAlertAction(title: "KML File", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportKMLDB()
    })
    
    let kmlcvsAction = UIAlertAction(title: "CSV/KML Files", style: .default, handler: {(alert: UIAlertAction) -> Void in
      self.exportCSVandKML()
    })
    
    let mapsAsPDF = UIAlertAction(title: "PDF Map", style: .default) { (alert: UIAlertAction) in
      self.exportMapsAsPDF()
    }
    
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    
    alertController.addAction(csvAction)
    alertController.addAction(kmlAction)
    alertController.addAction(kmlcvsAction)
    alertController.addAction(mapsAsPDF)
    alertController.addAction(cancel)
    
    self.present(alertController, animated: true, completion: nil)
 
  }
  
  func exportMapsAsPDF(){
    let image = captureScreen()!
    let A4paperSize = CGSize(width: 595, height: 842)
    let pdf = SimplePDF(pageSize: A4paperSize)
    
    pdf.setContentAlignment(.center)
    pdf.addText("Project: \(self.album!)")
    
    pdf.setContentAlignment(.center)
    pdf.addImage(image)
      
    let pdfData = pdf.generatePDFdata()
    
    sendPDF(file: pdfData)

  }
  
  func sendPDF(file : Data) {
    if MFMailComposeViewController.canSendMail() {
      
      let composeVC = MFMailComposeViewController()
      composeVC.mailComposeDelegate = self
      
      composeVC.setSubject("\(self.album!) - by \(Const.APP_NAME)")
      
      
      let filename = self.album?.slugify()
      
      composeVC.addAttachmentData(file, mimeType: "application/pdf", fileName: "\(filename!)")
      
      composeVC.setMessageBody("Map Details: \(self.album!)", isHTML: true)
      
      present(composeVC, animated: true)
    } else {
      let alert = Helpers.Instance.alertUser(title: "ERROR!", message: "Please make sure you have the Mail App installed and configured on your phone.")
      present(alert, animated: true, completion: nil)
    }
  }
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    switch result {
    case .cancelled:
      print("Mail cancelled")
    case .saved:
      print("Mail saved")
    case .sent:
      print("Mail sent")
    case .failed:
      print("Mail sent failure: \(error!.localizedDescription)")
    }
    controller.dismiss(animated: true, completion: nil)
  }

  
  func imageManager(_ imageManager: SDWebImageManager, shouldDownloadImageFor imageURL: URL?) -> Bool {
    return true
  }
  
  
  @IBAction func exportFiles(_ sender: UIButton) {
    exportHandler(sender)
  }


  func gpsInfoStr(texts: ImageData) -> String {
    var result : String!
    
    var latStr = ""
    var lonStr = ""
    var zoneStr = ""
    
    var accuStr = ""
    
    if texts.acurracy == "1" {
      accuStr  = "<b>Accuracy:</b> Added"
    } else {
      accuStr  = "<b>Accuracy:</b> ± \(texts.acurracy!)m"
    }
    
    //accuStr  = "<b>Accuracy:</b> ± \(texts.acurracy!)m"
    let datumStr = "<b>CoordSys:</b> \(texts.coordinateSystem!)"
    
    var idInfo : String!
    
    if let initial = keychain.get(Const.INITIALS) {
      idInfo = initial + " " + String( texts.id )
    } else {
      idInfo = String( texts.id )
    }
    
    
    
    let dateStr = "<b>\(texts.timestamp!)</b>"
    let symbolText = "<b>Defined Find:</b> \(texts.symbolText!)"
    
    
    
    let datum = defaults.integer(forKey: "dataFormat")
    
    if datum == 3 || datum == 4 || datum == 5 || datum == 6 {
      //inverted to put East up and Nor down
      lonStr = "<b>Northing:</b> \(texts.latitude!)"
      latStr = "<b>Easting:</b> \(texts.longitude!)"
      zoneStr = "<b>\(datum == 6 ? "Grid Ref:" : "Zone:")</b> \(texts.gZone!)"
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(album!)<br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        zoneStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.note!.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.note!)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
    } else {
      latStr = "<b>Latitude:</b> \(texts.latitude!)"
      lonStr = "<b>Longitude: </b>\(texts.longitude!)"
      zoneStr = ""
      
      
      result =  dateStr + "<br/><b>ID: </b> \(idInfo!)<br/><br/>" +
        "<b>Project Name:</b> \(album!) <br/>" +
        accuStr + "<br/>" +
        latStr + "<br/>" +
        lonStr + "<br/>" +
        datumStr + "<br/>" +
        symbolText + "<br/>" +
        (texts.note!.isEmpty ? "" : "<b>Predifined Text</b>:  \(texts.note!)") //+ "<br/><br/> <b>Image:</b> <br/>" +
        //"<img src=\"\(texts.url!)\" width=\"400\">"
      
      
    }
    
    
    return result
  }
  
  
  func exportDatabase() {
    let exportString = fetchData()
    let file = saveAndExportGeneral(exportString: exportString, ext: "csv")
    presentActivity(files: [file])
    
  }
  
  
  func exportKMLDB(){
    let file = saveAndExportGeneral(exportString: exportKMLFunction(), ext: "kml")
    presentActivity(files: [file])
  }
  
  func exportKMLFunction() -> String{
    var respondeKML : String = ""
    
    var header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n" +
      "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + "\n" +
      "<Document>" + "\n" +
      "<name>\(album!)</name>\"" + "\n"
    
    
    header += iconStyle()
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manageContext = appDelegate.managedObjectContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ImageData")
    
    
    request.returnsObjectsAsFaults = false
    
    let predicate = NSPredicate(format: "albumName = %@", album!)
    request.predicate = predicate
    
    
    do {
      let results = try manageContext.fetch(request)
      print(results.count)
      for result in results as! [ImageData]{
        let idInfo : String!
        let query = result.desc!
        let regex = try! NSRegularExpression(pattern:"<(.*?)>", options: [])
        let tmp = query as NSString
        var endResults = [String]()
        
        regex.enumerateMatches(in: query, options: [], range: NSMakeRange(0, query.count)) { result, flags, stop in
          if let range = result?.range(at: 1) {
            endResults.append(tmp.substring(with: range))
          }
        }
        
        let coord = endResults[0]
        let first = coord.components(separatedBy: ",")[1]
        let second = coord.components(separatedBy: ",")[0]
        
        if let initial = keychain.get(Const.INITIALS) {
          idInfo = initial + " " + String( result.id )
        } else {
          idInfo = String( result.id )
        }
        
        
        
        respondeKML =  respondeKML + "<Placemark>" + "\n" +
          "<name>\(idInfo!)</name>" + "\n" +
          "<styleUrl>#\(result.symbolLetter!)StyleMap</styleUrl>" + "\n" +
          "<description> <![CDATA[ \n \(gpsInfoStr(texts: result)) \n]]></description>" + "\n" +
          "<Point><coordinates>\(first),\(second)</coordinates></Point>" + "\n" +
          "</Placemark>" + "\n"
      }
      
    } catch {
      print("error")
    }
    
    let footer = "</Document>" + "\n" +
    "</kml>"
    
    return (header + respondeKML + footer)
  }
  
  
  func iconStyle() -> String {
    var style = ""
    
    let symbols = ["x",
                   "a",
                   "b",
                   "c",
                   "d",
                   "e",
                   "f",
                   "o",
                   "v"]
    
    
    for symbol in symbols {
      style += "<Style id=\"\(symbol)Icon\">" + "\n" +
        "<IconStyle>" + "\n" +
        "<Icon>" + "\n" +
        "<href>http://ismartdetect.com/wp-content/uploads/2017/01/\(symbol)@1.5x.png</href>" + "\n" +
        "</Icon>" + "\n" +
        "</IconStyle>" + "\n" +
        "</Style>" + "\n" + "\n" +
        "<StyleMap id=\"\(symbol)StyleMap\">" + "\n" +
        "<Pair>" + "\n" +
        "<key>normal</key>" + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>" + "\n" +
        "</Pair>" + "\n" +
        "<Pair>"  + "\n" +
        "<key>highlight</key>"  + "\n" +
        "<styleUrl>#\(symbol)Icon</styleUrl>"  + "\n" +
        "</Pair>"  + "\n" +
        "</StyleMap>" + "\n"
    }
    
    
    return style
  }
  
  
  func exportCSVandKML(){
    let firstFile = saveAndExportGeneral(exportString: exportKMLFunction(), ext: "kml" )
    let secondFile = saveAndExportGeneral(exportString: fetchData(), ext: "csv")
    
    presentActivity(files: [firstFile, secondFile])
    
    
  }
  
  
  
  func saveAndExportGeneral(exportString: String, ext: String) -> NSURL {
    var firstActivityItem : NSURL!
    
    let exportFilePath = NSTemporaryDirectory() + "\(album!.slugify())-\(appName!.slugify()).\(ext)"
    let exportFileURL = NSURL(fileURLWithPath: exportFilePath)
    
    FileManager.default.createFile(atPath: exportFilePath, contents: NSData() as Data, attributes: nil)
    var fileHandle: FileHandle? = nil
    do {
      fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
    } catch {
      print("Error with fileHandle")
    }
    
    if fileHandle != nil {
      fileHandle!.seekToEndOfFile()
      let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
      fileHandle!.write(csvData!)
      
      fileHandle!.closeFile()
      
      firstActivityItem = NSURL(fileURLWithPath: exportFilePath)
    }
    return firstActivityItem
    
  }
  
  
  func presentActivity(files: [NSURL]){
    let activityViewController : UIActivityViewController = UIActivityViewController(
      activityItems: files, applicationActivities: nil)
    
    activityViewController.excludedActivityTypes = [
      UIActivityType.assignToContact,
      UIActivityType.saveToCameraRoll,
      UIActivityType.postToFlickr,
      UIActivityType.postToVimeo,
      UIActivityType.postToTencentWeibo
    ]
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
      activityViewController.popoverPresentationController?.sourceView = self.view
      self.present(activityViewController, animated: true, completion: nil)
    } else{
      self.present(activityViewController, animated: true, completion: nil)
    }
  }
  
  

}


extension GoogleMapVC {
  override var shouldAutorotate: Bool {
    return true
  }
}
